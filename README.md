**fgeal** is a simple C++ wrapper library meant to provide common 
functionalities between some game libraries through a single API. It servers as 
a cross-platform abstraction layer to other low-level game libraries, such as 
[SDL](https://www.libsdl.org/). So, when developing a game using fgeal, a game 
could, theoretically, be coded once and compiled everywhere the underlying 
backend library supports. For example, when using a SDL 1.2 backend, it should 
be possible to build/use fgeal in every platform that SDL 1.2 supports.

The exposed features includes creating windows, drawing images (normal, rotated,
scaled), drawing text using [TrueType](https://en.wikipedia.org/wiki/TrueType) 
fonts, drawing a few graphics primitives (line, rectangle, ellipse, etc), 
playing sounds/music and event handling. There are some additional features as 
well, such as a sprite class, a simple state-based game class (based on 
[Slick2D](http://slick.ninjacave.com/)), and others, which implementation comes 
from fgeal itself.

The code is in an [Eclipse CDT](http://www.eclipse.org/cdt/) project folder 
structure. It should be simple to import the code using Eclipse's EGit ("Import 
projects from Git") or by cloning externally afterwards. Alternatively, there 
are *experimental* linux makefiles to build the library.

The main motivation for fgeal's creation was to create a "code once, compile 
anywhere" approach for some personal projects. After experiencing some 
frustation with inconsistent availability of game libraries on mainstream linux 
distros (mainly those with point release development model, like Ubuntu), 
version incompatibilities and other issues, I came up with these wrapper 
functions to try to solve these conflicts. When the code grew a little and 
started to include some wrapper classes as well, this repository was created to 
garther it all. 

Due to a single API, programs coded with fgeal can be built using different 
backend adapters that can be chosen according to the underlying library's 
availability on the intended operating system. Currently the following adapters/
backends are available:

- Allegro 5 adapter, compatible with Allegro 5.x versions. See 
[Allegro library](http://liballeg.org/) for more information.
- SDL adapter, compatible with SDL 1.2.x versions. See 
[SDL library](https://www.libsdl.org/) for more information.
- SDL2 adapter, compatible with SDL 2.0.x versions. See 
[SDL library](https://www.libsdl.org/) for more information.

Additionally, the following **experimental** adapters/backends are available as 
well:

- SFML adapter, compatible with SFML 1.6 and 1.5 versions. See 
[SFML library](https://www.sfml-dev.org/) for more information.
- SFML2 adapter, compatible with SFML 2.x versions. See 
[SFML library](https://www.sfml-dev.org/) for more information.
- Allegro 4 adapter, compatible with Allegro 4.4 and 4.2 versions. See 
[Allegro library](http://liballeg.org/) for more information.

In the future, it may support more features and adapters.

It should be noted, however, that although it is intended to be cross-platform, 
it was **never** tested on MacOSes.

**fgeal** stands for **f**geal **g**ame **e**ngine **a**bstraction **l**ayer.

See [BUILDING.md](https://gitlab.com/hydren/fgeal/blob/master/BUILDING.md) for 
information about building the library or compiling with/against it.
