/*
 * core.cpp
 *
 *  Created on: 20/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"

#include "fgeal/core.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#ifdef FGEAL_ALLEGRO4_LOADPNG_INSTALLED
	#include <loadpng.h>
#endif
#ifdef FGEAL_ALLEGRO4_JPGALLEG_INSTALLED
	#include <jpgalleg.h>
#endif
#ifdef FGEAL_ALLEGRO4_ALGIF_INSTALLED
	#include <algif.h>
#endif
#ifdef FGEAL_ALLEGRO4_LOGG_INSTALLED
	#include <logg.h>
#endif
#ifdef FGEAL_ALLEGRO4_ALOGG_INSTALLED
	#include <alogg/alogg.h>
#endif
#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED
	#include <allegttf.h>
#endif

#include <string>

namespace fgeal
{
	/* fgeal code based on Allegro 4.4 / 4.2 */
	const char* ADAPTER_NAME = "Allegro 4 Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "Allegro";
	const char* ADAPTED_LIBRARY_VERSION = ALLEGRO_VERSION_STR;

	static bool isAllegroInit;
	static long allegroTickCount;
	static void customAllegroUserTimerHandler();

	static int mouseButtonCount;  // needs to be of type int

	// initialize all allegro stuff
	void core::initialize()
	{
		if(allegro_init() != 0)                                        throw AdapterException("Allegro could not be initialized: %s", allegro_error);
		if(install_timer() != 0)                                       throw AdapterException("Could not install timer routines: %s", allegro_error);
		if(install_keyboard() != 0)                                    throw AdapterException("Could not install keyboard input subsystem: %s", allegro_error);
		if((mouseButtonCount = install_mouse()) == -1)                 throw AdapterException("Could not install mouse input subsystem: %s", allegro_error);
		if(install_joystick(JOY_TYPE_AUTODETECT) != 0)                 throw AdapterException("Could not install joystick input subsystem: %s", allegro_error);
		if(install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, null) != 0) throw AdapterException("Could not install sound subsystem: %s", allegro_error);

		#ifdef FGEAL_ALLEGRO4_LOADPNG_INSTALLED
		loadpng_init();  // registers png loader
		#endif

		#ifdef FGEAL_ALLEGRO4_JPGALLEG_INSTALLED
		jpgalleg_init();  // registers jpg loader
		register_bitmap_file_type("jpeg", load_jpg, save_jpg);  // register ".jpeg" extension as well
		#endif

		#ifdef FGEAL_ALLEGRO4_ALGIF_INSTALLED
		algif_init();  // registers gif loader
		#endif

		#ifdef FGEAL_ALLEGRO4_LOGG_INSTALLED
		register_sample_file_type("ogg", logg_load, null);
		#elif defined(FGEAL_ALLEGRO4_ALOGG_INSTALLED)
		alogg_init();
		#endif

		#ifdef FGEAL_ALLEGRO4_ALLEGROMP3_INSTALLED
		register_sample_file_type("mp3", customAllegroMp3LoadSample, null);
		#endif

		#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED
		antialias_init(null);
		register_font_file_type("ttf", customAllegTtfLoadFont);
		#endif

		set_keyboard_rate(0, 0);

		isAllegroInit = true;
		LOCK_VARIABLE(allegroTickCount);
		LOCK_FUNCTION(customAllegroUserTimerHandler);
		install_int(customAllegroUserTimerHandler, 1);
		setlocale(LC_NUMERIC, "C");  // avoids allegro's setting of current locale
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
		if(drawTextTransBufferBitmap != null)
			destroy_bitmap(drawTextTransBufferBitmap);

		#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED
		antialias_exit();
		#endif

		allegro_exit();
	}

	bool isProperlyInitialized()
	{
		return isAllegroInit;
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		::rest(seconds*1000);
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return allegroTickCount/1000.0f;
	}

	// --------------------------------------------------------------------

	static void customAllegroUserTimerHandler()
	{
		allegroTickCount++;
	}
	END_OF_FUNCTION(customAllegroUserTimerHandler)
}

#ifdef _MSC_VER
	// needed to avoid name conflict in agnostic_filesystem.hxx when building with MSVC
	#include <winalleg.h>
#endif
#include "fgeal/agnostic_filesystem.hxx"
#include "fgeal/agnostic_sound_stream.hxx"
