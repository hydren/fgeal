/*
 * event.cpp
 *
 *  Created on: 03/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#define isKeyRelease(rawByte) (rawByte & 0x80)
#define isKeyPress(rawByte) (!isKeyRelease(rawByte))
#define toScancode(rawByte) (rawByte & 0x7f)

#if ALLEGRO_SUB_VERSION < 2 or (ALLEGRO_SUB_VERSION == 2 and ALLEGRO_WIP_VERSION < 2)  // supported only on Allegro v4.2.2 RC1 and onwards
	#define MOUSE_FLAG_MOVE_W 0  //define flag with dummy value (will always make bitwise AND return 0)
#endif

#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL
	#define updateJoystickEvents(queueImpl) queueImpl.generateJoystickEvents()
#else
	#define updateJoystickEvents(queueImpl)  // replace with nothing at all
#endif

namespace fgeal
{
	// https://www.allegro.cc/forums/thread/607359
	// https://en.wikipedia.org/wiki/Event-driven_programming

	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		return self.type;
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.allegroKeyboardScancode);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		return self.mouseButton;
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		return self.mouseX;
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		return self.mouseY;
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.mouseWheelMotionAmount;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

		return self.joystickIndex;

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

		return self.joystickButtonIndex;

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

		// XXX this seems to be overly complex and possibly an overhead source
		for(int s = 0, previousAxesCount = 0; s < joy[self.joystickIndex].num_sticks; s++)
			if(s == self.joystickStickIndex)
				return previousAxesCount + self.joystickAxisIndex;
			else
				previousAxesCount += joy[self.joystickIndex].stick[s].num_axis;

		return -1;  // should never happen unless this is not a joystick event

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return -1;

		#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

		return self.joystickAxisPosition/128.f - (joy[self.joystickIndex].stick[self.joystickStickIndex].flags & JOYFLAG_SIGNED? 0.f : 1.f);  // converting range [-128 to 128] or [0 to 255] to [-1.0 to 1.0], depending on the stick's flag

		#else

		core::reportAbsentImplementation("This adapter does not support joystick events. Poll joystick state instead or re-compile fgeal without flag FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL.");
		return 0;

		#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		FGEAL_CHECK_INIT();
		return -1;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return 0;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		static bool needsToSetCallbacks = true;

		if(needsToSetCallbacks)
		{
			// set allegro 4 function pointers
			LOCK_VARIABLE(EventQueue::instance);

			LOCK_FUNCTION(implementation::customAllegroDisplayCloseButtonCallback);
			set_close_button_callback(implementation::customAllegroDisplayCloseButtonCallback);

			LOCK_FUNCTION(implementation::customAllegroKeyboardLowlevelCallback);
			keyboard_lowlevel_callback = implementation::customAllegroKeyboardLowlevelCallback;

			LOCK_FUNCTION(implementation::customAllegroMouseCallback);
			mouse_callback = implementation::customAllegroMouseCallback;

			needsToSetCallbacks = false;
		}

		#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

		self.joystickState.resize(num_joysticks);
		for(int j = 0; j < num_joysticks; j++)
		{
			self.joystickState[j].buttonState.resize(joy[j].num_buttons, false);  // resize buttons array
			self.joystickState[j].axisState.resize(joy[j].num_sticks);  // resize axis array by the "sticks" dimension
			for(int s = 0; s < joy[j].num_sticks; s++)
				self.joystickState[j].axisState[s].resize(joy[j].stick[s].num_axis, 0);  // resize axis array by the "axis" dimension
		}

		#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		while(not self.events.empty())
		if(self.events.front() != null)
		{
			delete self.events.front();
			self.events.pop();
		}

		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		return self.events.empty();
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		return not self.events.empty();
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return null;

		Event* const nextEvent = self.events.front();  // copy pointer to event
		self.events.pop();  // remove it from queue
		return nextEvent;  // and return it
	}

	bool EventQueue::getNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return false;

		container->self = self.events.front()->self;  // make a copy of the event (NOT of its pointer) to the container
		delete self.events.front();  // delete event
		self.events.pop();  // and remove its pointer from the queue
		return true;
	}

	bool EventQueue::skipNextEvent()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		if(self.events.empty())
			return false;

		delete self.events.front();  // delete event
		self.events.pop();  // and remove its pointer from the queue
		return true;
	}

	void EventQueue::waitNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		while(self.events.empty())
			::rest(100);

		if(container != null)
		{
			container->self = self.events.front()->self;  // make a copy of the event (NOT of its pointer) to the container
			delete self.events.front();  // delete event
			self.events.pop();  // and remove its pointer from the queue
		}
	}

	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();
		updateJoystickEvents(self);
		while(not self.events.empty())
		{
			delete self.events.front();  // delete event
			self.events.pop();  // and remove its pointer from the queue
		}
	}

	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		self.isBypassEnabled = bypass;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	#ifndef FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL

	void EventQueue::implementation::generateJoystickEvents()
	{
		if(joystickState.empty())
			return;

		EventQueue::implementation& queue = EventQueue::instance->self;
		if(not queue.isBypassEnabled)
		{
			poll_joystick();
			for(unsigned j = 0; j < joystickState.size(); j++)
			{
				for(unsigned b = 0; b < joystickState[j].buttonState.size(); b++)
				if(joystickState[j].buttonState[b] and not joy[j].button[b].b)
				{
					joystickState[j].buttonState[b] = false;
					Event* const joystickEvent = new Event();
					getImplementation(joystickEvent).type = Event::TYPE_JOYSTICK_BUTTON_RELEASE;
					getImplementation(joystickEvent).joystickIndex = j;
					getImplementation(joystickEvent).joystickButtonIndex = b;
					queue.events.push(joystickEvent);
				}
				else if(not joystickState[j].buttonState[b] and joy[j].button[b].b)
				{
					joystickState[j].buttonState[b] = true;
					Event* const joystickEvent = new Event();
					getImplementation(joystickEvent).type = Event::TYPE_JOYSTICK_BUTTON_PRESS;
					getImplementation(joystickEvent).joystickIndex = j;
					getImplementation(joystickEvent).joystickButtonIndex = b;
					queue.events.push(joystickEvent);
				}

				for(unsigned s = 0; s < joystickState[j].axisState.size(); s++)
				for(unsigned a = 0; a < joystickState[j].axisState[s].size(); a++)
				if(joystickState[j].axisState[s][a] != joy[j].stick[s].axis[a].pos)
				{
					joystickState[j].axisState[s][a] = joy[j].stick[s].axis[a].pos;
					Event* const joystickEvent = new Event();
					getImplementation(joystickEvent).type = Event::TYPE_JOYSTICK_AXIS_MOTION;
					getImplementation(joystickEvent).joystickIndex = j;
					getImplementation(joystickEvent).joystickStickIndex = s;
					getImplementation(joystickEvent).joystickAxisIndex = a;
					getImplementation(joystickEvent).joystickAxisPosition = joystickState[j].axisState[s][a];
					queue.events.push(joystickEvent);
				}
			}
		}
	}

	#endif  /* FGEAL_ALLEGRO4_DISABLE_JOYSTICK_EVENTS_IMPL */

	void EventQueue::implementation::customAllegroDisplayCloseButtonCallback()
	{
		if(EventQueue::instance == null)
			return;

		EventQueue::implementation& queue = EventQueue::instance->self;
		if(not queue.isBypassEnabled)
		{
			Event* const displayCloseEvent = new Event();
			getImplementation(displayCloseEvent).type = Event::TYPE_DISPLAY_CLOSURE;
			queue.events.push(displayCloseEvent);
		}
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroDisplayCloseButtonCallback)

	void EventQueue::implementation::customAllegroKeyboardLowlevelCallback(int rawByte)
	{
		if(EventQueue::instance == null)
			return;

		EventQueue::implementation& queue = EventQueue::instance->self;
		if(not queue.isBypassEnabled)
		{
			Event* const keyboardEvent = new Event();
			getImplementation(keyboardEvent).type = isKeyRelease(rawByte)? Event::TYPE_KEY_RELEASE : Event::TYPE_KEY_PRESS;
			getImplementation(keyboardEvent).allegroKeyboardScancode = toScancode(rawByte);
			queue.events.push(keyboardEvent);
		}
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroKeyboardLowlevelCallback)

	void EventQueue::implementation::customAllegroMouseCallback(int flag)
	{
		if(EventQueue::instance == null)
			return;

		EventQueue::implementation& queue = EventQueue::instance->self;
		if(not queue.isBypassEnabled)
		{
			const int currentMousePosition = mouse_pos,  // grab snapshot of mouse position
					  mouseZ = mouse_z,  // grab snapshot of mouse wheel position
					  mouseX = currentMousePosition >> 16,
					  mouseY = currentMousePosition & 0x0000ffff,
					  mouseWheelMotionAmount = queue.lastMouseZ == 0? mouseZ : mouseZ - queue.lastMouseZ;

			queue.lastMouseZ = mouseZ;

			if(flag & MOUSE_FLAG_MOVE)
			{
				Event* const mouseMoveEvent = new Event();
				getImplementation(mouseMoveEvent).mouseX = mouseX;
				getImplementation(mouseMoveEvent).mouseY = mouseY;
				getImplementation(mouseMoveEvent).type = Event::TYPE_MOUSE_MOTION;
				queue.events.push(mouseMoveEvent);
			}
			if(flag & MOUSE_FLAG_LEFT_DOWN)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_LEFT;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_PRESS;
				queue.events.push(mouseBtnDownEvent);
			}
			if(flag & MOUSE_FLAG_RIGHT_DOWN)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_RIGHT;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_PRESS;
				queue.events.push(mouseBtnDownEvent);
			}
			if(flag & MOUSE_FLAG_MIDDLE_DOWN)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_MIDDLE;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_PRESS;
				queue.events.push(mouseBtnDownEvent);
			}
			if(flag & MOUSE_FLAG_LEFT_UP)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_LEFT;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_RELEASE;
				queue.events.push(mouseBtnDownEvent);
			}
			if(flag & MOUSE_FLAG_RIGHT_UP)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_RIGHT;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_RELEASE;
				queue.events.push(mouseBtnDownEvent);
			}
			if(flag & MOUSE_FLAG_MIDDLE_UP)
			{
				Event* const mouseBtnDownEvent = new Event();
				getImplementation(mouseBtnDownEvent).mouseX = mouseX;
				getImplementation(mouseBtnDownEvent).mouseY = mouseY;
				getImplementation(mouseBtnDownEvent).mouseButton = Mouse::BUTTON_MIDDLE;
				getImplementation(mouseBtnDownEvent).type = Event::TYPE_MOUSE_BUTTON_RELEASE;
				queue.events.push(mouseBtnDownEvent);
			}
			if((flag & MOUSE_FLAG_MOVE_Z) or (flag & MOUSE_FLAG_MOVE_W))
			{
				Event* const mouseWheelMoveEvent = new Event();
				getImplementation(mouseWheelMoveEvent).mouseX = mouseX;
				getImplementation(mouseWheelMoveEvent).mouseY = mouseY;
				getImplementation(mouseWheelMoveEvent).mouseWheelMotionAmount = mouseWheelMotionAmount;
				getImplementation(mouseWheelMoveEvent).type = Event::TYPE_MOUSE_WHEEL_MOTION;
				queue.events.push(mouseWheelMoveEvent);
			}
		}
	}
	END_OF_FUNCTION(EventQueue::implementation::customAllegroMouseCallback)
}
