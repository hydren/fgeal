/*
 * font.cpp
 *
 *  Created on: 03/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

using std::string;

#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED

#include <allegttf.h>

namespace fgeal
{
	// simple struct meant to pass parameters to AllegTTF's load_ttf_font() call
	struct AllegTtfParams
	{
		int points, smooth;
		AllegTtfParams(unsigned size, bool antialiasing)
		: points(size * 0.8), smooth(not antialiasing? ALLEGTTF_NOSMOOTH : size > 8 ? ALLEGTTF_TTFSMOOTH : ALLEGTTF_REALSMOOTH) {}
	};

	// loads a font and return as expected by a load_font() call
	FONT* customAllegTtfLoadFont(AL_CONST char* filename, RGB* dummy, void* paramsPtr)
	{
		AllegTtfParams* const params = static_cast<AllegTtfParams*>(paramsPtr);
		solid_mode();  // to render TTF fonts, we need the drawing mode to be the solid one
		FONT* const font = load_ttf_font(filename, params->points, params->smooth);
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);  // restore trans drawing mode
		delete params;
		return font;
	}
	END_OF_FUNCTION(customAllegTtfLoadFont)
}

#endif /* FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED */

namespace fgeal
{
	/// Bitmap used as a buffer to improve Font::drawText performance when drawing to Image draw targets
	BITMAP* drawTextTransBufferBitmap = null;

	// XXX kludge to make textout work (barely) with bitmaps other than the backbuffer
	static inline void drawTextTrans(FONT* const allegroFont, const string& text, float x, float y, Color c)
	{
		const int renderedTextWidth = text_length(allegroFont, text.c_str());

		// if buffer is empty or too small, create a bigger one
		if(drawTextTransBufferBitmap == null or drawTextTransBufferBitmap->w < renderedTextWidth or drawTextTransBufferBitmap->h < text_height(allegroFont))
		{
			destroy_bitmap(drawTextTransBufferBitmap);  // passing null is allowed, don't worry
			drawTextTransBufferBitmap = create_bitmap(renderedTextWidth, text_height(allegroFont));
		}

		// take a const reference to the buffer pointer
		BITMAP*const& bufferBitmap = drawTextTransBufferBitmap;

		// get a color that contrasts with the requested to clear bitmap
		const Color bgc = Color::create((c.r + 128) % 255, (c.g + 128) % 255, (c.b + 128) % 255);

		// clean buffer with contrast color
		clear_to_color(bufferBitmap, makeacol32(bgc.r, bgc.g, bgc.b, 0));
		textout_ex(bufferBitmap, allegroFont, text.c_str(), 0, 0, makecol(c.r, c.g, c.b), -1);  // draw to buffer

		// search for pixels that were painted by the textout call and set its alpha values to the same as the color parameter
		solid_mode();
		for(int i = 0; i < renderedTextWidth; i++)  // use rendered text width instead of bufferBitmap->w to avoid searching unused pixels of the buffer
			for(int j = 0, color = getpixel(bufferBitmap, i, j); j < bufferBitmap->h; j++, color = getpixel(bufferBitmap, i, j))
				if(getr32(color) != bgc.r or getg32(color) != bgc.g or getb32(color) != bgc.b)
					putpixel(bufferBitmap, i, j, makeacol32(getr32(color), getg32(color), getb32(color), c.a));
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);

		// draw buffer to draw target
		draw_trans_sprite(fgeal::drawTargetBitmap, bufferBitmap, x, y);
	}

	//------------------------------------------------------------------------------------------------------------------------

	Font::Font(const string& filename, unsigned size, bool antialiasing, bool kerning)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		void* params = null;

		#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED
		if(filenameHasExtension(filename, "ttf"))
			params = new AllegTtfParams(size, antialiasing);  // parameters to be passed to AllegTTF load function
		#endif

		self.allegroFont = load_font(filename.c_str(), null, params);
		if(self.allegroFont == null)
			throw AdapterException("Font %s could not be loaded: Error %s", filename.c_str(), allegro_error);

		//XXX Allegro 4 adapter ignoring font kerning hint

		self.size = size;
		self.antialiasingEnabled = antialiasing;
		self.fontFilename = filename;
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		destroy_font(self.allegroFont);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color c)
	{
		FGEAL_CHECK_INIT();
		if(fgeal::isDrawTargetBackBuffer or bitmap_color_depth(screen) != 32)
			textout_ex(fgeal::drawTargetBitmap, self.allegroFont, text.c_str(), x, y, makecol(c.r, c.g, c.b), -1);
		else
			drawTextTrans(self.allegroFont, text, x, y, c);
	}

	float Font::getTextHeight() const
	{
		FGEAL_CHECK_INIT();
		return text_height(self.allegroFont);
	}

	float Font::getTextWidth(const string& text) const
	{
		FGEAL_CHECK_INIT();
		return text_length(self.allegroFont, text.c_str());
	}

	unsigned Font::getSize() const
	{
		FGEAL_CHECK_INIT();
		return self.size;
	}

	void Font::setSize(unsigned size)
	{
		FGEAL_CHECK_INIT();
		void* params = null;

		#ifdef FGEAL_ALLEGRO4_ALLEGTTF_INSTALLED
		if(filenameHasExtension(self.fontFilename, "ttf"))
			params = new AllegTtfParams(size, self.antialiasingEnabled);  // parameters to be passed to AllegTTF load function
		#endif

		self.allegroFont = load_font(self.fontFilename.c_str(), null, params);
		if(self.allegroFont == null)
			throw AdapterException("Font %s could not be loaded: Error %s", self.fontFilename.c_str(), allegro_error);
		self.size = size;
	}

	// ##################################################################################################################

	DrawableText::DrawableText(const string& text, Font* font, Color color)
	: self(*new implementation()), font(font)
	{
		self.content = text;
		self.color = color;
	}

	DrawableText::~DrawableText()
	{}

	DrawableText& DrawableText::operator=(const DrawableText& other)
	{
		if(this != &other)
		{
			font = other.font;
			self.content = other.self.content;
			self.color = other.self.color;
		}
		return *this;
	};

	DrawableText::DrawableText(const DrawableText& other)
	: self(*new implementation())
	{
		operator =(other);
	}

	float DrawableText::getWidth()
	{
		return font != null? font->getTextWidth(self.content) : 0;
	}

	float DrawableText::getHeight()
	{
		return font != null? font->getTextHeight() : 0;
	}

	void DrawableText::setFont(Font* font)
	{
		this->font = font;
	}

	void DrawableText::setContent(const string& str)
	{
		self.content = str;
	}

	string DrawableText::getContent()
	{
		return self.content;
	}

	void DrawableText::setColor(Color color)
	{
		self.color = color;
	}

	Color DrawableText::getColor()
	{
		return self.color;
	}

	void DrawableText::draw(float x, float y)
	{
		if(font != null)
			font->drawText(self.content, x, y, self.color);
	}
}
