/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"
#include "fgeal/generic_graphics.hxx"

#include <allegro.h>

#ifdef ALLEGRO_COLOR32
	#include <allegro/internal/aintern.h>
#endif

#include <cmath>
#include <utility>

using std::vector;
using std::pair;

namespace fgeal
{
	/// Current draw target bitmap
	BITMAP* drawTargetBitmap;

	/// Flag that indicates whether the current draw target is the back buffer of a display or an Image's internal bitmap
	bool isDrawTargetBackBuffer;

	#ifdef ALLEGRO_COLOR32
	// Sets a custom blender mode is able to blend a 32-bit RGBA source bitmap on a 32-bit RGBA destination bitmap, combining both source and destination alpha values.
	// If not in 32-bit color depth, works as set_alpha_blender()
	void setRgbaAlphaBlender()
	{
		// sets alpha blenders as recommended by Allegro
		set_alpha_blender();

		// if on 32-bit color depth screen
		if(bitmap_color_depth(screen) == 32)
		{
			// Sets a custom blender mode is able to blend a 32-bit RGBA source bitmap on a 32-bit RGBA destination bitmap, combining both source and destination alpha values.
			// Allegro 4 provides a built-in set_alpha_blender() function, but it sets an internal blender mode (namely _blender_alpha32) that uses only source alpha values,
			// ignoring destination alpha values altogether (although combining color components correctly). To circumvent this limitation, a custom blender mode is needed.
			// This blender calls the one set by set_alpha_blender(), but sets a result alpha component combined from source and destination bitmap.
			struct custom { static unsigned long rgba2rgbaBlender(unsigned long x, unsigned long y, unsigned long n) {
				n = _blender_alpha32(x, y, n);  // XXX dirty reference to an internal Allegro function (bad practice)
				return makeacol32(getr32(n), getg32(n), getb32(n), geta32(x) + geta32(y) - (geta32(x) * geta32(y))/255);
			}};

			// override 32-bit blender
			_blender_func32 = custom::rgba2rgbaBlender;  // XXX dirty reference to an internal Allegro function (bad practice)
		}
	}
	#endif

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		if(image == null)
		{
			fgeal::drawTargetBitmap = Display::instance->self.backBufferBitmap;
			fgeal::isDrawTargetBackBuffer = true;
			set_alpha_blender();
		}
		else
		{
			fgeal::drawTargetBitmap = image->self.allegroBitmap;
			fgeal::isDrawTargetBackBuffer = false;
			fgeal::setRgbaAlphaBlender();
		}
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetBitmap = display.self.backBufferBitmap;
		fgeal::isDrawTargetBackBuffer = true;
		set_alpha_blender();
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetBitmap = Display::instance->self.backBufferBitmap;
		fgeal::isDrawTargetBackBuffer = true;
		set_alpha_blender();
	}

	///XXX dirty workaround for allegro 4 magenta transparency behavior.
	static inline int makeacol2(const int r, const int g, const int b, const int a)
	{
		return (r == 255 and g == 0 and b == 255)? makeacol(255, 1, 255, a) : makeacol(r, g, b, a);
	}

	// =====================================================================================================================================================
	// Primitives

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
		else
			line(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickLine(x1, y1, x2, y2, thickness, c);
	}

	void Graphics::drawRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rect(fgeal::drawTargetBitmap, x, y, x+w, y+h, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rectfill(fgeal::drawTargetBitmap, x, y, x+w, y+h, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		circle(fgeal::drawTargetBitmap, cx, cy, radius, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		circlefill(fgeal::drawTargetBitmap, cx, cy, radius, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ellipse(fgeal::drawTargetBitmap, cx, cy, rx, ry, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ellipsefill(fgeal::drawTargetBitmap, cx, cy, rx, ry, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
		{
			fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
			fastline(fgeal::drawTargetBitmap, x1, y1, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
			fastline(fgeal::drawTargetBitmap, x3, y3, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
		}
		else
		{
			line(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
			line(fgeal::drawTargetBitmap, x1, y1, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
			line(fgeal::drawTargetBitmap, x3, y3, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
		}
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		triangle(fgeal::drawTargetBitmap, x1, y1, x2, y2, x3, y3, makeacol(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
		{
			fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
			fastline(fgeal::drawTargetBitmap, x2, y2, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
			fastline(fgeal::drawTargetBitmap, x3, y3, x4, y4, makeacol2(c.r, c.g, c.b, c.a));
			fastline(fgeal::drawTargetBitmap, x4, y4, x1, y1, makeacol2(c.r, c.g, c.b, c.a));
		}
		else
		{
			line(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
			line(fgeal::drawTargetBitmap, x2, y2, x3, y3, makeacol2(c.r, c.g, c.b, c.a));
			line(fgeal::drawTargetBitmap, x3, y3, x4, y4, makeacol2(c.r, c.g, c.b, c.a));
			line(fgeal::drawTargetBitmap, x4, y4, x1, y1, makeacol2(c.r, c.g, c.b, c.a));
		}
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const int vertices[8] = {(int) x1, (int) y1, (int) x2, (int) y2, (int) x3, (int) y3, (int) x4, (int) y4};
		polygon(fgeal::drawTargetBitmap, 4, vertices, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		arc(fgeal::drawTargetBitmap, cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a));
	}

	// force usage of fast version of line drawing primitives on GenericGraphics::drawThickArc, because this algorithm nulifies anti-aliasing effects
	void fastlineWrapper(float x1, float y1, float x2, float y2, const Color& c)  // ps: cannot be static because it needs to have external linkage in older compilers
	{
		fastline(fgeal::drawTargetBitmap, x1, y1, x2, y2, makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawThickArc(float cx, float cy, float r, float t, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickArc<fastlineWrapper>(cx, cy, r, t, ai, af, c);
	}

	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		struct { static void copyArcPoint(BITMAP *param, int x, int y, int)
		{
			reinterpret_cast<vector< pair<int, int> >*>(param)->push_back(std::make_pair(x, y));
		}
		} func;

		vector< pair<int, int> > coordCache;
		do_arc(reinterpret_cast<BITMAP*>(&coordCache), cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a), func.copyArcPoint);
		for(unsigned i = 0; i < coordCache.size(); i++)
			putpixel(fgeal::drawTargetBitmap, coordCache[i].first, coordCache[i].second, makeacol2(c.r, c.g, c.b, c.a));

		if(not coordCache.empty())
		{
			if(useFasterPrimitivesHint)
			{
				fastline(fgeal::drawTargetBitmap, cx, cy, coordCache.front().first, coordCache.front().second, makeacol2(c.r, c.g, c.b, c.a));
				fastline(fgeal::drawTargetBitmap, cx, cy, coordCache. back().first, coordCache. back().second, makeacol2(c.r, c.g, c.b, c.a));
			}
			else
			{
				line(fgeal::drawTargetBitmap, cx, cy, coordCache.front().first, coordCache.front().second, makeacol2(c.r, c.g, c.b, c.a));
				line(fgeal::drawTargetBitmap, cx, cy, coordCache. back().first, coordCache. back().second, makeacol2(c.r, c.g, c.b, c.a));
			}
		}
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		struct { static void copyArcPoint(BITMAP *param, int x, int y, int)
		{
			vector<int>& coordCache = *reinterpret_cast<vector<int>*>(param);
			coordCache.push_back(x); coordCache.push_back(y);
		}
		} func;

		vector<int> coordCache(2);
		coordCache[0] = cx; coordCache[1] = cy;
		do_arc(reinterpret_cast<BITMAP*>(&coordCache), cx, cy, -toAllegroFixedAngle(ai), -toAllegroFixedAngle(af), r, makeacol2(c.r, c.g, c.b, c.a), func.copyArcPoint);
		polygon(fgeal::drawTargetBitmap, coordCache.size()/2, &(coordCache[0]), makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawRoundedRectangle(x, y, width, height, rd, c);
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawFilledRoundedRectangle(x, y, width, height, rd, c);
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(x.size() > 1 and y.size() > 1)
		{
			const unsigned n = std::min(x.size(), y.size());
			if(useFasterPrimitivesHint)
			{
				fastline(fgeal::drawTargetBitmap, x[0], y[0], x[n-1], y[n-1], makeacol2(c.r, c.g, c.b, c.a));
				if(n > 2) for(unsigned i = 1; i < n; i++)
					fastline(fgeal::drawTargetBitmap, x[i], y[i], x[i-1], y[i-1], makeacol2(c.r, c.g, c.b, c.a));
			}
			else
			{
				line(fgeal::drawTargetBitmap, x[0], y[0], x[n-1], y[n-1], makeacol2(c.r, c.g, c.b, c.a));
				if(n > 2) for(unsigned i = 1; i < n; i++)
					line(fgeal::drawTargetBitmap, x[i], y[i], x[i-1], y[i-1], makeacol2(c.r, c.g, c.b, c.a));
			}
		}
		else if(not x.empty() and not y.empty())
			putpixel(fgeal::drawTargetBitmap, x[0], y[0], makeacol2(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<int> vertices(n*2);
		for(unsigned i = 0; i < n; i++)
			vertices[2*i] = x[i], vertices[2*i+1] = y[i];
		polygon(fgeal::drawTargetBitmap, n, &vertices[0], makeacol2(c.r, c.g, c.b, c.a));
	}
}
