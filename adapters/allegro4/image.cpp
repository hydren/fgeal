/*
 * image.cpp
 *
 *  Created on: 20/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#include <cmath>

using std::string;
using std::vector;

namespace fgeal
{
	// fallback optional code for older Allegro 4 versions (inefficient and low-performance, though)

	// for versions prior to 4.3.10
	#if ALLEGRO_SUB_VERSION < 3 or (ALLEGRO_SUB_VERSION == 3 and ALLEGRO_WIP_VERSION < 10)
		/** drawing modes for xxx_ex() functions */
		#define DRAW_SPRITE_NORMAL 0
		#define DRAW_SPRITE_LIT 1
		#define DRAW_SPRITE_TRANS 2

		/** flipping modes for xxx_ex() functions */
		#define DRAW_SPRITE_NO_FLIP 0x0
		#define DRAW_SPRITE_H_FLIP  0x1
		#define DRAW_SPRITE_V_FLIP  0x2
		#define DRAW_SPRITE_VH_FLIP 0x3

		///XXX Allegro 4 adapter: [SLOW] implementation of draw_sprite_ex() (available since 4.3.10), using older Allegro functions
		static inline void customSlowDrawSpriteEx(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int mode, const int flip)
		{
			switch (mode)
			{
				default:
				case DRAW_SPRITE_LIT:
				case DRAW_SPRITE_NORMAL:
					switch(flip)
					{
						default:
						case DRAW_SPRITE_NO_FLIP:  draw_sprite        (bmp, sprite, x, y); return;
						case DRAW_SPRITE_V_FLIP:   draw_sprite_v_flip (bmp, sprite, x, y); return;
						case DRAW_SPRITE_H_FLIP:   draw_sprite_h_flip (bmp, sprite, x, y); return;
						case DRAW_SPRITE_VH_FLIP:  draw_sprite_vh_flip(bmp, sprite, x, y); return;
					}
					return;

				case DRAW_SPRITE_TRANS:
					BITMAP* const tmpBitmap = create_bitmap(sprite->w, sprite->h);  // prepare for slow pre step to copy to a temporary bitmap
					if(tmpBitmap == null) return;  // fail silently
					switch(flip)
					{
						default:
						case DRAW_SPRITE_NO_FLIP:  draw_sprite        (tmpBitmap, sprite, 0, 0); break;
						case DRAW_SPRITE_V_FLIP:   draw_sprite_v_flip (tmpBitmap, sprite, 0, 0); break;
						case DRAW_SPRITE_H_FLIP:   draw_sprite_h_flip (tmpBitmap, sprite, 0, 0); break;
						case DRAW_SPRITE_VH_FLIP:  draw_sprite_vh_flip(tmpBitmap, sprite, 0, 0); break;
					}
					draw_trans_sprite(bmp, tmpBitmap, x, y);
					destroy_bitmap(tmpBitmap);
					return;
			}
		}

		/// using custom implementation provided by customSlowDrawSpriteEx()
		#define draw_sprite_ex(bmp, sprite, x, y, mode, flip) customSlowDrawSpriteEx(bmp, sprite, x, y, mode, flip)

	#endif /* ALLEGRO_SUB_VERSION < 3 or (ALLEGRO_SUB_VERSION == 3 and ALLEGRO_WIP_VERSION < 10) */
	// ..................................................................................................................................................................................................

	// for versions prior to 4.4
	#if ALLEGRO_SUB_VERSION < 4
		///XXX Allegro 4 adapter: [SLOW] implementation of pivot_sprite_(v_flip)_trans() functions (available since Allegro 4.4.0 RC2), using older Allegro functions
		template<void (*rotate_sprite_function)(BITMAP*const, BITMAP*const, const int, const int, const fixed)>
		static inline void customSlowPivotSpriteTrans(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int cx, const int cy, const fixed angle)
		{
			const float cosa=cos(M_PI*fixtof(angle)/-128.0), sina=sin(M_PI*fixtof(angle)/-128.0);  // compute sine and cosine for angle
			BITMAP* const tmpRotatedBitmap = create_bitmap(fabs(sprite->w*cosa) + fabs(sprite->h*sina), fabs(sprite->w*sina) + fabs(sprite->h*cosa));  // prepare for slow pre-step to copy to a temporary bitmap
			if(tmpRotatedBitmap == null) return;  // fail silently
			rectfill(tmpRotatedBitmap, 0, 0, tmpRotatedBitmap->w, tmpRotatedBitmap->h, makeacol(0, 0, 0, 0));  // clear the newly created bitmap
			rotate_sprite_function(tmpRotatedBitmap, sprite, 0.5*(tmpRotatedBitmap->w-sprite->w), 0.5*(tmpRotatedBitmap->h-sprite->h), angle);  // draws the bitmap rotated and optionally scaled
			const float ax2 = tmpRotatedBitmap->w/2 + (cx - sprite->w/2) * cosa + (cy - sprite->h/2) * sina,  // computes offsets needed to draw the bitmap centered properly
						ay2 = tmpRotatedBitmap->h/2 - (cx - sprite->w/2) * sina + (cy - sprite->h/2) * cosa;
			draw_trans_sprite(bmp, tmpRotatedBitmap, x - ax2, y - ay2);  // draws in a transparency-aware way
			destroy_bitmap(tmpRotatedBitmap);  // delete temporary
		}

		inline void external_rotate_sprite(BITMAP* const bmp, BITMAP* const spr, const int x, const int y, const fixed ang) { rotate_sprite(bmp, spr, x, y, ang); }
		inline void external_rotate_sprite_v_flip(BITMAP* const bmp, BITMAP* const spr, const int x, const int y, const fixed ang) { rotate_sprite_v_flip(bmp, spr, x, y, ang); }

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define pivot_sprite_trans(bmp, sprite, x, y, cx, cy, angle) customSlowPivotSpriteTrans<external_rotate_sprite>(bmp, sprite, x, y, cx, cy, angle)

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define pivot_sprite_v_flip_trans(bmp, sprite, x, y, cx, cy, angle) customSlowPivotSpriteTrans<external_rotate_sprite_v_flip>(bmp, sprite, x, y, cx, cy, angle)

		///XXX Allegro 4 adapter: [SLOW] implementation of pivot_scaled_sprite_(v_flip)_trans() functions (available since Allegro 4.4.0 RC2), using older Allegro functions
		template<void (*rotate_scaled_sprite_function)(BITMAP*const, BITMAP*const, const int, const int, const fixed, const fixed)>
		static inline void customSlowPivotScaledSpriteTrans(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int cx, const int cy, const fixed angle, const fixed scale)
		{
			const float cosa=cos(M_PI*fixtof(angle)/-128.0), sina=sin(M_PI*fixtof(angle)/-128.0), fscale = fixtof(scale);  // compute sine and cosine for angle
			BITMAP* const tmpRotatedScaledBitmap = create_bitmap(fabs(fscale*sprite->w*cosa) + fabs(fscale*sprite->h*sina), fabs(fscale*sprite->w*sina) + fabs(fscale*sprite->h*cosa));  // prepare for slow pre-step to copy to a temporary bitmap
			if(tmpRotatedScaledBitmap == null) return;  // fail silently
			rectfill(tmpRotatedScaledBitmap, 0, 0, tmpRotatedScaledBitmap->w, tmpRotatedScaledBitmap->h, makeacol(0, 0, 0, 0));  // clear the newly created bitmap
			rotate_scaled_sprite_function(tmpRotatedScaledBitmap, sprite, 0.5f*(tmpRotatedScaledBitmap->w-fscale*sprite->w), 0.5f*(tmpRotatedScaledBitmap->h-fscale*sprite->h), angle, scale);  // draws the bitmap rotated and optionally scaled
			const float ax2 = tmpRotatedScaledBitmap->w/2 + (cx - sprite->w/2) * cosa * fscale + (cy - sprite->h/2) * sina * fscale,  // computes offsets needed to draw the bitmap centered properly
						ay2 = tmpRotatedScaledBitmap->h/2 - (cx - sprite->w/2) * sina * fscale + (cy - sprite->h/2) * cosa * fscale;
			draw_trans_sprite(bmp, tmpRotatedScaledBitmap, x - ax2, y - ay2);  // draws in a transparency-aware way
			destroy_bitmap(tmpRotatedScaledBitmap);  // delete temporary
		}

		// these delegating functions are needed to factor out the rotate calls and allow them to be used as template argument.
		inline void external_rotate_scaled_sprite(BITMAP* const bmp, BITMAP* const spr, const int x, const int y, const fixed ang, const fixed sc) { rotate_scaled_sprite(bmp, spr, x, y, ang, sc); }
		inline void external_rotate_scaled_sprite_v_vflip(BITMAP* const bmp, BITMAP* const spr, const int x, const int y, const fixed ang, const fixed sc) { rotate_scaled_sprite_v_flip(bmp, spr, x, y, ang, sc); }

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define pivot_scaled_sprite_trans(bmp, sprite, x, y, cx, cy, angle, scale) customSlowPivotScaledSpriteTrans<external_rotate_scaled_sprite>(bmp, sprite, x, y, cx, cy, angle, scale)

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define rotate_scaled_sprite_trans(bmp, sprite, x, y, angle, scale) customSlowPivotScaledSpriteTrans<external_rotate_scaled_sprite>(bmp, sprite, x, y, (1-fixtof(scale))*sprite->w/2, (1-fixtof(scale))*sprite->h/2, angle, scale)

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define pivot_scaled_sprite_v_flip_trans(bmp, sprite, x, y, cx, cy, angle, scale) customSlowPivotScaledSpriteTrans<external_rotate_scaled_sprite_v_vflip>(bmp, sprite, x, y, cx, cy, angle, scale)

		/// using custom implementation provided by customSlowPivotScaledSpriteTrans()
		#define rotate_scaled_sprite_v_flip_trans(bmp, sprite, x, y, angle, scale) customSlowPivotScaledSpriteTrans<external_rotate_scaled_sprite_v_vflip>(bmp, sprite, x, y, (1-fixtof(scale))*sprite->w/2, (1-fixtof(scale))*sprite->h/2, angle, scale)

	#endif /* ALLEGRO_SUB_VERSION < 4 */
	// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	// draw/blit methods and custom auxiliary code

	/// get the corresponding allegro flag for the flipping mode
	static inline int toAllegroFlipFlag(const Image::FlipMode mode)
	{
		switch (mode)
		{
			default:
			case Image::FLIP_NONE:       return DRAW_SPRITE_NO_FLIP;
			case Image::FLIP_HORIZONTAL: return DRAW_SPRITE_H_FLIP;
			case Image::FLIP_VERTICAL:   return DRAW_SPRITE_V_FLIP;
		}
	}

	/// function to perform the same as allegro draw_trans_sprite() function, but with flip parameter
	static inline void customDrawSpriteFlip(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const Image::FlipMode flipmode)
	{
		switch(flipmode)
		{
			default:
			case Image::FLIP_NONE:       draw_sprite       (bmp, sprite, x, y); return;
			case Image::FLIP_VERTICAL:   draw_sprite_v_flip(bmp, sprite, x, y); return;
			case Image::FLIP_HORIZONTAL: draw_sprite_h_flip(bmp, sprite, x, y); return;
		}
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro blit() function, but transparency-aware
	static inline void customSlowBlitTrans(BITMAP* const source, BITMAP* const dest, const int source_x, const int source_y, const int dest_x, const int dest_y, const int width, const int height)
	{
		BITMAP* const tempBitmap = create_bitmap(width, height);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		::blit(source, tempBitmap, source_x, source_y, 0, 0, width, height);  // crop the bitmap and paste it on the temporary bitmap
		draw_trans_sprite(dest, tempBitmap, dest_x, dest_y);  // draw with trans-logic
		destroy_bitmap(tempBitmap);  // dispose temporary bitmap
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro blit() function, but with flip and draw mode parameters (in the same manner as draw_sprite_ex())
	static inline void customSlowBlitEx(BITMAP* const source, BITMAP* const dest, const int source_x, const int source_y, const int dest_x, const int dest_y, const int width, const int height, const int mode, const int flip)
	{
		BITMAP* const tempBitmap = create_bitmap(width, height);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		::blit(source, tempBitmap, source_x, source_y, 0, 0, width, height);
		draw_sprite_ex(dest, tempBitmap, dest_x, dest_y, mode, flip);
		destroy_bitmap(tempBitmap);
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro stretch_sprite() function, but with flip and draw mode parameters (in the same manner as draw_sprite_ex())
	static inline void customSlowStretchSpriteEx(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int w, const int h, const int mode, const int flip)
	{
		BITMAP* const tempBitmap = create_bitmap(w, h);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		stretch_sprite(tempBitmap, sprite, 0, 0, w, h);
		draw_sprite_ex(bmp, tempBitmap, x, y, mode, flip);
		destroy_bitmap(tempBitmap);
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro stretch_blit() function, but with flip and draw mode parameters (in the same manner as draw_sprite_ex())
	static inline void customSlowStretchBlitEx(BITMAP* const source, BITMAP* const dest, const int source_x, const int source_y, const int source_width, const int source_height, const int dest_x, const int dest_y, const int width, const int height, const int mode, const int flip)
	{
		BITMAP* const tempBitmap = create_bitmap(width, height);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		stretch_blit(source, tempBitmap, source_x, source_y, source_width, source_height, 0, 0, width, height);
		draw_sprite_ex(dest, tempBitmap, dest_x, dest_y, mode, flip);
		destroy_bitmap(tempBitmap);
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro pivot_sprite() function, but with full flip parameter
	static inline void customSlowPivotSpriteFlip(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int cx, const int cy, const fixed angle, const int flip)
	{
		BITMAP* const tempBitmap = create_bitmap(sprite->w, sprite->h);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		draw_sprite_ex(tempBitmap, sprite, 0, 0, DRAW_SPRITE_NORMAL, flip);  // flip step
		pivot_sprite(bmp, tempBitmap, x, y, cx, cy, angle);
		destroy_bitmap(tempBitmap);
	}

	///XXX Allegro 4 adapter: [SLOW] function to perform the same as allegro pivot_sprite_trans() function, but with full flip parameter
	static inline void customSlowPivotSpriteFlipTrans(BITMAP* const bmp, BITMAP* const sprite, const int x, const int y, const int cx, const int cy, const fixed angle, const int flip)
	{
		BITMAP* const tempBitmap = create_bitmap(sprite->w, sprite->h);  // prepare for slow pre step to copy to a temporary bitmap
		if(tempBitmap == null) return;  // fail silently
		draw_sprite_ex(tempBitmap, sprite, 0, 0, DRAW_SPRITE_NORMAL, flip);  // flip step
		pivot_sprite_trans(bmp, tempBitmap, x, y, cx, cy, angle);
		destroy_bitmap(tempBitmap);
	}

	static bool checkOpaqueFormats(const string& filename)
	{
		struct { string operator()(string str) { for(unsigned i = 0; i < str.size(); i++) str[i] = tolower(str[i]); return str; } } to_lower;
		const string extension = to_lower(filename.substr(filename.find_last_of(".") + 1));
		return extension == "jpg" or extension == "bmp";
	}

	// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		// to render some file types (ex: GIFs) and edit pixels, we need the solid drawing mode
		solid_mode();

		BITMAP* const bitmap = load_bitmap(filename.c_str(), null);
		if(bitmap == null)
			throw AdapterException("Could not load image: \"%s\". %s", filename.c_str(), allegro_error);

		bool hasTransPixel = false;
		const bool isOpaqueFormat = checkOpaqueFormats(filename);
		const int bpp = bitmap_color_depth(bitmap), maskColor = bitmap_mask_color(bitmap);
		for(int i = 0; i < bitmap->w; i++) for(int j = 0; j < bitmap->h; j++)
		{
			// if 32-bit depth, check for transparent pixels (non-zero alpha channel) since "trans"-versions of draw/blit functions are needed only when there are transparent pixels
			if(bpp == 32 and not hasTransPixel and geta_depth(bpp, getpixel(bitmap, i, j)) != 0)
				hasTransPixel = true;

			// XXX dirty workaround for allegro 4 magenta transparency behavior.
			const int pixel = getpixel(bitmap, i, j),
					  colorWithMaskAlpha = makeacol_depth(bpp, getr_depth(bpp, pixel), getg_depth(bpp, pixel), getb_depth(bpp, pixel), geta_depth(bpp, maskColor));  // same color but with same alpha as mask

			// replaces mask color in pixels (which allegro will understand as transparent) with another one almost exactly the same, causing it to not be treated as transparent.
			if(colorWithMaskAlpha == maskColor)  // compare color with mask ignoring alpha (by using same alpha on both)
				putpixel(bitmap, i, j, makeacol_depth(bpp, getr_depth(bpp, pixel), getg_depth(bpp, pixel)+1, getb_depth(bpp, pixel), isOpaqueFormat? 255 : geta_depth(bpp, pixel)));  // replace with similar color
			else if(isOpaqueFormat)  // workaround for non-transparent images that load without opaque alpha (JPG, BMP, etc)
				putpixel(bitmap, i, j, makeacol_depth(bpp, getr_depth(bpp, pixel), getg_depth(bpp, pixel), getb_depth(bpp, pixel), 255));
		}

		// restore trans drawing mode
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);

		self.allegroBitmap = bitmap;
		self.needsTransFunction = hasTransPixel;
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroBitmap = create_bitmap(w, h);
		if(self.allegroBitmap == null)
			throw AdapterException("Could create image with dimensions w=%d h=%d. %s", w, h, allegro_error);

		clear_bitmap(self.allegroBitmap);
		self.needsTransFunction = (bitmap_color_depth(self.allegroBitmap) == 32);
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		destroy_bitmap(self.allegroBitmap);
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.allegroBitmap->w;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.allegroBitmap->h;
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		FGEAL_CHECK_INIT();
		const int bpp = bitmap_color_depth(self.allegroBitmap), pixel = getpixel(self.allegroBitmap, x, y);
		return Color::create(getr_depth(bpp, pixel), getg_depth(bpp, pixel), getb_depth(bpp, pixel), geta_depth(bpp, pixel));
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();
		pixelData.resize(width);
		const int bpp = bitmap_color_depth(self.allegroBitmap);
		for(unsigned i = 0; i < width and x+i < (unsigned) self.allegroBitmap->w; i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and y+j < (unsigned) self.allegroBitmap->h; j++)
			{
				const int pixel = getpixel(self.allegroBitmap, x+i, y+j);
				pixelData[i][j].r = getr_depth(bpp, pixel);
				pixelData[i][j].g = getg_depth(bpp, pixel);
				pixelData[i][j].b = getb_depth(bpp, pixel);
				pixelData[i][j].a = geta_depth(bpp, pixel);
			}
		}
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		solid_mode();
		putpixel(self.allegroBitmap, x, y, makeacol_depth(bitmap_color_depth(self.allegroBitmap), c.r, c.g, c.b, c.a));
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& c)
	{
		FGEAL_CHECK_INIT();
		solid_mode();
		const int bpp = bitmap_color_depth(self.allegroBitmap);
		for(unsigned i = 0; i < c.size() and x+i < (unsigned) self.allegroBitmap->w; i++)
			for(unsigned j = 0; j < c[i].size() and y+j < (unsigned) self.allegroBitmap->h; j++)
				putpixel(self.allegroBitmap, x+i, y+j, makeacol_depth(bpp, c[i][j].r, c[i][j].g, c[i][j].b, c[i][j].a));
		drawing_mode(DRAW_MODE_TRANS, null, 0, 0);
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
			draw_trans_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y);  //draw all source region
		else
			draw_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y);  //draw all source region
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
			customSlowBlitTrans(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, x, y, fromWidth, fromHeight);
		else
			::blit(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, x, y, fromWidth, fromHeight);
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE)
				draw_trans_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y);
//			else if(flipmode == Image::FLIP_VERTICAL)
//				rotate_sprite_v_flip_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, itofix(0));
			else
				draw_sprite_ex(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, DRAW_SPRITE_TRANS, toAllegroFlipFlag(flipmode));
		}
		else
			customDrawSpriteFlip(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, flipmode);
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		customSlowBlitEx(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, x, y, fromWidth, fromHeight, (self.needsTransFunction? DRAW_SPRITE_TRANS : DRAW_SPRITE_NORMAL), toAllegroFlipFlag(flipmode));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE and xScale == yScale)
				rotate_scaled_sprite_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, itofix(0), ftofix(xScale));
			else if(flipmode == Image::FLIP_VERTICAL and xScale == yScale)
				rotate_scaled_sprite_v_flip_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, itofix(0), ftofix(xScale));
			else
				customSlowStretchSpriteEx(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h, DRAW_SPRITE_TRANS, toAllegroFlipFlag(flipmode));
		}
		else
		{
			if(flipmode == Image::FLIP_NONE)
				stretch_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h);
			else if(flipmode == Image::FLIP_VERTICAL and xScale == yScale)
				rotate_scaled_sprite_v_flip(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, itofix(0), ftofix(xScale));
			else
				customSlowStretchSpriteEx(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h, DRAW_SPRITE_NORMAL, toAllegroFlipFlag(flipmode));
		}
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
			customSlowStretchBlitEx(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, fromWidth, fromHeight, x, y, xScale*fromWidth, yScale*fromHeight, DRAW_SPRITE_TRANS, toAllegroFlipFlag(flipmode));
		else
		{
			if(flipmode == Image::FLIP_NONE)
				stretch_blit(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, fromWidth, fromHeight, x, y, xScale*fromWidth, yScale*fromHeight);
			else
				customSlowStretchBlitEx(self.allegroBitmap, fgeal::drawTargetBitmap, fromX, fromY, fromWidth, fromHeight, x, y, xScale*fromWidth, yScale*fromHeight, DRAW_SPRITE_NORMAL, toAllegroFlipFlag(flipmode));
		}
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlipTrans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		else
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlip(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		//XXX Allegro 4 adapter: slow pre step to copy to a temporary bitmap
		BITMAP* const croppedBitmap = create_bitmap(fromWidth, fromHeight);  // create a temporary cropped surface
		if(croppedBitmap == null) return;  // fail silently
		::blit(self.allegroBitmap, croppedBitmap, fromX, fromY, 0, 0, fromWidth, fromHeight);
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite_trans(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip_trans(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlipTrans(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		else
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlip(fgeal::drawTargetBitmap, croppedBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		destroy_bitmap(croppedBitmap);
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		if(self.needsTransFunction)
		{
			if(xScale == yScale and flipmode == Image::FLIP_NONE)
				pivot_scaled_sprite_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), ftofix(xScale));
			else if(xScale == yScale and flipmode == Image::FLIP_VERTICAL)
				pivot_scaled_sprite_v_flip_trans(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX, centerY, toAllegroFixedAngle(angle), ftofix(xScale));
			else
			{
				//XXX Allegro 4 adapter: slow pre step to copy to a temporary bitmap
				BITMAP* const scaledBitmap = create_bitmap(xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h);
				if(scaledBitmap == null) return;  // fail silently
				stretch_sprite(scaledBitmap, self.allegroBitmap, 0, 0, xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h);
				if(flipmode == Image::FLIP_NONE)
					pivot_sprite_trans(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
				else if(flipmode == Image::FLIP_VERTICAL)
					pivot_sprite_v_flip_trans(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
				else
					customSlowPivotSpriteFlipTrans(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
				destroy_bitmap(scaledBitmap);
			}
		}
		else
		{
			if(xScale == yScale and flipmode == Image::FLIP_NONE)
				pivot_scaled_sprite(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), ftofix(xScale));
			else if(xScale == yScale and flipmode == Image::FLIP_VERTICAL)
				pivot_scaled_sprite_v_flip(fgeal::drawTargetBitmap, self.allegroBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), ftofix(xScale));
			else
			{
				//XXX Allegro 4 adapter: slow pre step to copy to a temporary bitmap
				BITMAP* const scaledBitmap = create_bitmap(xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h);
				if(scaledBitmap == null) return;  // fail silently
				stretch_sprite(scaledBitmap, self.allegroBitmap, 0, 0, xScale*self.allegroBitmap->w, yScale*self.allegroBitmap->h);
				if(flipmode == Image::FLIP_NONE)
					pivot_sprite(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
				else if(flipmode == Image::FLIP_VERTICAL)
					pivot_sprite_v_flip(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
				else
					customSlowPivotSpriteFlip(fgeal::drawTargetBitmap, scaledBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
				destroy_bitmap(scaledBitmap);
			}
		}
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		//XXX Allegro 4 adapter: slow pre step to copy to a temporary bitmap
		BITMAP* const scaledCroppedBitmap = create_bitmap(xScale*fromWidth, yScale*fromHeight);
		if(scaledCroppedBitmap == null) return;  // fail silently
		stretch_blit(self.allegroBitmap, scaledCroppedBitmap, fromX, fromY, fromWidth, fromHeight, 0, 0, xScale*fromWidth, yScale*fromHeight);
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite_trans(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip_trans(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlipTrans(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		else
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlip(fgeal::drawTargetBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		destroy_bitmap(scaledCroppedBitmap);
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		fgeal::setRgbaAlphaBlender();
		if(fromWidth == 0) fromWidth = self.allegroBitmap->w;
		if(fromHeight == 0) fromHeight = self.allegroBitmap->h;
		//XXX Allegro 4 adapter: slow pre step to copy to a temporary bitmap
		BITMAP* const scaledCroppedBitmap = create_bitmap(xScale*fromWidth, yScale*fromHeight);
		if(scaledCroppedBitmap == null) return;  // fail silently
		stretch_blit(self.allegroBitmap, scaledCroppedBitmap, fromX, fromY, fromWidth, fromHeight, 0, 0, xScale*fromWidth, yScale*fromHeight);
		if(self.needsTransFunction)
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite_trans(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip_trans(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlipTrans(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		else
		{
			if(flipmode == Image::FLIP_NONE)
				pivot_sprite(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else if(flipmode == Image::FLIP_VERTICAL)
				pivot_sprite_v_flip(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle));
			else
				customSlowPivotSpriteFlip(img.self.allegroBitmap, scaledCroppedBitmap, x, y, centerX*xScale, centerY*yScale, toAllegroFixedAngle(angle), toAllegroFlipFlag(flipmode));
		}
		destroy_bitmap(scaledCroppedBitmap);
		set_alpha_blender();
	}
}
