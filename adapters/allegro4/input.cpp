/*
 * input.cpp
 *
 *  Created on: 28/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#include <allegro.h>

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

using std::vector;
using std::string;

namespace fgeal
{
	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		// Returns the fgeal-equivalent key enum of the given allegro keycode.
		Keyboard::Key toGenericKey(int allegroKeycode)
		{
			switch(allegroKeycode)
			{
				#define KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the Allegro 4.x keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				#define BACKWARDS_KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return __allegro_KEY_MAX;
			}
		}
	}


	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key k)
	{
		FGEAL_CHECK_INIT();
		if(keyboard_needs_poll()) poll_keyboard();
		return key[KeyboardKeyMapping::toUnderlyingLibraryKey(k)];
	}

	// =======================================================================================
	// Mouse
	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		switch(btn)
		{
			case Mouse::BUTTON_LEFT:   return (mouse_b & 0);
			case Mouse::BUTTON_MIDDLE: return (mouse_b & 1);
			case Mouse::BUTTON_RIGHT:  return (mouse_b & 2);
			default: return false;
		}
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		const int position = mouse_pos;
		const Point pt = {static_cast<float>(position >> 16), static_cast<float>(position & 0x0000ffff)};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		position_mouse(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		return mouse_x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		return mouse_y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		if(mouse_needs_poll()) poll_mouse();
		position_mouse(x, y);
	}

	// =======================================================================================
	// Joystick

	// nothing is needed
	void Joystick::configureAll()
	{}

	// nothing is needed
	void Joystick::releaseAll()
	{}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return num_joysticks;
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		core::reportAbsentImplementation("Getting joystick name is not supported on this adapter.");
		return std::string();  //XXX Allegro 4 adapter: there is no way to get a joystick's name
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return joy[joyIndex].num_buttons;
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		unsigned axesCount = 0;
		for(int s = 0; s < joy[joyIndex].num_sticks; s++)
			axesCount += joy[joyIndex].stick[s].num_axis;
		return axesCount;
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return joy[joyIndex].button[buttonIndex].b;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		poll_joystick();
		return joy[joyIndex].stick[axisIndex/2].axis[axisIndex%2].pos/128.f - (joy[joyIndex].stick[axisIndex/2].flags & JOYFLAG_SIGNED? 0.f : 1.f);  // converting range [-128 to 128] or [0 to 255] to [-1.0 to 1.0], depending on the stick's flag
	}
}
