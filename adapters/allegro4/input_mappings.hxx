/*
 * input_mappings.hxx
 *
 *  Created on: 11 de abr de 2022
 *      Author: hydren
 */

// dummy case for Eclipse CDT parser, so it won't flood warnings when viewing this file in the editor
#ifdef __CDT_PARSER__
	#define KEYBOARD_MAPPINGS
	#define MOUSE_MAPPINGS
	#define mapping(a, b)  a, b;
	#include "fgeal/input.hpp"
	#include <allegro/keyboard.h>
	namespace fgeal { void dummy() {
#else
	#ifdef BACKWARDS_KEYBOARD_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define KEYBOARD_MAPPINGS
		#undef BACKWARDS_KEYBOARD_MAPPINGS
	#endif

	#undef mapping
	#ifdef BACKWARDS_MAPPINGS
		#define mapping(a, b) case b: return a;
	#else
		#define mapping(a, b) case a: return b;
	#endif
	#undef BACKWARDS_MAPPINGS
#endif

#ifdef KEYBOARD_MAPPINGS
	mapping(__allegro_KEY_A,  Keyboard::KEY_A)
	mapping(__allegro_KEY_B,  Keyboard::KEY_B)
	mapping(__allegro_KEY_C,  Keyboard::KEY_C)
	mapping(__allegro_KEY_D,  Keyboard::KEY_D)
	mapping(__allegro_KEY_E,  Keyboard::KEY_E)
	mapping(__allegro_KEY_F,  Keyboard::KEY_F)
	mapping(__allegro_KEY_G,  Keyboard::KEY_G)
	mapping(__allegro_KEY_H,  Keyboard::KEY_H)
	mapping(__allegro_KEY_I,  Keyboard::KEY_I)
	mapping(__allegro_KEY_J,  Keyboard::KEY_J)
	mapping(__allegro_KEY_K,  Keyboard::KEY_K)
	mapping(__allegro_KEY_L,  Keyboard::KEY_L)
	mapping(__allegro_KEY_M,  Keyboard::KEY_M)
	mapping(__allegro_KEY_N,  Keyboard::KEY_N)
	mapping(__allegro_KEY_O,  Keyboard::KEY_O)
	mapping(__allegro_KEY_P,  Keyboard::KEY_P)
	mapping(__allegro_KEY_Q,  Keyboard::KEY_Q)
	mapping(__allegro_KEY_R,  Keyboard::KEY_R)
	mapping(__allegro_KEY_S,  Keyboard::KEY_S)
	mapping(__allegro_KEY_T,  Keyboard::KEY_T)
	mapping(__allegro_KEY_U,  Keyboard::KEY_U)
	mapping(__allegro_KEY_V,  Keyboard::KEY_V)
	mapping(__allegro_KEY_W,  Keyboard::KEY_W)
	mapping(__allegro_KEY_X,  Keyboard::KEY_X)
	mapping(__allegro_KEY_Y,  Keyboard::KEY_Y)
	mapping(__allegro_KEY_Z,  Keyboard::KEY_Z)

	mapping(__allegro_KEY_0,  Keyboard::KEY_0)
	mapping(__allegro_KEY_1,  Keyboard::KEY_1)
	mapping(__allegro_KEY_2,  Keyboard::KEY_2)
	mapping(__allegro_KEY_3,  Keyboard::KEY_3)
	mapping(__allegro_KEY_4,  Keyboard::KEY_4)
	mapping(__allegro_KEY_5,  Keyboard::KEY_5)
	mapping(__allegro_KEY_6,  Keyboard::KEY_6)
	mapping(__allegro_KEY_7,  Keyboard::KEY_7)
	mapping(__allegro_KEY_8,  Keyboard::KEY_8)
	mapping(__allegro_KEY_9,  Keyboard::KEY_9)

	mapping(__allegro_KEY_F1,  Keyboard::KEY_F1)
	mapping(__allegro_KEY_F2,  Keyboard::KEY_F2)
	mapping(__allegro_KEY_F3,  Keyboard::KEY_F3)
	mapping(__allegro_KEY_F4,  Keyboard::KEY_F4)
	mapping(__allegro_KEY_F5,  Keyboard::KEY_F5)
	mapping(__allegro_KEY_F6,  Keyboard::KEY_F6)
	mapping(__allegro_KEY_F7,  Keyboard::KEY_F7)
	mapping(__allegro_KEY_F8,  Keyboard::KEY_F8)
	mapping(__allegro_KEY_F9,  Keyboard::KEY_F9)
	mapping(__allegro_KEY_F10, Keyboard::KEY_F10)
	mapping(__allegro_KEY_F11, Keyboard::KEY_F11)
	mapping(__allegro_KEY_F12, Keyboard::KEY_F12)

	mapping(__allegro_KEY_UP,    Keyboard::KEY_ARROW_UP)
	mapping(__allegro_KEY_DOWN,  Keyboard::KEY_ARROW_DOWN)
	mapping(__allegro_KEY_LEFT,  Keyboard::KEY_ARROW_LEFT)
	mapping(__allegro_KEY_RIGHT, Keyboard::KEY_ARROW_RIGHT)

	mapping(__allegro_KEY_ENTER,      Keyboard::KEY_ENTER)
	mapping(__allegro_KEY_SPACE,      Keyboard::KEY_SPACE)
	mapping(__allegro_KEY_ESC,        Keyboard::KEY_ESCAPE)
	mapping(__allegro_KEY_LCONTROL,   Keyboard::KEY_LEFT_CONTROL)
	mapping(__allegro_KEY_RCONTROL,   Keyboard::KEY_RIGHT_CONTROL)
	mapping(__allegro_KEY_LSHIFT,     Keyboard::KEY_LEFT_SHIFT)
	mapping(__allegro_KEY_RSHIFT,     Keyboard::KEY_RIGHT_SHIFT)
	mapping(__allegro_KEY_ALT,        Keyboard::KEY_LEFT_ALT)
	mapping(__allegro_KEY_ALTGR,      Keyboard::KEY_RIGHT_ALT)
	mapping(__allegro_KEY_LWIN,       Keyboard::KEY_LEFT_SUPER)
	mapping(__allegro_KEY_RWIN,       Keyboard::KEY_RIGHT_SUPER)
	mapping(__allegro_KEY_MENU,       Keyboard::KEY_MENU)
	mapping(__allegro_KEY_TAB,        Keyboard::KEY_TAB)
	mapping(__allegro_KEY_BACKSPACE,  Keyboard::KEY_BACKSPACE)
	mapping(__allegro_KEY_MINUS,      Keyboard::KEY_MINUS)
	mapping(__allegro_KEY_EQUALS,     Keyboard::KEY_EQUALS)
	mapping(__allegro_KEY_OPENBRACE,  Keyboard::KEY_LEFT_BRACKET)
	mapping(__allegro_KEY_CLOSEBRACE, Keyboard::KEY_RIGHT_BRACKET)
	mapping(__allegro_KEY_SEMICOLON,  Keyboard::KEY_SEMICOLON)
	mapping(__allegro_KEY_COMMA,      Keyboard::KEY_COMMA)
	mapping(__allegro_KEY_STOP,       Keyboard::KEY_PERIOD)
	mapping(__allegro_KEY_SLASH,      Keyboard::KEY_SLASH)
	mapping(__allegro_KEY_BACKSLASH,  Keyboard::KEY_BACKSLASH)
	mapping(__allegro_KEY_QUOTE,      Keyboard::KEY_QUOTE)
	mapping(__allegro_KEY_TILDE,      Keyboard::KEY_TILDE)

	mapping(__allegro_KEY_INSERT, Keyboard::KEY_INSERT)
	mapping(__allegro_KEY_DEL,    Keyboard::KEY_DELETE)
	mapping(__allegro_KEY_HOME,   Keyboard::KEY_HOME)
	mapping(__allegro_KEY_END,    Keyboard::KEY_END)
	mapping(__allegro_KEY_PGUP,   Keyboard::KEY_PAGE_UP)
	mapping(__allegro_KEY_PGDN,   Keyboard::KEY_PAGE_DOWN)

	mapping(__allegro_KEY_0_PAD,  Keyboard::KEY_NUMPAD_0)
	mapping(__allegro_KEY_1_PAD,  Keyboard::KEY_NUMPAD_1)
	mapping(__allegro_KEY_2_PAD,  Keyboard::KEY_NUMPAD_2)
	mapping(__allegro_KEY_3_PAD,  Keyboard::KEY_NUMPAD_3)
	mapping(__allegro_KEY_4_PAD,  Keyboard::KEY_NUMPAD_4)
	mapping(__allegro_KEY_5_PAD,  Keyboard::KEY_NUMPAD_5)
	mapping(__allegro_KEY_6_PAD,  Keyboard::KEY_NUMPAD_6)
	mapping(__allegro_KEY_7_PAD,  Keyboard::KEY_NUMPAD_7)
	mapping(__allegro_KEY_8_PAD,  Keyboard::KEY_NUMPAD_8)
	mapping(__allegro_KEY_9_PAD,  Keyboard::KEY_NUMPAD_9)

	mapping(__allegro_KEY_PLUS_PAD,   Keyboard::KEY_NUMPAD_ADDITION)
	mapping(__allegro_KEY_MINUS_PAD,  Keyboard::KEY_NUMPAD_SUBTRACTION)
	mapping(__allegro_KEY_ASTERISK,   Keyboard::KEY_NUMPAD_MULTIPLICATION)
	mapping(__allegro_KEY_SLASH_PAD,  Keyboard::KEY_NUMPAD_DIVISION)
	mapping(__allegro_KEY_DEL_PAD,    Keyboard::KEY_NUMPAD_DECIMAL)
	mapping(__allegro_KEY_ENTER_PAD,  Keyboard::KEY_NUMPAD_ENTER)
#endif
#undef KEYBOARD_MAPPINGS

#ifdef __CDT_PARSER__
}}
#endif
