/*
 * sound.cpp
 *
 *  Created on: 29/03/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro.h>

#ifdef FGEAL_ALLEGRO4_ALLEGROMP3_INSTALLED
	#include <almp3.h>
#endif

using std::string;

namespace fgeal
{
	// pointer to the current midi music; it is null if no midi music is being played.
	Music* currentMidiMusic = null;

	/* Notes:
	 * - play_sample() appears to be a simpler approach to handle sample playing parameters, but it prevents us from pausing the
	 * sample with voice_stop(). So we are forced to use voice_start() instead.
	 * - adjust_sample() only works on the first instance playing, unlike stop_sample(), which affects all instances. So setVolume()
	 * doesn't work properly when the sound was paused and then played again because only the first instance is affected (even if
	 * the first instance has finished playing). So we are forced to use voice_set_xxx() functions on all voices allocated to the
	 * sample.
	 * - voice_get_position() appears to return -1 if the music is not playing (paused of stopped) despite the allegro 4
	 * documentation saying that it returns -1 when it has finished playing.
	 * */

	#ifdef FGEAL_ALLEGRO4_ALLEGROMP3_INSTALLED

	// loads a mp3 sample and returns as expected by a load_sample() call
	SAMPLE* customAllegroMp3LoadSample(AL_CONST char* filename)
	{
		PACKFILE* const mp3packfile = pack_fopen(filename, "r");  // open file for reading
		if(mp3packfile == null)
			return null;

		const long mp3filesize = file_size_ex(filename);  // estimate file size
		char* const mp3data = new char[mp3filesize];  // allocate buffer for data
		const long mp3datasize = pack_fread(mp3data, mp3filesize, mp3packfile);  // read and save actual data size (the amount of bytes read)
		ALMP3_MP3* const mp3 = almp3_create_mp3(mp3data, mp3datasize);  // create ALMP3_MP3 instance
		if(mp3 == null)
		{
			delete[] mp3data;
			pack_fclose(mp3packfile);
			return null;
		}

		SAMPLE* const sample = almp3_create_sample_from_mp3(mp3);
		// free temporary data (XXX Allegro 4 adapter: uncertain if these buffers should be free'd or kept until SAMPLE's lifetime - see https://allegro.cc/forums/thread/553582)
		almp3_destroy_mp3(mp3);
		delete[] mp3data;
		pack_fclose(mp3packfile);
		return sample;  // if 'sample'==null, null is returned, but we have already free'd 'mp3', 'mp3data' and 'mp3packfile'
	}
	END_OF_FUNCTION(customAllegroMp3LoadSample)

	#endif /* FGEAL_ALLEGRO4_ALLEGROMP3_INSTALLED */

	//----------------------------------------------------------------------------------------------------------------------------

	Sound::Sound(const string& filename, float volume)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroSample = load_sample(filename.c_str());

		if(self.allegroSample == null)
			throw AdapterException("Could not load audio file \"%s\". Error %s", filename.c_str(), allegro_error);

		self.frequency = self.allegroSample->freq;  // sample normal frequency
		self.volume = volume * 255;  // 255 is the maximum volume
	}

	Sound::~Sound()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroSample != null)
			destroy_sample(self.allegroSample);
		delete &self;
	}

	void Sound::play()
	{
		FGEAL_CHECK_INIT();
		const int voice = allocate_voice(self.allegroSample);
		if(voice < 0)
			throw AdapterException("Could not play sample: %s", allegro_error);

		voice_set_volume(voice, self.volume);
		voice_set_frequency(voice, self.frequency);
		voice_set_playmode(voice, PLAYMODE_PLAY);
		voice_start(voice);
		release_voice(voice);  // this ensures that the voice will be de-allocated from this sample when it stops playing
	}

	void Sound::loop()
	{
		FGEAL_CHECK_INIT();
		const int voice = allocate_voice(self.allegroSample);
		if(voice < 0)
			throw AdapterException("Could not play sample: %s", allegro_error);

		voice_set_volume(voice, self.volume);
		voice_set_frequency(voice, self.frequency);
		voice_set_playmode(voice, PLAYMODE_LOOP);
		voice_start(voice);
		release_voice(voice);  // this ensures that the voice will be de-allocated from this sample when it stops playing
	}

	void Sound::stop()
	{
		FGEAL_CHECK_INIT();
		stop_sample(self.allegroSample);
	}

	void Sound::pause()
	{
		FGEAL_CHECK_INIT();
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
			if(voice_check(i) == self.allegroSample)
			{
				// since we needed to already release the voice right after playing it, attempts to pause it will make the voice deallocate
				// so we need to reallocate the sample to some voice, but with the same position as the previous voice
				const int position = voice_get_position(i);
				if(position != -1)  // if not paused already
				{
					voice_stop(i);  // "pauses" by stopping
					const int otherVoice = allocate_voice(self.allegroSample);  // allocates sample again
					voice_set_position(otherVoice, position);  // set previous position, so when voice_start() called, it resumes properly
				}
			}
	}

	void Sound::resume()
	{
		FGEAL_CHECK_INIT();
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
			if(voice_check(i) == self.allegroSample)
			{
				voice_start(i);
				release_voice(i);  // this ensures that the voice will be de-allocated from this sample when it stops playing
			}
	}

	bool Sound::isPlaying()
	{
		FGEAL_CHECK_INIT();
		// if at least one voice is playing this sample, then its playing
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
			if(voice_get_position(i) != -1 and voice_check(i) == self.allegroSample)
				return true;
		return false;
	}

	bool Sound::isPaused()
	{
		FGEAL_CHECK_INIT();
		// if there is at least one voice with this sample and all those voices are not playing, then its paused
		bool hasVoiceAllocated = false;
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
		if(voice_check(i) == self.allegroSample)
		{
			if(voice_get_position(i) != -1)  // is playing
				return false;
			else
				hasVoiceAllocated = true;
		}

		return hasVoiceAllocated;
	}

	void Sound::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		self.volume = value * 255;
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
			if(voice_check(i) == self.allegroSample)
				voice_set_volume(i, self.volume);
	}

	float Sound::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.volume / 255.0f;
	}

	float Sound::getDuration()
	{
		FGEAL_CHECK_INIT();
		return static_cast<float>(self.allegroSample->len) / static_cast<float>(self.allegroSample->freq);
	}

	void Sound::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		self.frequency = (factor * self.allegroSample->freq);
		for(unsigned i = 0; i < DIGI_VOICES; i++)  // for all voices dealing with this sample
			if(voice_check(i) == self.allegroSample)
				voice_set_frequency(i, self.frequency);
	}

	float Sound::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return static_cast<float>(self.frequency) / static_cast<float>(self.allegroSample->freq);
	}

	// ##################################################################################################################

	Music::Music(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		if(filenameHasExtension(filename, "mid") or filenameHasExtension(filename, "midi"))
		{
			self.allegroMidi = load_midi(filename.c_str());
			self.midiPausedPosition = 0;
			self.fgealSoundStream = null;

			if(self.allegroMidi == null)
				throw AdapterException("Could not load midi music file \"%s\". Error %s", filename.c_str(), allegro_error);
		}
		else
		{
			self.allegroMidi = null;
			self.midiPausedPosition = -1;
			self.fgealSoundStream = new SoundStream(filename);
		}
	}

	Music::~Music()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
			destroy_midi(self.allegroMidi);
		if(self.fgealSoundStream != null)
			delete self.fgealSoundStream;
		delete &self;
	}

	void Music::play()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			if(currentMidiMusic != this and currentMidiMusic != null)
				currentMidiMusic->self.midiPausedPosition = 0;

			if(play_midi(self.allegroMidi, 0) != 0)
				throw AdapterException("Could not play midi: %s", allegro_error);

			currentMidiMusic = this;
		}
		else
			self.fgealSoundStream->play();
	}

	void Music::loop()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			if(currentMidiMusic != this and currentMidiMusic != null)
				currentMidiMusic->self.midiPausedPosition = 0;

			if(play_midi(self.allegroMidi, !0) != 0)
				throw AdapterException("Could not play midi: %s", allegro_error);

			currentMidiMusic = this;
		}
		else
			self.fgealSoundStream->loop();
	}

	void Music::stop()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			midi_pause();
			midi_seek(0);
			self.midiPausedPosition = 0;
		}
		else
			self.fgealSoundStream->stop();
	}

	void Music::pause()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			self.midiPausedPosition = midi_pos;
			midi_pause();
		}
		else
			self.fgealSoundStream->pause();
	}

	void Music::resume()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			if(currentMidiMusic != this)
			{
				if(currentMidiMusic != null)
					currentMidiMusic->self.midiPausedPosition = 0;

				play_midi(self.allegroMidi, 0);
				midi_seek(self.midiPausedPosition);
				currentMidiMusic = this;
			}
			else
				midi_resume();
		}
		else
			self.fgealSoundStream->resume();
	}

	bool Music::isPlaying()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			if(currentMidiMusic == this and midi_pos >= 0)
				return true;
			else
				return false;
		}
		else
			return self.fgealSoundStream->isPlaying();
	}

	bool Music::isPaused()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null)
		{
			if(self.midiPausedPosition != 0)
				return true;
			else
				return false;
		}
		else
			return self.fgealSoundStream->isPaused();
	}

	//XXX Allegro 4 adapter's Music::setVolume() doesn't work for midi's
	void Music::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null) core::reportAbsentImplementation("Allegro 4 cannot manipulate midi sound volume");
		else self.fgealSoundStream->setVolume(value);
	}

	float Music::getVolume()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroMidi != null) return 1.0f;
		return self.fgealSoundStream->getVolume();
	}
}
