/*
 * core.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fgeal/core.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

namespace fgeal
{
	/* fgeal code based on Allegro 5.x */
	const char* ADAPTER_NAME = "Allegro 5.x Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "Allegro";
	const char* ADAPTED_LIBRARY_VERSION = ALLEGRO_VERSION_STR;

	static const unsigned DEFAULT_RESERVED_SAMPLES_COUNT = 16;

	// initialize all allegro stuff
	void core::initialize()
	{
		if(not al_init()) 					throw AdapterException("Allegro could not be initialized: error %d", al_get_errno());
		if(not al_init_image_addon()) 		throw AdapterException("Allegro image addon could not be initialized: error %d", al_get_errno());
		if(not al_init_primitives_addon()) 	throw AdapterException("Allegro primitives addon could not be initialized: error %d", al_get_errno());
		al_init_font_addon();  				// never fails?
		if(not al_init_ttf_addon()) 		throw AdapterException("Allegro ttf addon could not be initialized: error %d", al_get_errno());
		if(not al_init_acodec_addon()) 		throw AdapterException("Allegro audio codec addon could not be initialized: error %d", al_get_errno());

		if(not al_install_audio()) 			throw AdapterException("Could not install audio: error %d", al_get_errno());
		if(not al_install_keyboard()) 		throw AdapterException("Could not install keyboard: error %d", al_get_errno());
		if(not al_install_mouse()) 			throw AdapterException("Could not install mouse: %d", al_get_errno());
		if(not al_install_joystick())       throw AdapterException("Could not install joystick: %d", al_get_errno());

		if(not al_reserve_samples(DEFAULT_RESERVED_SAMPLES_COUNT))
			throw AdapterException("Could not reserve audio samples: error %d", al_get_errno());
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
		al_uninstall_system();
	}

	bool isProperlyInitialized()
	{
		return al_is_system_installed();
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		al_rest(seconds);
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return al_get_time();
	}
}
