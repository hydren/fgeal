/*
 * event.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/event.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>

namespace fgeal
{
	Event::Event()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroEvent = new ALLEGRO_EVENT();
	}

	Event::~Event()
	{
		FGEAL_CHECK_INIT();
		delete self.allegroEvent;
		delete &self;
	}

	Event::Type Event::getEventType()
	{
		FGEAL_CHECK_INIT();
		switch(self.allegroEvent->type)
		{
			case ALLEGRO_EVENT_DISPLAY_CLOSE:         return Event::TYPE_DISPLAY_CLOSURE;
			case ALLEGRO_EVENT_KEY_DOWN:              return Event::TYPE_KEY_PRESS;
			case ALLEGRO_EVENT_KEY_UP:                return Event::TYPE_KEY_RELEASE;
			case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:     return Event::TYPE_MOUSE_BUTTON_PRESS;
			case ALLEGRO_EVENT_MOUSE_BUTTON_UP:       return Event::TYPE_MOUSE_BUTTON_RELEASE;
			case ALLEGRO_EVENT_MOUSE_AXES:            return (self.allegroEvent->mouse.dz != 0 or self.allegroEvent->mouse.dw != 0)?
																	      Event::TYPE_MOUSE_WHEEL_MOTION : Event::TYPE_MOUSE_MOTION;
			case ALLEGRO_EVENT_JOYSTICK_AXIS:         return Event::TYPE_JOYSTICK_AXIS_MOTION;
			case ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN:  return Event::TYPE_JOYSTICK_BUTTON_PRESS;
			case ALLEGRO_EVENT_JOYSTICK_BUTTON_UP:    return Event::TYPE_JOYSTICK_BUTTON_RELEASE;
//			case ALLEGRO_EVENT_JOYSTICK_CONFIGURATION:  return Event::TYPE_JOYSTICK_???;

			default: return Event::TYPE_UNKNOWN;
		}
	}

	Keyboard::Key Event::getEventKeyCode()
	{
		FGEAL_CHECK_INIT();
		return KeyboardKeyMapping::toGenericKey(self.allegroEvent->keyboard.keycode);
	}

	Mouse::Button Event::getEventMouseButton()
	{
		FGEAL_CHECK_INIT();
		return MouseButtonMapping::toGenericMouseButton(self.allegroEvent->mouse.button);
	}

	int Event::getEventMouseX()
	{
		FGEAL_CHECK_INIT();
		return self.allegroEvent->mouse.x;
	}

	int Event::getEventMouseY()
	{
		FGEAL_CHECK_INIT();
		return self.allegroEvent->mouse.y;
	}

	int Event::getEventMouseWheelMotionAmount()
	{
		FGEAL_CHECK_INIT();
		return self.allegroEvent->mouse.dz;
	}

	int Event::getEventJoystickIndex()
	{
		FGEAL_CHECK_INIT();

		for(unsigned j = 0; j < JoystickManagement::allegroJoystickPtrs.size(); j++)
			if(JoystickManagement::allegroJoystickPtrs[j] == self.allegroEvent->joystick.id)
				return j;

		/* this version avoids using the auxiliary data, but its unclear whether the al_get_joystick() call is cumbersome or not
		for(int j = 0; j < al_get_num_joysticks(); j++)
			if(al_get_joystick(j) == self.allegroEvent->joystick.id)
				return j;
		*/

		return -1;  // should never happen unless this is not a joystick event
	}

	int Event::getEventJoystickButtonIndex()
	{
		FGEAL_CHECK_INIT();
		return self.allegroEvent->joystick.button;
	}

	int Event::getEventJoystickAxisIndex()
	{
		FGEAL_CHECK_INIT();
		// XXX this seems to be overly complex and possibly an overhead source
		for(int s = 0, previousAxesCount = 0; s < al_get_joystick_num_sticks(self.allegroEvent->joystick.id); s++)
			if(s == self.allegroEvent->joystick.stick)
				return previousAxesCount + self.allegroEvent->joystick.axis;
			else
				previousAxesCount += al_get_joystick_num_axes(self.allegroEvent->joystick.id, s);

		return -1;  // should never happen unless this is not a joystick event
	}

	float Event::getEventJoystickAxisPosition()
	{
		FGEAL_CHECK_INIT();
		return self.allegroEvent->joystick.pos;
	}

	int Event::getEventJoystickSecondAxisIndex()
	{
		return -1;
	}

	float Event::getEventJoystickSecondAxisPosition()
	{
		return 0;
	}

	// ------------------------------------------------------------------------------------------------------------------------------

	EventQueue::EventQueue()
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		self.allegroEventQueue = al_create_event_queue();

		if(self.allegroEventQueue == null)
			throw AdapterException("Could not create event queue");

		al_register_event_source(self.allegroEventQueue, al_get_display_event_source(Display::instance->self.allegroDisplay));
		al_register_event_source(self.allegroEventQueue, al_get_keyboard_event_source());
		al_register_event_source(self.allegroEventQueue, al_get_mouse_event_source());
		al_register_event_source(self.allegroEventQueue, al_get_joystick_event_source());
	}

	EventQueue::~EventQueue()
	{
		FGEAL_CHECK_INIT();
		al_destroy_event_queue(self.allegroEventQueue);
		delete &self;
	}

	bool EventQueue::isEmpty()
	{
		FGEAL_CHECK_INIT();
		return al_is_event_queue_empty(self.allegroEventQueue);
	}

	bool EventQueue::hasEvents()
	{
		FGEAL_CHECK_INIT();
		return !al_is_event_queue_empty(self.allegroEventQueue);
	}

	Event* EventQueue::getNextEvent()
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_EVENT allegroEvent;
		if(al_get_next_event(self.allegroEventQueue, &allegroEvent) == true)
		{
			Event* event = new Event();
			*event->self.allegroEvent = allegroEvent;
			return event;
		}
		else return null;
	}

	bool EventQueue::getNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		return al_get_next_event(self.allegroEventQueue, container->self.allegroEvent);
	}

	bool EventQueue::skipNextEvent()
	{
		ALLEGRO_EVENT tmpEvent;
		return al_get_next_event(self.allegroEventQueue, &tmpEvent);
	}

	void EventQueue::waitNextEvent(Event* container)
	{
		FGEAL_CHECK_INIT();
		al_wait_for_event(self.allegroEventQueue, container == null ? null : container->self.allegroEvent);
	}

	void EventQueue::flushEvents()
	{
		FGEAL_CHECK_INIT();
		al_flush_event_queue(self.allegroEventQueue);
	}

	void EventQueue::setEventBypassingEnabled(bool bypass)
	{
		FGEAL_CHECK_INIT();
		if(bypass)
		{
			al_unregister_event_source(self.allegroEventQueue, al_get_display_event_source(Display::instance->self.allegroDisplay));
			al_unregister_event_source(self.allegroEventQueue, al_get_keyboard_event_source());
			al_unregister_event_source(self.allegroEventQueue, al_get_mouse_event_source());
			al_flush_event_queue(self.allegroEventQueue);
		}
		else
		{
			al_flush_event_queue(self.allegroEventQueue);
			al_register_event_source(self.allegroEventQueue, al_get_display_event_source(Display::instance->self.allegroDisplay));
			al_register_event_source(self.allegroEventQueue, al_get_keyboard_event_source());
			al_register_event_source(self.allegroEventQueue, al_get_mouse_event_source());
		}
	}
}
