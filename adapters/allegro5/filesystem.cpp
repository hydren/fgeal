/*
 * filesystem.cpp
 *
 *  Created on: 9 de out de 2019
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2019  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"

#ifndef FGEAL_ALLEGRO5_USE_PHYSFS_FILESYSTEM_IMPLEMENTATION
#include "fgeal/agnostic_filesystem.hxx"

#else  /* ****** Optional implementation of filesystem calls using Allegro 5's PhysicsFS addon ******* */
#include "fgeal/filesystem.hpp"
#include "fgeal/core.hpp"
#include "fgeal/exceptions.hpp"

#include <algorithm>

using std::vector;
using std::string;

namespace fgeal
{
	vector<string> filesystem::getFilenamesWithinDirectory(const string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		vector<string> filenames;
		ALLEGRO_FS_ENTRY* directory = al_create_fs_entry(directoryPath.c_str());

		if(not al_open_directory(directory))
			throw AdapterException("Could not open directory \"%s\": Allegro error %d", directoryPath.c_str(), al_get_errno());

		for(ALLEGRO_FS_ENTRY* entry = al_read_directory(directory); entry != null; entry = al_read_directory(directory))
		{
			filenames.push_back(al_get_fs_entry_name(entry));
			al_destroy_fs_entry(entry);
		}
		std::sort(filenames.begin(), filenames.end());

		al_destroy_fs_entry(directory);
		return filenames;
	}

	string filesystem::getCurrentWorkingDirectory()
	{
		FGEAL_CHECK_INIT();
		char* cPath = al_get_current_directory();

		if(cPath == null)
			throw AdapterException("Could not fetch current directory name: Allegro error %d", al_get_errno());

		string sPath = string(cPath);
		delete cPath;
		return sPath;
	}

	void filesystem::setWorkingDirectory(const std::string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		const bool successful = al_change_directory(directoryPath.c_str());

		if(not successful)
			throw AdapterException("Could set current directory: Allegro error %d", al_get_errno());
	}

	bool filesystem::isFilenameDirectory(const string& path)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_FS_ENTRY* entry = al_create_fs_entry(path.c_str());
		bool isDirectory = al_get_fs_entry_mode(entry) & ALLEGRO_FILEMODE_ISDIR;
		al_destroy_fs_entry(entry);
		return isDirectory;
	}

	bool filesystem::isFilenameArchive(const string& path)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_FS_ENTRY* entry = al_create_fs_entry(path.c_str());
		bool isFile = al_get_fs_entry_mode(entry) & ALLEGRO_FILEMODE_ISFILE;
		al_destroy_fs_entry(entry);
		return isFile;
	}
}
#endif
