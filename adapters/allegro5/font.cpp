/*
 * font.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

using std::string;

namespace fgeal
{
	Font::Font(const string& filename, unsigned size, bool antialiasing, bool kerning)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		const int flags = (antialiasing? 0 : ALLEGRO_TTF_MONOCHROME) | (kerning? 0 : ALLEGRO_TTF_NO_KERNING);
		self.allegroFont = al_load_font(filename.c_str(), size, flags);

		if(self.allegroFont == null)
			throw AdapterException("Font %s could not be loaded: Error %d", filename.c_str(), al_get_errno());

		self.fontFilename = filename;
		self.size = size;
		self.flags = flags;
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		al_destroy_font(self.allegroFont);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		al_draw_text(self.allegroFont, al_map_rgba(color.r, color.g, color.b, color.a), x, y, ALLEGRO_ALIGN_LEFT, text.c_str());
	}

	float Font::getTextHeight() const
	{
		FGEAL_CHECK_INIT();
		return al_get_font_line_height(self.allegroFont);
	}

	float Font::getTextWidth(const string& text) const
	{
		FGEAL_CHECK_INIT();
		return al_get_text_width(self.allegroFont, text.c_str());
	}

	unsigned Font::getSize() const
	{
		return self.size;
	}

	void Font::setSize(unsigned size)
	{
		FGEAL_CHECK_INIT();
		al_destroy_font(self.allegroFont);
		self.allegroFont = al_load_font(self.fontFilename.c_str(), size, self.flags);
		if(self.allegroFont == null)
			throw AdapterException("Could not resize font %s: Error %d", self.fontFilename.c_str(), al_get_errno());
		self.size = size;
	}

	// ##################################################################################################################

	DrawableText::DrawableText(const string& text, Font* font, Color color)
	: self(*new implementation()), font(font)
	{
		self.content = text;
		self.color = al_map_rgba(color.r, color.g, color.b, color.a);
	}

	DrawableText::~DrawableText()
	{}

	DrawableText& DrawableText::operator=(const DrawableText& other)
	{
		if(this != &other)
		{
			font = other.font;
			self.content = other.self.content;
			self.color = other.self.color;
		}
		return *this;
	};

	DrawableText::DrawableText(const DrawableText& other)
	: self(*new implementation())
	{
		operator =(other);
	}

	float DrawableText::getWidth()
	{
		return font != null? font->getTextWidth(self.content) : 0;
	}

	float DrawableText::getHeight()
	{
		return font != null? font->getTextHeight() : 0;
	}

	void DrawableText::setFont(Font* font)
	{
		this->font = font;
	}

	void DrawableText::setContent(const string& str)
	{
		self.content = str;
	}

	string DrawableText::getContent()
	{
		return self.content;
	}

	void DrawableText::setColor(Color color)
	{
		self.color = al_map_rgba(color.r, color.g, color.b, color.a);
	}

	Color DrawableText::getColor()
	{
		return Color::fromFloat(self.color.r, self.color.g, self.color.b, self.color.a);
	}

	void DrawableText::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		if(font != null)
			al_draw_text(font->self.allegroFont, self.color, x, y, ALLEGRO_ALIGN_LEFT, self.content.c_str());
	}
}
