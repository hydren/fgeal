/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include <cmath>
#include <algorithm>

using std::vector;
using std::min;

namespace fgeal
{
	#if ALLEGRO_SUB_VERSION == 0 and ALLEGRO_WIP_VERSION < 6  // implementation for signatures introduced in 5.0.6
	//XXX untested custom function to draw pie slice
	template<bool filled>
	static void custom_al_draw_pieslice(float cx, float cy, float r, float start_theta, float delta_theta, ALLEGRO_COLOR color)
	{
		 ALLEGRO_VERTEX vertex_cache[ALLEGRO_VERTEX_CACHE_SIZE];
		 int num_segments, ii;
		 num_segments = fabs(delta_theta / (2 * ALLEGRO_PI) * ALLEGRO_PRIM_QUALITY * sqrtf(r));
		 if (num_segments < 2)
			 return;
		 if (num_segments >= ALLEGRO_VERTEX_CACHE_SIZE)
		 {
			 num_segments = ALLEGRO_VERTEX_CACHE_SIZE - 1;
		 }
		 al_calculate_arc(&(vertex_cache[1].x), sizeof(ALLEGRO_VERTEX), cx, cy, r, r, start_theta, delta_theta, 0, num_segments);
		 vertex_cache[0].x = cx; vertex_cache[0].y = cy;
		 for (ii = 0; ii < num_segments + 1; ii++)
		 {
			 vertex_cache[ii].color = color;
			 vertex_cache[ii].z = 0;
		 }
		 if(filled) al_draw_prim(vertex_cache, 0, 0, 0, num_segments + 1, ALLEGRO_PRIM_TRIANGLE_FAN);
		 al_draw_prim(vertex_cache, 0, 0, 0, num_segments + 1, ALLEGRO_PRIM_LINE_LOOP);
	}

	//XXX delegate to custom substitute which partially implements circular sector drawing
	#define al_draw_pieslice(cx, cy, r, st, dt, c, th) custom_al_draw_pieslice<false>(cx, cy, r, st, dt, c)
	#define al_draw_filled_pieslice custom_al_draw_pieslice<true>

	#endif

	#if ALLEGRO_SUB_VERSION < 1 or (ALLEGRO_SUB_VERSION == 1 and ALLEGRO_WIP_VERSION < 12) // implementation for signatures introduced in 5.1.12
	static ALLEGRO_COLOR al_premul_rgba(unsigned char r, unsigned char g, unsigned char b, unsigned char a) { return al_map_rgba(r*a/255, g*a/255, b*a/255, a); }
	#endif

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		al_set_target_bitmap(image->self.allegroBitmap);  // @suppress("Invalid arguments")
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		al_set_target_backbuffer(display.self.allegroDisplay);
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		al_set_target_backbuffer(Display::instance->self.allegroDisplay);
	}

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_line(x1, y1, x2, y2, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_line(x1, y1, x2, y2, al_premul_rgba(c.r, c.g, c.b, c.a), thickness);
	}

	void Graphics::drawRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_rectangle(x, y, x+w, y+h, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledRectangle(float x, float y, float w, float h, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_rectangle(x-0.5f, y-0.5f, x+w+0.5f, y+h+0.5f, al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_circle(cx, cy, radius, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledCircle(float cx, float cy, float radius, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_circle(cx, cy, radius, al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_ellipse(cx, cy, rx, ry, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_ellipse(cx, cy, rx, ry, al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_triangle(x1, y1, x2, y2, x3, y3, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_triangle(x1, y1, x2, y2, x3, y3, al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const ALLEGRO_VERTEX vertices[4] = {
			{x1, y1, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x2, y2, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x3, y3, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x4, y4, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)}
		};
		al_draw_prim(vertices, null, null, 0, 4, ALLEGRO_PRIM_LINE_LOOP);
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const ALLEGRO_VERTEX vertices[4] = {
			{x1, y1, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x2, y2, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x3, y3, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)},
			{x4, y4, 0, 0, 0, al_premul_rgba(c.r, c.g, c.b, c.a)}
		};
		al_draw_prim(vertices, null, null, 0, 4, ALLEGRO_PRIM_TRIANGLE_FAN);
		al_draw_prim(vertices, null, null, 0, 4, ALLEGRO_PRIM_LINE_LOOP);
	}

	void Graphics::drawArc(float cx, float cy, float radius, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_arc(cx, cy, radius, -ai, -(af-ai), al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawThickArc(float cx, float cy, float radius, float thickness, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_arc(cx, cy, radius, -ai, -(af-ai), al_premul_rgba(c.r, c.g, c.b, c.a), thickness);
	}

	void Graphics::drawCircleSector(float cx, float cy, float radius, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_pieslice(cx, cy, radius, -ai, -(af-ai), al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float radius, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_pieslice(cx, cy, radius, -ai, -(af-ai), al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawRoundedRectangle(float x, float y, float w, float h, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_rounded_rectangle(x, y, x+w, y+h, r, r, al_premul_rgba(c.r, c.g, c.b, c.a), 1.0f);
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float w, float h, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		al_draw_filled_rounded_rectangle(x, y, x+w, y+h, r, r, al_premul_rgba(c.r, c.g, c.b, c.a));
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = min(x.size(), y.size());
		vector<ALLEGRO_VERTEX> vertices(n);
		for(unsigned i = 0; i < n; i++)
		{
			vertices[i].x = x[i]; vertices[i].y = y[i];
			vertices[i].z = vertices[i].u = vertices[i].v = 0;
			vertices[i].color = al_premul_rgba(c.r, c.g, c.b, c.a);
		}
		al_draw_prim(&vertices[0], null, null, 0, n, ALLEGRO_PRIM_LINE_LOOP);
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = min(x.size(), y.size());
		vector<ALLEGRO_VERTEX> vertices(n);
		for(unsigned i = 0; i < n; i++)
		{
			vertices[i].x = x[i]; vertices[i].y = y[i];
			vertices[i].z = vertices[i].u = vertices[i].v = 0;
			vertices[i].color = al_premul_rgba(c.r, c.g, c.b, c.a);
		}
		al_draw_prim(&vertices[0], null, null, 0, n, ALLEGRO_PRIM_TRIANGLE_FAN);
		al_draw_prim(&vertices[0], null, null, 0, n, ALLEGRO_PRIM_LINE_LOOP);
	}
}
