/*
 * image.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include <cmath>

using std::string;
using std::vector;

namespace fgeal
{
	// facade to other allegro calls. update this if better signatures come in the future.
	void custom_al_draw_rotated_bitmap_region(ALLEGRO_BITMAP *bitmap, float sx, float sy, float sw, float sh, float cx, float cy, float dx, float dy, float angle, int flags)
	{
		#if ALLEGRO_SUB_VERSION > 0 or ALLEGRO_WIP_VERSION >= 6  // using signature introduced in 5.0.6
			al_draw_tinted_scaled_rotated_bitmap_region(bitmap, sx, sy, sw, sh, al_map_rgba_f(1.0f, 1.0f, 1.0f, 1.0f), cx, cy, dx, dy, 1.0, 1.0, angle, flags);

		#else // older way
			ALLEGRO_BITMAP* sub_bitmap = al_create_sub_bitmap(bitmap, sx, sy, sw, sh);
			al_draw_rotated_bitmap(sub_bitmap, cx, cy, dx, dy, angle, flags);
			al_destroy_bitmap(sub_bitmap); // this may be wrong
		#endif
	}

	// facade to other allegro calls. update this if better signatures come in the future.
	void custom_al_draw_scaled_rotated_bitmap_region(ALLEGRO_BITMAP *bitmap, float sx, float sy, float sw, float sh, float cx, float cy, float dx, float dy, float xscale, float yscale, float angle, int flags)
	{
		#if ALLEGRO_SUB_VERSION > 0 or ALLEGRO_WIP_VERSION >= 6  // using signature introduced in 5.0.6
			al_draw_tinted_scaled_rotated_bitmap_region(bitmap, sx, sy, sw, sh, al_map_rgba_f(1.0f, 1.0f, 1.0f, 1.0f), cx, cy, dx, dy, xscale, yscale, angle, flags);

		#else // older way
			ALLEGRO_BITMAP* sub_bitmap = al_create_sub_bitmap(bitmap, sx, sy, sw, sh);
			al_draw_scaled_rotated_bitmap(sub_bitmap, cx, cy, dx, dy, xscale, yscale, angle, flags);
			al_destroy_bitmap(sub_bitmap); // this may be wrong
		#endif
	}

	static int toAllegroFlipFlag(const Image::FlipMode mode)
	{
		switch (mode) {
			default:case Image::FLIP_NONE:  return 0;
			case Image::FLIP_HORIZONTAL:    return ALLEGRO_FLIP_HORIZONTAL;
			case Image::FLIP_VERTICAL:      return ALLEGRO_FLIP_VERTICAL;
		}
	}

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroBitmap = al_load_bitmap(filename.c_str());
		if ( self.allegroBitmap == null)
			throw AdapterException("Could not load image: \"%s\". Error %d", filename.c_str(), al_get_errno());
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroBitmap = al_create_bitmap(w, h);
		if ( self.allegroBitmap == null)
			throw AdapterException("Could create image with dimensions w=%d h=%d. Error %d", w, h, al_get_errno());

		// clear the bitmap with transparent black
		al_set_target_bitmap(self.allegroBitmap);
		al_clear_to_color(al_map_rgba(0, 0, 0, 0));
		al_set_target_backbuffer(Display::instance->self.allegroDisplay);
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		al_destroy_bitmap(self.allegroBitmap);
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return al_get_bitmap_width(self.allegroBitmap);
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return al_get_bitmap_height(self.allegroBitmap);
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		FGEAL_CHECK_INIT();
		unsigned char r, g, b, a;
		al_unmap_rgba(al_get_pixel(self.allegroBitmap, x, y), &r, &g, &b, &a);
		return Color::create(r, g, b, a);
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();
		pixelData.resize(width);
		for(unsigned i = 0; i < width and x+i < (unsigned) al_get_bitmap_width(self.allegroBitmap); i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and y+j < (unsigned) al_get_bitmap_height(self.allegroBitmap); j++)
			{
				unsigned char r, g, b, a;
				al_unmap_rgba(al_get_pixel(self.allegroBitmap, x+i, y+j), &r, &g, &b, &a);
				pixelData[i][j].r = r; pixelData[i][j].g = g; pixelData[i][j].b = b; pixelData[i][j].a = a;
			}
		}
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_BITMAP* const currentTarget = al_get_target_bitmap();
		al_set_target_bitmap(self.allegroBitmap);
		al_put_pixel(x, y, al_map_rgba(c.r, c.g, c.b, c.a));
		al_set_target_bitmap(currentTarget);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& colors)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_BITMAP* const currentTarget = al_get_target_bitmap();
		al_set_target_bitmap(self.allegroBitmap);
		for(unsigned i = 0; i < colors.size() and x+i < (unsigned) al_get_bitmap_width(self.allegroBitmap); i++)
			for(unsigned j = 0; j < colors[i].size() and y+j < (unsigned) al_get_bitmap_height(self.allegroBitmap); j++)
				al_put_pixel(x+i, y+j, al_map_rgba(colors[i][j].r, colors[i][j].g, colors[i][j].b, colors[i][j].a));
		al_set_target_bitmap(currentTarget);
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		al_draw_bitmap(self.allegroBitmap, x, y, 0); //draw all source region
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		al_draw_bitmap_region(self.allegroBitmap, fromX, fromY, fromWidth, fromHeight, x, y, 0);
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		al_draw_bitmap(self.allegroBitmap, x, y, toAllegroFlipFlag(flipmode));
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		al_draw_bitmap_region(self.allegroBitmap, fromX, fromY, fromWidth, fromHeight, x, y, toAllegroFlipFlag(flipmode));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		al_draw_scaled_bitmap(self.allegroBitmap, 0, 0, al_get_bitmap_width(self.allegroBitmap), al_get_bitmap_height(self.allegroBitmap), x, y, al_get_bitmap_width(self.allegroBitmap)*xScale, al_get_bitmap_height(self.allegroBitmap)*yScale, toAllegroFlipFlag(flipmode));
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		al_draw_scaled_bitmap(self.allegroBitmap, fromX, fromY, fromWidth, fromHeight, x, y, fromWidth*xScale, fromHeight*yScale, toAllegroFlipFlag(flipmode));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		al_draw_rotated_bitmap(self.allegroBitmap, centerX, centerY, x, y, -angle, toAllegroFlipFlag(flipmode));
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		custom_al_draw_rotated_bitmap_region(self.allegroBitmap, fromX, fromY, fromWidth, fromHeight, centerX, centerY, x, y, -angle, toAllegroFlipFlag(flipmode));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		al_draw_scaled_rotated_bitmap(self.allegroBitmap, centerX, centerY, x, y, xScale, yScale, -angle, toAllegroFlipFlag(flipmode)); //draw all source region
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		custom_al_draw_scaled_rotated_bitmap_region(self.allegroBitmap, fromX, fromY, fromWidth, fromHeight, centerX, centerY, x, y, xScale, yScale, -angle, toAllegroFlipFlag(flipmode));
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		al_set_target_bitmap(img.self.allegroBitmap);
		this->delegateDraw(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
		al_set_target_backbuffer(Display::instance->self.allegroDisplay);
	}
}
