/*
 * implementation.hpp (originally impl.hpp)
 *
 *  Created on: 25/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_ADAPTERS_ALLEGRO5_IMPL_HPP_
#define FGEAL_ADAPTERS_ALLEGRO5_IMPL_HPP_

#include "fgeal/display.hpp"
#include "fgeal/image.hpp"
#include "fgeal/event.hpp"
#include "fgeal/font.hpp"
#include "fgeal/sound.hpp"
#include "fgeal/input.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_audio.h>

#include <string>

/// Uncomment this macro to use implementation of filesystem calls using Allegro 5's PhysicsFS addon instead of the generic one provided by fgeal.
//#define FGEAL_ALLEGRO5_USE_PHYSFS_FILESYSTEM_IMPLEMENTATION

namespace fgeal
{
	struct Display::implementation
	{
		ALLEGRO_DISPLAY* allegroDisplay;
		std::string currentTitle;
		ALLEGRO_BITMAP* icon;
	};

	struct Image::implementation
	{
		ALLEGRO_BITMAP* allegroBitmap;
	};

	struct Event::implementation
	{
		ALLEGRO_EVENT* allegroEvent;
	};

	struct EventQueue::implementation
	{
		ALLEGRO_EVENT_QUEUE* allegroEventQueue;
	};

	struct Font::implementation
	{
		ALLEGRO_FONT* allegroFont;
		std::string fontFilename;
		unsigned size;
		int flags;
	};

	struct DrawableText::implementation
	{
		std::string content;
		ALLEGRO_COLOR color;
	};

	struct Sound::implementation
	{
		ALLEGRO_SAMPLE* allegroSample;
		ALLEGRO_SAMPLE_INSTANCE* allegroSampleInstance;
	};

	struct SoundStream::implementation
	{
		ALLEGRO_AUDIO_STREAM* allegroAudioStream;
	};

	struct Music::implementation
	{
		ALLEGRO_AUDIO_STREAM* allegroAudioStream;
	};

	namespace KeyboardKeyMapping
	{
		/// Returns the fgeal-equivalent key enum of the given Allegro 5 keycode.
		Keyboard::Key toGenericKey(int allegroKeycode);

		/// Returns the Allegro 5 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key);
	}

	namespace MouseButtonMapping
	{
		/// Returns the fgeal-equivalent mouse button enum of the given Allegro 5 mouse button number.
		Mouse::Button toGenericMouseButton(int allegroButtonNumber);

		/// Returns the Allegro 5 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button);
	}

	namespace JoystickManagement
	{
		/// needed to map indexes to pointers
		extern std::vector<ALLEGRO_JOYSTICK*> allegroJoystickPtrs;
	}
}

#endif /* FGEAL_ADAPTERS_ALLEGRO5_IMPL_HPP_ */
