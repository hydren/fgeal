/*
 * input.cpp
 *
 *  Created on: 25/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

#undef DELETE

using std::vector;
using std::string;

namespace fgeal
{
	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		// Returns the fgeal-equivalent key enum of the given allegro keycode.
		Keyboard::Key toGenericKey(int allegroKeycode)
		{
			switch(allegroKeycode)
			{
				#define KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the Allegro 5 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				#define BACKWARDS_KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return ALLEGRO_KEY_MAX;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_KEYBOARD_STATE state;
		al_get_keyboard_state(&state);
		return al_key_down(&state, KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		// Returns the fgeal-equivalent mouse button enum of the given allegro mouse button number.
		Mouse::Button toGenericMouseButton(int allegroButtonNumber)
		{
			switch(allegroButtonNumber)
			{
				#define MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the Allegro 5 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				#define BACKWARDS_MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return 0;
			}
		}
	}

	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return al_mouse_button_down(&state, MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		Point pt = {(float) state.x, (float) state.y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return state.x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		static ALLEGRO_MOUSE_STATE state;
		al_get_mouse_state(&state);
		return state.y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		al_set_mouse_xy(Display::instance->self.allegroDisplay, x, y);
	}

	// =======================================================================================
	// Joystick

	vector<ALLEGRO_JOYSTICK*> JoystickManagement::allegroJoystickPtrs;

	void Joystick::configureAll()
	{
		for(int j = 0; j < al_get_num_joysticks(); j++)
			JoystickManagement::allegroJoystickPtrs.push_back(al_get_joystick(j));
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::allegroJoystickPtrs.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return al_get_num_joysticks();
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return al_get_joystick_name(JoystickManagement::allegroJoystickPtrs[joyIndex]);
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return al_get_joystick_num_buttons(JoystickManagement::allegroJoystickPtrs[joyIndex]);
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		unsigned axesCount = 0;
		for(int s = 0; s < al_get_joystick_num_sticks(JoystickManagement::allegroJoystickPtrs[joyIndex]); s++)
			axesCount += al_get_joystick_num_axes(JoystickManagement::allegroJoystickPtrs[joyIndex], s);
		return axesCount;
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_JOYSTICK_STATE state;
		al_get_joystick_state(JoystickManagement::allegroJoystickPtrs[joyIndex], &state);
		return state.button[buttonIndex] > 0;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_JOYSTICK_STATE state;
		al_get_joystick_state(JoystickManagement::allegroJoystickPtrs[joyIndex], &state);
		return state.stick[axisIndex/2].axis[axisIndex%2];
	}
}
