/*
 * sound.cpp
 *
 *  Created on: 26/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include "fgeal/exceptions.hpp"

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>

using std::string;

namespace fgeal
{
	static const unsigned
		DEFAULT_AUDIO_STREAM_BUFFER_COUNT = 4,
		DEFAULT_AUDIO_STREAM_SAMPLES_COUNT = 2048;

	Sound::Sound(const string& filename, float volume)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.allegroSample = null;
		self.allegroSampleInstance = null;

		ALLEGRO_SAMPLE* sample = al_load_sample(filename.c_str());

		if(sample == null)
			throw AdapterException("Could not load audio file \"%s\". Error %d", filename.c_str(), al_get_errno());

		self.allegroSample = sample;

		ALLEGRO_SAMPLE_INSTANCE* instance = al_create_sample_instance(sample);

		if(instance == null)
		{
			const int error = al_get_errno();  // copy the original errno because al_destroy_xxx may generate other errors.

			self.allegroSample = null;
			al_destroy_sample(sample);

			throw AdapterException("Could not load audio file \"%s\". Failed to create instance. Error %d", filename.c_str(), error);
		}

		self.allegroSampleInstance = instance;

		if(not al_attach_sample_instance_to_mixer(self.allegroSampleInstance, al_get_default_mixer()))
		{
			const int error = al_get_errno();  // copy the original errno because al_destroy_xxx may generate other errors.

			self.allegroSampleInstance = null;
			al_destroy_sample_instance(instance);

			self.allegroSample = null;
			al_destroy_sample(sample);

			throw AdapterException("Could not load audio file \"%s\". Failed to attach to mixer. Error %d", filename.c_str(), error);
		}

		if(volume != 1.0f)
			al_set_sample_instance_gain(self.allegroSampleInstance, volume);
	}

	Sound::~Sound()
	{
		FGEAL_CHECK_INIT();

		if(self.allegroSampleInstance != null)
			al_destroy_sample_instance(self.allegroSampleInstance);

		if(self.allegroSample != null)
			al_destroy_sample(self.allegroSample);

		delete &self;
	}

	void Sound::play()
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_playmode(self.allegroSampleInstance, ALLEGRO_PLAYMODE_ONCE);
		al_set_sample_instance_position(self.allegroSampleInstance, 0);
		al_play_sample_instance(self.allegroSampleInstance);
	}

	void Sound::loop()
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_playmode(self.allegroSampleInstance, ALLEGRO_PLAYMODE_LOOP);
		al_set_sample_instance_position(self.allegroSampleInstance, 0);
		al_play_sample_instance(self.allegroSampleInstance);
	}

	void Sound::stop()
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_playing(self.allegroSampleInstance, false);
		al_set_sample_instance_position(self.allegroSampleInstance, 0);
	}

	void Sound::pause()
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_playing(self.allegroSampleInstance, false);
	}

	void Sound::resume()
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_playing(self.allegroSampleInstance, true);
	}

	bool Sound::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return al_get_sample_instance_playing(self.allegroSampleInstance);
	}

	bool Sound::isPaused()
	{
		FGEAL_CHECK_INIT();
		return not al_get_sample_instance_playing(self.allegroSampleInstance)
			   and al_get_sample_instance_position(self.allegroSampleInstance) != 0;
	}

	void Sound::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_gain(self.allegroSampleInstance, value);
	}

	float Sound::getVolume()
	{
		FGEAL_CHECK_INIT();
		return al_get_sample_instance_gain(self.allegroSampleInstance);
	}

	float Sound::getDuration()
	{
		FGEAL_CHECK_INIT();
		return al_get_sample_instance_time(self.allegroSampleInstance);
	}

	void Sound::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		al_set_sample_instance_speed(self.allegroSampleInstance, factor);
	}

	float Sound::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return al_get_sample_instance_speed(self.allegroSampleInstance);
	}

	// ##################################################################################################################

	SoundStream::SoundStream(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_AUDIO_STREAM* stream = al_load_audio_stream(filename.c_str(), DEFAULT_AUDIO_STREAM_BUFFER_COUNT, DEFAULT_AUDIO_STREAM_SAMPLES_COUNT);

		if(stream == null)
			throw AdapterException("Could not load audio (streaming) file \"%s\". Error %d", filename.c_str(), al_get_errno());

		al_set_audio_stream_playing(stream, false); //pause stream immediately

		self.allegroAudioStream = stream;

		if(not al_attach_audio_stream_to_mixer(stream, al_get_default_mixer()))
		{
			const int error = al_get_errno();  // copy the original errno because al_destroy_xxx may generate other errors.
			self.allegroAudioStream = null;
			al_destroy_audio_stream(self.allegroAudioStream);
			throw AdapterException("Could not load audio file \"%s\". Failed to attach stream to mixer. Error %d", filename.c_str(), error);
		}
	}

	SoundStream::~SoundStream()
	{
		FGEAL_CHECK_INIT();
		if(self.allegroAudioStream != null)
			al_destroy_audio_stream(self.allegroAudioStream);
		delete &self;
	}

	void SoundStream::play()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playmode(self.allegroAudioStream, ALLEGRO_PLAYMODE_ONCE);
		al_rewind_audio_stream(self.allegroAudioStream);
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	void SoundStream::loop()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playmode(self.allegroAudioStream, ALLEGRO_PLAYMODE_LOOP);
		al_rewind_audio_stream(self.allegroAudioStream);
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	void SoundStream::stop()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, false);
		al_rewind_audio_stream(self.allegroAudioStream);
	}

	void SoundStream::pause()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, false);
	}

	void SoundStream::resume()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	bool SoundStream::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_playing(self.allegroAudioStream);
	}

	bool SoundStream::isPaused()
	{
		FGEAL_CHECK_INIT();
		return not al_get_audio_stream_playing(self.allegroAudioStream)
			   and al_get_audio_stream_position_secs(self.allegroAudioStream) != 0;
	}

	void SoundStream::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_gain(self.allegroAudioStream, value);
	}

	float SoundStream::getVolume()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_gain(self.allegroAudioStream);
	}

	bool SoundStream::isStreaming()
	{
		FGEAL_CHECK_INIT();
		return true;
	}

	float SoundStream::getDuration()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_length_secs(self.allegroAudioStream);
	}

	void SoundStream::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_speed(self.allegroAudioStream, factor);
	}

	float SoundStream::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_speed(self.allegroAudioStream);
	}

	// ##################################################################################################################

	Music::Music(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		ALLEGRO_AUDIO_STREAM* stream = al_load_audio_stream(filename.c_str(), DEFAULT_AUDIO_STREAM_BUFFER_COUNT, DEFAULT_AUDIO_STREAM_SAMPLES_COUNT);

		if(stream == null)
			throw AdapterException("Could not load music file \"%s\". Error %d", filename.c_str(), al_get_errno());

		al_set_audio_stream_playing(stream, false); //pause stream immediately

		self.allegroAudioStream = stream;

		if(not al_attach_audio_stream_to_mixer(stream, al_get_default_mixer()))
			throw AdapterException("Could not load music file \"%s\". Failed to attach stream to mixer. Error %d", filename.c_str(), al_get_errno());
	}

	Music::~Music()
	{
		FGEAL_CHECK_INIT();
		al_destroy_audio_stream(self.allegroAudioStream);
		delete &self;
	}

	void Music::play()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playmode(self.allegroAudioStream, ALLEGRO_PLAYMODE_ONCE);
		al_rewind_audio_stream(self.allegroAudioStream);
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	void Music::loop()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playmode(self.allegroAudioStream, ALLEGRO_PLAYMODE_LOOP);
		al_rewind_audio_stream(self.allegroAudioStream);
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	void Music::stop()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, false);
		al_rewind_audio_stream(self.allegroAudioStream);
	}

	void Music::pause()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, false);
	}

	void Music::resume()
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_playing(self.allegroAudioStream, true);
	}

	bool Music::isPlaying()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_playing(self.allegroAudioStream);
	}

	bool Music::isPaused()
	{
		FGEAL_CHECK_INIT();
		return not al_get_audio_stream_playing(self.allegroAudioStream)
			   and al_get_audio_stream_position_secs(self.allegroAudioStream) != 0;
	}

	void Music::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		al_set_audio_stream_gain(self.allegroAudioStream, value);
	}

	float Music::getVolume()
	{
		FGEAL_CHECK_INIT();
		return al_get_audio_stream_gain(self.allegroAudioStream);
	}
}
