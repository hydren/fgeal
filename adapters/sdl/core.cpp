/*
 * core.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_thread.h>

#define AUX_MACRO_NAME_TO_STR(A) #A
#define AUX_MACRO_VALUE_TO_STR(A) AUX_MACRO_NAME_TO_STR(A)
#define SDL_VERSION_LITERAL SDL_MAJOR_VERSION.SDL_MINOR_VERSION.SDL_PATCHLEVEL
#define SDL_VERSION_STR AUX_MACRO_VALUE_TO_STR(SDL_VERSION_LITERAL)

// IMG_Init is present only with SDL_image version 1.2.8 or higher
#if SDL_IMAGE_MAJOR_VERSION > 1 or (SDL_IMAGE_MAJOR_VERSION == 1 and (SDL_IMAGE_MINOR_VERSION > 2 or (SDL_IMAGE_MINOR_VERSION == 2 and SDL_IMAGE_PATCHLEVEL >= 8)))
	#define FGEAL_SDL_IMG_INIT_AVAILABLE
#endif

// IMG_INIT_WEBP is present only with SDL_image version 1.2.11 or higher
#if SDL_IMAGE_MAJOR_VERSION < 1 or (SDL_IMAGE_MAJOR_VERSION == 1 and (SDL_IMAGE_MINOR_VERSION < 2 or (SDL_IMAGE_MINOR_VERSION == 2 and SDL_IMAGE_PATCHLEVEL < 11)))
	#define IMG_INIT_WEBP 0
#endif

// Mix_Init is present only with SDL_mixer version 1.2.10 or higher
#if MIX_MAJOR_VERSION > 1 or (MIX_MAJOR_VERSION == 1 and (MIX_MINOR_VERSION > 2 or (MIX_MINOR_VERSION == 2 and MIX_PATCHLEVEL >= 10)))
	#define FGEAL_SDL_MIX_INIT_AVAILABLE
#endif


namespace fgeal
{
	/* fgeal code based on SDL 1.2 */
	const char* ADAPTER_NAME = "SDL 1.2 Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "SDL";
	const char* ADAPTED_LIBRARY_VERSION = SDL_VERSION_STR;

	// extern-expected on implementation.hpp
	SDL_mutex* MIX_MUSIC_MUTEX = null;

	// extern-expected on implementation.hpp
	namespace AudioSpec
	{
		int allocatedMixChannelsCount = 0;  // number of mix channels allocated

		// read-only!
		Uint16 format;  // current audio format constant
		int frequency;  // frequency rate of the current audio format
		int channelCount;  // number of channels of the current audio format
	}

	// SDL init flags
	static const Uint32 sdlInitFlags = (SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);  // maybe try SDL_INIT_EVENTTHREAD

	// initialize all SDL stuff
	void core::initialize()
	{
		if(SDL_Init( sdlInitFlags ) < 0)
			throw AdapterException("SDL could not be initialized: %s", SDL_GetError());

		if(TTF_Init() == -1)
			throw AdapterException("SDL_ttf could not be initialized: %s", TTF_GetError());

		#ifdef FGEAL_SDL_IMG_INIT_AVAILABLE

		// preloads libs needed to load this image formats to prevent constantly loading and unloading DLLs
		if( IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP) == -1 )
			throw AdapterException("IMG_Init() failed to preload some libraries, subsequent errors are likely. Error: %s", IMG_GetError());

		#endif  /* FGEAL_SDL_IMG_INIT_AVAILABLE */

		#ifdef FGEAL_SDL_MIX_INIT_AVAILABLE

		// preloads libs needed to load this audio formats to prevent constantly loading and unloading DLLs
		if( Mix_Init(MIX_INIT_FLAC | MIX_INIT_OGG | MIX_INIT_MP3) == -1)
			throw AdapterException("Mix_Init() failed to preload some libraries, subsequent errors are likely. Error: %s", Mix_GetError());

		#endif  /* FGEAL_SDL_MIX_INIT_AVAILABLE */

		MIX_MUSIC_MUTEX = SDL_CreateMutex();

		int frequency           = MIX_DEFAULT_FREQUENCY,
			format              = MIX_DEFAULT_FORMAT,
			numberOfChannels    = MIX_DEFAULT_CHANNELS, // number of channels (mono (1), stereo (2)); this is NOT the same as 'numberOfMixChannels'
			chunkSize           = 4096, // default suggested by documentation
			numberOfMixChannels = MIX_CHANNELS; // number of mix channels; this is NOT the same as 'numberOfChannels'

		// overrides
		frequency = 44100; // frequency set to cd quality
		chunkSize = 2048; // reduces delay
		numberOfMixChannels = 16; // increase capacity to play simultaneously

		if(Mix_OpenAudio(frequency, format, numberOfChannels, chunkSize) == -1)
			throw AdapterException("SDL_mixer could not be initialized: %s", Mix_GetError());

		// query specs
		Mix_QuerySpec(&AudioSpec::frequency, &AudioSpec::format, &AudioSpec::channelCount);

		// update 'allocatedChannelsCount' to how many was actually allocated
		AudioSpec::allocatedMixChannelsCount = Mix_AllocateChannels(numberOfMixChannels);

		// enforce disabled key repeat (even if its off by default)
		SDL_EnableKeyRepeat(0, SDL_DEFAULT_REPEAT_INTERVAL);
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
		Mix_CloseAudio();
		SDL_DestroyMutex(MIX_MUSIC_MUTEX);

		#ifdef FGEAL_SDL_MIX_INIT_AVAILABLE
		Mix_Quit();
		#endif

		#ifdef FGEAL_SDL_IMG_INIT_AVAILABLE
		IMG_Quit();
		#endif

		TTF_Quit();
		SDL_Quit();
	}

	bool isProperlyInitialized()
	{
		return (SDL_WasInit(sdlInitFlags) == sdlInitFlags) and TTF_WasInit();
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		int ms = (seconds*1000.0);
		SDL_Delay(ms>0? ms : 1);
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return ((double) SDL_GetTicks())/1000.0;
	}
}

#include "fgeal/agnostic_filesystem.hxx"
#include "fgeal/agnostic_sound_stream.hxx"
