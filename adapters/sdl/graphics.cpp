/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"
#include "fgeal/generic_graphics.hxx"

#include "SDL/SDL_gfxPrimitives.h"

using std::vector;

// Useful macro to improve readability and reduce typing
#define asQuad(a1, a2, a3, a4) {static_cast<Sint16>(a1), static_cast<Sint16>(a2), static_cast<Sint16>(a3), static_cast<Sint16>(a4)}

// signatures only available in SDL_gfx 2.0.17 or higher
#if SDL_GFXPRIMITIVES_MAJOR > 2 or (SDL_GFXPRIMITIVES_MAJOR == 2 and (SDL_GFXPRIMITIVES_MINOR > 0 or (SDL_GFXPRIMITIVES_MINOR == 0 and SDL_GFXPRIMITIVES_MICRO >= 17)))
	// not sure if arcRGBA was created at this version, but changelog suggests that at least arcColor was
	#define FGEAL_SDL_GFX_ARC_PRIMITIVE_AVAILABLE
#endif

// signatures only available in SDL_gfx 2.0.22 or higher
#if SDL_GFXPRIMITIVES_MAJOR > 2 or (SDL_GFXPRIMITIVES_MAJOR == 2 and (SDL_GFXPRIMITIVES_MINOR > 0 or (SDL_GFXPRIMITIVES_MINOR == 0 and SDL_GFXPRIMITIVES_MICRO >= 22)))
	#define FGEAL_SDL_GFX_ROUNDED_RECTANGLE_PRIMITIVE_AVAILABLE
	#define FGEAL_SDL_GFX_THICK_LINE_PRIMITIVE_AVAILABLE
#endif

namespace fgeal
{
	/// extern-expected on implementation.hpp
	SDL_Surface* drawTargetSurface = null;

	/// extern-expected on implementation.hpp
	bool isDrawTargetImage;

	static void blendAlphaChannels(SDL_Surface* const src, const Sint16 srcrectx, const Sint16 srcrecty, const SDL_Rect& dstrect)
	{
		SDL_LockSurface(src);
		SDL_LockSurface(drawTargetSurface);

		for(int i = dstrect.x, si = srcrectx; i < dstrect.x + dstrect.w; i++, si++)
		for(int j = dstrect.y, sj = srcrecty; j < dstrect.y + dstrect.h; j++, sj++)
		{
			Uint32& pixel = getSdlRgbaPixel(drawTargetSurface, i, j);  // Get a reference to the current pixel.
			Uint8 r, g, b, a, sa, dummy;
			SDL_GetRGBA(pixel, drawTargetSurface->format, &r, &g, &b, &a);  // Get the target pixel components.
			SDL_GetRGBA(getSdlRgbaPixel(src, si, sj), src->format, &dummy, &dummy, &dummy, &sa);  // Get the source pixel components.
			pixel = SDL_MapRGBA(drawTargetSurface->format, r, g, b, sa + a - (sa * a)/255);  // Set the pixel alpha as a combination of both target and source ones.
		}

		SDL_UnlockSurface(src);
		SDL_UnlockSurface(drawTargetSurface);
	}

	/// extern-expected on implementation.hpp
	int blitSurface(SDL_Surface* const src, const SDL_Rect& srcrect, Sint16 dstx, Sint16 dsty)
	{
		SDL_Rect dstrect = { dstx, dsty };
		const int status = SDL_BlitSurface(src, const_cast<SDL_Rect*>(&srcrect), drawTargetSurface, &dstrect);
		if(isDrawTargetImage and drawTargetSurface->format->BitsPerPixel == 32)
			blendAlphaChannels(src, srcrect.x - (dstx - dstrect.x), srcrect.y - (dsty - dstrect.y), dstrect);
		return status;
	}

	// =====================================================================================================================================================

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		if(image == null)
		{
			fgeal::drawTargetSurface = Display::instance->self.sdlDisplaySurface;
			fgeal::isDrawTargetImage = false;
		}
		else
		{
			fgeal::drawTargetSurface = image->self.sdlSurface;
			fgeal::isDrawTargetImage = true;
		}
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetSurface = display.self.sdlDisplaySurface;
		fgeal::isDrawTargetImage = false;
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		fgeal::drawTargetSurface = Display::instance->self.sdlDisplaySurface;
		fgeal::isDrawTargetImage = false;
	}

	// =====================================================================================================================================================
	// Primitives

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			lineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
		else
			aalineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL_GFX_THICK_LINE_PRIMITIVE_AVAILABLE
			thickLineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, thickness, c.r, c.g, c.b, c.a);
		#else
			GenericGraphics::drawThickLine(x1, y1, x2, y2, thickness, c);
		#endif
	}

	void Graphics::drawRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rectangleRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		boxRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			circleRGBA(fgeal::drawTargetSurface, cx, cy, r, c.r, c.g, c.b, c.a);
		else
			aacircleRGBA(fgeal::drawTargetSurface, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledCircleRGBA(fgeal::drawTargetSurface, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			ellipseRGBA(fgeal::drawTargetSurface, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
		else
			aaellipseRGBA(fgeal::drawTargetSurface, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledEllipseRGBA(fgeal::drawTargetSurface, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			trigonRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
		else
			aatrigonRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledTrigonRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		if(useFasterPrimitivesHint)
			polygonRGBA(fgeal::drawTargetSurface, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
		else
			aapolygonRGBA(fgeal::drawTargetSurface, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		filledPolygonRGBA(fgeal::drawTargetSurface, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	#ifndef FGEAL_SDL_GFX_ARC_PRIMITIVE_AVAILABLE
	void pixelRGBAWrapper(float x, float y, const Color& c)  // ps: cannot be static because it needs to have external linkage in older compilers
	{
		pixelRGBA(fgeal::drawTargetSurface, x, y, c.r, c.g, c.b, c.a);
	}
	#endif

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL_GFX_ARC_PRIMITIVE_AVAILABLE
			arcRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
		#else
			GenericGraphics::drawArc<pixelRGBAWrapper>(cx, cy, r, ai, af, c);
		#endif
	}

	// callback to force usage of fast version of line drawing primitives on GenericGraphics::drawThickArc, because this algorithm nulifies anti-aliasing effects
	void lineRGBAWrapper(float x1, float y1, float x2, float y2, const Color& c)  // ps: cannot be static because it needs to have external linkage in older compilers
	{
		lineRGBA(fgeal::drawTargetSurface, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawThickArc(float cx, float cy, float r, float t, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickArc<lineRGBAWrapper>(cx, cy, r, t, ai, af, c);
	}

	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		pieRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledPieRGBA(fgeal::drawTargetSurface, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL_GFX_ROUNDED_RECTANGLE_PRIMITIVE_AVAILABLE
			roundedRectangleRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
		#else
			GenericGraphics::drawRoundedRectangle(x, y, width, height, rd, c);
		#endif
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		#ifdef FGEAL_SDL_GFX_ROUNDED_RECTANGLE_PRIMITIVE_AVAILABLE
			roundedBoxRGBA(fgeal::drawTargetSurface, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
		#else
			GenericGraphics::drawFilledRoundedRectangle(x, y, width, height, rd, c);
		#endif
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		if(useFasterPrimitivesHint)
			polygonRGBA(fgeal::drawTargetSurface, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
		else
			aapolygonRGBA(fgeal::drawTargetSurface, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		filledPolygonRGBA(fgeal::drawTargetSurface, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}
}
