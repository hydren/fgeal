/*
 * image.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"
#include "fgeal/graphics.hpp"

#include "SDL/SDL_image.h"
#include "SDL/SDL_rotozoom.h"
#include "SDL/SDL_gfxPrimitives.h"  // needed for version checking

#include <cmath>

using std::string;
using std::vector;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	#define FGEAL_SDL_DEFAULT_AMASK	0x000000FF
#else
	#define FGEAL_SDL_DEFAULT_AMASK	0xFF000000
#endif

#define FLIP_HORIZ 1
#define FLIP_VERTI 2

namespace fgeal
{
	// function that rotates a surface using either rotateSurface90Degrees (if available) or rotozoomSurface
	inline static SDL_Surface* customRotateSurface(SDL_Surface* src, double angle, int flip, int smooth)
	{
		if(flip)  // if flipping is requested, delegating to rotozoomXY is required
			return rotozoomSurfaceXY(src, angle, (flip == FLIP_HORIZ? -1 : 1), (flip == FLIP_VERTI? -1 : 1), smooth);

		// check if rotateSurface90Degrees() is available (only when using SDL_gfx 2.0.17 or higher)
		#if SDL_GFXPRIMITIVES_MAJOR > 2 or (SDL_GFXPRIMITIVES_MAJOR == 2 and (SDL_GFXPRIMITIVES_MINOR > 0 or (SDL_GFXPRIMITIVES_MINOR == 0 and SDL_GFXPRIMITIVES_MICRO >= 17)))
		if(src->format->BitsPerPixel == 32) // rotateSurface90Degrees() only supports 32bit RGBA/ABGR (and fails otherwise)
		{
			double turns;
			if(fabs(modf(angle/90, &turns)) == 0) // only use if angle is a multiple of 90 degrees.
				return rotateSurface90Degrees(src, static_cast<int>(-turns)); // optimized 90-degree rotations
		}
		#endif

		// fallback to standard rotation function
		return rotozoomSurface(src, angle, 1.0, smooth);
	}

	// function with same behavior as rotozoomSurfaceXY, but avoiding a known glitch with rotozoomSurfaceXY when distinct zoomx and zoomy are passed
	inline static SDL_Surface* customRotozoomSurfaceXY(SDL_Surface * src, double angle, double zoomx, double zoomy, int smooth)
	{
		#ifdef FGEAL_SDL_GFX_ROTOZOOMSURFACEXY_GLITCH_WORKAROUND_ENABLED
		if(zoomx != zoomy)
		{
			// emulates the behavior of rotozoomSurfaceXY by using zoomSurface and rotozoomSurface
			SDL_Surface* const scaledSurface = zoomSurface(src, zoomx, zoomy, smooth);
			SDL_Surface* const rotatedScaledSurface = rotozoomSurface(scaledSurface, angle, 1.0, smooth);
			SDL_FreeSurface(scaledSurface);
			return rotatedScaledSurface;
		}
		#endif

		// use rotozoomSurfaceXY anyway if its glitch is fixed or zoomx and zoomy are equal
		return rotozoomSurfaceXY(src, angle, zoomx, zoomy, smooth);
	}

	// =====================================================================================================================================================

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlSurface = IMG_Load(filename.c_str() );
		if ( self.sdlSurface == null)
			throw AdapterException("Could not load image \"%s\": %s", filename.c_str(), IMG_GetError());

		// attempt to use 32bit format, which are better suited to rotozoom
		if(self.sdlSurface->format->BitsPerPixel != 32)
		{
			SDL_PixelFormat format32 = *(self.sdlSurface->format);
			format32.BitsPerPixel = 32;
			SDL_Surface* surf = SDL_ConvertSurface(self.sdlSurface, &format32, 0);
			if(surf != null)  // use 32-bit surf if it could be obtained, otherwise move on
			{
				SDL_FreeSurface(self.sdlSurface);
				self.sdlSurface = surf;
			}
		}
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlSurface = SDL_CreateRGBSurface(SDL_HWSURFACE & SDL_SRCALPHA, w, h,
												fgeal::drawTargetSurface != null? fgeal::drawTargetSurface->format->BitsPerPixel : 32,
												fgeal::drawTargetSurface != null? fgeal::drawTargetSurface->format->Rmask : 0,
												fgeal::drawTargetSurface != null? fgeal::drawTargetSurface->format->Gmask : 0,
												fgeal::drawTargetSurface != null? fgeal::drawTargetSurface->format->Bmask : 0,
												FGEAL_SDL_DEFAULT_AMASK);
		if(self.sdlSurface == null)
			throw AdapterException("Could not create image with dimensions w=%d h=%d :%s", w, h, SDL_GetError());

		// clear the surfarce with transparent black
		SDL_FillRect(self.sdlSurface, null, SDL_MapRGBA(self.sdlSurface->format, 0, 0, 0, SDL_ALPHA_TRANSPARENT));
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		SDL_FreeSurface(self.sdlSurface);
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sdlSurface->w;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sdlSurface->h;
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		FGEAL_CHECK_INIT();
		Uint8 r, g, b, a;
		SDL_GetRGBA(getSdlRgbaPixel(self.sdlSurface, x, y), self.sdlSurface->format, &r, &g, &b, &a);
		return Color::create(r, g, b, a);
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();
		pixelData.resize(width);
		for(unsigned i = 0; i < width and x+i < (unsigned) self.sdlSurface->w; i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and y+j < (unsigned) self.sdlSurface->h; j++)
			{
				Uint8 r, g, b, a;
				SDL_GetRGBA(getSdlRgbaPixel(self.sdlSurface, x+i, y+j), self.sdlSurface->format, &r, &g, &b, &a);
				pixelData[i][j].r = r; pixelData[i][j].g = g; pixelData[i][j].b = b; pixelData[i][j].a = a;
			}
		}
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		getSdlRgbaPixel(self.sdlSurface, x, y) = SDL_MapRGBA(self.sdlSurface->format, c.r, c.g, c.b, c.a);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& colors)
	{
		FGEAL_CHECK_INIT();
		for(unsigned i = 0; i < colors.size() and x+i < (unsigned) self.sdlSurface->w; i++)
			for(unsigned j = 0; j < colors[i].size() and y+j < (unsigned) self.sdlSurface->h; j++)
				getSdlRgbaPixel(self.sdlSurface, x+i, y+j) = SDL_MapRGBA(self.sdlSurface->format, colors[i][j].r, colors[i][j].g, colors[i][j].b, colors[i][j].a);
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		// draws all source region
		fgeal::blitSurface(self.sdlSurface, x, y);
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		fgeal::blitSurface(self.sdlSurface, toSdlRect(fromX, fromY, fromWidth, fromHeight), x, y);
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == Image::FLIP_HORIZONTAL), flipY = (flipmode == Image::FLIP_VERTICAL);
		SDL_Surface* flippedSurface = zoomSurface(self.sdlSurface, (flipX? -1 : 1), (flipY? -1 : 1), SMOOTHING_OFF);
		// draws all source region
		fgeal::blitSurface(flippedSurface, x, y);
		SDL_FreeSurface(flippedSurface); flippedSurface = null;
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == Image::FLIP_HORIZONTAL), flipY = (flipmode == Image::FLIP_VERTICAL);
		SDL_Surface* flippedSurface = zoomSurface(self.sdlSurface, (flipX? -1 : 1), (flipY? -1 : 1), SMOOTHING_OFF);
		const SDL_Rect srcrect = toSdlRect(flipX? (self.sdlSurface->w - fromX - fromWidth) : fromX,  // draws selected region
										   flipY? (self.sdlSurface->h - fromY - fromHeight): fromY,
										   fromWidth, fromHeight);
		fgeal::blitSurface(flippedSurface, srcrect, x, y);
		SDL_FreeSurface(flippedSurface); flippedSurface = null;
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const double zoomx = (flipmode == Image::FLIP_HORIZONTAL? -xScale : xScale), zoomy = (flipmode == Image::FLIP_VERTICAL? -yScale : yScale);
		SDL_Surface* scaledSurface = zoomSurface(self.sdlSurface, zoomx, zoomy, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);
		//draws all source region
		fgeal::blitSurface(scaledSurface, x, y);
		SDL_FreeSurface(scaledSurface); scaledSurface = null;
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		#ifndef FGEAL_SDL_LEANER_DRAW_SCALED_REGION

		//crop surface first
		SDL_Surface* croppedSurface = SDL_CreateRGBSurface(self.sdlSurface->flags, fromWidth, fromHeight, self.sdlSurface->format->BitsPerPixel, self.sdlSurface->format->Rmask, self.sdlSurface->format->Gmask, self.sdlSurface->format->Bmask, self.sdlSurface->format->Amask);
		SDL_FillRect(croppedSurface, null, SDL_MapRGBA(croppedSurface->format, 0, 0, 0, SDL_ALPHA_TRANSPARENT));
		SDL_Rect srcrect = toSdlRect(fromX, fromY, fromWidth, fromHeight);
		SDL_SetAlpha(self.sdlSurface, 0, SDL_ALPHA_OPAQUE);  // needed to blend correctly
		SDL_BlitSurface(self.sdlSurface, &srcrect, croppedSurface, null);
		SDL_SetAlpha(self.sdlSurface, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);  // restore previous setting (not really sure if it is the proper way of restoring per-surface alpha to its default value, but it seems to work)

		const double zoomx = (flipmode == Image::FLIP_HORIZONTAL? -xScale : xScale), zoomy = (flipmode == Image::FLIP_VERTICAL? -yScale : yScale);
		SDL_Surface* scaledCroppedSurface = zoomSurface(croppedSurface, zoomx, zoomy, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);

		fgeal::blitSurface(scaledCroppedSurface, x, y);

		// we dont need these anymore
		SDL_FreeSurface(croppedSurface); croppedSurface = null;
		SDL_FreeSurface(scaledCroppedSurface); scaledCroppedSurface = null;

		#else  // slightly faster and less memory-hungry implementation since it creates just one temporary surface, at the cost of producing slightly inaccurate results
		const bool flipX = (flipmode == Image::FLIP_HORIZONTAL), flipY = (flipmode == Image::FLIP_VERTICAL);
		SDL_Surface* scaledSurface = zoomSurface(self.sdlSurface, (flipX? -xScale : xScale), (flipY? -yScale : yScale), useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);
		const SDL_Rect srcrect = toSdlRect( //draws selected region
											xScale*(flipX? (self.sdlSurface->w - fromX - fromWidth) : fromX),
											yScale*(flipY? (self.sdlSurface->h - fromY - fromHeight): fromY),
											xScale*fromWidth, yScale*fromHeight);
		fgeal::blitSurface(scaledSurface, srcrect, x, y);
		SDL_FreeSurface(scaledSurface); scaledSurface = null;
		#endif
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		SDL_Surface* rotatedSurface = customRotateSurface(self.sdlSurface, rad2deg(angle), flipmode == FLIP_HORIZONTAL? FLIP_HORIZ : flipmode == FLIP_VERTICAL? FLIP_VERTI : 0, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);

		const float sina = sin(-angle), cosa = cos(-angle);

		const float w1 = self.sdlSurface->w,
					h1 = self.sdlSurface->h,
					w2 = rotatedSurface->w,
					h2 = rotatedSurface->h;

		const float ax2 = w2/2 + (centerX - w1/2) * cosa - (centerY - h1/2) * sina;
		const float ay2 = h2/2 + (centerX - w1/2) * sina + (centerY - h1/2) * cosa;

		//draws all source region
		fgeal::blitSurface(rotatedSurface, (x - ax2), (y - ay2));
		SDL_FreeSurface(rotatedSurface); rotatedSurface = null;
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		//crop surface first
		SDL_Surface* croppedSurface = SDL_CreateRGBSurface(self.sdlSurface->flags, fromWidth, fromHeight, self.sdlSurface->format->BitsPerPixel, self.sdlSurface->format->Rmask, self.sdlSurface->format->Gmask, self.sdlSurface->format->Bmask, self.sdlSurface->format->Amask);
		SDL_FillRect(croppedSurface, null, SDL_MapRGBA(croppedSurface->format, 0, 0, 0, SDL_ALPHA_TRANSPARENT));
		SDL_Rect srcrect = toSdlRect(fromX, fromY, fromWidth, fromHeight);
		SDL_SetAlpha(self.sdlSurface, 0, SDL_ALPHA_OPAQUE);  // needed to blend correctly
		SDL_BlitSurface(self.sdlSurface, &srcrect, croppedSurface, null);
		SDL_SetAlpha(self.sdlSurface, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);  // restore previous setting (not really sure if it is the proper way of restoring per-surface alpha to its default value, but it seems to work)

		SDL_Surface* rotatedCroppedSurface = customRotateSurface(croppedSurface, rad2deg(angle), flipmode == FLIP_HORIZONTAL? FLIP_HORIZ : flipmode == FLIP_VERTICAL? FLIP_VERTI : 0, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);

		const float sina = sin(-angle), cosa = cos(-angle);

		const float w1 = croppedSurface->w,
					h1 = croppedSurface->h,
					w2 = rotatedCroppedSurface->w,
					h2 = rotatedCroppedSurface->h;

		const float ax2 = w2/2 + (centerX - w1/2) * cosa - (centerY - h1/2) * sina;
		const float ay2 = h2/2 + (centerX - w1/2) * sina + (centerY - h1/2) * cosa;

		fgeal::blitSurface(rotatedCroppedSurface, (x - ax2), (y - ay2));

		// we dont need these anymore
		SDL_FreeSurface(croppedSurface); croppedSurface = null;
		SDL_FreeSurface(rotatedCroppedSurface); rotatedCroppedSurface = null;
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const double zoomx = (flipmode == Image::FLIP_HORIZONTAL? -xScale : xScale), zoomy = (flipmode == Image::FLIP_VERTICAL? -yScale : yScale);
		SDL_Surface* rotatedScaledSurface = customRotozoomSurfaceXY(self.sdlSurface, rad2deg(angle), zoomx, zoomy, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);

		const float sina = sin(-angle), cosa = cos(-angle);

		const float w1 = self.sdlSurface->w,
					h1 = self.sdlSurface->h,
					w2 = rotatedScaledSurface->w,
					h2 = rotatedScaledSurface->h;

		const float ax2 = w2/2 + (centerX - w1/2) * cosa * xScale - (centerY - h1/2) * sina * yScale;
		const float ay2 = h2/2 + (centerX - w1/2) * sina * xScale + (centerY - h1/2) * cosa * yScale;

		//draws all source region
		fgeal::blitSurface(rotatedScaledSurface, (x - ax2), (y - ay2));
		SDL_FreeSurface(rotatedScaledSurface); rotatedScaledSurface = null;
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		//crop surface first
		SDL_Surface* croppedSurface = SDL_CreateRGBSurface(self.sdlSurface->flags, fromWidth, fromHeight, self.sdlSurface->format->BitsPerPixel, self.sdlSurface->format->Rmask, self.sdlSurface->format->Gmask, self.sdlSurface->format->Bmask, self.sdlSurface->format->Amask);
		SDL_FillRect(croppedSurface, null, SDL_MapRGBA(croppedSurface->format, 0, 0, 0, SDL_ALPHA_TRANSPARENT));
		SDL_Rect srcrect = toSdlRect(fromX, fromY, fromWidth, fromHeight);
		SDL_SetAlpha(self.sdlSurface, 0, SDL_ALPHA_OPAQUE);  // needed to blend correctly
		SDL_BlitSurface(self.sdlSurface, &srcrect, croppedSurface, null);
		SDL_SetAlpha(self.sdlSurface, SDL_SRCALPHA, SDL_ALPHA_OPAQUE);  // restore previous setting (not really sure if it is the proper way of restoring per-surface alpha to its default value, but it seems to work)

		const double zoomx = (flipmode == Image::FLIP_HORIZONTAL? -xScale : xScale), zoomy = (flipmode == Image::FLIP_VERTICAL? -yScale : yScale);
		SDL_Surface* rotatedScaledCroppedSurface = customRotozoomSurfaceXY(croppedSurface, rad2deg(angle), zoomx, zoomy, useImageTransformSmoothingHint? SMOOTHING_ON : SMOOTHING_OFF);

		const float sina = sin(-angle), cosa = cos(-angle);

		const float w1 = croppedSurface->w,
					h1 = croppedSurface->h,
					w2 = rotatedScaledCroppedSurface->w,
					h2 = rotatedScaledCroppedSurface->h;

		const float ax2 = w2/2 + (centerX - w1/2) * cosa * xScale - (centerY - h1/2) * sina * yScale;
		const float ay2 = h2/2 + (centerX - w1/2) * sina * xScale + (centerY - h1/2) * cosa * yScale;

		fgeal::blitSurface(rotatedScaledCroppedSurface, (x - ax2), (y - ay2));

		// we dont need these anymore
		SDL_FreeSurface(croppedSurface); croppedSurface = null;
		SDL_FreeSurface(rotatedScaledCroppedSurface); rotatedScaledCroppedSurface = null;
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Surface* const displayBackup = fgeal::drawTargetSurface;
		const bool flagBackup = fgeal::isDrawTargetImage;
		fgeal::drawTargetSurface = img.self.sdlSurface;
		fgeal::isDrawTargetImage = true;
		this->delegateDraw(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
		fgeal::drawTargetSurface = displayBackup;
		fgeal::isDrawTargetImage = flagBackup;
	}
}
