/*
 * input_mappings.hxx
 *
 *  Created on: 11 de abr de 2022
 *      Author: hydren
 */

// dummy case for Eclipse CDT parser, so it won't flood warnings when viewing this file in the editor
#ifdef __CDT_PARSER__
	#define KEYBOARD_MAPPINGS
	#define MOUSE_MAPPINGS
	#define mapping(a, b)  a, b;
	#include "fgeal/input.hpp"
	#include <SDL/SDL_keysym.h>
	#include <SDL/SDL_mouse.h>
	namespace fgeal { void dummy() {
#else
	#ifdef BACKWARDS_KEYBOARD_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define KEYBOARD_MAPPINGS
		#undef BACKWARDS_KEYBOARD_MAPPINGS
	#endif
	#ifdef BACKWARDS_MOUSE_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define MOUSE_MAPPINGS
		#undef BACKWARDS_MOUSE_MAPPINGS
	#endif

	#undef mapping
	#ifdef BACKWARDS_MAPPINGS
		#define mapping(a, b) case b: return a;
	#else
		#define mapping(a, b) case a: return b;
	#endif
	#undef BACKWARDS_MAPPINGS
#endif

#ifdef KEYBOARD_MAPPINGS
	mapping(SDLK_a, Keyboard::KEY_A)
	mapping(SDLK_b, Keyboard::KEY_B)
	mapping(SDLK_c, Keyboard::KEY_C)
	mapping(SDLK_d, Keyboard::KEY_D)
	mapping(SDLK_e, Keyboard::KEY_E)
	mapping(SDLK_f, Keyboard::KEY_F)
	mapping(SDLK_g, Keyboard::KEY_G)
	mapping(SDLK_h, Keyboard::KEY_H)
	mapping(SDLK_i, Keyboard::KEY_I)
	mapping(SDLK_j, Keyboard::KEY_J)
	mapping(SDLK_k, Keyboard::KEY_K)
	mapping(SDLK_l, Keyboard::KEY_L)
	mapping(SDLK_m, Keyboard::KEY_M)
	mapping(SDLK_n, Keyboard::KEY_N)
	mapping(SDLK_o, Keyboard::KEY_O)
	mapping(SDLK_p, Keyboard::KEY_P)
	mapping(SDLK_q, Keyboard::KEY_Q)
	mapping(SDLK_r, Keyboard::KEY_R)
	mapping(SDLK_s, Keyboard::KEY_S)
	mapping(SDLK_t, Keyboard::KEY_T)
	mapping(SDLK_u, Keyboard::KEY_U)
	mapping(SDLK_v, Keyboard::KEY_V)
	mapping(SDLK_w, Keyboard::KEY_W)
	mapping(SDLK_x, Keyboard::KEY_X)
	mapping(SDLK_y, Keyboard::KEY_Y)
	mapping(SDLK_z, Keyboard::KEY_Z)

	mapping(SDLK_0, Keyboard::KEY_0)
	mapping(SDLK_1, Keyboard::KEY_1)
	mapping(SDLK_2, Keyboard::KEY_2)
	mapping(SDLK_3, Keyboard::KEY_3)
	mapping(SDLK_4, Keyboard::KEY_4)
	mapping(SDLK_5, Keyboard::KEY_5)
	mapping(SDLK_6, Keyboard::KEY_6)
	mapping(SDLK_7, Keyboard::KEY_7)
	mapping(SDLK_8, Keyboard::KEY_8)
	mapping(SDLK_9, Keyboard::KEY_9)

	mapping(SDLK_F1,  Keyboard::KEY_F1)
	mapping(SDLK_F2,  Keyboard::KEY_F2)
	mapping(SDLK_F3,  Keyboard::KEY_F3)
	mapping(SDLK_F4,  Keyboard::KEY_F4)
	mapping(SDLK_F5,  Keyboard::KEY_F5)
	mapping(SDLK_F6,  Keyboard::KEY_F6)
	mapping(SDLK_F7,  Keyboard::KEY_F7)
	mapping(SDLK_F8,  Keyboard::KEY_F8)
	mapping(SDLK_F9,  Keyboard::KEY_F9)
	mapping(SDLK_F10, Keyboard::KEY_F10)
	mapping(SDLK_F11, Keyboard::KEY_F11)
	mapping(SDLK_F12, Keyboard::KEY_F12)

	mapping(SDLK_UP,    Keyboard::KEY_ARROW_UP)
	mapping(SDLK_DOWN,  Keyboard::KEY_ARROW_DOWN)
	mapping(SDLK_LEFT,  Keyboard::KEY_ARROW_LEFT)
	mapping(SDLK_RIGHT, Keyboard::KEY_ARROW_RIGHT)

	mapping(SDLK_RETURN,    Keyboard::KEY_ENTER)
	mapping(SDLK_SPACE,     Keyboard::KEY_SPACE)
	mapping(SDLK_ESCAPE,    Keyboard::KEY_ESCAPE)
	mapping(SDLK_LCTRL,     Keyboard::KEY_LEFT_CONTROL)
	mapping(SDLK_RCTRL,     Keyboard::KEY_RIGHT_CONTROL)
	mapping(SDLK_LSHIFT,    Keyboard::KEY_LEFT_SHIFT)
	mapping(SDLK_RSHIFT,    Keyboard::KEY_RIGHT_SHIFT)
	mapping(SDLK_LALT,      Keyboard::KEY_LEFT_ALT)
	mapping(SDLK_RALT,      Keyboard::KEY_RIGHT_ALT)
	mapping(SDLK_LSUPER,    Keyboard::KEY_LEFT_SUPER)
	mapping(SDLK_RSUPER,    Keyboard::KEY_RIGHT_SUPER)
	mapping(SDLK_MENU,      Keyboard::KEY_MENU)
	mapping(SDLK_TAB,       Keyboard::KEY_TAB)
	mapping(SDLK_BACKSPACE, Keyboard::KEY_BACKSPACE)

	mapping(SDLK_MINUS,        Keyboard::KEY_MINUS)
	mapping(SDLK_EQUALS,       Keyboard::KEY_EQUALS)
	mapping(SDLK_LEFTBRACKET,  Keyboard::KEY_LEFT_BRACKET)
	mapping(SDLK_RIGHTBRACKET, Keyboard::KEY_RIGHT_BRACKET)
	mapping(SDLK_SEMICOLON,    Keyboard::KEY_SEMICOLON)
	mapping(SDLK_COMMA,        Keyboard::KEY_COMMA)
	mapping(SDLK_PERIOD,       Keyboard::KEY_PERIOD)
	mapping(SDLK_SLASH,        Keyboard::KEY_SLASH)
	mapping(SDLK_BACKSLASH,    Keyboard::KEY_BACKSLASH)
	mapping(SDLK_QUOTE,        Keyboard::KEY_QUOTE)
	mapping(SDLK_BACKQUOTE,    Keyboard::KEY_TILDE)

	mapping(SDLK_INSERT,   Keyboard::KEY_INSERT)
	mapping(SDLK_DELETE,   Keyboard::KEY_DELETE)
	mapping(SDLK_HOME,     Keyboard::KEY_HOME)
	mapping(SDLK_END,      Keyboard::KEY_END)
	mapping(SDLK_PAGEUP,   Keyboard::KEY_PAGE_UP)
	mapping(SDLK_PAGEDOWN, Keyboard::KEY_PAGE_DOWN)

	mapping(SDLK_KP0, Keyboard::KEY_NUMPAD_0)
	mapping(SDLK_KP1, Keyboard::KEY_NUMPAD_1)
	mapping(SDLK_KP2, Keyboard::KEY_NUMPAD_2)
	mapping(SDLK_KP3, Keyboard::KEY_NUMPAD_3)
	mapping(SDLK_KP4, Keyboard::KEY_NUMPAD_4)
	mapping(SDLK_KP5, Keyboard::KEY_NUMPAD_5)
	mapping(SDLK_KP6, Keyboard::KEY_NUMPAD_6)
	mapping(SDLK_KP7, Keyboard::KEY_NUMPAD_7)
	mapping(SDLK_KP8, Keyboard::KEY_NUMPAD_8)
	mapping(SDLK_KP9, Keyboard::KEY_NUMPAD_9)

	mapping(SDLK_KP_PLUS,     Keyboard::KEY_NUMPAD_ADDITION)
	mapping(SDLK_KP_MINUS,    Keyboard::KEY_NUMPAD_SUBTRACTION)
	mapping(SDLK_KP_MULTIPLY, Keyboard::KEY_NUMPAD_MULTIPLICATION)
	mapping(SDLK_KP_DIVIDE,   Keyboard::KEY_NUMPAD_DIVISION)
	mapping(SDLK_KP_PERIOD,   Keyboard::KEY_NUMPAD_DECIMAL)
	mapping(SDLK_KP_ENTER,    Keyboard::KEY_NUMPAD_ENTER)
#endif
#undef KEYBOARD_MAPPINGS

#ifdef MOUSE_MAPPINGS
	mapping(SDL_BUTTON_LEFT,   Mouse::BUTTON_LEFT)
	mapping(SDL_BUTTON_MIDDLE, Mouse::BUTTON_MIDDLE)
	mapping(SDL_BUTTON_RIGHT,  Mouse::BUTTON_RIGHT)
#endif
#undef MOUSE_MAPPINGS

#ifdef __CDT_PARSER__
}}
#endif
