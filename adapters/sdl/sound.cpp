/*
 * sound.cpp
 *
 *  Created on: 26/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <cmath>

using std::string;

namespace fgeal
{
	// Pointer to the current music; it is null if no music is being played. IMPORTANT: ONLY ACCESS/MODIFY IT AFTER GETTING MUTEX ACCESS (MIX_MUSIC_MUTEX)
	Music* currentMusic = null;

	static inline Uint16 formatSampleSize(Uint16 format) { return (format & 0xFF) / 8; }

	// Get chunk time length (in ms) given its size and current audio format
	static int computeChunkLengthMillisec(int chunkSize)
	{
		const Uint32 points = chunkSize / formatSampleSize(AudioSpec::format);  // bytes / samplesize == sample points
		const Uint32 frames = (points / AudioSpec::channelCount);               // sample points / channels == sample frames
		return ((frames * 1000) / AudioSpec::frequency);                        // (sample frames * 1000) / frequency == play length, in ms
	}

	// Custom handler object to control which part of the Mix_Chunk's audio data will be played, with which pitch-related modifications.
	// This needed to be a template because the actual Mix_Chunk's data format may vary (AUDIO_U8, AUDIO_S16, etc) and the data type varies with it (Uint8, Sint16, etc)
	// The AudioFormatType should be the data type that is compatible with the current SDL_mixer-initialized audio format.
	template<typename AudioFormatType>
	struct PlaybackSpeedEffectHandler
	{
		const AudioFormatType* const chunkData;  // pointer to the chunk sample data (as array)
		const float& speedFactor;  // the playback speed factor
		float position;  // current position of the sound, in ms
		const int duration;  // the duration of the sound, in ms
		const int chunkSize;  // the size of the sound, as a number of indexes (or sample points). thinks of this as a array size when using the proper array type (instead of just Uint8*).
		const bool loop;  // flags whether playback should stay looping
		const bool attemptSelfHalting;  // flags whether playback should be halted by this callback when playback is finished
		bool altered;  // true if this playback has been pitched by this handler

		PlaybackSpeedEffectHandler(const Mix_Chunk& chunk, const float& speed, bool loop, bool trySelfHalt)
		: chunkData(reinterpret_cast<AudioFormatType*>(chunk.abuf)), speedFactor(speed),
		  position(0), duration(computeChunkLengthMillisec(chunk.alen)),
		  chunkSize(chunk.alen / formatSampleSize(AudioSpec::format)),
		  loop(loop), attemptSelfHalting(trySelfHalt), altered(false)
		{}

		// processing function to be able to change chunk speed/pitch.
		void modifyStreamPlaybackSpeed(int mixChannel, void* stream, int length)
		{
			const static int& channelCount = AudioSpec::channelCount, &frequency = AudioSpec::frequency;  // some aliases to make expressions shorter
			AudioFormatType* buffer = static_cast<AudioFormatType*>(stream);
			const int bufferSize = length / sizeof(AudioFormatType);  // buffer size (as array)
			const float speedFactor = this->speedFactor;  // take a "snapshot" of speed factor

			// if there is still sound to be played
			if(position < duration or loop)
			{
				const float delta = 1000.0 / frequency,  // normal duration of each sample
							vdelta = delta * speedFactor;  // virtual stretched duration, scaled by 'speedFactor'

				// if playback is unaltered and pitch is required (for the first time)
				if(not altered and speedFactor != 1.0f)
					altered = true;  // flags playback modification and proceed to the pitch routine.

				if(altered)  // if unaltered, this pitch routine is skipped
				{
					for(int i = 0; i < bufferSize; i += channelCount)
					{
						const int j = i/channelCount;  // j goes from 0 to size/channelCount, incremented 1 by 1
						const float x = position + j * vdelta;  // get "virtual" index. its corresponding value will be interpolated.
						const int k = floor(x / delta);  // get left index to interpolate from original chunk data (right index will be this plus 1)
						const float prop = (x / delta) - k;  // get the proportion of the right value (left will be 1.0 minus this)
						// const float prop2 = prop * prop;  // cache the square of the proportion (needed only for cubic interpolation)

						// usually just 2 channels: 0 (left) and 1 (right), but who knows...
						for(int c = 0; c < channelCount; c++)
						{
							// check if k will be within bounds
							if(k * channelCount + channelCount - 1 < chunkSize or loop)
							{
								AudioFormatType  v0 = chunkData[(  k   * channelCount + c) % chunkSize],
												 // v_ = chunkData[((k-1) * channelCount + c) % chunkSize],
												 // v2 = chunkData[((k+2) * channelCount + c) % chunkSize],
												 v1 = chunkData[((k+1) * channelCount + c) % chunkSize];

								// put interpolated value on 'data'
								// buffer[i + c] = (1 - prop) * v0 + prop * v1;  // linear interpolation
								buffer[i + c] = v0 + prop * (v1 - v0);  // linear interpolation (one multiplication version)
								// buffer[i + c] = v0 + 0.5f * prop * ((prop - 3) * v0 - (prop - 2) * 2 * v1 + (prop - 1) * v2);  // quadratic interpolation
								// buffer[i + c] = v0 + (prop / 6) * ((3 * prop - prop2 - 2) * v_ + (prop2 - 2 * prop - 1) * 3 * v0 + (prop - prop2 + 2) * 3 * v1 + (prop2 - 1) * v2);  // cubic interpolation
								// buffer[i + c] = v0 + 0.5f * prop * ((2 * prop2 - 3 * prop - 1) * (v0 - v1) + (prop2 - 2 * prop + 1) * (v0 - v_) + (prop2 - prop) * (v2 - v2));  // cubic spline interpolation
							}
							else  // if k will be out of bounds (chunk bounds), it means we already finished; thus, we'll pass silence
							{
								buffer[i + c] = 0;
							}
						}
					}
				}

				// update position
				position += (bufferSize / channelCount) * vdelta;

				// reset position if looping
				if(loop) while(position > duration)
					position -= duration;
			}
			else  // if we already played the whole sound but finished earlier than expected by SDL_mixer (due to faster playback speed)
			{
				// set silence on the buffer since Mix_HaltChannel() poops out some of it for a few ms.
				for(int i = 0; i < bufferSize; i++)
					buffer[i] = 0;

				if(attemptSelfHalting)
					Mix_HaltChannel(mixChannel);  // XXX unsafe call, since it locks audio; no safer solution was found yet...
			}
		}

		// Mix_EffectFunc_t callback that redirects to handler method (handler passed via userData)
		static void mixEffectFuncCallback(int channel, void* stream, int length, void* userData)
		{
			static_cast<PlaybackSpeedEffectHandler*>(userData)->modifyStreamPlaybackSpeed(channel, stream, length);
		}

		// Mix_EffectDone_t callback that deletes the handler at the end of the effect usage  (handler passed via userData)
		static void mixEffectDoneCallback(int, void *userData)
		{
			delete static_cast<PlaybackSpeedEffectHandler*>(userData);
		}

		// function to register a handler to this channel for the next playback.
		static void registerEffect(int channel, const Mix_Chunk& chunk, const float& speed, bool loop, bool trySelfHalt)
		{
			Mix_RegisterEffect(channel, mixEffectFuncCallback, mixEffectDoneCallback, new PlaybackSpeedEffectHandler(chunk, speed, loop, trySelfHalt));
		}
	};

	// Register playback speed effect handler according to the current audio format; effect valid for a single playback; if playback is looped, lasts until it's halted
	void setupPlaybackSpeedEffect(const Mix_Chunk* const chunk, const float& speed, int channel, bool loop=false, bool trySelfHalt=false)
	{
		// select the register function for the current audio format and register the effect using the compatible handlers
		// XXX is it correct to behave the same way to all S16 and U16 formats? Should we create case statements for AUDIO_S16SYS, AUDIO_S16LSB, AUDIO_S16MSB, etc, individually?
		switch(AudioSpec::format)
		{
			case AUDIO_U8:  PlaybackSpeedEffectHandler<Uint8 >::registerEffect(channel, *chunk, speed, loop, trySelfHalt); break;
			case AUDIO_S8:  PlaybackSpeedEffectHandler<Sint8 >::registerEffect(channel, *chunk, speed, loop, trySelfHalt); break;
			case AUDIO_U16: PlaybackSpeedEffectHandler<Uint16>::registerEffect(channel, *chunk, speed, loop, trySelfHalt); break;
			default:
			case AUDIO_S16: PlaybackSpeedEffectHandler<Sint16>::registerEffect(channel, *chunk, speed, loop, trySelfHalt); break;
		}
	}

	// ============================== SOUND ======================================

	Sound::Sound(const string& filename, float volume)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		Mix_Chunk* chunk = Mix_LoadWAV(filename.c_str());

		if(chunk == null)
			throw AdapterException("Could not load audio file \"%s\": %s", filename.c_str(), Mix_GetError());

		self.sdlmixerChunk = chunk;
		self.speed = 1.0f;
		self.dynamicSpeedHinted = false;
		if(volume != 1.0f)
			Mix_VolumeChunk(self.sdlmixerChunk, volume*MIX_MAX_VOLUME);
	}

	Sound::~Sound()
	{
		FGEAL_CHECK_INIT();
		Mix_FreeChunk(self.sdlmixerChunk);
		delete &self;
	}

	void Sound::play()
	{
		FGEAL_CHECK_INIT();
		if(self.speed == 1.0f and not self.dynamicSpeedHinted)  // check for normal playback speed to avoid registering effect callback, optimizing
			Mix_PlayChannel(-1, self.sdlmixerChunk, 0);
		else
		{
			int channel;
			if(self.dynamicSpeedHinted)
				channel = Mix_PlayChannel(-1, self.sdlmixerChunk, -1);  // play as loop since it may need more time to play than original chunk; callback will halt channel (XXX unsafe)
			else
				// same as above, but since static speed was hinted, limit duration to a estimated length with current playback speed; XXX if speed do change, chunk may be cut short or delay to halt
				channel = Mix_PlayChannelTimed(-1, self.sdlmixerChunk, -1, self.speed * computeChunkLengthMillisec(self.sdlmixerChunk->alen));

			if(channel != -1)
				setupPlaybackSpeedEffect(self.sdlmixerChunk, self.speed, channel, false, self.dynamicSpeedHinted);
		}
	}

	void Sound::loop()
	{
		FGEAL_CHECK_INIT();
		if(self.speed == 1.0f and not self.dynamicSpeedHinted)  // check for normal playback speed to avoid registering effect callback, optimizing
			Mix_PlayChannel(-1, self.sdlmixerChunk, -1);
		else
		{
			const int channel = Mix_PlayChannel(-1, self.sdlmixerChunk, -1);
			if(channel != -1)
				setupPlaybackSpeedEffect(self.sdlmixerChunk, self.speed, channel, true);
		}
	}

	void Sound::stop()
	{
		FGEAL_CHECK_INIT();
		for(int i = 0; i < AudioSpec::allocatedMixChannelsCount; i++)  // for all channels dealing with this chunk
			if(Mix_GetChunk(i) == self.sdlmixerChunk)
				Mix_HaltChannel(i);
	}

	void Sound::pause()
	{
		FGEAL_CHECK_INIT();
		for(int i = 0; i < AudioSpec::allocatedMixChannelsCount; i++)  // for all channels dealing with this chunk
		if(Mix_Playing(i) and Mix_GetChunk(i) == self.sdlmixerChunk)
			Mix_Pause(i);
	}

	void Sound::resume()
	{
		FGEAL_CHECK_INIT();
		for(int i = 0; i < AudioSpec::allocatedMixChannelsCount; i++)  // for all channels dealing with this chunk
		if(Mix_Paused(i) and Mix_GetChunk(i) == self.sdlmixerChunk)
			Mix_Resume(i);
	}

	bool Sound::isPlaying()
	{
		FGEAL_CHECK_INIT();
		// if at least one channel is playing, then its playing
		for(int i = 0; i < AudioSpec::allocatedMixChannelsCount; i++)  // for all channels dealing with this chunk
		if(Mix_Playing(i) and not Mix_Paused(i) and Mix_GetChunk(i) == self.sdlmixerChunk)
			return true;

		return false;
	}

	bool Sound::isPaused()
	{
		FGEAL_CHECK_INIT();
		// if there is at least one channel with this chunk and all those channels are not playing, then its paused
		bool hasPausedChannelAssociatedWith = false;
		for(int i = 0; i < AudioSpec::allocatedMixChannelsCount; i++)
		if(Mix_GetChunk(i) == self.sdlmixerChunk)
		{
			if(Mix_Playing(i) and not Mix_Paused(i))
				return false;
			else if(Mix_Paused(i))
				hasPausedChannelAssociatedWith = true;
		}
		return hasPausedChannelAssociatedWith;
	}

	void Sound::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		Mix_VolumeChunk(self.sdlmixerChunk, value*MIX_MAX_VOLUME);
	}

	float Sound::getVolume()
	{
		FGEAL_CHECK_INIT();
		return Mix_VolumeChunk(self.sdlmixerChunk, -1)/static_cast<float>(MIX_MAX_VOLUME);
	}

	void Sound::setPlaybackSpeed(float factor, bool hintDynamicChange)
	{
		FGEAL_CHECK_INIT();
		self.speed = factor;  // to be used by effect callback
		self.dynamicSpeedHinted = hintDynamicChange;  // to be used by registerProperEffectHandler()
	}

	float Sound::getPlaybackSpeed()
	{
		FGEAL_CHECK_INIT();
		return self.speed;
	}

	float Sound::getDuration()
	{
		FGEAL_CHECK_INIT();
		return 0.001f * static_cast<float>(computeChunkLengthMillisec(self.sdlmixerChunk->alen));
	}

	// ============================== MUSIC ==============================

	Music::Music(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		Mix_Music* music = Mix_LoadMUS(filename.c_str());

		if(music == null)
			throw AdapterException("Could not load audio (music) file \"%s\": %s", filename.c_str(), Mix_GetError());

		self.sdlmixerMusic = music;
		self.volume = MIX_MAX_VOLUME;
		self.loop = false;
		self.position = 0;
		self.timeStarted = 0;
		self.timePaused = 0;
	}

	Music::~Music()
	{
		FGEAL_CHECK_INIT();
		Mix_FreeMusic(self.sdlmixerMusic);
		delete &self;
	}

	void Music::play()
	{
		FGEAL_CHECK_INIT();
		synchronized(MIX_MUSIC_MUTEX)
		{
			// if there was another music playing
			if(this != currentMusic and Mix_PlayingMusic())
			{
				// pause current music to play this one since SDL doesnt support multiple musics at the same time
				Mix_PauseMusic();
				currentMusic->self.timePaused = SDL_GetTicks();
				currentMusic->self.position += currentMusic->self.timePaused - currentMusic->self.timeStarted;
			}

			Mix_VolumeMusic(self.volume);
			Mix_PlayMusic(self.sdlmixerMusic, 0);
			self.timeStarted = SDL_GetTicks();
			self.timePaused = 0;
			self.position = 0;
			self.loop = false;
			currentMusic = this;
		}
	}

	void Music::loop()
	{
		FGEAL_CHECK_INIT();
		synchronized(MIX_MUSIC_MUTEX)
		{
			// if there was another music playing
			if(this != currentMusic and Mix_PlayingMusic())
			{
				// pause current music to play this one since SDL doesnt support multiple musics at the same time
				Mix_PauseMusic();
				currentMusic->self.timePaused = SDL_GetTicks();
				currentMusic->self.position += currentMusic->self.timePaused - currentMusic->self.timeStarted;
			}

			Mix_VolumeMusic(self.volume);
			Mix_PlayMusic(self.sdlmixerMusic, -1);
			self.timeStarted = SDL_GetTicks();
			self.timePaused = 0;
			self.position = 0;
			self.loop = true;
			currentMusic = this;
		}
	}

	void Music::stop()
	{
		FGEAL_CHECK_INIT();
		if(Mix_PlayingMusic() or Mix_PausedMusic())
		{
			synchronized(MIX_MUSIC_MUTEX)
			{
				if(this == currentMusic)
				{
					Mix_HaltMusic();
					currentMusic = null;
				}
			}
		}

		self.loop = false;  // reset
		self.position = 0;
		self.timeStarted = 0;
		self.timePaused = 0;
	}

	void Music::pause()
	{
		FGEAL_CHECK_INIT();
		if(Mix_PlayingMusic())
		{
			synchronized(MIX_MUSIC_MUTEX)
			{
				if(this == currentMusic)
				{
					Mix_PauseMusic();
					self.timePaused = SDL_GetTicks();
					self.position += self.timePaused - self.timeStarted;
				}
			}
		}
	}

	void Music::resume()
	{
		FGEAL_CHECK_INIT();
		synchronized(MIX_MUSIC_MUTEX)
		{
			// if there was another music playing
			if(this != currentMusic and Mix_PlayingMusic())
			{
				// pause current music to play this one since SDL doesnt support multiple musics at the same time
				Mix_PauseMusic();
				currentMusic->self.timePaused = SDL_GetTicks();
				currentMusic->self.position += currentMusic->self.timePaused - currentMusic->self.timeStarted;
			}

			Mix_VolumeMusic(self.volume); // use this music's volume

			// if we are the current music, just resume straightforwardly
			if(this == currentMusic)
				Mix_ResumeMusic();

			else  // plays it again manually
			{
				Mix_PlayMusic(self.sdlmixerMusic, self.loop? -1 : 0);
				Mix_RewindMusic();
				// FIXME this could go wrong after looping since position would be higher than the music's length. a modulo operation should be used, yet we would need to know music's time length for this.
				Mix_SetMusicPosition(static_cast<double>(self.position)/1000.0);
				currentMusic = this;
			}

			// remember when started playing again
			self.timeStarted = SDL_GetTicks();
		}
	}

	bool Music::isPlaying()
	{
		FGEAL_CHECK_INIT();
		if(not Mix_PlayingMusic() or Mix_PausedMusic()) return false;

		bool answer = false;
		synchronized(MIX_MUSIC_MUTEX)
		{
			if(this == currentMusic)
				answer = true;
		}
		return answer;
	}

	bool Music::isPaused()
	{
		FGEAL_CHECK_INIT();
		bool answer = false;
		synchronized(MIX_MUSIC_MUTEX)
		{
			if(this == currentMusic)
				answer = Mix_PausedMusic();

			else answer = (self.timePaused > 0);
		}
		return answer;
	}

	void Music::setVolume(float value)
	{
		FGEAL_CHECK_INIT();
		if(isPlaying())
			Mix_VolumeMusic(self.volume);

		self.volume = value*MIX_MAX_VOLUME;
	}

	float Music::getVolume()
	{
		FGEAL_CHECK_INIT();
		return self.volume/static_cast<float>(MIX_MAX_VOLUME);
	}
}
