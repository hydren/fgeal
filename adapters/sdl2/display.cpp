/*
 * display.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using std::string;
using std::vector;
using std::pair;

namespace fgeal
{
	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		Uint32 flags = 0;
		int posX = SDL_WINDOWPOS_UNDEFINED, posY = SDL_WINDOWPOS_UNDEFINED;
		if(options.fullscreen)
			flags |= SDL_WINDOW_FULLSCREEN;
		else
		{
			if(options.isUserResizeable)
				flags |= SDL_WINDOW_RESIZABLE;

			if(options.positioning == Display::Options::POSITION_CENTERED)
				posX = posY = SDL_WINDOWPOS_CENTERED;

			else if(options.positioning == Display::Options::POSITION_UNDEFINED)
				posX = posY = SDL_WINDOWPOS_UNDEFINED;

			else if(options.positioning == Display::Options::POSITION_DEFINED)
			{
				posX = options.position.x;
				posY = options.position.y;
			}
		}

		self.sdlWindow = SDL_CreateWindow(options.title.c_str(), posX, posY, options.width, options.height, flags);
		if(self.sdlWindow == null)
			throw AdapterException("Could not create display! Error: %s", SDL_GetError());

		self.sdlRenderer = SDL_CreateRenderer(self.sdlWindow, -1, SDL_RENDERER_TARGETTEXTURE);
		if(self.sdlRenderer == null)
			throw AdapterException("Could not create renderer! Error: %s", SDL_GetError());

		if(not options.iconFilename.empty())
			this->setIcon(options.iconFilename);
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		SDL_DestroyRenderer(self.sdlRenderer);
		SDL_DestroyWindow(self.sdlWindow);
		delete &self;
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		SDL_RenderPresent(self.sdlRenderer);
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		if ( (SDL_SetRenderDrawColor( self.sdlRenderer, 0, 0, 0, 0xFF) == -1)
		  or (SDL_RenderClear(self.sdlRenderer) == -1))
			throw AdapterException("SDL Display clear error! %s", SDL_GetError());
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		int w;
		SDL_GetWindowSize(self.sdlWindow, &w, null);
		return w;
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		int h;
		SDL_GetWindowSize(self.sdlWindow, null, &h);
		return h;
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		SDL_SetWindowSize(self.sdlWindow, width, height);
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		return string(SDL_GetWindowTitle(self.sdlWindow));
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		SDL_SetWindowTitle(self.sdlWindow, title.c_str());
	}

	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		SDL_Surface* iconSurface = IMG_Load(iconFilename.c_str());
		if(iconSurface == null)
			throw AdapterException("Error while trying to set display icon. Could not load image \"%s\": %s", iconFilename.c_str(), IMG_GetError());

		SDL_SetWindowIcon(self.sdlWindow, iconSurface);
		SDL_FreeSurface(iconSurface);
		iconSurface = null;
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return SDL_GetWindowFlags(self.sdlWindow) & SDL_WINDOW_FULLSCREEN;
	}

	void Display::setFullscreen(bool choice)
	{
		if(isFullscreen() == choice)  // the call to isFullscreen already calls checkInit
			return;  // if same mode, nothing needs to be done

		const int status = SDL_SetWindowFullscreen(self.sdlWindow, choice==true? SDL_WINDOW_FULLSCREEN : 0);
		if(status == -1)
			throw AdapterException("Could not set display mode! Error: %s", SDL_GetError());
	}

	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		SDL_SetWindowPosition(self.sdlWindow, pos.x, pos.y);
	}

	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		SDL_SetWindowPosition(self.sdlWindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	}

	void Display::setMouseCursorVisible(bool enable)
	{
		FGEAL_CHECK_INIT();
		SDL_ShowCursor(enable? SDL_ENABLE : SDL_DISABLE);
	}

	vector<Display::Mode> Display::Mode::getList(pair<unsigned, unsigned> requestedAspect)
	{
		FGEAL_CHECK_INIT();
		const int sdlModeCount = SDL_GetNumDisplayModes(0);

		if(sdlModeCount < 0)
			throw AdapterException("Could not gather display modes! Error: %s", SDL_GetError());

		vector<Mode> list;
		for(int i = 0; i < sdlModeCount; i++)
		{
			SDL_DisplayMode sdlMode;
			if(SDL_GetDisplayMode(0, i, &sdlMode) < 0)
				throw AdapterException("Error while gathering display modes! Error: %s", SDL_GetError());

			bool alreadyAdded = false;
			for(unsigned j = 0; j < list.size() and not alreadyAdded; j++)
				if(list[j].width == (unsigned) sdlMode.w and list[j].height == (unsigned) sdlMode.h)
					alreadyAdded = true;

			if(not alreadyAdded)
			{
				Mode mode(sdlMode.w, sdlMode.h);

				// gather description info from generic list
				const vector<Mode> genericList = Mode::getGenericList(requestedAspect);
				for(unsigned j = 0; j < genericList.size(); j++) if(mode.width == genericList[j].width and mode.height == genericList[j].height)
				{
					mode.description = genericList[j].description;
					mode.aspectRatio = genericList[j].aspectRatio;
				}
				list.push_back(mode);
			}
		}
		return list;
	}
}
