/*
 * font.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

using std::string;

namespace fgeal
{
	static SDL_Texture* createRenderedTextTexture(SDL_Renderer* renderer, TTF_Font* const font, const string& str, const Color& color, bool antialias)
	{
		const SDL_Color sdlColor = { color.r, color.g, color.b };
		SDL_Surface* const renderedTextSurface = (antialias? TTF_RenderUTF8_Blended : TTF_RenderUTF8_Solid)(font, str.c_str(), sdlColor);

		if(renderedTextSurface == null)
			throw AdapterException("Failed to render text from font: %s \n Failed when trying to draw \"%s\"", TTF_GetError(), str.c_str());

		SDL_Texture* const renderedTextTexture = SDL_CreateTextureFromSurface(renderer, renderedTextSurface);

		// free temporary surface
		SDL_FreeSurface(renderedTextSurface);

		if(renderedTextTexture == null)
			throw AdapterException("Failed to create texture from text surface: %s \n Failed when trying to draw \"%s\"", SDL_GetError(), str.c_str());

		// force alpha on text since SDL_ttf ignores SDL_Color's 'a' field.
		if(color.a != SDL_ALPHA_OPAQUE)
			SDL_SetTextureAlphaMod(renderedTextTexture, color.a);

		return renderedTextTexture;
	}

	// ===========================================================================================================

	Font::Font(const string& filename, unsigned size, bool antialiasing, bool kerning)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlttfFont = TTF_OpenFont(filename.c_str(), size);
		if(self.sdlttfFont == null)
			throw AdapterException("Font %s could not be loaded: %s", filename.c_str(), TTF_GetError());
		TTF_SetFontKerning(self.sdlttfFont, kerning);

		self.size = size;
		self.isAntialiased = antialiasing;
		self.isKerningEnabled = kerning;
		self.fontFilename = filename;
	}

	Font::~Font()
	{
		FGEAL_CHECK_INIT();
		TTF_CloseFont(self.sdlttfFont);
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		if(text.empty())
			return;

		SDL_Texture* const renderedTextTexture = createRenderedTextTexture(Display::instance->self.sdlRenderer, self.sdlttfFont, text, color, self.isAntialiased);
		SDL_Rect dstrect = {(int) x, (int) y};
		SDL_QueryTexture(renderedTextTexture, null, null, &dstrect.w, &dstrect.h);

		if(SDL_RenderCopy(Display::instance->self.sdlRenderer, renderedTextTexture, null, &dstrect) != 0)
			throw AdapterException("Failed to draw text texture: %s \n Failed when trying to draw \"%s\"", SDL_GetError(), text.c_str());

		// free temporary texture
		SDL_DestroyTexture(renderedTextTexture);
	}

	float Font::getTextHeight() const
	{
		FGEAL_CHECK_INIT();
		return TTF_FontLineSkip(self.sdlttfFont);
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		int width;
		const int queryStatus = TTF_SizeUTF8(self.sdlttfFont, text.c_str(), &width, null);
		if(queryStatus != 0)
			throw AdapterException("Font width could not be obtained: %s", TTF_GetError());
		return width;
	}

	unsigned Font::getSize() const
	{
		return self.size;
	}

	void Font::setSize(unsigned size)
	{
		FGEAL_CHECK_INIT();
		TTF_CloseFont(self.sdlttfFont);  // close current font

		// create new TTF_Font object with the requested size
		self.sdlttfFont = TTF_OpenFont(self.fontFilename.c_str(), size);
		if(self.sdlttfFont == null)
			throw AdapterException("Could not resize font %s: %s", self.fontFilename.c_str(), TTF_GetError());
		TTF_SetFontKerning(self.sdlttfFont, self.isKerningEnabled);
		self.size = size;
	}
}

namespace fgeal
{
	// Since SDL_ttf by itself draw texts slowly, cache resulting surface
	void DrawableText::updateRenderedText()
	{
		FGEAL_CHECK_INIT();
		if(self.renderedText != null)
			SDL_DestroyTexture(self.renderedText), self.renderedText = null, self.fontSize = 0;

		if(not self.content.empty() and font != null)
		{
			self.renderedText = createRenderedTextTexture(Display::instance->self.sdlRenderer, font->self.sdlttfFont, self.content, self.color, font->self.isAntialiased);
			SDL_QueryTexture(self.renderedText, null, null, &self.renderedTextWidth, &self.renderedTextHeight);
			self.fontSize = font->self.size;
		}
	}

	DrawableText::DrawableText(const string& text, Font* font, Color color)
	: self(*new implementation()), font(font)
	{
		self.content = text;
		self.color = color;
		self.renderedText = null;
		updateRenderedText();
	}

	DrawableText::~DrawableText()
	{
		if(self.renderedText != null)
			SDL_DestroyTexture(self.renderedText), self.renderedText = null;
		delete &self;
	}

	DrawableText& DrawableText::operator=(const DrawableText& other)
	{
		if(this != &other)
		{
			font = other.font;
			self.content = other.self.content;
			self.color = other.self.color;
			self.renderedText = null;
			updateRenderedText();
		}
		return *this;
	};

	DrawableText::DrawableText(const DrawableText& other)
	: self(*new implementation())
	{
		self.renderedText = null;
		operator =(other);
	}

	float DrawableText::getWidth()
	{
		if(font != null and font->self.size != self.fontSize)
			updateRenderedText();

		if(self.renderedText != null)
			return self.renderedTextWidth;
		else
			return 0;
	}

	float DrawableText::getHeight()
	{
		if(font != null and font->self.size != self.fontSize)
			updateRenderedText();

		if(self.renderedText != null)
			return self.renderedTextHeight;
		else
			return 0;
	}

	void DrawableText::setFont(Font* fnt)
	{
		if(fnt != font or (font != null and font->self.size != self.fontSize))
		{
			font = fnt;
			updateRenderedText();
		}
	}

	void DrawableText::setContent(const string& str)
	{
		if(str != self.content or (font != null and font->getSize() != self.fontSize))
		{
			self.content = str;
			updateRenderedText();
		}
	}

	string DrawableText::getContent()
	{
		return self.content;
	}

	void DrawableText::setColor(Color color)
	{
		if(color != self.color or (font != null and font->getSize() != self.fontSize))
		{
			self.color = color;
			updateRenderedText();
		}
	}

	Color DrawableText::getColor()
	{
		return self.color;
	}

	void DrawableText::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		if(font != null and font->getSize() != self.fontSize)
			updateRenderedText();

		if(self.renderedText != null)
		{
			SDL_Rect dstrect = {(int) x, (int) y, self.renderedTextWidth, self.renderedTextHeight};
			if(SDL_RenderCopy(Display::instance->self.sdlRenderer, self.renderedText, null, &dstrect) != 0)
				throw AdapterException("Failed to draw drawable text: %s \n Failed when trying to draw \"%s\"", SDL_GetError(), self.content.c_str());
		}
	}
}
