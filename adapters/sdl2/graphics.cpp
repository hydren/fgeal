/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"
#include "fgeal/generic_graphics.hxx"

#include <SDL2/SDL2_gfxPrimitives.h>

using std::vector;

// Useful macros to improve readability and reduce typing
#define displayRenderer Display::instance->self.sdlRenderer
#define asQuad(a1, a2, a3, a4) {static_cast<Sint16>(a1), static_cast<Sint16>(a2), static_cast<Sint16>(a3), static_cast<Sint16>(a4)}

namespace fgeal
{
	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		SDL_SetTextureBlendMode(image->self.sdlTexture, SDL_BLENDMODE_BLEND);
		SDL_SetRenderTarget(displayRenderer, image->self.sdlTexture);
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		SDL_SetRenderTarget(display.self.sdlRenderer, null);
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		SDL_SetRenderTarget(displayRenderer, null);
	}

	// =====================================================================================================================================================
	// Primitives

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			lineRGBA(displayRenderer, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
		else
			aalineRGBA(displayRenderer, x1, y1, x2, y2, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& c)
	{
		FGEAL_CHECK_INIT();
		thickLineRGBA(displayRenderer, x1, y1, x2, y2, thickness, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		rectangleRGBA(displayRenderer, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledRectangle(float x, float y, float width, float height, const Color& c)
	{
		FGEAL_CHECK_INIT();
		boxRGBA(displayRenderer, x, y, x+width, y+height, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			circleRGBA(displayRenderer, cx, cy, r, c.r, c.g, c.b, c.a);
		else
			aacircleRGBA(displayRenderer, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircle(float cx, float cy, float r, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledCircleRGBA(displayRenderer, cx, cy, r, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			ellipseRGBA(displayRenderer, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
		else
			aaellipseRGBA(displayRenderer, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledEllipseRGBA(displayRenderer, cx, cy, rx, ry, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		if(useFasterPrimitivesHint)
			trigonRGBA(displayRenderer, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
		else
			aatrigonRGBA(displayRenderer, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledTrigonRGBA(displayRenderer, x1, y1, x2, y2, x3, y3, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		if(useFasterPrimitivesHint)
			polygonRGBA(displayRenderer, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
		else
			aapolygonRGBA(displayRenderer, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const Sint16 verticesX[4] = asQuad(x1, x2, x3, x4), verticesY[4] = asQuad(y1, y2, y3, y4);
		filledPolygonRGBA(displayRenderer, verticesX, verticesY, 4, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		arcRGBA(displayRenderer, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawThickArc(float cx, float cy, float r, float t, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickArc<drawLine>(cx, cy, r, t, ai, af, c);
	}
	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		pieRGBA(displayRenderer, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		filledPieRGBA(displayRenderer, cx, cy, r, rad2deg(-af), rad2deg(-ai), c.r, c.g, c.b, c.a);
	}

	void Graphics::drawRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		roundedRectangleRGBA(displayRenderer, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float width, float height, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		roundedBoxRGBA(displayRenderer, x, y, x+width, y+height, rd, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		if(useFasterPrimitivesHint)
			polygonRGBA(displayRenderer, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
		else
			aapolygonRGBA(displayRenderer, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<Sint16> verticesX(n), verticesY(n);
		for(unsigned i = 0; i < n; i++) { verticesX[i] = x[i]; verticesY[i] = y[i]; }
		filledPolygonRGBA(displayRenderer, &verticesX[0], &verticesY[0], n, c.r, c.g, c.b, c.a);
	}
}
