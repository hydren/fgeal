/*
 * image.cpp
 *
 *  Created on: 24/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#ifndef SDL_PIXELFORMAT_RGBA32
	#define SDL_PIXELFORMAT_RGBA32 SDL_PIXELFORMAT_RGBA8888
#endif

#if SDL_MINOR_VERSION == 0 and SDL_PATCHLEVEL < 5
	/* missing API call if SDL < 2.0.5 */
	SDL_Surface* SDL_CreateRGBSurfaceWithFormat(Uint32, int w, int h, int bpp, Uint32 fmt)
	{
		Uint32 Rmask, Gmask, Bmask, Amask;
		SDL_PixelFormatEnumToMasks(fmt, &bpp, &Rmask, &Gmask, &Bmask, &Amask);
		return SDL_CreateRGBSurface(0, w, h, bpp, Rmask, Gmask, Bmask, Amask);
	}
#endif

// Useful macros to improve readability and reduce typing
#define displayRenderer Display::instance->self.sdlRenderer
#define toSDLRect(x, y, w, h) {static_cast<Sint16>(x), static_cast<Sint16>(y), static_cast<Uint16>(w), static_cast<Uint16>(h)}
#define toSDLRect2(x, y)      {static_cast<Sint16>(x), static_cast<Sint16>(y)}
#define toSDLPoint(x, y)      {static_cast<int>(x),    static_cast<int>(y)}

using std::string;
using std::vector;

namespace fgeal
{
	static SDL_RendererFlip toSdlRendererFlipFlag(const Image::FlipMode mode)
	{
		switch (mode) {
			default:case Image::FLIP_NONE:  return SDL_FLIP_NONE;
			case Image::FLIP_HORIZONTAL:    return SDL_FLIP_HORIZONTAL;
			case Image::FLIP_VERTICAL:      return SDL_FLIP_VERTICAL;
		}
	}

	inline Uint32& getSdlRgbaPixel(const SDL_Surface* const surface, unsigned x, unsigned y)
	{
	    return *(Uint32*)((Uint8*) surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
	}

	// =====================================================================================================================================================

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlTexture = IMG_LoadTexture(displayRenderer, filename.c_str());
		if (self.sdlTexture == null)
			throw AdapterException("Could not load image \"%s\": %s", filename.c_str(), IMG_GetError());

		SDL_QueryTexture(self.sdlTexture, null, null, &self.w, &self.h);
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		self.sdlTexture = SDL_CreateTexture(displayRenderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, w, h);
		if (self.sdlTexture == null)
			throw AdapterException("Could not create image with dimensions w=%d h=%d : %s", w, h, SDL_GetError());

		// clear the surfarce with transparent black
		SDL_SetRenderTarget(displayRenderer, self.sdlTexture);
		SDL_SetTextureBlendMode(self.sdlTexture, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(displayRenderer, 0, 0, 0, SDL_ALPHA_TRANSPARENT);
		SDL_RenderClear(displayRenderer);
		SDL_SetRenderTarget(displayRenderer, null);
		self.w = w;
		self.h = h;
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		SDL_DestroyTexture(self.sdlTexture);
		delete &self;
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		vector<vector<Color> > pixelData;
		getPixels(pixelData, x, y, 1, 1);
		return pixelData[0][0];
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();

		Uint32 format;
		SDL_QueryTexture(self.sdlTexture, &format, null, null, null);

		SDL_Surface* const tmpSurface = SDL_CreateRGBSurfaceWithFormat(0, width, height, SDL_BITSPERPIXEL(format), format);
		SDL_Texture* const currentTarget = SDL_GetRenderTarget(displayRenderer);
		const SDL_Rect region = toSDLRect(x, y, width, height);

		SDL_SetRenderTarget(displayRenderer, self.sdlTexture);
		SDL_RenderReadPixels(displayRenderer, &region, format, tmpSurface->pixels, tmpSurface->pitch);
		SDL_SetRenderTarget(displayRenderer, currentTarget);

		pixelData.resize(width);
		for(unsigned i = 0; i < width and i < (unsigned) self.w; i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and j < (unsigned) self.h; j++)
			{
				Uint8 r, g, b, a;
				SDL_GetRGBA(getSdlRgbaPixel(tmpSurface, i, j), tmpSurface->format, &r, &g, &b, &a);
				pixelData[i][j].r = r; pixelData[i][j].g = g; pixelData[i][j].b = b; pixelData[i][j].a = a;
			}
		}

		SDL_FreeSurface(tmpSurface);
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		vector<vector<Color> > vc(1);
		vc[0].push_back(c);
		this->setPixels(x, y, vc);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& c)
	{
		FGEAL_CHECK_INIT();
		// workaround for failing method for pixel editing via SDL_LockTexture()
		// this method draws with no blending instead of trying to set pixels, with the same results, apparently

		SDL_Texture* const currentTarget = SDL_GetRenderTarget(displayRenderer);
		SDL_SetRenderTarget(displayRenderer, self.sdlTexture);

		SDL_BlendMode currentBlendMode;
		SDL_GetRenderDrawBlendMode(displayRenderer, &currentBlendMode);
		SDL_SetRenderDrawBlendMode(displayRenderer, SDL_BLENDMODE_NONE);

		for(unsigned i = 0; i < c.size() and x+i < (unsigned) self.w; i++)
			for(unsigned j = 0; j < c[i].size() and y+j < (unsigned) self.h; j++)
			{
				SDL_SetRenderDrawColor(displayRenderer, c[i][j].r, c[i][j].g, c[i][j].b, c[i][j].a);
				SDL_RenderDrawPoint(displayRenderer, x+i, y+j);
			}

		SDL_SetRenderDrawBlendMode(displayRenderer, currentBlendMode);
		SDL_SetRenderTarget(displayRenderer, currentTarget);

		/*
		// this way seemed the "proper" way to do it, but either doesn't work or crash (probably because texture was not created with SDL_TEXTUREACCESS_STREAMING)
		Uint32 format;
		SDL_QueryTexture(self.sdlTexture, &format, null, null, null);
		SDL_PixelFormat* const pixelFormat = SDL_AllocFormat(format);
		void* pixels;
		int pitch, bytesPerPixel = SDL_BYTESPERPIXEL(format);
		SDL_LockTexture(self.sdlTexture, null, &pixels, &pitch);
		if(pixels != null and pixelFormat != null)
			for(unsigned i = 0; i < c.size() and x+i < (unsigned) self.w; i++)
				for(unsigned j = 0; j < c[i].size() and y+j < (unsigned) self.h; j++)
				{
					Uint32& pixel = *(Uint32*) ((Uint8*) pixels + (y+j) * pitch + (x+i) * bytesPerPixel);
					pixel = SDL_MapRGBA(pixelFormat, c[i][j].r, c[i][j].g, c[i][j].b, c[i][j].a);
				}
		SDL_UnlockTexture(self.sdlTexture);
		SDL_FreeFormat(pixelFormat);
		*/
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.w;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.h;
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, self.w, self.h);
		SDL_RenderCopy(displayRenderer, self.sdlTexture, null, &dstrect);
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopy(displayRenderer, self.sdlTexture, &srcrect, &dstrect);
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, self.w, self.h);
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		SDL_Rect dstrect = toSDLRect(x, y, (self.w * xScale), (self.h * yScale));
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Rect dstrect = toSDLRect(x, y, (fromWidth * xScale), (fromHeight * yScale));
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, 0, null, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(centerX, centerY);
		SDL_Rect dstrect = toSDLRect2((x - centerX), (y - centerY));
		SDL_QueryTexture(self.sdlTexture, null, null, &(dstrect.w), &(dstrect.h));  //@suppress("Invalid arguments")
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(centerX, centerY);
		SDL_Rect dstrect = toSDLRect((x - centerX), (y - centerY), fromWidth, fromHeight);
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(xScale*centerX, yScale*centerY);
		//draws all source region
		SDL_Rect dstrect = toSDLRect((x - xScale*centerX), (y - yScale*centerY), (self.w * xScale), (self.h * yScale));
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, null, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		SDL_Point center = toSDLPoint(xScale*centerX, yScale*centerY);
		SDL_Rect dstrect = toSDLRect((x - xScale*centerX), (y - yScale*centerY), (fromWidth * xScale), (fromHeight * yScale));
		SDL_Rect srcrect = toSDLRect(fromX, fromY, fromWidth, fromHeight);  // draws selected region
		SDL_RenderCopyEx(displayRenderer, self.sdlTexture, &srcrect, &dstrect, rad2deg(-angle), &center, toSdlRendererFlipFlag(flipmode));
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		SDL_BlendMode previousMode;
		SDL_GetTextureBlendMode(img.self.sdlTexture, &previousMode);
		SDL_SetTextureBlendMode(img.self.sdlTexture, SDL_BLENDMODE_BLEND);

		// set img as target
		SDL_SetRenderTarget(displayRenderer, img.self.sdlTexture);

		// draw
		this->delegateDraw(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);

		// restores Display::instance target
		SDL_SetRenderTarget(displayRenderer, null);
		SDL_SetTextureBlendMode(img.self.sdlTexture, previousMode);
	}
}
