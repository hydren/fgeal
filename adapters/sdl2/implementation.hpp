/*
 * implementation.hpp (originally impl.hpp)
 *
 *  Created on: 25/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_ADAPTERS_SDL2_IMPL_HPP_
#define FGEAL_ADAPTERS_SDL2_IMPL_HPP_
#include <ciso646>

#include "fgeal/display.hpp"
#include "fgeal/image.hpp"
#include "fgeal/event.hpp"
#include "fgeal/font.hpp"
#include "fgeal/sound.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include <string>
#include <vector>

//works kinda like java's synchronized block
#define synchronized(mutex) for(bool mutex##_SYNCHRONIZED=SDL_mutexP(mutex)||true;mutex##_SYNCHRONIZED;mutex##_SYNCHRONIZED=SDL_mutexV(mutex)&&false)

namespace fgeal
{
	/// Mutex to hepl controlling Mix_Music playback
	extern SDL_mutex* MIX_MUSIC_MUTEX;

	/// Information about the audio parameters
	namespace AudioSpec
	{
		extern int allocatedMixChannelsCount;  // number of mix channels allocated

		// read-only!
		extern Uint16 format;  // current audio format constant
		extern int frequency;  // frequency rate of the current audio format
		extern int channelCount;  // number of channels of the current audio format
	}

	/// Enumeration of types of hat motion
	enum PovAxisMotion { NONE, POV_X, POV_Y, BOTH_AXIS };

	struct Display::implementation
	{
		SDL_Window* sdlWindow;
		SDL_Renderer* sdlRenderer;
	};

	struct Image::implementation
	{
		SDL_Texture* sdlTexture;
		int w, h; //read-only!
	};

	struct Event::implementation
	{
		SDL_Event* sdlEvent;

		// needed to know whether the delta fields are corretly updated or not
		bool isHatPovStateUpdated;

		// needed to distinguish axis motion of joystick hat events that involves motion of both axis
		PovAxisMotion hatPovMotion;
		short hatPovX, hatPovY;

		/** Records, if needed, the joystick hat POV state and identifies what kind of axis motion this event corresponds to.
		 *  Its MANDATORY to call this method before delivering this event to the user. */
		void fillJoystickEventHatFieldsIfNeeded();
	};

	struct EventQueue::implementation
	{
		// there is a global event queue only
	};

	struct Font::implementation
	{
		TTF_Font* sdlttfFont;
		std::string fontFilename;
		unsigned size;
		bool isAntialiased, isKerningEnabled;

		static inline SDL_Renderer* getDisplayRenderer() { return Display::instance->self.sdlRenderer; }
	};

	struct DrawableText::implementation
	{
		std::string content;
		Color color;
		unsigned fontSize;
		SDL_Texture* renderedText;
		int renderedTextWidth, renderedTextHeight;
	};

	struct Sound::implementation
	{
		Mix_Chunk* sdlmixerChunk;
		float speed;  // needed for the pitch callback
		bool dynamicSpeedHinted;  // needed to apply the pitch callback only when needed
	};

	struct Music::implementation
	{
		Mix_Music* sdlmixerMusic;

		// additional info needed because SDL doesn't keep track of these per Music, individually
		short volume;  // the volume set for this music.
		bool loop;  // true if this music was set to loop
		Uint32 position;  // the current position (ms) of this music
		Uint32 timeStarted;  // the time (ticks, ms) when this music last started to play
		Uint32 timePaused;  // the time (ticks, ms) when this music last paused
	};

	namespace KeyboardKeyMapping
	{
		/// Returns the fgeal-equivalent key enum of the given SDL 2.0 keycode.
		Keyboard::Key toGenericKey(int allegroKeycode);

		/// Returns the SDL 2.0 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key);
	}

	namespace MouseButtonMapping
	{
		/// Returns the fgeal-equivalent mouse button enum of the given SDL 2.0 mouse button number.
		Mouse::Button toGenericMouseButton(int allegroButtonNumber);

		/// Returns the SDL 2.0 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button);
	}

	namespace JoystickManagement
	{
		struct AuxData
		{
			SDL_Joystick* sdlJoystick;  // the pointer to the SDL_Joystick struct
			std::vector<Uint8> lastHatsStates;  // holds the last state of each hat
		};

		/// list of auxiliary management data of each joystick, by device index
		extern std::vector<AuxData> auxData;

		// from SDL 2.0.4, this function can be replaced by SDL_JoystickFromInstanceID
		/// returns the opened SDL_Joystick pointer with the given SDL_JoystickID or null if none is found.
		SDL_Joystick* getSdlJoystickByID(SDL_JoystickID id);

		float getAxisValueFromHatPov(float value, bool isPovY);

		/// make the previous hat state be the current one
		void recordJoystickEventHatPovStateIfNeeded(SDL_Event*);
	}
}

#endif /* FGEAL_ADAPTERS_SDL2_IMPL_HPP_ */
