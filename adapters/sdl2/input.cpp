/*
 * input.cpp
 *
 *  Created on: 25/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;

namespace fgeal
{
	using std::vector;
	using std::string;
	using JoystickManagement::AuxData;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		// Returns the fgeal-equivalent key enum of the given SDL 2.0 keycode.
		Keyboard::Key toGenericKey(int sdlKeyCode)
		{
			switch(sdlKeyCode)
			{
				#define KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SDL 2.0 keycode of the given fgeal-equivalent key enum.
		int toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				#define BACKWARDS_KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return 0;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return (bool) SDL_GetKeyboardState(null)[SDL_GetScancodeFromKey(KeyboardKeyMapping::toUnderlyingLibraryKey(key))];
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		// Returns the fgeal-equivalent mouse button enum of the given SDL 2.0 mouse button number.
		Mouse::Button toGenericMouseButton(int sdlMouseButtonNumber)
		{
			switch(sdlMouseButtonNumber)
			{
				#define MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SDL 2.0 mouse button number of the fgeal-equivalent mouse button enum.
		int toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				#define BACKWARDS_MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return 0;
			}
		}
	}


	// API functions

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return SDL_GetMouseState(null, null) & SDL_BUTTON(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		static int x, y;
		SDL_GetMouseState(&x, &y);
		Point pt = {(float) x, (float) y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		static int x;
		SDL_GetMouseState(&x, null);
		return x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		static int y;
		SDL_GetMouseState(null, &y);
		return y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		SDL_WarpMouseInWindow(Display::instance->self.sdlWindow, x, y);
	}


	// =======================================================================================
	// Joystick

	vector<JoystickManagement::AuxData> JoystickManagement::auxData;

	SDL_Joystick* JoystickManagement::getSdlJoystickByID(SDL_JoystickID id)
	{
		for(unsigned j = 0; j < JoystickManagement::auxData.size(); j++)
		{
			AuxData& aux = JoystickManagement::auxData[j];
			if(SDL_JoystickInstanceID(aux.sdlJoystick) == id)
				return aux.sdlJoystick;
		}
		return NULL;
	}

	float JoystickManagement::getAxisValueFromHatPov(float currentValue, bool isPovY)
	{
		if(isPovY)  // pov_y
		{
			if(currentValue == SDL_HAT_UP or currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_RIGHTUP)
				return -1;
			else if(currentValue == SDL_HAT_DOWN or currentValue == SDL_HAT_LEFTDOWN or currentValue == SDL_HAT_RIGHTDOWN)
				return 1;
			else
				return 0;
		}
		else  // pov_x
		{
			if(currentValue == SDL_HAT_LEFT or currentValue == SDL_HAT_LEFTUP or currentValue == SDL_HAT_LEFTDOWN)
				return -1;
			else if(currentValue == SDL_HAT_RIGHT or currentValue == SDL_HAT_RIGHTUP or currentValue == SDL_HAT_RIGHTDOWN)
				return 1;
			else
				return 0;
		}
	}

	void JoystickManagement::recordJoystickEventHatPovStateIfNeeded(SDL_Event* sdlEvent)
	{
		if(sdlEvent->type == SDL_JOYHATMOTION)
			JoystickManagement::auxData[sdlEvent->jhat.which].lastHatsStates[sdlEvent->jhat.hat] = sdlEvent->jhat.value;
	}

	void Joystick::configureAll()
	{
		for(int j = 0; j < SDL_NumJoysticks(); j++)
		{
			SDL_Joystick* sdlJoystick = SDL_JoystickOpen(j);
			if(sdlJoystick != null)
			{
				JoystickManagement::auxData.push_back(AuxData());
				AuxData& aux = JoystickManagement::auxData[j];  // or JoystickManagement::auxData.back()

				aux.sdlJoystick = sdlJoystick;
				for(int h = 0; h < SDL_JoystickNumHats(sdlJoystick); h++)
					aux.lastHatsStates.push_back(SDL_HAT_CENTERED);  // save state of each hat
			}
			else {} // TODO create a proper exception when a joystick could not be opened (or a message on cout/cerr).
		}
	}

	void Joystick::releaseAll()
	{
		for(unsigned j = 0; j < JoystickManagement::auxData.size(); j++)
			SDL_JoystickClose(JoystickManagement::auxData[j].sdlJoystick);

		JoystickManagement::auxData.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return SDL_NumJoysticks();
	}

	string Joystick::getJoystickName(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickName(JoystickManagement::auxData[joyIndex].sdlJoystick);
	}

	/** Returns the number of buttons on this joystick. */
	unsigned Joystick::getButtonCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickNumButtons(JoystickManagement::auxData[joyIndex].sdlJoystick);
	}

	/** Returns the number of axis on this joystick. */
	unsigned Joystick::getAxisCount(unsigned joyIndex)
	{
		FGEAL_CHECK_INIT();
		SDL_Joystick* const sdlJoyPtr = JoystickManagement::auxData[joyIndex].sdlJoystick;
		return SDL_JoystickNumAxes(sdlJoyPtr) + 2*SDL_JoystickNumHats(sdlJoyPtr) + 2*SDL_JoystickNumBalls(sdlJoyPtr);
	}

	/** Returns true if the given button (by index) is being pressed on this joystick. */
	bool Joystick::isButtonPressed(unsigned joyIndex, unsigned buttonIndex)
	{
		FGEAL_CHECK_INIT();
		return SDL_JoystickGetButton(JoystickManagement::auxData[joyIndex].sdlJoystick, buttonIndex) > 0;
	}

	/** Returns the current position of the given axis (by index) on this joystick. */
	float Joystick::getAxisPosition(unsigned joyIndex, unsigned axisIndex)
	{
		FGEAL_CHECK_INIT();
		SDL_Joystick* const sdlJoyPtr = JoystickManagement::auxData[joyIndex].sdlJoystick;
		const unsigned axesCount = SDL_JoystickNumAxes(sdlJoyPtr),
					   hatsCount = SDL_JoystickNumHats(sdlJoyPtr),
					   ballCount = SDL_JoystickNumBalls(sdlJoyPtr);

		if(axisIndex < axesCount)
			return SDL_JoystickGetAxis(sdlJoyPtr, axisIndex)/32768.0f;  // converting range [-32768 to 32767] to [-1.0 to 1.0]

		else if(axisIndex < axesCount + 2*hatsCount)
		{
			const unsigned hat = (axisIndex - axesCount)/2,
					       pov = (axisIndex - axesCount)%2;

			return JoystickManagement::getAxisValueFromHatPov(SDL_JoystickGetHat(sdlJoyPtr, hat), (pov == 1));
		}
		else if(axisIndex < axesCount + 2*hatsCount + 2*ballCount)
		{
			const unsigned ball = (axisIndex - axesCount - 2*hatsCount)/2,
					       dir  = (axisIndex - axesCount - 2*hatsCount)%2;
			int dx, dy;
			SDL_JoystickGetBall(sdlJoyPtr, ball, &dx, &dy);
			return dir == 0? dx : dy;
		}
		else
			return 0;
	}
}
