/*
 * core.cpp
 *
 *  Created on: 14/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fgeal/core.hpp"

#include <SFML/System.hpp>

namespace fgeal
{
	/* fgeal code based on SFML 1.6 */
	const char* ADAPTER_NAME = "SFML 1.6 Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "SFML";
	const char* ADAPTED_LIBRARY_VERSION = "1.x";

	sf::Clock uptimeClock;

	void core::initialize()
	{
		fgeal::uptimeClock.Reset();
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
	}

	bool isProperlyInitialized()
	{
		return true;
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		sf::Sleep(seconds);
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return fgeal::uptimeClock.GetElapsedTime();
	}
}

#include "fgeal/agnostic_filesystem.hxx"
