/*
 * display.cpp
 *
 *  Created on: 14/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <string>

using std::string;
using std::vector;
using std::pair;

namespace fgeal
{
	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		unsigned long settings = sf::Style::Close;
		if(options.fullscreen)
			settings |= sf::Style::Fullscreen;
		else if(options.isUserResizeable)
			settings |= sf::Style::Resize;

		// apparently these calls "cannot fail"...
		self.sfmlRenderWindow.Create(sf::VideoMode(options.width, options.height), options.title, settings);
		self.sfmlRenderWindow.EnableKeyRepeat(false);
		self.currentTitle = options.title;
		self.style = settings;
		self.posx = self.posy = 0;

		if(not options.fullscreen and options.positioning != Options::POSITION_UNDEFINED)
		{
			if(options.positioning == Options::POSITION_DEFINED)
			{
				self.posx = options.position.x;
				self.posy = options.position.y;
				self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
			}

			else if(options.positioning == Options::POSITION_CENTERED)
			{
				const sf::VideoMode desktop = sf::VideoMode::GetDesktopMode();
				self.posx = (desktop.Width - options.width)/2;
				self.posy = (desktop.Height - options.height)/2;
				self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
			}
		}

		if(not options.iconFilename.empty())
			this->setIcon(options.iconFilename);
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.Display();
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.Clear();
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlRenderWindow.GetWidth();
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlRenderWindow.GetHeight();
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		// curiously, Window::SetSize does not resize the window at all, as it only changes the size of the rendering region of the window
		// since we cant set do it another way, we'll recreate the window.
		self.sfmlRenderWindow.Create(sf::VideoMode(width, height), self.currentTitle, self.style);
		self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		return self.currentTitle;
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		if(title == self.currentTitle)
			return;  // nothing to be done if title is already what is wanted
		else
			self.currentTitle = title;

		// it seems to be overkill, but since we cant set title after window creation in SFML 1.6, we'll recreate the window.
		self.sfmlRenderWindow.Create(sf::VideoMode(self.sfmlRenderWindow.GetWidth(), self.sfmlRenderWindow.GetHeight()), self.currentTitle, self.style);
		self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
	}

	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		sf::Image sfmlImageIcon;
		if(not sfmlImageIcon.LoadFromFile(iconFilename))
			throw AdapterException("Error while trying to set display icon. Could not load image: \"%s\"", iconFilename.c_str());

		self.sfmlRenderWindow.SetIcon(sfmlImageIcon.GetWidth(), sfmlImageIcon.GetHeight(), sfmlImageIcon.GetPixelsPtr());
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return self.style & sf::Style::Fullscreen;
	}

	void Display::setFullscreen(bool choice)
	{
		// the call to isFullscreen already calls FGEAL_CHECK_INIT
		if(isFullscreen() == choice)
			return;  // if same mode, nothing needs to be done

		self.style ^= sf::Style::Fullscreen;  // flips fullscreen flag (FFF lol)
		self.sfmlRenderWindow.Show(false);
		self.sfmlRenderWindow.Create(sf::VideoMode(self.sfmlRenderWindow.GetWidth(), self.sfmlRenderWindow.GetHeight()), self.currentTitle, self.style);
		if(choice == false)
			self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
	}

	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		self.posx = pos.x;
		self.posy = pos.y;
		self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
	}

	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		const sf::VideoMode desktop = sf::VideoMode::GetDesktopMode();
		self.posx = (desktop.Width - self.sfmlRenderWindow.GetWidth())/2;
		self.posy = (desktop.Height - self.sfmlRenderWindow.GetHeight())/2;
		self.sfmlRenderWindow.SetPosition(self.posx, self.posy);
	}

	void Display::setMouseCursorVisible(bool choice)
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.ShowMouseCursor(choice);
	}

	vector<Display::Mode> Display::Mode::getList(pair<unsigned, unsigned> requestedAspect)
	{
		FGEAL_CHECK_INIT();
		const unsigned sfmlModeCount = sf::VideoMode::GetModesCount();
		vector<Mode> list;
		for(unsigned i = 0; i < sfmlModeCount; i++)
		{
			const sf::VideoMode sfmlMode = sf::VideoMode::GetMode(i);
			bool alreadyAdded = false;
			for(unsigned j = 0; j < list.size() and not alreadyAdded; j++)
				if(list[j].width == (unsigned) sfmlMode.Width and list[j].height == (unsigned) sfmlMode.Height)
					alreadyAdded = true;

			if(not alreadyAdded)
			{
				Mode mode(sfmlMode.Width, sfmlMode.Height);

				// gather description info from generic list
				const vector<Mode> genericList = Mode::getGenericList(requestedAspect);
				for(unsigned j = 0; j < genericList.size(); j++) if(mode.width == genericList[j].width and mode.height == genericList[j].height)
				{
					mode.description = genericList[j].description;
					mode.aspectRatio = genericList[j].aspectRatio;
				}
				list.push_back(mode);
			}
		}
		return list;
	}
}
