/*
 * font.cpp
 *
 *  Created on: 23/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

#include <iterator>

using std::string;

#define renderWindow Display::instance->self.sfmlRenderWindow

static inline sf::Unicode::Text utf8ToSfmlText(const string& str)
{
	const std::basic_string<unsigned char> tmp(str.begin(), str.end());  // stupid step to make conversion work...
	sf::Unicode::UTF32String text;  // sole UTF type SFML 1.x can work with...
	sf::Unicode::UTF8ToUTF32(tmp.begin(), tmp.end(), std::back_inserter(text));  // only works with basic_string<unsigned char>, crashes if convert from normal string
	return sf::Unicode::Text(text);
}

namespace fgeal
{
	Font::Font(const string& filename, unsigned size, bool antialiasing, bool kerning)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Font* font = new sf::Font();
		if(not font->LoadFromFile(filename, size))
		{
			delete font; font = null;
			throw AdapterException("Font \"%s\" could not be loaded.", filename.c_str());
		}

		self.sfmlString.SetFont(*font);
		self.sfmlString.SetBlendMode(antialiasing? sf::Blend::Alpha : sf::Blend::None);
		self.sfmlString.SetSize(size);
		//XXX SFML 1.6 adapter ignoring font kerning hint
	}

	Font::~Font()
	{
		delete &self.sfmlString.GetFont();
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText(utf8ToSfmlText(text));
		self.sfmlString.SetPosition(x, y);
		self.sfmlString.SetColor(sf::Color(color.r, color.g, color.b, color.a));
		renderWindow.Draw(self.sfmlString);
	}

	float Font::getTextHeight() const
	{
		FGEAL_CHECK_INIT();
		static const sf::Unicode::Text sample("|");  // TODO: test if using an alphanumeric sample (like "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890") instead would make this more reliable
		self.sfmlString.SetText(sample);
		return self.sfmlString.GetRect().GetHeight();
	}
	
	float Font::getTextWidth(const string& text) const
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText(utf8ToSfmlText(text));
		return self.sfmlString.GetRect().GetWidth();
	}

	unsigned Font::getSize() const
	{
		FGEAL_CHECK_INIT();
		return self.sfmlString.GetSize();
	}

	void Font::setSize(unsigned size)
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetSize(size);
	}

	// ##################################################################################################################

	DrawableText::DrawableText(const std::string& text, Font* font, Color c)
	: self(*new implementation()), font(font)
	{
		FGEAL_CHECK_INIT();
		if(font != null)
		{
			self.sfmlString.SetFont(font->self.sfmlString.GetFont());
			self.sfmlString.SetSize(font->self.sfmlString.GetSize());
		}
		self.sfmlString.SetText(utf8ToSfmlText(text));
		self.sfmlString.SetColor(sf::Color(c.r, c.g, c.b, c.a));
	}

	DrawableText::~DrawableText()
	{
		delete &self;
	}

	DrawableText& DrawableText::operator=(const DrawableText& other)
	{
		if(this != &other)
		{
			font = other.font;
			self.sfmlString = other.self.sfmlString;
		}
		return *this;
	};

	DrawableText::DrawableText(const DrawableText& other)
	: self(*new implementation())
	{
		operator =(other);
	}

	float DrawableText::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlString.GetRect().GetWidth();
	}

	float DrawableText::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlString.GetRect().GetHeight();
	}

	void DrawableText::setFont(Font* font)
	{
		FGEAL_CHECK_INIT();
		this->font = font;
		if(font != null)
		{
			self.sfmlString.SetFont(font->self.sfmlString.GetFont());
			self.sfmlString.SetSize(font->self.sfmlString.GetSize());
		}
		else
		{
			const sf::Unicode::Text contentBackup = self.sfmlString.GetText();
			const sf::Color colorBackup = self.sfmlString.GetColor();
			self.sfmlString = sf::String();  // can only nullify sf::String's font this way
			self.sfmlString.SetText(contentBackup);
			self.sfmlString.SetColor(colorBackup);
		}
	}

	void DrawableText::setContent(const string& str)
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetText(utf8ToSfmlText(str));
	}

	string DrawableText::getContent()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlString.GetText();
	}

	void DrawableText::setColor(Color c)
	{
		FGEAL_CHECK_INIT();
		self.sfmlString.SetColor(sf::Color(c.r, c.g, c.b, c.a));
	}

	Color DrawableText::getColor()
	{
		FGEAL_CHECK_INIT();
		const sf::Color& sfc(self.sfmlString.GetColor());
		return Color::create(sfc.r, sfc.g, sfc.b, sfc.a);
	}

	void DrawableText::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		if(self.sfmlString.GetSize() != font->self.sfmlString.GetSize())
			self.sfmlString.SetSize(font->self.sfmlString.GetSize());
		self.sfmlString.SetPosition(x, y);
		renderWindow.Draw(self.sfmlString);
	}
}
