/*
 * image.cpp
 *
 *  Created on: 22/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/exceptions.hpp"

#include <algorithm>

using std::string;
using std::vector;

// auxiliary macros
#define renderWindow Display::instance->self.sfmlRenderWindow
#define imageWidth sfmlSprite.GetImage()->GetWidth()
#define imageHeight sfmlSprite.GetImage()->GetHeight()

namespace fgeal
{
	static inline Color sfmlColorToFgealColor(sf::Color c) { return Color::create(c.r, c.g, c.b, c.a); }

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Image* image = new sf::Image();
		if(not image->LoadFromFile(filename))
		{
			delete image; image = null;
			throw AdapterException("Could not load image: \"%s\"", filename.c_str());
		}
		image->SetSmooth(useImageTransformSmoothingHint);
		self.sfmlSprite.SetImage(*image);
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Image* image = new sf::Image(w, h, sf::Color(0, 0, 0, 0));
		image->SetSmooth(useImageTransformSmoothingHint);
		self.sfmlSprite.SetImage(*image);
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		delete self.sfmlSprite.GetImage();
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.imageWidth;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.imageHeight;
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		FGEAL_CHECK_INIT();
		return sfmlColorToFgealColor(self.sfmlSprite.GetImage()->GetPixel(x, y));
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();
		pixelData.resize(width);
		for(unsigned i = 0; i < width and x+i < self.imageWidth; i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and y+j < self.imageHeight; j++)
			{
				const sf::Color c = self.sfmlSprite.GetImage()->GetPixel(x+i, y+j);
				pixelData[i][j].r = c.r; pixelData[i][j].g = c.g; pixelData[i][j].b = c.b; pixelData[i][j].a = c.a;
			}
		}
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		FGEAL_CHECK_INIT();
		sf::Image& editedImage = *new sf::Image(*self.sfmlSprite.GetImage());
		editedImage.SetPixel(x, y, sf::Color(c.r, c.g, c.b, c.a));
		delete self.sfmlSprite.GetImage();
		self.sfmlSprite.SetImage(editedImage);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& colors)
	{
		FGEAL_CHECK_INIT();
		sf::Image& editedImage = *new sf::Image(*self.sfmlSprite.GetImage());
		for(unsigned i = 0; i < colors.size() and x+i < self.imageWidth; i++)
			for(unsigned j = 0; j < colors[i].size() and y+j < self.imageHeight; j++)
				editedImage.SetPixel(x+i, y+j, sf::Color(colors[i][j].r, colors[i][j].g, colors[i][j].b, colors[i][j].a));
		delete self.sfmlSprite.GetImage();
		self.sfmlSprite.SetImage(editedImage);
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);
	}

	void Image::drawRegion(float x, float y, float from_x, float from_y, float w, float h)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(from_x, from_y, from_x+w, from_y+h));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		//restore source region to full
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		//restore source region to full, clear flip
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draws all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draw all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(rad2deg(angle));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(rad2deg(angle));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left, clear rotation and restore source region to full
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();

		//draw all source region
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(rad2deg(angle));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();

		self.sfmlSprite.SetSubRect(sf::IntRect(fromX, fromY, fromX+fromWidth, fromY+fromHeight));
		self.sfmlSprite.FlipX(flipmode == FLIP_HORIZONTAL);
		self.sfmlSprite.FlipY(flipmode == FLIP_VERTICAL);
		self.sfmlSprite.SetScale(xScale, yScale);
		self.sfmlSprite.SetCenter(centerX, centerY);
		self.sfmlSprite.SetRotation(rad2deg(angle));
		self.sfmlSprite.SetPosition(x, y);
		renderWindow.Draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.SetCenter(0, 0);
		self.sfmlSprite.SetRotation(0);
		self.sfmlSprite.SetScale(1.0, 1.0);
		self.sfmlSprite.FlipX(false);
		self.sfmlSprite.FlipY(false);
		self.sfmlSprite.SetSubRect(sf::IntRect(0, 0, self.imageWidth, self.imageHeight));
	}

	//FIXME SFML 1.6 Image::blit() is partially implementated
	void Image::blit(Image& img, float x, float y, float sx, float sy, float angle, float cx, float cy, const FlipMode flipmode, float fromX, float fromY, float width, float height)
	{
		FGEAL_CHECK_INIT();

		if(width == 0) width = self.imageWidth;
		if(height == 0) height = self.imageHeight;

		const sf::Image& thisImage = *self.sfmlSprite.GetImage();
		if(fromX < 0) { width  += fromX; fromX = 0; }
		if(fromY < 0) { height += fromY; fromY = 0; }
		if(fromX + width  > thisImage.GetWidth())  width  = thisImage.GetWidth()  - fromX;
		if(fromY + height > thisImage.GetHeight()) height = thisImage.GetHeight() - fromY;

		const bool flipX = flipmode == FLIP_HORIZONTAL, flipY = flipmode == FLIP_VERTICAL;
		const sf::Image& targetImage = *img.self.sfmlSprite.GetImage();
		if(x < 0) { width  += x / sx; fromX -= x; x = 0; }
		if(y < 0) { height += y / sy; fromY -= y; y = 0; }
		if(x + sx*width  > targetImage.GetWidth())  { const float prevWidth  = width;   width = (targetImage.GetWidth()  - x) / sx; if(flipX) fromX -=  width - prevWidth; }
		if(y + sy*height > targetImage.GetHeight()) { const float prevHeight = height; height = (targetImage.GetHeight() - y) / sy; if(flipY) fromY -= height - prevHeight; }

		if(width  <= 0) return;
		if(height <= 0) return;

		// TODO check if this could be made faster by using GetPixelsPtr instead of GetPixel (maybe even using LoadFromPixels instead of SetPixel, as a bulk operation)
		// FIXME this ignores rotation
		sf::Image& resultImage = *new sf::Image(targetImage);
		for(unsigned i = 0; i < width * sx;  i++) for(unsigned j = 0; j < height * sy; j++)
		{
			const unsigned fi = flipX? sx * width - 1 - i : i, fj = flipY? sy * height - 1 - j : j;
			const sf::Color src = thisImage.GetPixel(fromX + fi/sx, fromY + fj/sy), dst = targetImage.GetPixel(x + i, y + j);
			const float srca = src.a / 255.f, dsta = dst.a / 255.f;
			resultImage.SetPixel(x+i, y+j, sf::Color(src.r * srca + dst.r * (1 - srca),
													 src.g * srca + dst.g * (1 - srca),
													 src.b * srca + dst.b * (1 - srca),
													   255 *(srca + dsta  * (1 - srca))));
		}
		delete img.self.sfmlSprite.GetImage();
		img.self.sfmlSprite.SetImage(resultImage);
	}
}
