/*
 * implementation.hpp (originally impl.hpp)
 *
 *  Created on: 14/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_ADAPTERS_SFML_IMPL_HPP_
#define FGEAL_ADAPTERS_SFML_IMPL_HPP_

#include "fgeal/display.hpp"
#include "fgeal/image.hpp"
#include "fgeal/event.hpp"
#include "fgeal/font.hpp"
#include "fgeal/sound.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

#include <string>

// Uncomment this if you are trying to compile this adapter with SFML older than 1.6. Not guaranteed to work, though.
//#define FGEAL_SFML_PRE_1_6

namespace fgeal
{
	extern sf::Clock uptimeClock;

	/// Enumeration of types of hat motion
	enum PovAxisMotion { NONE, POV_X, POV_Y, BOTH_AXIS };

	struct Display::implementation
	{
		sf::RenderWindow sfmlRenderWindow;
		std::string currentTitle;
		sf::Uint32 style;
		int posx, posy;
	};

	struct Image::implementation
	{
		sf::Sprite sfmlSprite;
	};

	struct Event::implementation
	{
		sf::Event sfmlEvent;

		// needed to know whether the delta fields are corretly updated or not
		bool isHatPovStateUpdated;

		// needed to distinguish axis motion of joystick hat events that involves motion of both axis
		PovAxisMotion hatPovMotion;
		short hatPovX, hatPovY;

		/** Records, if needed, the joystick hat POV state and identifies what kind of axis motion this event corresponds to.
		 *  Its MANDATORY to call this method before delivering this event to the user. */
		void recordJoystickEventHatPovStateIfNeeded();
	};

	struct EventQueue::implementation
	{
		sf::Event cachedSfmlEvent;
		bool cached;
	};

	struct Font::implementation
	{
		sf::String sfmlString;
	};

	struct DrawableText::implementation
	{
		sf::String sfmlString;
	};

	struct Sound::implementation
	{
		sf::Sound sfmlSound;
	};

	struct SoundStream::implementation
	{
		sf::Music sfmlSoundStream;
	};

	struct Music::implementation
	{
		sf::Music sfmlMusic;
	};

	namespace KeyboardKeyMapping
	{
		/// Returns the fgeal-equivalent key enum of the given SFML keycode.
		Keyboard::Key toGenericKey(sf::Key::Code sfmlKeycode);

		/// Returns the SFML keycode of the given fgeal-equivalent key enum.
		sf::Key::Code toUnderlyingLibraryKey(Keyboard::Key key);
	}

	namespace MouseButtonMapping
	{
		/// Returns the fgeal-equivalent mouse button enum of the given SFML mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlButtonNumber);

		/// Returns the SFML mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button);
	}

	namespace JoystickManagement
	{
		/// holds the last state of the hat of each joystick
		extern std::vector<float> lastHatsStates;

		float getAxisValueFromHatPov(float value, bool povY);

		void recordJoystickEventHatPovStateIfNeeded(sf::Event&);
	}
}

#endif /* FGEAL_ADAPTERS_SFML_IMPL_HPP_ */
