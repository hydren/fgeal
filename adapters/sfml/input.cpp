/*
 * input.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define window Display::instance->self.sfmlRenderWindow

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;
#define window Display::instance->self.sfmlRenderWindow

/* Tweaks to attempt to work with SFML versions older than 1.6. Not guaranteed, though.
 * Starting with SFML 1.6, sf::Mouse::Count was renamed to sf::Mouse::ButtonCount */
#ifdef FGEAL_SFML_PRE_1_6
	#define SFML_MOUSE_BUTTON_COUNT sf::Mouse::Count
	#define SFML_JOYSTICK_AXIS_COUNT sf::Joy::Count
	#define SFML_JOYSTICK_BUTTON_COUNT 16
#else
	#define SFML_MOUSE_BUTTON_COUNT sf::Mouse::ButtonCount
	#define SFML_JOYSTICK_AXIS_COUNT sf::Joy::AxisCount
	#define SFML_JOYSTICK_BUTTON_COUNT sf::Joy::ButtonCount
#endif

namespace fgeal
{
	using std::vector;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		// Returns the fgeal-equivalent key enum of the given SFML keycode.
		Keyboard::Key toGenericKey(sf::Key::Code sfmlKeycode)
		{
			switch(sfmlKeycode)
			{
				#define KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SFML keycode of the given fgeal-equivalent key enum.
		sf::Key::Code toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				#define BACKWARDS_KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				case Keyboard::KEY_NUMPAD_DECIMAL: return static_cast<sf::Key::Code>(0);  // XXX SFML does not recognize the decimal point numpad key
				case Keyboard::KEY_NUMPAD_ENTER:   return sf::Key::Return;  // XXX SFML does not makes distinction between Enter and the numpad Enter
				default: return sf::Key::Count;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().IsKeyDown(KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		// Returns the fgeal-equivalent mouse button enum of the given SFML mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlMouseButton)
		{
			switch(sfmlMouseButton)
			{
				#define MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SFML mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				#define BACKWARDS_MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return SFML_MOUSE_BUTTON_COUNT;
			}
		}
	}

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().IsMouseButtonDown(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		Point pt = {(float) window.GetInput().GetMouseX(), (float) window.GetInput().GetMouseY()};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().GetMouseX();
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		return window.GetInput().GetMouseY();
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		window.SetCursorPosition(x, y);
	}

	// =======================================================================================
	// Joystick

	vector<float> JoystickManagement::lastHatsStates;

	float JoystickManagement::getAxisValueFromHatPov(float value, bool isPovY)
	{
		return value < 0? 0
				: isPovY?  (value == 90 or value == 270? 0 : value < 90 or value > 270? -1 : 1)
				: /*PovX*/ (value == 0  or value == 180? 0 : value > 180?               -1 : 1 );
	}

	void JoystickManagement::recordJoystickEventHatPovStateIfNeeded(sf::Event& sfmlEvent)
	{
		// skip if it's not a joystick hat motion event
		if(sfmlEvent.Type == sf::Event::JoyMoved)
			JoystickManagement::lastHatsStates[sfmlEvent.JoyMove.JoystickId] = sfmlEvent.JoyMove.Position;
	}

	// nothing is needed
	void Joystick::configureAll()
	{
		JoystickManagement::lastHatsStates.resize(sf::Joy::Count, -1);
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::lastHatsStates.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return sf::Joy::Count;  //XXX SFML 1.6 adapter: there is no way to get the actual number of joysticks
	}

	std::string Joystick::getJoystickName(unsigned joystickIndex)
	{
		core::reportAbsentImplementation("Getting joystick name is not supported on this adapter.");
		return std::string();  //XXX SFML 1.6 adapter: there is no way to get a joystick's name
	}

	unsigned Joystick::getButtonCount(unsigned joystickIndex)
	{
		return SFML_JOYSTICK_BUTTON_COUNT;  //XXX SFML 1.6 adapter: there is no way to get the actual number of buttons of a joystick
	}

	unsigned Joystick::getAxisCount(unsigned joystickIndex)
	{
		return SFML_JOYSTICK_AXIS_COUNT+1;  //XXX SFML 1.6 adapter: there is no way to get the actual number of axis of a joystick
	}

	bool Joystick::isButtonPressed(unsigned joystickIndex, unsigned buttonIndex)
	{
		return window.GetInput().IsJoystickButtonDown(joystickIndex, buttonIndex);
	}

	float Joystick::getAxisPosition(unsigned joystickIndex, unsigned axisIndex)
	{
		const sf::Joy::Axis sfmlAxis = static_cast<sf::Joy::Axis>(axisIndex < SFML_JOYSTICK_AXIS_COUNT? axisIndex : SFML_JOYSTICK_AXIS_COUNT-1);
		if(sfmlAxis != sf::Joy::AxisPOV)
			return window.GetInput().GetJoystickAxis(joystickIndex, sfmlAxis) * 0.01f;
		else
		{
			const bool isPovY = (axisIndex == SFML_JOYSTICK_AXIS_COUNT);
			const float value = window.GetInput().GetJoystickAxis(joystickIndex, sfmlAxis);
			return JoystickManagement::getAxisValueFromHatPov(value, isPovY);
		}
	}
}
