/*
 * input_mappings.hxx
 *
 *  Created on: 11 de abr de 2022
 *      Author: hydren
 */

// dummy case for Eclipse CDT parser, so it won't flood warnings when viewing this file in the editor
#ifdef __CDT_PARSER__
	#define KEYBOARD_MAPPINGS
	#define MOUSE_MAPPINGS
	#define mapping(a, b)  a, b;
	#include "fgeal/input.hpp"
	#include <SFML/Window/Event.hpp>
	namespace fgeal { void dummy() {
#else
	#ifdef BACKWARDS_KEYBOARD_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define KEYBOARD_MAPPINGS
		#undef BACKWARDS_KEYBOARD_MAPPINGS
	#endif
	#ifdef BACKWARDS_MOUSE_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define MOUSE_MAPPINGS
		#undef BACKWARDS_MOUSE_MAPPINGS
	#endif

	#undef mapping
	#ifdef BACKWARDS_MAPPINGS
		#define mapping(a, b) case b: return a;
	#else
		#define mapping(a, b) case a: return b;
	#endif
	#undef BACKWARDS_MAPPINGS
#endif

#ifdef KEYBOARD_MAPPINGS
	mapping(sf::Key::A, Keyboard::KEY_A)
	mapping(sf::Key::B, Keyboard::KEY_B)
	mapping(sf::Key::C, Keyboard::KEY_C)
	mapping(sf::Key::D, Keyboard::KEY_D)
	mapping(sf::Key::E, Keyboard::KEY_E)
	mapping(sf::Key::F, Keyboard::KEY_F)
	mapping(sf::Key::G, Keyboard::KEY_G)
	mapping(sf::Key::H, Keyboard::KEY_H)
	mapping(sf::Key::I, Keyboard::KEY_I)
	mapping(sf::Key::J, Keyboard::KEY_J)
	mapping(sf::Key::K, Keyboard::KEY_K)
	mapping(sf::Key::L, Keyboard::KEY_L)
	mapping(sf::Key::M, Keyboard::KEY_M)
	mapping(sf::Key::N, Keyboard::KEY_N)
	mapping(sf::Key::O, Keyboard::KEY_O)
	mapping(sf::Key::P, Keyboard::KEY_P)
	mapping(sf::Key::Q, Keyboard::KEY_Q)
	mapping(sf::Key::R, Keyboard::KEY_R)
	mapping(sf::Key::S, Keyboard::KEY_S)
	mapping(sf::Key::T, Keyboard::KEY_T)
	mapping(sf::Key::U, Keyboard::KEY_U)
	mapping(sf::Key::V, Keyboard::KEY_V)
	mapping(sf::Key::W, Keyboard::KEY_W)
	mapping(sf::Key::X, Keyboard::KEY_X)
	mapping(sf::Key::Y, Keyboard::KEY_Y)
	mapping(sf::Key::Z, Keyboard::KEY_Z)

	mapping(sf::Key::Num0, Keyboard::KEY_0)
	mapping(sf::Key::Num1, Keyboard::KEY_1)
	mapping(sf::Key::Num2, Keyboard::KEY_2)
	mapping(sf::Key::Num3, Keyboard::KEY_3)
	mapping(sf::Key::Num4, Keyboard::KEY_4)
	mapping(sf::Key::Num5, Keyboard::KEY_5)
	mapping(sf::Key::Num6, Keyboard::KEY_6)
	mapping(sf::Key::Num7, Keyboard::KEY_7)
	mapping(sf::Key::Num8, Keyboard::KEY_8)
	mapping(sf::Key::Num9, Keyboard::KEY_9)

	mapping(sf::Key::F1, Keyboard::KEY_F1)
	mapping(sf::Key::F2, Keyboard::KEY_F2)
	mapping(sf::Key::F3, Keyboard::KEY_F3)
	mapping(sf::Key::F4, Keyboard::KEY_F4)
	mapping(sf::Key::F5, Keyboard::KEY_F5)
	mapping(sf::Key::F6, Keyboard::KEY_F6)
	mapping(sf::Key::F7, Keyboard::KEY_F7)
	mapping(sf::Key::F8, Keyboard::KEY_F8)
	mapping(sf::Key::F9, Keyboard::KEY_F9)
	mapping(sf::Key::F10, Keyboard::KEY_F10)
	mapping(sf::Key::F11, Keyboard::KEY_F11)
	mapping(sf::Key::F12, Keyboard::KEY_F12)

	mapping(sf::Key::Up,    Keyboard::KEY_ARROW_UP)
	mapping(sf::Key::Down,  Keyboard::KEY_ARROW_DOWN)
	mapping(sf::Key::Left,  Keyboard::KEY_ARROW_LEFT)
	mapping(sf::Key::Right, Keyboard::KEY_ARROW_RIGHT)

	mapping(sf::Key::Return, Keyboard::KEY_ENTER)
	mapping(sf::Key::Space,  Keyboard::KEY_SPACE)
	mapping(sf::Key::Escape, Keyboard::KEY_ESCAPE)
	mapping(sf::Key::LControl,  Keyboard::KEY_LEFT_CONTROL)
	mapping(sf::Key::RControl,  Keyboard::KEY_RIGHT_CONTROL)
	mapping(sf::Key::LShift,    Keyboard::KEY_LEFT_SHIFT)
	mapping(sf::Key::RShift,    Keyboard::KEY_RIGHT_SHIFT)
	mapping(sf::Key::LAlt,      Keyboard::KEY_LEFT_ALT)
	mapping(sf::Key::RAlt,      Keyboard::KEY_RIGHT_ALT)
	mapping(sf::Key::LSystem,   Keyboard::KEY_LEFT_SUPER)
	mapping(sf::Key::RSystem,   Keyboard::KEY_RIGHT_SUPER)
	mapping(sf::Key::Menu,      Keyboard::KEY_MENU)
	mapping(sf::Key::Tab,       Keyboard::KEY_TAB)
	mapping(sf::Key::Back, Keyboard::KEY_BACKSPACE)

	mapping(sf::Key::Dash,      Keyboard::KEY_MINUS)
	mapping(sf::Key::Equal,     Keyboard::KEY_EQUALS)
	mapping(sf::Key::LBracket,  Keyboard::KEY_LEFT_BRACKET)
	mapping(sf::Key::RBracket,  Keyboard::KEY_RIGHT_BRACKET)
	mapping(sf::Key::SemiColon, Keyboard::KEY_SEMICOLON)
	mapping(sf::Key::Comma,     Keyboard::KEY_COMMA)
	mapping(sf::Key::Period,    Keyboard::KEY_PERIOD)
	mapping(sf::Key::Slash,     Keyboard::KEY_SLASH)
	mapping(sf::Key::BackSlash, Keyboard::KEY_BACKSLASH)
	mapping(sf::Key::Quote,     Keyboard::KEY_QUOTE)
	mapping(sf::Key::Tilde,     Keyboard::KEY_TILDE)

	mapping(sf::Key::Insert,   Keyboard::KEY_INSERT)
	mapping(sf::Key::Delete,   Keyboard::KEY_DELETE)
	mapping(sf::Key::Home,     Keyboard::KEY_HOME)
	mapping(sf::Key::End,      Keyboard::KEY_END)
	mapping(sf::Key::PageUp,   Keyboard::KEY_PAGE_UP)
	mapping(sf::Key::PageDown, Keyboard::KEY_PAGE_DOWN)

	mapping(sf::Key::Numpad0, Keyboard::KEY_NUMPAD_0)
	mapping(sf::Key::Numpad1, Keyboard::KEY_NUMPAD_1)
	mapping(sf::Key::Numpad2, Keyboard::KEY_NUMPAD_2)
	mapping(sf::Key::Numpad3, Keyboard::KEY_NUMPAD_3)
	mapping(sf::Key::Numpad4, Keyboard::KEY_NUMPAD_4)
	mapping(sf::Key::Numpad5, Keyboard::KEY_NUMPAD_5)
	mapping(sf::Key::Numpad6, Keyboard::KEY_NUMPAD_6)
	mapping(sf::Key::Numpad7, Keyboard::KEY_NUMPAD_7)
	mapping(sf::Key::Numpad8, Keyboard::KEY_NUMPAD_8)
	mapping(sf::Key::Numpad9, Keyboard::KEY_NUMPAD_9)

	mapping(sf::Key::Add,      Keyboard::KEY_NUMPAD_ADDITION)
	mapping(sf::Key::Subtract, Keyboard::KEY_NUMPAD_SUBTRACTION)
	mapping(sf::Key::Multiply, Keyboard::KEY_NUMPAD_MULTIPLICATION)
	mapping(sf::Key::Divide,   Keyboard::KEY_NUMPAD_DIVISION)
#endif
#undef KEYBOARD_MAPPINGS

#ifdef MOUSE_MAPPINGS
	mapping(sf::Mouse::Left,   Mouse::BUTTON_LEFT)
	mapping(sf::Mouse::Middle, Mouse::BUTTON_MIDDLE)
	mapping(sf::Mouse::Right,  Mouse::BUTTON_RIGHT)
#endif
#undef MOUSE_MAPPINGS

#ifdef __CDT_PARSER__
}}
#endif
