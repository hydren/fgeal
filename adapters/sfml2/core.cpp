/*
 * core.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "fgeal/core.hpp"

#include <SFML/System.hpp>

#define AUX_MACRO_NAME_TO_STR(A) #A
#define AUX_MACRO_VALUE_TO_STR(A) AUX_MACRO_NAME_TO_STR(A)
#ifdef SFML_VERSION_PATCH
	#define SFML_VERSION_LITERAL SFML_VERSION_MAJOR.SFML_VERSION_MINOR.SFML_VERSION_PATCH
#else  // when SFML_VERSION_PATCH is not defined, prevent from including it in the version literal
	#define SFML_VERSION_LITERAL SFML_VERSION_MAJOR.SFML_VERSION_MINOR
#endif
#define SFML_VERSION_STR AUX_MACRO_VALUE_TO_STR(SFML_VERSION_LITERAL)

namespace fgeal
{
	/* fgeal code based on SFML 2.x */
	const char* ADAPTER_NAME = "SFML 2.x Adapter for fgeal";
	const char* ADAPTED_LIBRARY_NAME = "SFML";
	const char* ADAPTED_LIBRARY_VERSION = SFML_VERSION_STR;

	sf::Clock uptimeClock;

	void core::initialize()
	{
		fgeal::uptimeClock.restart();
	}

	void core::finalize()
	{
		FGEAL_CHECK_INIT();
	}

	bool isProperlyInitialized()
	{
		return true;
	}

	void rest(double seconds)
	{
		FGEAL_CHECK_INIT();
		sf::sleep(sf::seconds(seconds));
	}

	double uptime()
	{
		FGEAL_CHECK_INIT();
		return fgeal::uptimeClock.getElapsedTime().asSeconds();
	}
}

#include "fgeal/agnostic_filesystem.hxx"
