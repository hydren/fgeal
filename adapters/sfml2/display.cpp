/*
 * display.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/display.hpp"

#include "fgeal/exceptions.hpp"

#include <string>

using std::string;
using std::vector;
using std::pair;

namespace fgeal
{
	Display::Display(const Options& options)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();

		unsigned long settings = sf::Style::Close;
		if(options.fullscreen)
			settings |= sf::Style::Fullscreen;
		else if(options.isUserResizeable)
			settings |= sf::Style::Resize;

		// apparently these calls "cannot fail"...
		self.sfmlRenderWindow.create(sf::VideoMode(options.width, options.height), options.title, settings);
		self.sfmlRenderWindow.setKeyRepeatEnabled(false);
		self.currentTitle = options.title;
		self.style = settings;

		if(not options.fullscreen and options.positioning != Options::POSITION_UNDEFINED)
		{
			if(options.positioning == Options::POSITION_DEFINED)
				self.sfmlRenderWindow.setPosition(sf::Vector2i(options.position.x, options.position.y));

			else if(options.positioning == Options::POSITION_CENTERED)
			{
				const sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
				self.sfmlRenderWindow.setPosition(sf::Vector2i((desktop.width - options.width)/2, (desktop.height - options.height)/2));
			}
		}

		if(not options.iconFilename.empty())
			this->setIcon(options.iconFilename);

		fgeal::drawRenderTarget = &self.sfmlRenderWindow;
		fgeal::drawRenderTargetSprite = null;
	}

	Display::~Display()
	{
		FGEAL_CHECK_INIT();
		delete &self;
	}

	void Display::refresh()
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.display();
	}

	void Display::clear()
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.clear();
	}

	unsigned Display::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlRenderWindow.getSize().x;
	}

	unsigned Display::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlRenderWindow.getSize().y;
	}

	void Display::setSize(unsigned width, unsigned height)
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.setSize(sf::Vector2u(width, height));
		self.sfmlRenderWindow.setView(sf::View(sf::FloatRect(0, 0, width, height)));
	}

	string Display::getTitle() const
	{
		FGEAL_CHECK_INIT();
		return self.currentTitle;
	}

	void Display::setTitle(const string& title)
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.setTitle(title);
	}

	void Display::setIcon(const std::string& iconFilename)
	{
		FGEAL_CHECK_INIT();
		sf::Image sfmlImageIcon;
		if(not sfmlImageIcon.loadFromFile(iconFilename))
			throw AdapterException("Error while trying to set display icon. Could not load image: \"%s\"", iconFilename.c_str());

		self.sfmlRenderWindow.setIcon(sfmlImageIcon.getSize().x, sfmlImageIcon.getSize().y, sfmlImageIcon.getPixelsPtr());
	}

	bool Display::isFullscreen()
	{
		FGEAL_CHECK_INIT();
		return self.style & sf::Style::Fullscreen;
	}

	void Display::setFullscreen(bool choice)
	{
		FGEAL_CHECK_INIT();
		if(isFullscreen() == choice)  // the call to isFullscreen already calls checkInit
			return;  // if same mode, nothing needs to be done

		self.style ^= sf::Style::Fullscreen;  // flips fullscreen flag (FFF lol)
		self.sfmlRenderWindow.setVisible(false);
		self.sfmlRenderWindow.create(sf::VideoMode(self.sfmlRenderWindow.getSize().x, self.sfmlRenderWindow.getSize().y), self.currentTitle, self.style);
	}

	void Display::setPosition(const Point& pos)
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.setPosition(sf::Vector2i(pos.x, pos.y));
	}

	void Display::setPositionOnCenter()
	{
		FGEAL_CHECK_INIT();
		const sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
		self.sfmlRenderWindow.setPosition(sf::Vector2i((desktop.width - self.sfmlRenderWindow.getSize().x)/2, (desktop.height - self.sfmlRenderWindow.getSize().y)/2));
	}

	void Display::setMouseCursorVisible(bool choice)
	{
		FGEAL_CHECK_INIT();
		self.sfmlRenderWindow.setMouseCursorVisible(choice);
	}

	vector<Display::Mode> Display::Mode::getList(pair<unsigned, unsigned> requestedAspect)
	{
		FGEAL_CHECK_INIT();
		const vector<sf::VideoMode>& sfmlModes = sf::VideoMode::getFullscreenModes();
		vector<Mode> list;
		for(unsigned i = 0; i < sfmlModes.size(); i++)
		{
			const sf::VideoMode& sfmlMode = sfmlModes[i];
			bool alreadyAdded = false;
			for(unsigned j = 0; j < list.size() and not alreadyAdded; j++)
				if(list[j].width == sfmlMode.width and list[j].height == sfmlMode.height)
					alreadyAdded = true;

			if(not alreadyAdded)
			{
				Mode mode(sfmlMode.width, sfmlMode.height);

				// gather description info from generic list
				const vector<Mode> genericList = Mode::getGenericList(requestedAspect);
				for(unsigned j = 0; j < genericList.size(); j++) if(mode.width == genericList[j].width and mode.height == genericList[j].height)
				{
					mode.description = genericList[j].description;
					mode.aspectRatio = genericList[j].aspectRatio;
				}
				list.push_back(mode);
			}
		}
		return list;
	}
}
