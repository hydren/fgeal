/*
 * font.cpp
 *
 *  Created on: 30/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/font.hpp"

#include "fgeal/exceptions.hpp"

using std::string;

// fallback for SFML older than 2.4.
// from SFML 2.4 and upwards, setFillColor is the preferred way to set text color.
#if SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR < 4
	#define setFillColor(sfText, color) sfText.setColor(color)
	#define getFillColor(sfText) sfText.getColor()
#else
	#define setFillColor(sfText, color) sfText.setFillColor(color)
	#define getFillColor(sfText) sfText.getFillColor()
#endif

static inline sf::String utf8ToSfmlString(const string& str)
{
	// fallback for SFML older than 2.2.
	// from SFML 2.2 and upwards, the fromUtf8() method is available.
	#if SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR < 2
		std::basic_string<sf::Uint32> tmp;
		sf::Utf8::toUtf32(str.begin(), str.end(), std::back_inserter(tmp));
		return sf::String(tmp);
	#else
		return sf::String::fromUtf8(str.begin(), str.end());
	#endif
}

namespace fgeal
{
	Font::Font(const string& filename, unsigned size, bool antialiasing, bool kerning)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Font* font = new sf::Font();
		if(not font->loadFromFile(filename))
		{
			delete font; font = null;
			throw AdapterException("Font \"%s\" could not be loaded.", filename.c_str());
		}
		self.sfmlText.setFont(*font);
		self.sfmlText.setCharacterSize(size);

		//XXX SFML2.x adapter ignoring font anti-aliasing hint
		//XXX SFML2.x adapter ignoring font kerning hint
	}

	Font::~Font()
	{
		delete self.sfmlText.getFont();
		delete &self;
	}

	void Font::drawText(const string& text, float x, float y, Color color)
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(utf8ToSfmlString(text));
		self.sfmlText.setPosition(x, y - self.sfmlText.getLocalBounds().top * 0.5f);
		setFillColor(self.sfmlText, sf::Color(color.r, color.g, color.b, color.a));
		fgeal::drawRenderTarget->draw(self.sfmlText);
	}

	float Font::getTextHeight() const
	{
		FGEAL_CHECK_INIT();
		return self.sfmlText.getFont()->getLineSpacing(self.sfmlText.getCharacterSize());
	}

	float Font::getTextWidth(const std::string& text) const
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(utf8ToSfmlString(text));
		return self.sfmlText.getLocalBounds().width;
	}

	unsigned Font::getSize() const
	{
		FGEAL_CHECK_INIT();
		return self.sfmlText.getCharacterSize();
	}

	void Font::setSize(unsigned size)
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setCharacterSize(size);
	}

	// ##################################################################################################################

	DrawableText::DrawableText(const std::string& text, Font* font, Color c)
	: self(*new implementation()), font(font)
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(utf8ToSfmlString(text));
		if(font != null)
		{
			self.sfmlText.setFont(*font->self.sfmlText.getFont());
			self.sfmlText.setCharacterSize(font->self.sfmlText.getCharacterSize());
			self.sfmlText.setOrigin(0, self.sfmlText.getLocalBounds().top * 0.5f);
		}
		setFillColor(self.sfmlText, sf::Color(c.r, c.g, c.b, c.a));
	}

	DrawableText::~DrawableText()
	{
		delete &self;
	}

	DrawableText& DrawableText::operator=(const DrawableText& other)
	{
		if(this != &other)
		{
			font = other.font;
			self.sfmlText = other.self.sfmlText;
		}
		return *this;
	};

	DrawableText::DrawableText(const DrawableText& other)
	: self(*new implementation())
	{
		operator =(other);
	}

	float DrawableText::getWidth()
	{
		FGEAL_CHECK_INIT();
		// ensures correct size, since Font size may have changed
		if(self.sfmlText.getCharacterSize() != font->self.sfmlText.getCharacterSize())
		{
			self.sfmlText.setCharacterSize(font->self.sfmlText.getCharacterSize());
			self.sfmlText.setOrigin(0, self.sfmlText.getLocalBounds().top * 0.5f);
		}
		return self.sfmlText.getLocalBounds().width;
	}

	float DrawableText::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlText.getFont()->getLineSpacing(font->self.sfmlText.getCharacterSize());
	}

	void DrawableText::setFont(Font* font)
	{
		FGEAL_CHECK_INIT();
		this->font = font;
		if(font != null)
		{
			self.sfmlText.setFont(*font->self.sfmlText.getFont());
			self.sfmlText.setCharacterSize(font->self.sfmlText.getCharacterSize());
			self.sfmlText.setOrigin(0, self.sfmlText.getLocalBounds().top * 0.5f);
		}
		else  // null font
		{
			const sf::String contentBackup = self.sfmlText.getString();
			const sf::Color colorBackup = getFillColor(self.sfmlText);
			self.sfmlText = sf::Text();  // can only nullify sf::Text's font this way
			self.sfmlText.setString(contentBackup);
			setFillColor(self.sfmlText, colorBackup);
		}
	}

	void DrawableText::setContent(const string& str)
	{
		FGEAL_CHECK_INIT();
		self.sfmlText.setString(utf8ToSfmlString(str));
		self.sfmlText.setOrigin(0, self.sfmlText.getLocalBounds().top * 0.5f);
	}

	string DrawableText::getContent()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlText.getString();
	}

	void DrawableText::setColor(Color c)
	{
		FGEAL_CHECK_INIT();
		setFillColor(self.sfmlText, sf::Color(c.r, c.g, c.b, c.a));
	}

	Color DrawableText::getColor()
	{
		FGEAL_CHECK_INIT();
		const sf::Color& sfc = getFillColor(self.sfmlText);
		return Color::create(sfc.r, sfc.g, sfc.b, sfc.a);
	}

	void DrawableText::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		// ensures correct size, since Font size may have changed
		if(self.sfmlText.getCharacterSize() != font->self.sfmlText.getCharacterSize())
		{
			self.sfmlText.setCharacterSize(font->self.sfmlText.getCharacterSize());
			self.sfmlText.setOrigin(0, self.sfmlText.getLocalBounds().top * 0.5f);
		}
		self.sfmlText.setPosition(x, y);
		fgeal::drawRenderTarget->draw(self.sfmlText);
	}
}
