/*
 * graphics.cpp
 *
 *  Created on: 21/06/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/graphics.hpp"
#include "fgeal/generic_graphics.hxx"
#include "fgeal/exceptions.hpp"

#include <cmath>
#include <algorithm>

// fallback for SFML older than 2.4.
// from SFML 2.4 and upwards, some PrimitiveType's used here were renamed
#if SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR < 4
	#define LineStrip LinesStrip
	#define TriangleFan	TrianglesFan
#endif

using std::vector;

namespace fgeal
{
	/// extern-expected on implementation.hpp
	sf::Sprite* drawRenderTargetSprite = null;

	/// extern-expected on implementation.hpp
	sf::RenderTarget* drawRenderTarget = null;

	// static
	void Graphics::setDrawTarget(const Image* image)
	{
		FGEAL_CHECK_INIT();
		// if current draw render target is a render texture, close it first
		if(drawRenderTarget != null and fgeal::drawRenderTargetSprite != null)
		{
			// grab reference (pointer) to previous draw render target
			sf::RenderTexture& previousRenderTexture = *static_cast<sf::RenderTexture*>(fgeal::drawRenderTarget);

			// must call in the end of rendering
			previousRenderTexture.display();

			// create a copy of the sf::Texture of the sf::Sprite of the fgeal::Image set as draw target
			sf::Texture& drawRenderTargetSpriteNewTexture = *new sf::Texture(previousRenderTexture.getTexture());
			drawRenderTargetSpriteNewTexture.setSmooth(Image::useImageTransformSmoothingHint);

			// replace the sf::Texture of the sf::Sprite of the fgeal::Image set as draw target with a copy of itself since the original will be destroyed when the sf::RenderTarget gets destroyed.
			fgeal::drawRenderTargetSprite->setTexture(drawRenderTargetSpriteNewTexture);

			// delete the now-unused sf::RenderTexture
			delete &previousRenderTexture;

			// replace sf::RenderTexture with display's sf::RenderWindow as draw render target, if possible
			fgeal::drawRenderTarget = Display::instance == null? null : &Display::instance->self.sfmlRenderWindow;

			// nullify previous target's sprite
			fgeal::drawRenderTargetSprite = null;
		}

		if(image != null)
		{
			// create render target for the image
			sf::RenderTexture& renderTexture = *new sf::RenderTexture();
			if(not renderTexture.create(image->self.sfmlSprite.getTexture()->getSize().x, image->self.sfmlSprite.getTexture()->getSize().y))
				throw AdapterException("Failed to create RenderTexture at Image::setDrawTarget() method.");

			// draw current content of image on the render texture
			renderTexture.clear(sf::Color::Transparent);
			renderTexture.draw(sf::Sprite(*image->self.sfmlSprite.getTexture()));  // don't know why, but the new sf::Sprite instance is needed for this to work; otherwise it just doesn't work (which is confusing since the new instance uses the exactly same texture :S)
			renderTexture.display();

			// replace image's inner texture with the render texture's internal texture
			delete image->self.sfmlSprite.getTexture();
			image->self.sfmlSprite.setTexture(renderTexture.getTexture());

			// update draw render target reference
			fgeal::drawRenderTarget = &renderTexture;
			fgeal::drawRenderTargetSprite = &image->self.sfmlSprite;
		}
		else  // set default display
		{
			fgeal::drawRenderTarget = Display::instance == null? null : &Display::instance->self.sfmlRenderWindow;
			fgeal::drawRenderTargetSprite = null;
		}
	}

	// static
	void Graphics::setDrawTarget(const Display& display)
	{
		FGEAL_CHECK_INIT();
		Graphics::setDrawTarget(null);  // this call removes previous render texture targets, if any
		fgeal::drawRenderTarget = &display.self.sfmlRenderWindow;
	}

	// static
	void Graphics::setDefaultDrawTarget()
	{
		FGEAL_CHECK_INIT();
		Graphics::setDrawTarget(null);
	}

	// =====================================================================================================================================================
	// Primitives

	void Graphics::drawLine(float x1, float y1, float x2, float y2, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const sf::Vertex line[] = {
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x2, y2), sf::Color(color.r, color.g, color.b, color.a))
		};
		fgeal::drawRenderTarget->draw(line, 2, sf::Lines);
	}

	void Graphics::drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& color)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickLine(x1, y1, x2, y2, thickness, color);
	}

	void Graphics::drawRectangle(float x, float y, float width, float height, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::RectangleShape shape(sf::Vector2f(width, height));
		shape.setPosition(x, y);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(1.0);
		shape.setFillColor(sf::Color::Transparent);
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawFilledRectangle(float x, float y, float width, float height, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::RectangleShape shape(sf::Vector2f(width, height));
		shape.setPosition(x, y);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(0.5);
		shape.setFillColor(sf::Color(color.r, color.g, color.b, color.a));
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawCircle(float cx, float cy, float r, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::CircleShape shape(r);
		shape.setPosition(cx-r, cy-r);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(1.0);
		shape.setFillColor(sf::Color::Transparent);
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawFilledCircle(float cx, float cy, float r, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::CircleShape shape(r);
		shape.setPosition(cx-r, cy-r);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(1.0);
		shape.setFillColor(sf::Color(color.r, color.g, color.b, color.a));
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawEllipse(float cx, float cy, float rx, float ry, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::CircleShape shape(std::min(rx, ry));
		shape.scale(rx > ry? rx/ry : 1.0f, rx < ry? ry/rx : 1.0f);
		shape.setPosition(cx-rx, cy-ry);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(1.0);
		shape.setFillColor(sf::Color::Transparent);
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawFilledEllipse(float cx, float cy, float rx, float ry, const Color& color)
	{
		FGEAL_CHECK_INIT();
		sf::CircleShape shape(std::min(rx, ry));
		shape.scale(rx > ry? rx/ry : 1.0f, rx < ry? ry/rx : 1.0f);
		shape.setPosition(cx-rx, cy-ry);
		shape.setOutlineColor(sf::Color(color.r, color.g, color.b, color.a));
		shape.setOutlineThickness(1.0);
		shape.setFillColor(sf::Color(color.r, color.g, color.b, color.a));
		fgeal::drawRenderTarget->draw(shape);
	}

	void Graphics::drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const sf::Vertex vertices[4] = {
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x2, y2), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x3, y3), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a))  // repeated the first coordinate because SFML lacks a line strip primitive with loop.
		};
		fgeal::drawRenderTarget->draw(vertices, 4, sf::LineStrip);
	}

	void Graphics::drawFilledTriangle(float x1, float y1, float x2, float y2, float x3, float y3, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const sf::Vertex vertices[3] = {
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x2, y2), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x3, y3), sf::Color(color.r, color.g, color.b, color.a)),
		};
		fgeal::drawRenderTarget->draw(vertices, 3, sf::TriangleFan);
	}

	void Graphics::drawQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const sf::Vertex vertices[5] = {
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x2, y2), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x3, y3), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x4, y4), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a))  // repeated the first coordinate because SFML lacks a line strip primitive with loop.
		};
		fgeal::drawRenderTarget->draw(vertices, 5, sf::LineStrip);
	}

	void Graphics::drawFilledQuadrangle(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const sf::Vertex vertices[4] = {
			sf::Vertex(sf::Vector2f(x1, y1), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x2, y2), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x3, y3), sf::Color(color.r, color.g, color.b, color.a)),
			sf::Vertex(sf::Vector2f(x4, y4), sf::Color(color.r, color.g, color.b, color.a)),
		};
		fgeal::drawRenderTarget->draw(vertices, 4, sf::TriangleFan);
	}

	static std::vector<sf::Vertex> sfmlVertexBuffer;
	void storeSfmlVertex(float x, float y, const Color& c)  // ps: cannot be static because it needs to have external linkage in older compilers
	{
		sfmlVertexBuffer.push_back(sf::Vertex (sf::Vector2f(x, y), sf::Color(c.r, c.g, c.b, c.a)));
	}

	void Graphics::drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		sfmlVertexBuffer.clear();
		GenericGraphics::drawArc<storeSfmlVertex>(cx, cy, r, ai, af, c);
		if(not sfmlVertexBuffer.empty())
			fgeal::drawRenderTarget->draw(&sfmlVertexBuffer[0], sfmlVertexBuffer.size(), sf::Points);
	}

	void Graphics::drawThickArc(float cx, float cy, float r, float t, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawThickArc<drawLine>(cx, cy, r, t, ai, af, c);
	}

	void Graphics::drawCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		drawArc(cx, cy, r, ai, af, c);
		const sf::Vertex vertices[3] = {
			sf::Vertex(sf::Vector2f(cx + r*sin(M_PI_2 + ai), cy + r*cos(M_PI_2 + ai)), sf::Color(c.r, c.g, c.b, c.a)),
			sf::Vertex(sf::Vector2f(cx, cy), sf::Color(c.r, c.g, c.b, c.a)),
			sf::Vertex(sf::Vector2f(cx + r*sin(M_PI_2 + af), cy + r*cos(M_PI_2 + af)), sf::Color(c.r, c.g, c.b, c.a)),
		};
		fgeal::drawRenderTarget->draw(vertices, 3, sf::LineStrip);
	}

	static inline
	unsigned computeArcSegmentCount(float radius, float initialAngle, float finalAngle)
	{
		return M_2_PI * std::sqrt(radius) * (finalAngle-initialAngle);
	}

	void Graphics::drawFilledCircleSector(float cx, float cy, float r, float ai, float af, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const sf::Color sfmlColor(c.r, c.g, c.b, c.a);
		const unsigned arcSegmentCount = computeArcSegmentCount(r, ai, af), arcVertexCount = arcSegmentCount+1;
		const float da = (af-ai)/arcSegmentCount;
		vector<sf::Vertex> vertices(arcVertexCount+1);

		// add center point first
		vertices[0].position.x = cx;
		vertices[0].position.y = cy;
		vertices[0].color = sfmlColor;
		for(unsigned i = 0; i < arcVertexCount; i++)
		{
			vertices[i+1].position.x = cx + r*sin(M_PI_2 + ai + da*i);
			vertices[i+1].position.y = cy + r*cos(M_PI_2 + ai + da*i);
			vertices[i+1].color = sfmlColor;
		}
		fgeal::drawRenderTarget->draw(&vertices[0], arcVertexCount+1, sf::TriangleFan);
	}

	void Graphics::drawRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		GenericGraphics::drawRoundedRectangle(x, y, w, h, rd, c);
	}

	void Graphics::drawFilledRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c)
	{
		FGEAL_CHECK_INIT();
		const sf::Color sfmlColor(c.r, c.g, c.b, c.a);
		const unsigned arcSegmentCount = computeArcSegmentCount(rd, 0, M_PI_2), arcVertexCount = arcSegmentCount+1;
		const float da = M_PI_2/arcSegmentCount;
		vector<sf::Vertex> vertices(4*arcVertexCount);
		for(unsigned i = 0; i < arcVertexCount; i++)
		{
			const float sindai = sin(da*i), cosdai = cos(da*i);

			vertices[i].position.x = x + rd - rd*sindai;
			vertices[i].position.y = y + rd - rd*cosdai;
			vertices[i].color = sfmlColor;

			vertices[i+arcVertexCount].position.x =   x   + rd - rd*cosdai;
			vertices[i+arcVertexCount].position.y = y + h - rd + rd*sindai;
			vertices[i+arcVertexCount].color = sfmlColor;

			vertices[i+arcVertexCount*2].position.x = x + w - rd + rd*sindai;
			vertices[i+arcVertexCount*2].position.y = y + h - rd + rd*cosdai;
			vertices[i+arcVertexCount*2].color = sfmlColor;

			vertices[i+arcVertexCount*3].position.x = x + w - rd + rd*cosdai;
			vertices[i+arcVertexCount*3].position.y =   y   + rd - rd*sindai;
			vertices[i+arcVertexCount*3].color = sfmlColor;
		}
		fgeal::drawRenderTarget->draw(&vertices[0], 4*arcVertexCount, sf::TriangleFan);
	}

	void Graphics::drawPolygon(const vector<float>& x, const vector<float>& y, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<sf::Vertex> vertices(n+1);
		for(unsigned i = 0; i < n; i++)
		{
			vertices[i].position.x = x[i]; vertices[i].position.y = y[i];
			vertices[i].color.r = color.r; vertices[i].color.g = color.g;
			vertices[i].color.b = color.b; vertices[i].color.a = color.a;
		}
		// repeated the first coordinate because SFML lacks a line strip primitive with loop.
		vertices[n].position.x = x[0]; vertices[n].position.y = y[0];
		vertices[n].color.r = color.r; vertices[n].color.g = color.g;
		vertices[n].color.b = color.b; vertices[n].color.a = color.a;

		fgeal::drawRenderTarget->draw(&vertices[0], n+1, sf::LineStrip);
	}

	void Graphics::drawFilledPolygon(const vector<float>& x, const vector<float>& y, const Color& color)
	{
		FGEAL_CHECK_INIT();
		const unsigned n = std::min(x.size(), y.size());
		vector<sf::Vertex> vertices(n);
		for(unsigned i = 0; i < n; i++)
		{
			vertices[i].position.x = x[i]; vertices[i].position.y = y[i];
			vertices[i].color.r = color.r; vertices[i].color.g = color.g;
			vertices[i].color.b = color.b; vertices[i].color.a = color.a;
		}
		fgeal::drawRenderTarget->draw(&vertices[0], n, sf::TriangleFan);
	}
}
