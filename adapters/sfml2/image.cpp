/*
 * image.cpp
 *
 *  Created on: 30/01/2017
*
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/image.hpp"

#include "fgeal/graphics.hpp"
#include "fgeal/exceptions.hpp"

#include <cmath>
#include <algorithm>

using std::string;
using std::vector;

// auxiliary macros
#define textureWidth sfmlSprite.getTexture()->getSize().x
#define textureHeight sfmlSprite.getTexture()->getSize().y

namespace fgeal
{
	static inline Color sfmlColorToFgealColor(sf::Color c) { return Color::create(c.r, c.g, c.b, c.a); }

	Image::Image(const string& filename)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::Texture& texture = *new sf::Texture();
		if(not texture.loadFromFile(filename))
		{
			delete &texture;
			throw AdapterException("Could not load image \"%s\"", filename.c_str());
		}
		texture.setSmooth(useImageTransformSmoothingHint);
		self.sfmlSprite.setTexture(texture);
	}

	Image::Image(int w, int h)
	: self(*new implementation())
	{
		FGEAL_CHECK_INIT();
		sf::RenderTexture tempRenderTexture;  // needed to create a "clear" texture
		if(not tempRenderTexture.create(w, h))
			throw AdapterException("Could not create image with dimensions %dx%d", w, h);

		tempRenderTexture.clear(sf::Color::Transparent);
		tempRenderTexture.display();  // update texture on the render texture
		sf::Texture& texture = *new sf::Texture(tempRenderTexture.getTexture());  // clone the render texture's texture
		texture.setSmooth(useImageTransformSmoothingHint);
		self.sfmlSprite.setTexture(texture);
	}

	Image::~Image()
	{
		FGEAL_CHECK_INIT();
		delete self.sfmlSprite.getTexture();
		delete &self;
	}

	int Image::getWidth()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSprite.getTexture()->getSize().x;
	}

	int Image::getHeight()
	{
		FGEAL_CHECK_INIT();
		return self.sfmlSprite.getTexture()->getSize().y;
	}

	Color Image::getPixel(unsigned x, unsigned y) const
	{
		FGEAL_CHECK_INIT();
		return sfmlColorToFgealColor(self.sfmlSprite.getTexture()->copyToImage().getPixel(x, y));
	}

	void Image::getPixels(vector<vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const
	{
		FGEAL_CHECK_INIT();
		pixelData.resize(width);
		const sf::Image tmpImage = self.sfmlSprite.getTexture()->copyToImage();
		for(unsigned i = 0; i < width and x+i < self.sfmlSprite.getTexture()->getSize().x; i++)
		{
			pixelData[i].resize(height);
			for(unsigned j = 0; j < height and y+j < self.sfmlSprite.getTexture()->getSize().y; j++)
			{
				const sf::Color c = tmpImage.getPixel(x+i, y+j);
				pixelData[i][j].r = c.r; pixelData[i][j].g = c.g; pixelData[i][j].b = c.b; pixelData[i][j].a = c.a;
			}
		}
	}

	void Image::setPixel(unsigned x, unsigned y, const Color& c)
	{
		vector<vector<Color> > vc(1);
		vc[0].push_back(c);
		this->setPixels(x, y, vc);
	}

	void Image::setPixels(unsigned x, unsigned y, const vector<vector<Color> >& colors)
	{
		FGEAL_CHECK_INIT();
		sf::Image tmpImg(self.sfmlSprite.getTexture()->copyToImage());
		for(unsigned i = 0; i < colors.size() and x+i < self.sfmlSprite.getTexture()->getSize().x; i++)
			for(unsigned j = 0; j < colors[i].size() and y+j < self.sfmlSprite.getTexture()->getSize().y; j++)
				tmpImg.setPixel(x+i, y+j, sf::Color(colors[i][j].r, colors[i][j].g, colors[i][j].b, colors[i][j].a));
		delete self.sfmlSprite.getTexture();
		sf::Texture& editedTexture = *new sf::Texture();
		editedTexture.loadFromImage(tmpImg);
		self.sfmlSprite.setTexture(editedTexture);
	}

	void Image::draw(float x, float y)
	{
		FGEAL_CHECK_INIT();
		//draws all source region
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);
	}

	void Image::drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		self.sfmlSprite.setTextureRect(sf::IntRect(fromX, fromY, fromWidth, fromHeight));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		//restore source region to full
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, self.textureWidth, self.textureHeight));
	}

	void Image::drawFlipped(float x, float y, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		//draws all source region
		self.sfmlSprite.setTextureRect(sf::IntRect((flipX? width : 0), (flipY? height : 0), (flipX? -width : width), (flipY? -height : height)));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		//restore source region to unflipped
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		self.sfmlSprite.setTextureRect(sf::IntRect(fromX + (flipX? fromWidth : 0), fromY + (flipY? fromHeight : 0), (flipX? -fromWidth : fromWidth), (flipY? -fromHeight : fromHeight)));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		//restore source region to full, without flip
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		//draws all source region
		self.sfmlSprite.setTextureRect(sf::IntRect((flipX? width : 0), (flipY? height : 0), (flipX? -width : width), (flipY? -height : height)));
		self.sfmlSprite.setScale(xScale, yScale);
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		//neutralize scale, restore source region to unflipped
		self.sfmlSprite.setScale(1.0, 1.0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		self.sfmlSprite.setTextureRect(sf::IntRect(fromX + (flipX? fromWidth : 0), fromY + (flipY? fromHeight : 0), (flipX? -fromWidth : fromWidth), (flipY? -fromHeight : fromHeight)));
		self.sfmlSprite.setScale(xScale, yScale);
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		//neutralize scale, restore source region to full, without flip
		self.sfmlSprite.setScale(1.0, 1.0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		//draws all source region
		self.sfmlSprite.setTextureRect(sf::IntRect((flipX? width : 0), (flipY? height : 0), (flipX? -width : width), (flipY? -height : height)));
		self.sfmlSprite.setOrigin(centerX, centerY);
		self.sfmlSprite.setRotation(rad2deg(-angle));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		// restore center to top-left and clear rotation, restore source region to unflipped
		self.sfmlSprite.setOrigin(0, 0);
		self.sfmlSprite.setRotation(0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		self.sfmlSprite.setTextureRect(sf::IntRect(fromX + (flipX? fromWidth : 0), fromY + (flipY? fromHeight : 0), (flipX? -fromWidth : fromWidth), (flipY? -fromHeight : fromHeight)));
		self.sfmlSprite.setOrigin(centerX, centerY);
		self.sfmlSprite.setRotation(rad2deg(-angle));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		// restore center to top-left, clear rotation and restore source region to full
		self.sfmlSprite.setOrigin(0, 0);
		self.sfmlSprite.setRotation(0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		//draws all source region
		self.sfmlSprite.setTextureRect(sf::IntRect((flipX? width : 0), (flipY? height : 0), (flipX? -width : width), (flipY? -height : height)));
		self.sfmlSprite.setScale(xScale, yScale);
		self.sfmlSprite.setOrigin(centerX, centerY);
		self.sfmlSprite.setRotation(rad2deg(-angle));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.setOrigin(0, 0);
		self.sfmlSprite.setRotation(0);
		self.sfmlSprite.setScale(1.0, 1.0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		const bool flipX = (flipmode == FLIP_HORIZONTAL), flipY = (flipmode == FLIP_VERTICAL);
		const int width = self.textureWidth, height = self.textureHeight;

		if(fromWidth == 0) fromWidth = this->getWidth();
		if(fromHeight == 0) fromHeight = this->getHeight();

		self.sfmlSprite.setTextureRect(sf::IntRect(fromX + (flipX? fromWidth : 0), fromY + (flipY? fromHeight : 0), (flipX? -fromWidth : fromWidth), (flipY? -fromHeight : fromHeight)));
		self.sfmlSprite.setScale(xScale, yScale);
		self.sfmlSprite.setOrigin(centerX, centerY);
		self.sfmlSprite.setRotation(rad2deg(-angle));
		self.sfmlSprite.setPosition(x, y);
		drawRenderTarget->draw(self.sfmlSprite);

		// restore center to top-left and clear rotation
		self.sfmlSprite.setOrigin(0, 0);
		self.sfmlSprite.setRotation(0);
		self.sfmlSprite.setScale(1.0, 1.0);
		self.sfmlSprite.setTextureRect(sf::IntRect(0, 0, width, height));
	}

	void Image::blit(Image& img, float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		FGEAL_CHECK_INIT();
		Graphics::setDrawTarget(&img);
		this->drawScaledRotatedRegion(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
		Graphics::setDefaultDrawTarget();
	}
}
