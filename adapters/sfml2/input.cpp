/*
 * input.cpp
 *
 *  Created on: 30/01/2017
*
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "implementation.hpp"
#include "fgeal/core.hpp"
#include "fgeal/input.hpp"

#define FORWARD_MAPPING(a, b)  case a: return b;
#define BACKWARD_MAPPING(a, b) case b: return a;
#define window Display::instance->self.sfmlRenderWindow

namespace fgeal
{
	using std::string;
	using std::vector;

	// =======================================================================================
	// Keyboard

	namespace KeyboardKeyMapping
	{
		// Returns the fgeal-equivalent key enum of the given SFML 2.x keycode.
		Keyboard::Key toGenericKey(sf::Keyboard::Key sfmlKeycode)
		{
			switch(sfmlKeycode)
			{
				#define KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				default: return Keyboard::KEY_UNKNOWN;
			}
		}

		// Returns the SFML 2.x keycode of the given fgeal-equivalent key enum.
		sf::Keyboard::Key toUnderlyingLibraryKey(Keyboard::Key key)
		{
			switch(key)
			{
				#define BACKWARDS_KEYBOARD_MAPPINGS
				#include "input_mappings.hxx"
				case Keyboard::KEY_NUMPAD_DECIMAL: return sf::Keyboard::Unknown;  // XXX SFML does not recognize the decimal point numpad key
				case Keyboard::KEY_NUMPAD_ENTER:   return sf::Keyboard::Return;  // XXX SFML does not makes distinction between Enter and the numpad Enter
				default: return sf::Keyboard::KeyCount;
			}
		}
	}

	// API functions

	bool Keyboard::isKeyPressed(Keyboard::Key key)
	{
		FGEAL_CHECK_INIT();
		return sf::Keyboard::isKeyPressed(KeyboardKeyMapping::toUnderlyingLibraryKey(key));
	}

	// =======================================================================================
	// Mouse

	namespace MouseButtonMapping
	{
		// Returns the fgeal-equivalent mouse button enum of the given SFML 2.x mouse button number.
		Mouse::Button toGenericMouseButton(sf::Mouse::Button sfmlMouseButton)
		{
			switch(sfmlMouseButton)
			{
				#define MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return Mouse::BUTTON_UNKNOWN;
			}
		}

		// Returns the SFML 2.x mouse button number of the fgeal-equivalent mouse button enum.
		sf::Mouse::Button toUnderlyingLibraryMouseButton(Mouse::Button button)
		{
			switch(button)
			{
				#define BACKWARDS_MOUSE_MAPPINGS
				#include "input_mappings.hxx"
				default: return sf::Mouse::ButtonCount;
			}
		}
	}

	bool Mouse::isButtonPressed(Mouse::Button btn)
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::isButtonPressed(MouseButtonMapping::toUnderlyingLibraryMouseButton(btn));
	}

	Point Mouse::getPosition()
	{
		FGEAL_CHECK_INIT();
		Point pt = {(float) sf::Mouse::getPosition(window).x, (float) sf::Mouse::getPosition(window).y};
		return pt;
	}

	void Mouse::setPosition(const Point& position)
	{
		FGEAL_CHECK_INIT();
		Mouse::setPosition(position.x, position.y);
	}

	int Mouse::getPositionX()
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::getPosition(window).x;
	}

	int Mouse::getPositionY()
	{
		FGEAL_CHECK_INIT();
		return sf::Mouse::getPosition(window).y;
	}

	void Mouse::setPosition(int x, int y)
	{
		FGEAL_CHECK_INIT();
		sf::Mouse::setPosition(sf::Vector2i(x, y));
	}

	// =======================================================================================
	// Joystick

	vector< vector<sf::Joystick::Axis> > JoystickManagement::existingAxes;

	void Joystick::configureAll()
	{
		sf::Joystick::update();
		for(unsigned j = 0; j < sf::Joystick::Count; j++)
		if(sf::Joystick::isConnected(j))
		{
			// increments the amount of joysticks implicitly
			JoystickManagement::existingAxes.push_back(vector<sf::Joystick::Axis>());

			// for each "possible" axis
			for(unsigned a = 0; a < sf::Joystick::AxisCount; a++)
				if(sf::Joystick::hasAxis(j, static_cast<sf::Joystick::Axis>(a)))
					JoystickManagement::existingAxes[j].push_back(static_cast<sf::Joystick::Axis>(a));
		}
		else return;  // XXX is it the correct behavior? can SFML behave in a way that there are "holes" in the joysticks list? ex: 0,1,_,_,4,_,6,7,8
	}

	void Joystick::releaseAll()
	{
		JoystickManagement::existingAxes.clear();
	}

	unsigned Joystick::getCount()
	{
		FGEAL_CHECK_INIT();
		return JoystickManagement::existingAxes.size();  // implicitly has the number of joysticks
	}

	//XXX SFML 2.x adapter: when version is less than 2.2, sf::Joystick::getIdentification is not available
	string Joystick::getJoystickName(unsigned joystickIndex)
	{
		// sf::Joystick::getIdentification() is only available with SFML 2.2 or higher
		#if SFML_VERSION_MAJOR > 2 or (SFML_VERSION_MAJOR == 2 and SFML_VERSION_MINOR >= 2)
			return sf::Joystick::getIdentification(joystickIndex).name;
		#else
			core::reportAbsentImplementation("Getting joystick name is not supported on this adapter when using SFML versions older than 2.2.");
			return string();
		#endif
	}

	unsigned Joystick::getButtonCount(unsigned joystickIndex)
	{
		return sf::Joystick::getButtonCount(joystickIndex);
	}

	unsigned Joystick::getAxisCount(unsigned joystickIndex)
	{
		return JoystickManagement::existingAxes[joystickIndex].size();  // implicitly has the number of axis
	}

	bool Joystick::isButtonPressed(unsigned joystickIndex, unsigned buttonIndex)
	{
		return sf::Joystick::isButtonPressed(joystickIndex, buttonIndex);
	}

	float Joystick::getAxisPosition(unsigned joystickIndex, unsigned axisIndex)
	{
		return sf::Joystick::getAxisPosition(joystickIndex, JoystickManagement::existingAxes[joystickIndex][axisIndex]) * 0.01f;
	}
}
