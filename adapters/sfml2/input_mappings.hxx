/*
 * input_mappings.hxx
 *
 *  Created on: 11 de abr de 2022
 *      Author: hydren
 */

// dummy case for Eclipse CDT parser, so it won't flood warnings when viewing this file in the editor
#ifdef __CDT_PARSER__
	#define KEYBOARD_MAPPINGS
	#define MOUSE_MAPPINGS
	#define mapping(a, b)  a, b;
	#include "fgeal/input.hpp"
	#include <SFML/Window/Keyboard.hpp>
	#include <SFML/Window/Mouse.hpp>
	namespace fgeal { void dummy() {
#else
	#ifdef BACKWARDS_KEYBOARD_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define KEYBOARD_MAPPINGS
		#undef BACKWARDS_KEYBOARD_MAPPINGS
	#endif
	#ifdef BACKWARDS_MOUSE_MAPPINGS
		#define BACKWARDS_MAPPINGS
		#define MOUSE_MAPPINGS
		#undef BACKWARDS_MOUSE_MAPPINGS
	#endif

	#undef mapping
	#ifdef BACKWARDS_MAPPINGS
		#define mapping(a, b) case b: return a;
	#else
		#define mapping(a, b) case a: return b;
	#endif
	#undef BACKWARDS_MAPPINGS
#endif

#ifdef KEYBOARD_MAPPINGS
	mapping(sf::Keyboard::A, Keyboard::KEY_A)
	mapping(sf::Keyboard::B, Keyboard::KEY_B)
	mapping(sf::Keyboard::C, Keyboard::KEY_C)
	mapping(sf::Keyboard::D, Keyboard::KEY_D)
	mapping(sf::Keyboard::E, Keyboard::KEY_E)
	mapping(sf::Keyboard::F, Keyboard::KEY_F)
	mapping(sf::Keyboard::G, Keyboard::KEY_G)
	mapping(sf::Keyboard::H, Keyboard::KEY_H)
	mapping(sf::Keyboard::I, Keyboard::KEY_I)
	mapping(sf::Keyboard::J, Keyboard::KEY_J)
	mapping(sf::Keyboard::K, Keyboard::KEY_K)
	mapping(sf::Keyboard::L, Keyboard::KEY_L)
	mapping(sf::Keyboard::M, Keyboard::KEY_M)
	mapping(sf::Keyboard::N, Keyboard::KEY_N)
	mapping(sf::Keyboard::O, Keyboard::KEY_O)
	mapping(sf::Keyboard::P, Keyboard::KEY_P)
	mapping(sf::Keyboard::Q, Keyboard::KEY_Q)
	mapping(sf::Keyboard::R, Keyboard::KEY_R)
	mapping(sf::Keyboard::S, Keyboard::KEY_S)
	mapping(sf::Keyboard::T, Keyboard::KEY_T)
	mapping(sf::Keyboard::U, Keyboard::KEY_U)
	mapping(sf::Keyboard::V, Keyboard::KEY_V)
	mapping(sf::Keyboard::W, Keyboard::KEY_W)
	mapping(sf::Keyboard::X, Keyboard::KEY_X)
	mapping(sf::Keyboard::Y, Keyboard::KEY_Y)
	mapping(sf::Keyboard::Z, Keyboard::KEY_Z)

	mapping(sf::Keyboard::Num0, Keyboard::KEY_0)
	mapping(sf::Keyboard::Num1, Keyboard::KEY_1)
	mapping(sf::Keyboard::Num2, Keyboard::KEY_2)
	mapping(sf::Keyboard::Num3, Keyboard::KEY_3)
	mapping(sf::Keyboard::Num4, Keyboard::KEY_4)
	mapping(sf::Keyboard::Num5, Keyboard::KEY_5)
	mapping(sf::Keyboard::Num6, Keyboard::KEY_6)
	mapping(sf::Keyboard::Num7, Keyboard::KEY_7)
	mapping(sf::Keyboard::Num8, Keyboard::KEY_8)
	mapping(sf::Keyboard::Num9, Keyboard::KEY_9)

	mapping(sf::Keyboard::F1, Keyboard::KEY_F1)
	mapping(sf::Keyboard::F2, Keyboard::KEY_F2)
	mapping(sf::Keyboard::F3, Keyboard::KEY_F3)
	mapping(sf::Keyboard::F4, Keyboard::KEY_F4)
	mapping(sf::Keyboard::F5, Keyboard::KEY_F5)
	mapping(sf::Keyboard::F6, Keyboard::KEY_F6)
	mapping(sf::Keyboard::F7, Keyboard::KEY_F7)
	mapping(sf::Keyboard::F8, Keyboard::KEY_F8)
	mapping(sf::Keyboard::F9, Keyboard::KEY_F9)
	mapping(sf::Keyboard::F10, Keyboard::KEY_F10)
	mapping(sf::Keyboard::F11, Keyboard::KEY_F11)
	mapping(sf::Keyboard::F12, Keyboard::KEY_F12)

	mapping(sf::Keyboard::Up,    Keyboard::KEY_ARROW_UP)
	mapping(sf::Keyboard::Down,  Keyboard::KEY_ARROW_DOWN)
	mapping(sf::Keyboard::Left,  Keyboard::KEY_ARROW_LEFT)
	mapping(sf::Keyboard::Right, Keyboard::KEY_ARROW_RIGHT)

	mapping(sf::Keyboard::Return,    Keyboard::KEY_ENTER)
	mapping(sf::Keyboard::Space,     Keyboard::KEY_SPACE)
	mapping(sf::Keyboard::Escape,    Keyboard::KEY_ESCAPE)
	mapping(sf::Keyboard::LControl,  Keyboard::KEY_LEFT_CONTROL)
	mapping(sf::Keyboard::RControl,  Keyboard::KEY_RIGHT_CONTROL)
	mapping(sf::Keyboard::LShift,    Keyboard::KEY_LEFT_SHIFT)
	mapping(sf::Keyboard::RShift,    Keyboard::KEY_RIGHT_SHIFT)
	mapping(sf::Keyboard::LAlt,      Keyboard::KEY_LEFT_ALT)
	mapping(sf::Keyboard::RAlt,      Keyboard::KEY_RIGHT_ALT)
	mapping(sf::Keyboard::LSystem,   Keyboard::KEY_LEFT_SUPER)
	mapping(sf::Keyboard::RSystem,   Keyboard::KEY_RIGHT_SUPER)
	mapping(sf::Keyboard::Menu,      Keyboard::KEY_MENU)
	mapping(sf::Keyboard::Tab,       Keyboard::KEY_TAB)
	mapping(sf::Keyboard::BackSpace, Keyboard::KEY_BACKSPACE)

	mapping(sf::Keyboard::Dash,      Keyboard::KEY_MINUS)
	mapping(sf::Keyboard::Equal,     Keyboard::KEY_EQUALS)
	mapping(sf::Keyboard::LBracket,  Keyboard::KEY_LEFT_BRACKET)
	mapping(sf::Keyboard::RBracket,  Keyboard::KEY_RIGHT_BRACKET)
	mapping(sf::Keyboard::SemiColon, Keyboard::KEY_SEMICOLON)
	mapping(sf::Keyboard::Comma,     Keyboard::KEY_COMMA)
	mapping(sf::Keyboard::Period,    Keyboard::KEY_PERIOD)
	mapping(sf::Keyboard::Slash,     Keyboard::KEY_SLASH)
	mapping(sf::Keyboard::BackSlash, Keyboard::KEY_BACKSLASH)
	mapping(sf::Keyboard::Quote,     Keyboard::KEY_QUOTE)
	mapping(sf::Keyboard::Tilde,     Keyboard::KEY_TILDE)

	mapping(sf::Keyboard::Insert,   Keyboard::KEY_INSERT)
	mapping(sf::Keyboard::Delete,   Keyboard::KEY_DELETE)
	mapping(sf::Keyboard::Home,     Keyboard::KEY_HOME)
	mapping(sf::Keyboard::End,      Keyboard::KEY_END)
	mapping(sf::Keyboard::PageUp,   Keyboard::KEY_PAGE_UP)
	mapping(sf::Keyboard::PageDown, Keyboard::KEY_PAGE_DOWN)

	mapping(sf::Keyboard::Numpad0, Keyboard::KEY_NUMPAD_0)
	mapping(sf::Keyboard::Numpad1, Keyboard::KEY_NUMPAD_1)
	mapping(sf::Keyboard::Numpad2, Keyboard::KEY_NUMPAD_2)
	mapping(sf::Keyboard::Numpad3, Keyboard::KEY_NUMPAD_3)
	mapping(sf::Keyboard::Numpad4, Keyboard::KEY_NUMPAD_4)
	mapping(sf::Keyboard::Numpad5, Keyboard::KEY_NUMPAD_5)
	mapping(sf::Keyboard::Numpad6, Keyboard::KEY_NUMPAD_6)
	mapping(sf::Keyboard::Numpad7, Keyboard::KEY_NUMPAD_7)
	mapping(sf::Keyboard::Numpad8, Keyboard::KEY_NUMPAD_8)
	mapping(sf::Keyboard::Numpad9, Keyboard::KEY_NUMPAD_9)

	mapping(sf::Keyboard::Add,      Keyboard::KEY_NUMPAD_ADDITION)
	mapping(sf::Keyboard::Subtract, Keyboard::KEY_NUMPAD_SUBTRACTION)
	mapping(sf::Keyboard::Multiply, Keyboard::KEY_NUMPAD_MULTIPLICATION)
	mapping(sf::Keyboard::Divide,   Keyboard::KEY_NUMPAD_DIVISION)
#endif
#undef KEYBOARD_MAPPINGS

#ifdef MOUSE_MAPPINGS
	mapping(sf::Mouse::Left,   Mouse::BUTTON_LEFT)
	mapping(sf::Mouse::Middle, Mouse::BUTTON_MIDDLE)
	mapping(sf::Mouse::Right,  Mouse::BUTTON_RIGHT)
#endif
#undef MOUSE_MAPPINGS

#ifdef __CDT_PARSER__
}}
#endif
