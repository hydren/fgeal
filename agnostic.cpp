/*
 * agnostic.cpp (originally core_agnostic.cpp)
 *
 *  Created on: 24/08/2014
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2014  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "version.h"

#include "core.hpp"
#include "exceptions.hpp"
#include "geometry.hpp"
#include "colors.hpp"

#include "display.hpp"
#include "graphics.hpp"
#include "image.hpp"
#include "font.hpp"
#include "event.hpp"
#include "sound.hpp"

#include <iostream>
#include <algorithm>

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cerrno>

#if __cplusplus >= 201103L and (not defined(_MSC_VER) or _MSC_VER >= 1800)
	using std::round;
#elif __cplusplus < 201103L and defined(__GNUG__)
	using ::round;
#else
	static double round(double number) { return number < 0.0 ? ceil(number - 0.5) : floor(number + 0.5); }
#endif

// Turn 'macro' into a string literal without expanding macro definitions (however, if invoked from a macro, macro arguments are expanded).
#define STRINGIZE(macro) #macro

// Turn 'macro' into a string literal after macro-expanding it.
#define STRINGIFY(macro) STRINGIZE(macro)

#ifdef FGEAL_DEV_VERSION
	#define FGEAL_VERSION_STR STRINGIFY(FGEAL_MAJOR_VERSION) "." STRINGIFY(FGEAL_MINOR_VERSION) "." STRINGIFY(FGEAL_REVIS_VERSION) "-" STRINGIFY(FGEAL_DEV_VERSION)
#else
	#define FGEAL_VERSION_STR STRINGIFY(FGEAL_MAJOR_VERSION) "." STRINGIFY(FGEAL_MINOR_VERSION) "." STRINGIFY(FGEAL_REVIS_VERSION)
#endif

using std::string;
using std::vector;
using std::pair;

namespace fgeal
{
	//******************* FILE-SCOPE MISC.
	static float random_decimal_between(float min, float max)
	{
		if (max == min)
			return min;

		if(max < min) //swap
		{
			float t = max;
			max = min;
			min = t;
		}

	    float f = (static_cast<float>(rand())) / RAND_MAX;
	    return min + f * (max - min);
	}

	//******************* STATICS AND GLOBAL VARIABLES/CONSTANTS

	// as defined in core.hpp
	const char *VERSION = FGEAL_VERSION_STR;
	bool fgeal::core::isInitialized = false;
	core::AbsentImplementationPolicy core::absentImplementationPolicy = core::THROW_EXCEPTION;

	// as defined in vector2d.hpp
	const Vector2D Vector2D::NULL_VECTOR = {0.0f, 0.0f};
	const Vector2D Vector2D::X_VERSOR = {1.0f, 0.0f};
	const Vector2D Vector2D::Y_VERSOR = {0.0f, 1.0f};

	// as defined in image.hpp
	const Rectangle Image::WHOLE_REGION = {0.0f, 0.0f, 0.0f, 0.0f};
	const Vector2D Image::NATURAL_SCALE = {1.0f, 1.0f};
	const Point Image::IMAGE_CENTER = {0.0f, 0.0f};

	// as defined in graphics.hpp
	bool Graphics::useFasterPrimitivesHint = false, Image::useImageTransformSmoothingHint = false;

	//******************* CORE FUNCTIONS

	void initialize()
	{
		core::initialize();
		core::isInitialized = true;
		Joystick::configureAll();
	}

	void finalize()
	{
		if(Display::instance != null)
		{
			delete Display::instance;
			Display::instance = null;
		}
		if(EventQueue::instance != null)
		{
			delete EventQueue::instance;
			EventQueue::instance = null;
		}

		Joystick::releaseAll();
		core::finalize();
		core::isInitialized = false;
	}

	bool isInitialized()
	{
		return core::isInitialized;
	}

	void core::checkInit()
	{
		if(not fgeal::isInitialized())
			throw AdapterException("Fatal error: attempt to use fgeal library without initialization!");
	}

	void core::reportAbsentImplementation(const std::string& description)
	{
		switch(core::absentImplementationPolicy)
		{
			case IGNORE_SILENTLY:
				break;

			case IGNORE_BUT_PRINT_STDERR:
				std::cerr << "Error:" << description << std::endl;
				break;

			case THROW_EXCEPTION:
				throw NotImplementedException(description);
				break;

			default:
			case HALT_PROGRAM:
				std::cerr << "Error:" << description << std::endl;
				exit(EXIT_FAILURE);
				break;
		}
	}

	//******************* EXCEPTIONS

	#define fgeal_exception_variadic_constructor(fmt) \
		va_list args; \
		va_start(args, fmt); \
		char buffer[1024]; \
		vsprintf(buffer, fmt, args); \
		message = std::string(buffer); \
		va_end(args)

	Exception::Exception(const char* format, ...)
	: message()
	{
		fgeal_exception_variadic_constructor(format);
	}

	AdapterException::AdapterException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	NotImplementedException::NotImplementedException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	GameException::GameException(const char* format, ...)
	{
		fgeal_exception_variadic_constructor(format);
	}

	//******************* KEYBOARD

	char Keyboard::parseKeyStroke(Keyboard::Key key)
	{
		char typed;
		switch(key)
		{
			default: typed = '\n'; break;
			case Keyboard::KEY_A: typed = 'a'; break;
			case Keyboard::KEY_B: typed = 'b'; break;
			case Keyboard::KEY_C: typed = 'c'; break;
			case Keyboard::KEY_D: typed = 'd'; break;
			case Keyboard::KEY_E: typed = 'e'; break;
			case Keyboard::KEY_F: typed = 'f'; break;
			case Keyboard::KEY_G: typed = 'g'; break;
			case Keyboard::KEY_H: typed = 'h'; break;
			case Keyboard::KEY_I: typed = 'i'; break;
			case Keyboard::KEY_J: typed = 'j'; break;
			case Keyboard::KEY_K: typed = 'k'; break;
			case Keyboard::KEY_L: typed = 'l'; break;
			case Keyboard::KEY_M: typed = 'm'; break;
			case Keyboard::KEY_N: typed = 'n'; break;
			case Keyboard::KEY_O: typed = 'o'; break;
			case Keyboard::KEY_P: typed = 'p'; break;
			case Keyboard::KEY_Q: typed = 'q'; break;
			case Keyboard::KEY_R: typed = 'r'; break;
			case Keyboard::KEY_S: typed = 's'; break;
			case Keyboard::KEY_T: typed = 't'; break;
			case Keyboard::KEY_U: typed = 'u'; break;
			case Keyboard::KEY_V: typed = 'v'; break;
			case Keyboard::KEY_W: typed = 'w'; break;
			case Keyboard::KEY_X: typed = 'x'; break;
			case Keyboard::KEY_Y: typed = 'y'; break;
			case Keyboard::KEY_Z: typed = 'z'; break;
			case Keyboard::KEY_NUMPAD_0:
			case Keyboard::KEY_0:  typed = '0'; break;
			case Keyboard::KEY_NUMPAD_1:
			case Keyboard::KEY_1:  typed = '1'; break;
			case Keyboard::KEY_NUMPAD_2:
			case Keyboard::KEY_2:  typed = '2'; break;
			case Keyboard::KEY_NUMPAD_3:
			case Keyboard::KEY_3:  typed = '3'; break;
			case Keyboard::KEY_NUMPAD_4:
			case Keyboard::KEY_4:  typed = '4'; break;
			case Keyboard::KEY_NUMPAD_5:
			case Keyboard::KEY_5:  typed = '5'; break;
			case Keyboard::KEY_NUMPAD_6:
			case Keyboard::KEY_6:  typed = '6'; break;
			case Keyboard::KEY_NUMPAD_7:
			case Keyboard::KEY_7:  typed = '7'; break;
			case Keyboard::KEY_NUMPAD_8:
			case Keyboard::KEY_8:  typed = '8'; break;
			case Keyboard::KEY_NUMPAD_9:
			case Keyboard::KEY_9:  typed = '9'; break;
			case Keyboard::KEY_SPACE:         typed = ' '; break;
			case Keyboard::KEY_PERIOD:        typed = '.'; break;
			case Keyboard::KEY_COMMA:         typed = ','; break;
			case Keyboard::KEY_SEMICOLON:     typed = ';'; break;
			case Keyboard::KEY_MINUS:         typed = '-'; break;
			case Keyboard::KEY_EQUALS:        typed = '='; break;
			case Keyboard::KEY_SLASH:         typed = '/'; break;
			case Keyboard::KEY_BACKSLASH:     typed = '\\'; break;
			case Keyboard::KEY_QUOTE:         typed = '\''; break;
			case Keyboard::KEY_LEFT_BRACKET:  typed = '['; break;
			case Keyboard::KEY_RIGHT_BRACKET: typed = ']'; break;
		}

		if(typed >= 'a' and typed <= 'z')
		if(Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT)
		or Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_SHIFT))
			typed -= 32;

		if(typed == '-')
		if(Keyboard::isKeyPressed(Keyboard::KEY_LEFT_SHIFT)
		or Keyboard::isKeyPressed(Keyboard::KEY_RIGHT_SHIFT))
			typed = '_';

		return typed;
	}

	//******************* VECTOR2D

	Vector2D Vector2D::random(float magnitude)
	{
		const float angle = random_decimal_between(0.0, 2.0*M_PI);
		Vector2D v = {magnitude*static_cast<float>(cos(angle)), magnitude*static_cast<float>(sin(angle))};
		return v;
	}

	/** Returns true if this vector coordinates equals the ones from the given vector */
	bool Vector2D::equals(const Vector2D& v) const
	{
		return v.x == x and v.y == y;
	}

	/** Returns true if this vector coordinates differs from the ones of the given vector */
	bool Vector2D::differs(const Vector2D& v) const
	{
		return v.x != x or v.y != y;
	}

	// ------- utils

	/** Creates a string with this vector coordinates (x, y) */
	string Vector2D::toString() const
	{
		static char buffer[32];
		string str("(");
		sprintf(buffer, "%f", x);
		str.append(buffer).append(", ");
		sprintf(buffer, "%f", y);
		str.append(buffer).append(")");
		return str;
	}

	/** Creates and returns an array with this Vectors coordinates, in correct order.  */
	float* Vector2D::getCoordinates() const
	{
		float* coord = new float[2];
		coord[0] = x;
		coord[1] = y;
		return coord;
	}

	// ------- magnitude/length

	/** Returns the length/magnitude of this vector. */
	float Vector2D::magnitude() const
	{
		return sqrt( x*x + y*y );
	}

	// ------- normalization

	/** Creates a vector with length 1 and same direction as this vector. In other words, a new vector that is a normalized version of this vector. Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::unit() const
	{
		const Vector2D v = {isZero()? 0 : x/magnitude(), isZero()? 0 : y/magnitude()};
		return v;
	}

	/** Divides this vector's coordinates by its length/magnitude, normalizing it.
	The returned object is the vector instance itself after normalization. */
	Vector2D& Vector2D::normalize()
	{
		if(not isZero())
		{
			static float length = magnitude();
			if(length > 0)
			{
				x /= length;
				y /= length;
			}
		}
		return *this;
	}

	// ------- reflection

	/** Creates and returns the opposite of this vector. In other words, returns a vector with same coordinates as this, but with changed signal. Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::opposite() const
	{
		const Vector2D v = {-x, -y};
		return v;
	}

	/** Changes the signal of this vector coordinates, effectively reflecting it.
	<br> The returned object is <b>the vector instance itself</b> after reflection. */
	Vector2D& Vector2D::reflect()
	{
		x = -x; y = -y;
		return *this;
	}

	Vector2D& Vector2D::reflectX()
	{
		x = -x;
		return *this;
	}

	Vector2D& Vector2D::reflectY()
	{
		y = -y;
		return *this;
	}

	// ------- basic arithmetic

	/** Creates and returns a vector that represents the sum of this vector and the given vector. Note that <b>the original vector remains unchanged</b>.*/
	Vector2D Vector2D::sum(const Vector2D& v) const
	{
		const Vector2D result = {x + v.x, y + v.y};
		return result;
	}

	/** Adds to this vector the given vector. In other words, it performs an addition to this vector coordinates.
	<br> The returned object is <b>the vector instance itself</b> after summation. */
	Vector2D& Vector2D::add(const Vector2D& v)
	{
		x += v.x;
		y += v.y;
		return *this;
	}

	/** Creates a vector that represents the difference/displacement of this vector and the given vector, in this order. It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
	<br> Note that <b>the original vector remains unchanged</b>. */
	Vector2D Vector2D::difference(const Vector2D& v) const
	{
		const Vector2D result = {x - v.x, y - v.y};
		return result;
	}

	/** Subtracts from this vector the given vector. In other words, it performs an subtraction to this vector coordinates.
	It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
	<br> The returned object is the <b>the vector instance itself</b> after subtraction. */
	Vector2D& Vector2D::subtract(const Vector2D& v)
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	/** Creates a vector that represents the scalar multiplication of this vector by the given factor. Note that <b>the original vector remains unchanged</b>.*/
	Vector2D Vector2D::times(const float factor) const
	{
		const Vector2D result = {x * factor, y * factor};
		return result;
	}

	/** Multiply this vectors coordinates by the given factor. The returned object is <b>the vector instance itself</b> after multiplication.*/
	Vector2D& Vector2D::scale(float factor)
	{
		x *= factor;
		y *= factor;
		return *this;
	}

	// ------- miscellaneous operations

	/** Compute the distance between this vector and the given vector. In other words, returns the length/magnitude of the displacement between this and the given vector. */
	float Vector2D::distance(const Vector2D& v) const
	{
		return ((*this) - v).magnitude();
	}

	/** Compute the inner product (aka dot product or scalar product) between this and the given vector. */
	float Vector2D::innerProduct(const Vector2D& v) const
	{
		return x*v.x + y*v.y;
	}

	/** Compute the entrywise product (aka Hadamard product or Schur product) between this and the given vector */
	Vector2D Vector2D::entrywiseProduct(const Vector2D& v) const
	{
		const Vector2D result = { x*v.x, y*v.y };
		return result;
	}

	// ------- projection operations (XXX not tested)

	/* Creates a vector that represents the projection of this vector on the given vector v. */
	Vector2D Vector2D::projection(const Vector2D& v) const  // FIXME projection operator is incorrect, not passing tests
	{
		return (*this) * (((*this)^v)/(v^v));
	}

	/* Creates a vector that represents the rejection of this vector on the given vector v. The rejection is defined as rej(u, v) = u - proj(u, v) */
	Vector2D Vector2D::rejection(const Vector2D& v) const
	{
		return (*this) - ((*this) || v);
	}

	/* Creates a vector that represents the reflection of this vector in the axis represented by the given vector v. */
	Vector2D Vector2D::reflection(const Vector2D& v) const
	{
		return (*this) - (this->rejection(v)*2);
	}

	// ------- rotation operations (XXX not tested)

	Vector2D Vector2D::rotation(const float& radians) const
	{
		const Vector2D result = { x*static_cast<float>(cos(radians)) - y*static_cast<float>(sin(radians)) , x*static_cast<float>(sin(radians)) + y*static_cast<float>(cos(radians)) };
		return result;
	}

	Vector2D& Vector2D::rotate(const float& radians)
	{
		const float nx = x*cos(radians) - y*sin(radians),
					ny = x*sin(radians) + y*cos(radians);
		x = nx;
		y = ny;
		return *this;
	}

	//******************* SINGLETONS

	Display* Display::instance = null;
	Display& Display::getInstance()
	{
		return *Display::instance;
	}

	Display& Display::create(unsigned width, unsigned height, bool isFullscreen, const string& title, const string& iconFilename)
	{
		Options options;
		options.width = width;
		options.height = height;
		options.fullscreen = isFullscreen;
		options.title = title;
		options.iconFilename = iconFilename;
		return Display::create(options);
	}

	Display& Display::create(const Display::Options& options)
	{
		// this check should NOT be ommited for performance reasons, as this funtion is usually called once.
		if(Display::instance != null)
			throw AdapterException("Attempt to create a Display object twice (currently not supported).");

		Display::instance = new Display(options);

		return *Display::instance;
	}

	bool Display::wasCreated()
	{
		return (Display::instance != null);
	}

	EventQueue* EventQueue::instance = null;
	EventQueue& EventQueue::getInstance()
	{
		if(EventQueue::instance == null)
			EventQueue::instance = new EventQueue();

		return *EventQueue::instance;
	}

	//******************* DISPLAY

	#define makeAspectRatio(x, y) std::make_pair(x, y)

	Display::Mode::Mode(unsigned width, unsigned height)
	: width(width), height(height), aspectRatio(), description()
	{}

	Display::Mode::Mode(unsigned width, unsigned height, std::pair<unsigned, unsigned> aspect, std::string description)
	: width(width), height(height), aspectRatio(aspect), description(description)
	{}

	vector<Display::Mode> Display::Mode::getGenericList(pair<unsigned, unsigned> requestedAspect)
	{
		const pair<unsigned, unsigned> undecidedAspect = std::make_pair(0, 0);
		pair<unsigned, unsigned> aspect;

		vector<Mode> resolutions;

		aspect = makeAspectRatio(4, 3);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 320,  240, aspect, "QVGA"));
			resolutions.push_back(Mode( 384,  288, aspect, "CIF / SIF(625)"));
			resolutions.push_back(Mode( 400,  300, aspect, "qSVGA"));
			resolutions.push_back(Mode( 480,  360, aspect, "HVGA"));
			resolutions.push_back(Mode( 512,  384, aspect, "0.2M3"));
			resolutions.push_back(Mode( 640,  480, aspect, "VGA / SD"));
			resolutions.push_back(Mode( 768,  576, aspect, "4CIF / 4SIF(625)"));
			resolutions.push_back(Mode( 800,  600, aspect, "SVGA / UVGA"));
			resolutions.push_back(Mode( 832,  624, aspect, "SVGA, 0.52M3"));
			resolutions.push_back(Mode( 960,  720, aspect, "720p DVCPRO HD"));
			resolutions.push_back(Mode(1024,  768, aspect, "XGA"));
			resolutions.push_back(Mode(1152,  864, aspect, "XGA+"));
			resolutions.push_back(Mode(1200,  900, aspect, ""));
			resolutions.push_back(Mode(1280,  960, aspect, "SXGA- / UVGA"));
			resolutions.push_back(Mode(1360, 1020, aspect, ""));
			resolutions.push_back(Mode(1400, 1050, aspect, "SXGA+"));
			resolutions.push_back(Mode(1440, 1080, aspect, "HDV 1080i"));
			resolutions.push_back(Mode(1536, 1152, aspect, "16CIF"));
			resolutions.push_back(Mode(1600, 1200, aspect, "UXGA"));
			resolutions.push_back(Mode(1920, 1440, aspect, ""));
			resolutions.push_back(Mode(2048, 1536, aspect, "QXGA"));
			resolutions.push_back(Mode(2560, 1920, aspect, "max. CRT"));
			resolutions.push_back(Mode(2800, 2100, aspect, "QSXGA+"));
			resolutions.push_back(Mode(3200, 2400, aspect, "QUXGA"));
			resolutions.push_back(Mode(4096, 3072, aspect, "HXGA"));
			resolutions.push_back(Mode(6400, 4800, aspect, "HUXGA"));
		}

		aspect = makeAspectRatio(16, 9);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 426,  240, aspect, "Wide NTSC / 240p"));
			resolutions.push_back(Mode( 640,  360, aspect, "nHD"));
			resolutions.push_back(Mode( 480,  270, aspect, "HVGA"));
			resolutions.push_back(Mode( 854,  480, aspect, "FWVGA"));
			resolutions.push_back(Mode( 960,  540, aspect, "qHD"));
			resolutions.push_back(Mode(1024,  576, aspect, "WSVGA"));
			resolutions.push_back(Mode(1136,  640, aspect, "WDVGA"));
			resolutions.push_back(Mode(1280,  720, aspect, "HD / 720p / WXGA-H"));
			resolutions.push_back(Mode(1360,  768, aspect, "WXGA*"));
			resolutions.push_back(Mode(1366,  768, aspect, "WXGA"));
			resolutions.push_back(Mode(1600,  900, aspect, "HD+ / 900p"));
			resolutions.push_back(Mode(1920, 1080, aspect, "Full HD / 1080p"));
			resolutions.push_back(Mode(2048, 1152, aspect, "QWXGA"));
			resolutions.push_back(Mode(2560, 1440, aspect, "QHD / WQHD / 1440p"));
			resolutions.push_back(Mode(3200, 1800, aspect, "QHD+ / WQXGA+"));
			resolutions.push_back(Mode(3840, 2160, aspect, "4K UHD"));
			resolutions.push_back(Mode(5120, 2880, aspect, "5K/UHD+"));
			resolutions.push_back(Mode(7680, 4320, aspect, "8K UHD"));
		}

		aspect = makeAspectRatio(16, 10);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 320,  200, aspect, "CGA"));
			resolutions.push_back(Mode( 384,  240, aspect, "WQVGA"));
			resolutions.push_back(Mode( 768,  480, aspect, "WVGA"));
			resolutions.push_back(Mode(1280,  800, aspect, "WXGA"));
			resolutions.push_back(Mode(1440,  900, aspect, "WXGA+ / WSXGA"));
			resolutions.push_back(Mode(1680, 1050, aspect, "WSXGA+"));
			resolutions.push_back(Mode(1920, 1200, aspect, "WUXGA"));
			resolutions.push_back(Mode(2560, 1600, aspect, "WQXGA"));
			resolutions.push_back(Mode(3840, 2400, aspect, "WQUXGA"));
			resolutions.push_back(Mode(5120, 3200, aspect, "WHXGA"));
			resolutions.push_back(Mode(7680, 4800, aspect, "WHUXGA"));
		}

		aspect = makeAspectRatio(3, 2);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 360,  240, aspect, "WQVGA"));
			resolutions.push_back(Mode( 480,  320, aspect, "HVGA"));
			resolutions.push_back(Mode( 720,  480, aspect, "WVGA"));
			resolutions.push_back(Mode( 960,  640, aspect, "DVGA"));
			resolutions.push_back(Mode(2160, 1440, aspect, "Full HD+"));
		}

		aspect = makeAspectRatio(5, 3);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 400, 240, aspect, "WQVGA"));
			resolutions.push_back(Mode( 800, 480, aspect, "WVGA"));
			resolutions.push_back(Mode(1280, 768, aspect, "WXGA"));
		}

		aspect = makeAspectRatio(5, 4);
		if(aspect == requestedAspect or requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode(1280, 1024, aspect, "SXGA"));
			resolutions.push_back(Mode(2560, 2048, aspect, "QSXGA"));
			resolutions.push_back(Mode(5120, 4096, aspect, "HSXGA"));
		}

		if(requestedAspect == undecidedAspect)
		{
			resolutions.push_back(Mode( 432,  240, makeAspectRatio( 9,  5), "WQVGA"));
			resolutions.push_back(Mode( 480,  272, makeAspectRatio(30, 17), "PSP"));
			resolutions.push_back(Mode( 720,  348, makeAspectRatio(60, 29), "HGC"));
			resolutions.push_back(Mode( 640,  350, makeAspectRatio(64, 35), "EGA"));
			resolutions.push_back(Mode(1600,  768, makeAspectRatio(25, 12), "UWXGA"));
			resolutions.push_back(Mode(2048, 1080, makeAspectRatio(19, 10), "DCI 2K"));
			resolutions.push_back(Mode(2560, 1080, makeAspectRatio(64, 27), "UW-UXGA"));
			resolutions.push_back(Mode(1920, 1400, makeAspectRatio( 7,  5), "TXGA"));
			resolutions.push_back(Mode(3440, 1440, makeAspectRatio(43, 18), "UWQHD"));
			resolutions.push_back(Mode(3840, 1600, makeAspectRatio(12,  5), "UW4K"));
			resolutions.push_back(Mode(3200, 2048, makeAspectRatio(25, 16), "WQSXGA"));
			resolutions.push_back(Mode(4096, 2160, makeAspectRatio(19, 10), "DCI 4K"));
			resolutions.push_back(Mode(5120, 2160, makeAspectRatio(64, 27), "UW5K"));
			resolutions.push_back(Mode(6400, 4096, makeAspectRatio(25, 16), "WHSXGA"));
			resolutions.push_back(Mode(10240,4320, makeAspectRatio(21,  9), "UW10K"));
		}

		return resolutions;
	}

	vector< pair<unsigned, unsigned> > Display::Mode::getAspectRatioList()
	{
		vector< pair<unsigned, unsigned> > aspects;
		aspects.push_back(makeAspectRatio(4, 3));
		aspects.push_back(makeAspectRatio(16, 9));
		aspects.push_back(makeAspectRatio(16, 10));
		aspects.push_back(makeAspectRatio(3, 2));
		aspects.push_back(makeAspectRatio(5, 3));
		aspects.push_back(makeAspectRatio(5, 4));
		return aspects;
	}

	//******************* IMAGE

	Image* Image::getCopy(float xScaledBy, float yScaledBy, float rotatedBy, const FlipMode flipped, float cropFromX, float cropFromY, float cropFromWidth, float cropFromHeight)
	{
		FGEAL_CHECK_INIT();
		Image* copy = null;
		if(cropFromWidth != 0 and cropFromHeight != 0)
		{
			if(rotatedBy != 0)
			{
				const int newWidth  = xScaledBy * cropFromWidth * cos(rotatedBy) + yScaledBy * cropFromHeight * sin(rotatedBy),
						  newHeight = xScaledBy * cropFromWidth * sin(rotatedBy) + yScaledBy * cropFromHeight * cos(rotatedBy);

				copy = new Image(newWidth, newHeight);
			}
			else
			{
				copy = new Image(xScaledBy * cropFromWidth, yScaledBy * cropFromHeight);
			}
		}
		else
		{
			if(rotatedBy != 0)
			{
				const int newWidth  = xScaledBy * this->getWidth() * cos(rotatedBy) + yScaledBy * this->getHeight() * sin(rotatedBy),
						  newHeight = xScaledBy * this->getWidth() * sin(rotatedBy) + yScaledBy * this->getHeight() * cos(rotatedBy);

				copy = new Image(newWidth, newHeight);
			}
			else
			{
				copy = new Image(xScaledBy * this->getWidth(), yScaledBy * this->getHeight());
			}
		}

		this->blit(*copy, 0, 0, xScaledBy, yScaledBy, rotatedBy, 0.5*copy->getWidth(), 0.5*copy->getHeight(), flipped, cropFromX, cropFromY, cropFromWidth, cropFromHeight);
		return copy;
	}

	void Image::delegateDraw(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight)
	{
		if(fromWidth == 0 and fromHeight == 0)
			if(angle == 0)
				if(xScale == 1.0f and yScale == 1.0f)
					if(flipmode == FLIP_NONE)
						this->draw(x, y);
					else
						this->drawFlipped(x, y, flipmode);
				else
					this->drawScaled(x, y, xScale, yScale, flipmode);
			else
				if(xScale == 1.0f and yScale == 1.0f)
					this->drawRotated(x, y, angle, centerX, centerY, flipmode);
				else
					this->drawScaledRotated(x, y, xScale, yScale, angle, centerX, centerY, flipmode);
		else
			if(angle == 0)
				if(xScale == 1.0f and yScale == 1.0f)
					if(flipmode == FLIP_NONE)
						this->drawRegion(x, y, fromX, fromY, fromWidth, fromHeight);
					else
						this->drawFlippedRegion(x, y, flipmode, fromX, fromY, fromWidth, fromHeight);
				else
					this->drawScaledRegion(x, y, xScale, yScale, flipmode, fromX, fromY, fromWidth, fromHeight);
			else
				if(xScale == 1.0f and yScale == 1.0f)
					this->drawRotatedRegion(x, y, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
				else
					this->drawScaledRotatedRegion(x, y, xScale, yScale, angle, centerX, centerY, flipmode, fromX, fromY, fromWidth, fromHeight);
	}

	//******************* COLOR

	Color Color::fromHsl(float h, float s, float l)
	{
		float rgb[3];
		if(s == 0)  // no saturation (achromatic)
			rgb[0] = rgb[1] = rgb[2] = 1;
		else
		{	const float q = l < 0.5f ? l * (1 + s) : l + s - l * s,
						p = 2 * l - q;

			for(int i = 0; i < 3; i++)
			{
				float t = h + (1 - i)/3.f;
				if(t < 0) t += 1;
				if(t > 1) t -= 1;

				if(t < 1/6.f)
					rgb[i] = p + (q - p) * 6 * t;
				else if(t < 1/2.f)
					rgb[i] = q;
				else if(t < 2/3.f)
					rgb[i] = p + (q - p) * (2/3.f - t) * 6;
				else
					rgb[i] = p;
			}
		}
		return Color::create(round(rgb[0] * 255), floor(rgb[1] * 255), floor(rgb[2] * 255), 255);
	}

	Color Color::fromHsv(float h, float s, float v)
	{
		float rgb[3] = {};
		const unsigned i = h * 6;
		const float f = h * 6 - i,
					p = v * (1 - s),
					q = v * (1 - f * s),
					t = v * (1 - (1 - f) * s);

		switch(i % 6)
		{
			default:
			case 0: rgb[0] = v, rgb[1] = t, rgb[2] = p; break;
			case 1: rgb[0] = q, rgb[1] = v, rgb[2] = p; break;
			case 2: rgb[0] = p, rgb[1] = v, rgb[2] = t; break;
			case 3: rgb[0] = p, rgb[1] = q, rgb[2] = v; break;
			case 4: rgb[0] = t, rgb[1] = p, rgb[2] = v; break;
			case 5: rgb[0] = v, rgb[1] = p, rgb[2] = q; break;
		}
		return Color::create(round(rgb[0] * 255), floor(rgb[1] * 255), floor(rgb[2] * 255), 255);
	}

	// aux. function used by Color::parseCStr()
	static vector<string> split(const string& str, const string& delimiter)
	{
		vector<string> tokens;
		if(delimiter.size() == 0)
			tokens.push_back(str);
		else
		{
			size_t start = 0, end = 0;
			while(end != string::npos)
			{
				end = str.find(delimiter, start);
				tokens.push_back( str.substr( start, (end == string::npos) ? string::npos : end - start));
				start = ( (end > (string::npos - delimiter.size()))? string::npos : end + delimiter.size());
			}
		}
		return tokens;
	}

	// aux. function used by Color::parseCStr()
	static long parseHex(const char* str)
	{
		char* end;
		errno = 0;
		const long x = strtol(str, &end, 16);
		if(errno == ERANGE) throw std::out_of_range(str);
		if(*str == '\0' or *end != '\0') throw std::invalid_argument(str);
		return x;
	}

	// aux. function used by Color::parseCStr()
	static unsigned long parseUnsigned(const char* str)
	{
		char* end;
		errno = 0;
		const unsigned long x = strtoul(str, &end, 10);
		if(errno == ERANGE) throw std::out_of_range(str);
		if(*str == '\0' or *end != '\0') throw std::invalid_argument(str);
		return x;
	}

	// aux. function used by Color::parseCStr()
	static string trim(string str)
	{
		str.erase(str.find_last_not_of(" \t\n\r\f\v") + 1); //trailing
		str.erase(0, str.find_first_not_of(" \t\n\r\f\v")); //leading
		return str;
	}

	template<int ri, int gi, int bi, int ai>
	static inline Color parseRgbFromTokens(const vector<string>& tokens)
	{
		return Color::create(parseUnsigned(trim(tokens[ri]).c_str()),
					 parseUnsigned(trim(tokens[gi]).c_str()),
					 parseUnsigned(trim(tokens[bi]).c_str()),
					 parseUnsigned(trim(tokens[ai]).c_str()));
	}

	Color Color::parse(const string& str, bool alphaFirst)
	{
		const vector<string> tokens = split(str, ",");
		if(tokens.size() == 4)
			return alphaFirst? parseRgbFromTokens<1,2,3,0>(tokens) : parseRgbFromTokens<0,1,2,3>(tokens);

		else if(tokens.size() == 3)
			return Color::create(parseUnsigned(trim(tokens[0]).c_str()),
						 parseUnsigned(trim(tokens[1]).c_str()),
						 parseUnsigned(trim(tokens[2]).c_str()));

		else if(tokens.size() == 1)
		{
			string str = trim(tokens[0]);
			if(str.empty()) return Color::create();
			if(str[0] == '#') str = str.substr(1);
			long value = parseHex(str.c_str());
			return alphaFirst? fromArgbHex(value) : fromRgbaHex(value);
		}

		else throw std::invalid_argument(str);
	}

	string Color::toRgbString()
	{
		static char buffer[12];
		sprintf(buffer, "%u,%u,%u", r, g, b);
		return buffer;
	}

	string Color::toRgbaString()
	{
		static char buffer[16];
		sprintf(buffer, "%u,%u,%u,%u", r, g, b, a);
		return buffer;
	}

	string Color::toArgbString()
	{
		static char buffer[16];
		sprintf(buffer, "%u,%u,%u,%u", a, r, g, b);
		return buffer;
	}

	const Color
		Color::_TRANSPARENT = create(  0,   0,   0,   0),
		Color::BLACK        = create(  0,   0,   0),
		Color::GREY         = create(127, 127, 127),
		Color::WHITE        = create(255, 255, 255),

		// Basic colors (color wheel)
		Color::RED          = create(255,   0,   0),
		Color::MAROON       = create(127,   0,   0),
		Color::GREEN        = create(  0, 255,   0),
		Color::DARK_GREEN   = create(  0, 127,   0),
		Color::BLUE         = create(  0,   0, 255),
		Color::NAVY         = create(  0,   0, 127),
		Color::YELLOW       = create(255, 255,   0),
		Color::OLIVE        = create(127, 127,   0),
		Color::CYAN         = create(  0, 255, 255),
		Color::TEAL         = create(  0, 127, 127),
		Color::MAGENTA      = create(255,   0, 255),
		Color::PURPLE       = create(127,   0, 127),

		Color::ORANGE       = create(255, 127,   0),
		Color::CHARTREUSE   = create(127, 255,   0),
		Color::SPRING_GREEN = create(0  , 255, 127),
		Color::AZURE        = create(0  , 127, 255),
		Color::ROSE         = create(255,   0, 127),
		Color::VIOLET       = create(127,   0, 255),

		// Basic shades of grey
		Color::LIGHT_GREY   = create(192, 192, 192),
		Color::DARK_GREY    = create( 96,  96,  96),

		// Other common colors
		Color::PINK         = create(255, 192, 192),
		Color::BROWN        = create(144,  92,  48);

	Color Color::fromName(ColorName name)
	{
		switch(name)
		{
			case VERMILION: return Color::create(255,  69,   0);
			case AMBER:     return Color::create(255, 192,   0);
			case GOLD:      return Color::create(255, 215,   0);
			case FUSCHIA:   return MAGENTA;
			case INDIGO:    return Color::create( 75,   0, 130);
			case BEIGE:     return Color::create(245, 245, 220);
			case LIME:      return Color::create(192, 255,   0);
			case SCARLET:   return Color::create(253,  14,  53);
			case MINT:      return Color::create(116, 195, 101);
			case TURQUOISE: return Color::create( 64, 224, 208);
			case SALMON:    return Color::create(250, 127, 114);
			case BRONZE:    return Color::create(205, 127,  50);
			case WINE:      return Color::create(196,  30,  58);
			case CELESTE:   return Color::create(  0, 191, 255);
			case FLAME:     return Color::create(226,  88,  34);
			case CREAM:     return Color::create(253, 252, 143);
			case CARAMEL:   return Color::create(193, 154, 107);
			case RUBY:      return Color::create(224,  17,  95);
			case JADE:      return Color::create(  0, 168, 107);
			case CERULEAN:  return Color::create(  0, 123, 167);
			case AQUA:      return CYAN;

			// X11/W3C colors
			case X11_ALICE_BLUE:          return Color::create(240, 248, 255);
			case X11_ANTIQUE_WHITE:       return Color::create(250, 235, 215);
			case X11_AQUA:                return fromName(X11_CYAN);
			case X11_AQUAMARINE:          return Color::create(127, 255, 212);
			case X11_AZURE:               return Color::create(240, 255, 255);
			case X11_BEIGE:               return Color::create(245, 245, 220);
			case X11_BISQUE:              return Color::create(255, 228, 196);
			case X11_BLACK:               return BLACK;
			case X11_BLANCHED_ALMOND:     return Color::create(255, 235, 205);
			case X11_BLUE:                return BLUE;
			case X11_BLUE_VIOLET:         return Color::create(138,  43, 226);
			case X11_BROWN:               return Color::create(165,  42,  42);
			case X11_BURLYWOOD:           return Color::create(222, 184, 135);
			case X11_CADET_BLUE:          return Color::create( 95, 158, 160);
			case X11_CHARTREUSE:          return CHARTREUSE;
			case X11_CHOCOLATE :          return Color::create(210, 105,  30);
			case X11_CORAL:               return Color::create(255, 127,  30);
			case X11_CORNFLOWER:          return Color::create(100, 149, 237);
			case X11_CORNSILK:            return Color::create(255, 248, 220);
			case X11_CRIMSON:             return Color::create(220,  20,  60);
			case X11_CYAN:  return CYAN;
			case X11_DARK_BLUE:           return Color::create(  0,   0, 139);
			case X11_DARK_CYAN:           return Color::create(  0, 139, 139);
			case X11_DARK_GOLDENROD:      return Color::create(184, 134,  11);
			case X11_DARK_GRAY:
			case X11_DARK_GREY:           return Color::create(169, 169, 169);
			case X11_DARK_GREEN:          return Color::create(  0, 100,   0);
			case X11_DARK_KHAKI:          return Color::create(189, 183, 107);
			case X11_DARK_MAGENTA:        return Color::create(139,   0, 139);
			case X11_DARK_OLIVE_GREEN:    return Color::create( 85, 107,  47);
			case X11_DARK_ORANGE:         return Color::create(255, 140,   0);
			case X11_DARK_ORCHID:         return Color::create(153,  50, 204);
			case X11_DARK_RED:            return Color::create(139,   0,   0);
			case X11_DARK_SALMON:         return Color::create(233, 150, 122);
			case X11_DARK_SEA_GREEN:      return Color::create(143, 188, 143);
			case X11_DARK_SLATE_BLUE:     return Color::create( 72,  61, 139);
			case X11_DARK_SLATE_GRAY:
			case X11_DARK_SLATE_GREY:     return Color::create( 47,  79,  79);
			case X11_DARK_TURQUOISE:      return Color::create(  0, 206, 209);
			case X11_DARK_VIOLET:         return Color::create(148,   0, 211);
			case X11_DEEP_PINK:           return Color::create(255,  20, 147);
			case X11_DEEP_SKY_BLUE:       return Color::create(  0, 191, 255);
			case X11_DIM_GRAY:
			case X11_DIM_GREY:            return Color::create(105, 105, 105);
			case X11_DODGE_BLUE:          return Color::create( 30, 144, 255);
			case X11_FIREBRICK:           return Color::create(178,  34,  34);
			case X11_FLORAL_WHITE:        return Color::create(255, 240, 240);
			case X11_FOREST_GREEN:        return Color::create( 34, 139,  34);
			case X11_FUCHSIA:             return fromName(X11_MAGENTA);
			case X11_GAINSBORO:           return Color::create(220, 220, 220);
			case X11_GHOST_WHITE:         return Color::create(248, 248, 255);
			case X11_GOLD:                return fromName(GOLD);
			case X11_GOLDENROD:           return Color::create(218, 165,  32);
			case X11_GRAY:
			case X11_GREY:                return Color::create(190, 190, 190);
			case X11_GREEN:               return GREEN;
			case X11_GREEN_YELLOW:        return Color::create(173, 255,  47);
			case X11_HONEYDEW:            return Color::create(240, 255, 240);
			case X11_HOT_PINK:            return Color::create(255, 105, 180);
			case X11_INDIAN_RED:          return Color::create(205,  92,  92);
			case X11_INDIGO:              return fromName(INDIGO);
			case X11_IVORY:               return Color::create(255, 255, 240);
			case X11_KHAKI:               return Color::create(240, 230, 140);
			case X11_LAVENDER:            return Color::create(230, 230, 250);
			case X11_LAVENDER_BUSH:       return Color::create(255, 240, 245);
			case X11_LAWN_GREEN:          return Color::create(124, 252,   0);
			case X11_LEMON_CHIFFON:       return Color::create(255, 250, 205);
			case X11_LIGHT_BLUE:          return Color::create(173, 216, 230);
			case X11_LIGHT_CORAL:         return Color::create(240, 128, 128);
			case X11_LIGHT_CYAN:          return Color::create(224, 255, 255);
			case X11_LIGHT_GOLDENROD:     return Color::create(250, 250, 210);
			case X11_LIGHT_GRAY:
			case X11_LIGHT_GREY:          return Color::create(211, 211, 211);
			case X11_LIGHT_GREEN:         return Color::create(144, 238, 144);
			case X11_LIGHT_PINK:          return Color::create(255, 182, 193);
			case X11_LIGHT_SALMON:        return Color::create(255, 160, 122);
			case X11_LIGHT_SEA_GREEN:     return Color::create( 32, 178, 170);
			case X11_LIGHT_SKY_BLUE:      return Color::create(135, 206, 250);
			case X11_LIGHT_SLATE_GRAY:
			case X11_LIGHT_SLATE_GREY:    return Color::create(119, 136, 153);
			case X11_LIGHT_STEEL_BLUE:    return Color::create(176, 196, 222);
			case X11_LIGHT_YELLOW:        return Color::create(255, 255, 224);
			case X11_LIME:                return fromName(X11_GREEN);
			case X11_LIME_GREEN:          return Color::create( 50, 205,  50);
			case X11_LINEN:               return Color::create(250, 240, 230);
			case X11_MAGENTA:             return MAGENTA;
			case X11_MAROON:              return Color::create(176,  48,  96);
			case X11_MEDIUM_AQUAMARINE:   return Color::create(102, 205, 170);
			case X11_MEDIUM_BLUE:         return Color::create(  0,   0, 205);
			case X11_MEDIUM_ORCHID:       return Color::create(186,  85, 211);
			case X11_MEDIUM_PURPLE:       return Color::create(147, 112, 219);
			case X11_MEDIUM_SEA_GREEN:    return Color::create( 60, 179, 113);
			case X11_MEDIUM_SLATE_BLUE:   return Color::create(123, 104, 238);
			case X11_MEDIUM_SPRING_GREEN: return Color::create(  0, 250, 154);
			case X11_MEDIUM_TURQUOISE:    return Color::create( 72, 209, 204);
			case X11_MEDIUM_VIOLET_RED:   return Color::create(199,  21, 133);
			case X11_MIDNIGHT_BLUE:       return Color::create( 25,  25, 112);
			case X11_MINT_CREAM:          return Color::create(245, 255, 250);
			case X11_MISTY_ROSE:          return Color::create(255, 228, 225);
			case X11_MOCCASIN:            return Color::create(255, 228, 181);
			case X11_NAVAJO_WHITE:        return Color::create(255, 222, 173);
			case X11_NAVY_BLUE:
			case X11_NAVY:                return NAVY;
			case X11_OLD_LACE:            return Color::create(253, 245, 230);
			case X11_OLIVE:               return OLIVE;
			case X11_OLIVE_DRAB:          return Color::create(107, 142,  35);
			case X11_ORANGE:              return Color::create(255, 165,   0);
			case X11_ORANGE_RED:          return Color::create(255,  69,   0);
			case X11_ORCHID:              return Color::create(218, 112, 214);
			case X11_PALE_GOLDENROD:      return Color::create(238, 232, 170);
			case X11_PALE_GREEN:          return Color::create(152, 251, 152);
			case X11_PALE_TURQUOISE:      return Color::create(175, 238, 238);
			case X11_PALE_VIOLET_RED:     return Color::create(219, 112, 147);
			case X11_PAPAYA_WHIP:         return Color::create(255, 239, 213);
			case X11_PEACH_PUFF:          return Color::create(255, 218, 185);
			case X11_PERU:                return Color::create(205, 133,  63);
			case X11_PINK:                return Color::create(255, 192, 203);
			case X11_PLUM:                return Color::create(221, 160, 221);
			case X11_POWDER_BLUE:         return Color::create(176, 224, 230);
			case X11_PURPLE:              return Color::create(160,  32, 240);
			case X11_REBECCA_PURPLE:      return Color::create(102,  51, 153);
			case X11_RED:                 return RED;
			case X11_ROSY_BROWN:          return Color::create(188, 143, 143);
			case X11_ROYAL_BLUE:          return Color::create( 65, 105, 225);
			case X11_SADDLE_BROWN:        return Color::create(139,  69,  19);
			case X11_SALMON:              return fromName(SALMON);
			case X11_SANDY_BROWN:         return Color::create(244, 164,  96);
			case X11_SEA_GREEN:           return Color::create( 46, 139,  87);
			case X11_SEASHELL:            return Color::create(255, 245, 238);
			case X11_SIENNA:              return Color::create(160,  82,  45);
			case X11_SILVER:              return LIGHT_GREY;
			case X11_SKY_BLUE:            return Color::create(135, 206, 235);
			case X11_SLATE_BLUE:          return Color::create(106,  90, 205);
			case X11_SLATE_GRAY:
			case X11_SLATE_GREY:          return Color::create(112, 128, 144);
			case X11_SNOW:                return Color::create(255, 250, 250);
			case X11_SPRING_GREEN:        return SPRING_GREEN;
			case X11_STEEL_BLUE:          return Color::create( 70, 130, 180);
			case X11_TAN:                 return Color::create(210, 180, 140);
			case X11_TEAL:                return TEAL;
			case X11_THISTLE:             return Color::create(216, 191, 216);
			case X11_TOMATO:              return Color::create(255,  99,  71);
			case X11_TURQUOISE:           return Color::create( 64, 224, 208);
			case X11_VIOLET:              return Color::create(238, 130, 238);
			case X11_WHEAT:               return Color::create(245, 222, 179);
			case X11_WHITE:               return WHITE;
			case X11_WHITE_SMOKE:         return Color::create(245, 245, 245);
			case X11_YELLOW:              return YELLOW;
			case X11_YELLOW_GREEN:        return Color::create(154, 205,  50);

			// W3C colors (name clashed with X11)
			case WEB_GRAY:
			case WEB_GREY:   return GREY;
			case WEB_GREEN:  return DARK_GREEN;
			case WEB_MAROON: return MAROON;
			case WEB_PURPLE: return PURPLE;

			default: return Color();
		}
	}
}
