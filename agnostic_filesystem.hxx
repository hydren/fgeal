/*
 * agnostic_filesystem.hxx
 *
 *  Created on: 9 de out de 2019
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2019  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGNOSTIC_FILESYSTEM_HXX_
#define AGNOSTIC_FILESYSTEM_HXX_

#include "fgeal/core.hpp"
#include "fgeal/exceptions.hpp"
#include "fgeal/filesystem.hpp"

#include <vector>
#include <string>
#include <algorithm>

#include <cerrno>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef _MSC_VER
	#include <direct.h>
	#ifndef getcwd
		#define getcwd _getcwd
	#endif
	#ifndef chdir
		#define chdir _chdir
	#endif
	#ifndef stat
		#define stat _stat
	#endif
#else
	#include <unistd.h>
#endif

using std::string;
using std::vector;

// max path name size expected by getCurrentWorkingDirectory()
#ifndef FGEAL_MAXPATHLEN
	#define FGEAL_MAXPATHLEN 1024
#endif

namespace fgeal
{
	vector<string> filesystem::getFilenamesWithinDirectory(const string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		vector<string> filenames;
		DIR *dir;
		struct dirent *ent;
		if((dir = opendir (directoryPath.c_str())) != null)
		{
			while((ent = readdir (dir)) != null)
			{
				if(string(ent->d_name).compare("..") != 0 && string(ent->d_name).compare(".") != 0)  // exclude . and ..
					filenames.push_back(directoryPath+"/"+string(ent->d_name));
			}
			closedir (dir);
		}
		else throw AdapterException("Could not open directory \"%s\"", directoryPath.c_str());
		std::sort(filenames.begin(), filenames.end());
		return filenames;
	}

	string filesystem::getCurrentWorkingDirectory()
	{
		FGEAL_CHECK_INIT();
		static char buffer[FGEAL_MAXPATHLEN];
		errno = 0;
		if(getcwd(buffer, FGEAL_MAXPATHLEN) != null)
			return string(buffer);
		else
		{
			if(errno == EACCES)
				throw AdapterException("Could not fetch current working directory filename. Permission to read or search a component of the file name was denied.");
			else if(errno == ERANGE)
				throw AdapterException("Could not fetch current working directory filename. Name is too long. (>%d)", FGEAL_MAXPATHLEN);
			else if(errno == EINVAL)
				throw AdapterException("Could not fetch current working directory filename. Internal logic error.");
			else
				throw AdapterException("Could not fetch current working directory filename (errno: %d).", errno);
		}
	}

	void filesystem::setWorkingDirectory(const std::string& directoryPath)
	{
		FGEAL_CHECK_INIT();
		errno = 0;
		if(chdir(directoryPath.c_str()) != 0)
		{
			if(errno == ENOENT)
				throw AdapterException("Could not set working directory \"%s\". Path could not be found.", directoryPath.c_str());
			else if(errno == ENOTDIR)
				throw AdapterException("Could not set working directory \"%s\". Path is not a directory.", directoryPath.c_str());
			else
				throw AdapterException("Could not set working directory \"%s\". errno: %d", directoryPath.c_str(), errno);
		}
	}

	bool filesystem::isFilenameDirectory(const string& path)
	{
		FGEAL_CHECK_INIT();
		struct stat info;
		errno = 0;
		if(stat(path.c_str(), &info) == 0)
			return info.st_mode & S_IFDIR;
		else if(errno == ENOENT)
			return false;
		else
			throw AdapterException("Could not fetch directory information. errno: %d", errno);
	}

	bool filesystem::isFilenameArchive(const string& path)
	{
		FGEAL_CHECK_INIT();
		struct stat info;
		errno = 0;
		if(stat(path.c_str(), &info) == 0)
			return info.st_mode & S_IFREG;
		else if(errno == ENOENT)
			return false;
		else
			throw AdapterException("Could not fetch directory information. errno: %d", errno);
	}
}

#endif /* AGNOSTIC_FILESYSTEM_HXX_ */
