/*
 * agnostic_sound_stream.hxx
 *
 *  Created on: 9 de out de 2019
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2019  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef AGNOSTIC_SOUND_STREAM_HXX_
#define AGNOSTIC_SOUND_STREAM_HXX_

#include "fgeal/core.hpp"
#include "fgeal/sound.hpp"

#include <string>
using std::string;

namespace fgeal
{
	/** Agnostic SoundStream class implementation to be used in adapters which don't define an implementation for it.
	 *  This shall delegate everything to a fgeal::Sound instance, contained within SoundStream::implementation. */
	struct SoundStream::implementation extends Sound { implementation(const string& filename) : Sound(filename) {} };

	SoundStream::SoundStream(const string& filename) : self(*new implementation(filename)) {}

	SoundStream::~SoundStream()
	{
		delete &self;
	}

	// delegate all
	void SoundStream::play() { self.play(); }
	void SoundStream::loop() { self.loop(); }
	void SoundStream::stop() { self.stop(); }
	void SoundStream::pause() { self.pause(); }
	void SoundStream::resume() { self.resume(); }
	bool SoundStream::isPlaying() { return self.isPlaying(); }
	bool SoundStream::isPaused() { return self.isPaused(); }
	void SoundStream::setVolume(float value) { self.setVolume(value); }
	float SoundStream::getVolume() { return self.getVolume(); }
	bool SoundStream::isStreaming() { return false; }
	float SoundStream::getDuration() { return self.getDuration(); }
	void SoundStream::setPlaybackSpeed(float factor, bool hint) { self.setPlaybackSpeed(factor, hint); }
	float SoundStream::getPlaybackSpeed() { return self.getPlaybackSpeed(); }
}

#endif /* AGNOSTIC_SOUND_STREAM_HXX_ */
