/*
 * colors.hpp
 *
 *  Created on: 9/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <ciso646>

#include <string>

#ifndef FGEAL_COLORS_HPP_
#define FGEAL_COLORS_HPP_

namespace fgeal
{
	/** A struct containing a color description (RGBA format).
	 *  The components are within the 0-255 range.
	 *  This is a POD to be mostly used as a way to specify colors for graphics operations. */
	struct Color
	{
		/** The red component of this color. */
		unsigned char r;

		/** The green component of this color. */
		unsigned char g;

		/** The blue component of this color. */
		unsigned char b;

		/** The alpha component of this color. (0 is transparent, 255 is opaque) */
		unsigned char a;

		/** Creates a color object with the given parameters. */
		inline static Color create(unsigned char r=0, unsigned char g=0, unsigned char b=0, unsigned char a=255)
		{
			const Color color = { r, g, b, a }; return color;
		}

		/** Creates a color object with the given float component parameters in the [0.0, 1.0] range. */
		inline static Color fromFloat(float r=0.0f, float g=0.0f, float b=0.0f, float a=1.0f)
		{
			return Color::create(255*r, 255*g, 255*b, 255*a);
		}

		/** Creates a color object with the given hex triplet (0xRRGGBB format; 0x000000-0xFFFFFF range) and an optional alpha value (255 by default). */
		inline static Color fromHex(unsigned long hexcode, unsigned char a=255)
		{
			return Color::create((hexcode & 0xFF0000) >> 16, (hexcode & 0xFF00) >> 8, (hexcode & 0xFF), a);
		}

		/** Creates a color object with the given hex triplet (0xRRGGBB format; 0x000000-0xFFFFFF range) and a float alpha value in the [0.0, 1.0] range. */
		inline static Color fromHex(unsigned long hexcode, float alpha)
		{
			return Color::create((hexcode) >> 16, (hexcode & 0xFF00) >> 8, (hexcode & 0xFF), 255*alpha);
		}

		/** Creates a color object with the given ARGB hex color (0xAARRGGBB format; 0x00000000-0xFFFFFFFF range). */
		inline static Color fromArgbHex(unsigned long argbHex)
		{
			return Color::create((argbHex & 0xFF0000) >> 16, (argbHex & 0xFF00) >> 8, (argbHex & 0xFF), (argbHex & 0xFF000000) >> 24);
		}

		/** Creates a color object with the given RGBA hex color (0xRRGGBBAA format; 0x00000000-0xFFFFFFFF range). */
		inline static Color fromRgbaHex(unsigned long rgbaHex)
		{
			return Color::create((rgbaHex & 0xFF000000) >> 24, (rgbaHex & 0xFF0000) >> 16, (rgbaHex & 0xFF00) >> 8, (rgbaHex & 0xFF));
		}

		/** Creates a color object with the given HSL parameters in the [0.0, 1.0] range.  */
		static Color fromHsl(float hue, float saturation, float lightness);

		/** Creates a color object with the given HSV parameters in the [0.0, 1.0] range.  */
		static Color fromHsv(float hue, float saturation, float value);

		/** Parses the given string for a color. The string's format is expected to be either:
		 *  - Comma separated values (R,G,B), (R,G,B,A) or, if 'alphaFirst' is true, (A,R,G,B);
		 *  - Or an hexadecimal integer 0xRRGGBB or #RRGGBB, 0xRRGGBBAA or #RRGGBBAA or, if 'alphaFirst' is true, 0xAARRGGBB or #AARRGGBB. */
		static Color parse(const std::string& str, bool alphaFirst);

		/** Parses the given string for a color. The string's format is expected to be either:
		 *  - Comma separated values (R,G,B), (R,G,B,A);
		 *  - An hexadecimal integer 0xRRGGBB or #RRGGBB, 0xRRGGBBAA or #RRGGBBAA. */
		inline static Color parse(const std::string& str) { return parse(str, false); }

		/** Parses the given C-string for a color. The string's format is expected to be either:
		 *  - Comma separated values (R,G,B), (R,G,B,A) or, if 'alphaFirst' is true, (A,R,G,B);
		 *  - Or an hexadecimal integer 0xRRGGBB or #RRGGBB, 0xRRGGBBAA or #RRGGBBAA or, if 'alphaFirst' is true, 0xAARRGGBB or #AARRGGBB. */
		inline static Color parseCStr(const char* cstr, bool alphaFirst=false) { return parse(cstr, alphaFirst); }

		inline bool operator == (const Color& c) const { return r == c.r and g == c.g and b == c.b and a == c.a; }
		inline bool operator != (const Color& c) const { return r != c.r  or g != c.g  or b != c.b  or a != c.a; }

		/** Returns a brighter version of this color. */
		inline Color getBrighter() const { return Color::create((255+r)/2, (255+g)/2, (255+b)/2, a); }

		/** Returns a darker version of this color. */
		inline Color getDarker() const { return Color::create(r/2, g/2, b/2, a); }

		/** Returns a color that cancels out this color, resulting in a grayscale color. Alpha component will be the same as this color. */
		inline Color getComplementary() const { return Color::create(255 - (r & 255), 255 - (g & 255), 255 - (b & 255), a); }

		/** Returns a transparent version of this color.
		 *  The 'alpha' argument will be the returned color's alpha in case this color is opaque.
		 *  If this color is not opaque, then the returned color's alpha will be the combination of both.  */
		inline Color getTransparent(unsigned char alpha=128) const { return Color::create(r, g, b, (a * alpha)/255); }

		/** Returns true if this color is set at maximum opacity  */
		inline bool isOpaque() const { return a == 255; }

		/** Returns a hex triplet (0xRRGGBB format) representation of this color. */
		inline unsigned long toHex()
		{
			return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
		}

		/** Returns a ARGB hex code (0xAARRGGBB format) representation of this color. */
		inline unsigned long toArgbHex()
		{
			return ((a & 0xff) << 24) + ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff);
		}

		/** Returns a RGBA hex code (0xRRGGBBAA format) representation of this color. */
		inline unsigned long toRgbaHex()
		{
			return ((r & 0xff) << 24) + ((g & 0xff) << 16) + ((b & 0xff) << 8) + (a & 0xff);
		}

		/** Returns a comma-separated RGB string representation of this color. */
		std::string toRgbString();

		/** Returns a comma-separated RGBA string representation of this color. */
		std::string toRgbaString();

		/** Returns a comma-separated ARGB string representation of this color. */
		std::string toArgbString();

		// Predefined built-in colors
		static const Color

		// Transparent (zero color)
		_TRANSPARENT, /* (  0,  0,  0,  0) */

		// Basic monochrome colors
		BLACK,	/* (  0,   0,   0) */
		GREY,	/* (127, 127, 127) */
		WHITE,	/* (255, 255, 255) */

		// Basic colors (RGB color wheel)
		RED,	/* (255,   0,   0) */ 	MAROON,			/* (127,   0,   0) */
		GREEN,	/* (  0, 255,   0) */ 	DARK_GREEN,		/* (  0, 127,   0) */
		BLUE,	/* (  0,   0, 255) */ 	NAVY,			/* (  0,   0, 127) */
		YELLOW,	/* (255, 255,   0) */ 	OLIVE,			/* (127, 127,   0) */
		CYAN,	/* (  0, 255, 255) */	TEAL,			/* (  0, 127, 127) */
		MAGENTA,/* (255,   0, 255) */	PURPLE,			/* (127,   0, 127) */

		ORANGE, 		/* (255, 127,   0) */	CHARTREUSE,		/* (127, 255,   0) */
		SPRING_GREEN,	/* (0  , 255, 127) */	AZURE,			/* (  0, 127, 255) */
		ROSE,			/* (255,   0, 127) */	VIOLET,			/* (127,   0, 255) */

		// Basic shades of grey
		LIGHT_GREY,	/* (192, 192, 192) */		DARK_GREY, /* (64, 64, 64) */

		// Common non-basic colors
		PINK,		// (255, 192, 192)
		BROWN;		// (144,  92,  48)

		enum ColorName
		{
			NONE,  // "null" color (same as zero initialized)

			VERMILION,	// (255,  69,   0)
			AMBER,		// (255, 192,   0)
			GOLD,		// (255, 215,   0)
			FUSCHIA,	// same as MAGENTA
			INDIGO,		// ( 75,   0, 130)
			LIME,		// (192, 255,   0)
			BEIGE,		// (245, 245, 220)
			SCARLET,	// (253,  14,  53)
			MINT,		// (116, 195, 101)
			TURQUOISE,	// ( 64, 224, 208)
			SALMON,		// (250, 127, 114)
			BRONZE,		// (205, 127,  50)
			WINE,		// (196,  30,  58)
			CELESTE,	// (  0, 192, 255)
			FLAME,		// (226,  88,  34)
			CREAM,		// (253, 252, 143)
			CARAMEL,	// (193, 154, 107)
			RUBY,		// (224,  17,  95)
			JADE,		// (  0, 168, 107)
			CERULEAN,	// (  0, 123, 167)
			AQUA,		// same as CYAN

			// X11/W3C colors
			X11_ALICE_BLUE,          // (240, 248, 255)
			X11_ANTIQUE_WHITE,       // (250, 235, 215)
			X11_AQUA,                // same as X11_CYAN
			X11_AQUAMARINE,          // (127, 255, 212)
			X11_AZURE,               // (240, 255, 255)
			X11_BEIGE,               // (245, 245, 220)
			X11_BISQUE,              // (255, 228, 196)
			X11_BLACK,               // same as BLACK
			X11_BLANCHED_ALMOND,     // (255, 235, 205)
			X11_BLUE,                // same as BLUE
			X11_BLUE_VIOLET,         // (138,  43, 226)
			X11_BROWN,               // (165,  42,  42)
			X11_BURLYWOOD,           // (222, 184, 135)
			X11_CADET_BLUE,          // ( 95, 158, 160)
			X11_CHARTREUSE,          // same as CHARTREUSE
			X11_CHOCOLATE ,          // (210, 105,  30)
			X11_CORAL,               // (255, 127,  30)
			X11_CORNFLOWER,          // (100, 149, 237)
			X11_CORNSILK,            // (255, 248, 220)
			X11_CRIMSON,             // (220,  20,  60)
			X11_CYAN,                // same as CYAN
			X11_DARK_BLUE,           // (  0,   0, 139)
			X11_DARK_CYAN,           // (  0, 139, 139)
			X11_DARK_GOLDENROD,      // (184, 134,  11)
			X11_DARK_GRAY,           // same as X11_DARK_GREY
			X11_DARK_GREY,           // (169, 169, 169)
			X11_DARK_GREEN,          // (  0, 100,   0)
			X11_DARK_KHAKI,          // (189, 183, 107)
			X11_DARK_MAGENTA,        // (139,   0, 139)
			X11_DARK_OLIVE_GREEN,    // ( 85, 107,  47)
			X11_DARK_ORANGE,         // (255, 140,   0)
			X11_DARK_ORCHID,         // (153,  50, 204)
			X11_DARK_RED,            // (139,   0,   0)
			X11_DARK_SALMON,         // (233, 150, 122)
			X11_DARK_SEA_GREEN,      // (143, 188, 143)
			X11_DARK_SLATE_BLUE,     // ( 72,  61, 139)
			X11_DARK_SLATE_GRAY,     // same X11_DARK_SLATE_GREY
			X11_DARK_SLATE_GREY,     // ( 47,  79,  79)
			X11_DARK_TURQUOISE,      // (  0, 206, 209)
			X11_DARK_VIOLET,         // (148,   0, 211)
			X11_DEEP_PINK,           // (255,  20, 147)
			X11_DEEP_SKY_BLUE,       // (  0, 191, 255)
			X11_DIM_GRAY,            // same as X11_DIM_GREY
			X11_DIM_GREY,            // (105, 105, 105)
			X11_DODGE_BLUE,          // ( 30, 144, 255)
			X11_FIREBRICK,           // (178,  34,  34)
			X11_FLORAL_WHITE,        // (255, 240, 240)
			X11_FOREST_GREEN,        // ( 34, 139,  34)
			X11_FUCHSIA,             // same as X11_MAGENTA
			X11_GAINSBORO,           // (220, 220, 220)
			X11_GHOST_WHITE,         // (248, 248, 255)
			X11_GOLD,                // same as GOLD
			X11_GOLDENROD,           // (218, 165,  32)
			X11_GRAY,                // same as X11_GREY
			X11_GREY,                // (190, 190, 190)
			X11_GREEN,               // same as GREEN
			X11_GREEN_YELLOW,        // (173, 255,  47)
			X11_HONEYDEW,            // (240, 255, 240)
			X11_HOT_PINK,            // (255, 105, 180)
			X11_INDIAN_RED,          // (205,  92,  92)
			X11_INDIGO,              // same as INDIGO
			X11_IVORY,               // (255, 255, 240)
			X11_KHAKI,               // (240, 230, 140)
			X11_LAVENDER,            // (230, 230, 250)
			X11_LAVENDER_BUSH,       // (255, 240, 245)
			X11_LAWN_GREEN,          // (124, 252,   0)
			X11_LEMON_CHIFFON,       // (255, 250, 205)
			X11_LIGHT_BLUE,          // (173, 216, 230)
			X11_LIGHT_CORAL,         // (240, 128, 128)
			X11_LIGHT_CYAN,          // (224, 255, 255)
			X11_LIGHT_GOLDENROD,     // (250, 250, 210)
			X11_LIGHT_GRAY,          // same as X11_LIGHT_GREY
			X11_LIGHT_GREY,          // (211, 211, 211)
			X11_LIGHT_GREEN,         // (144, 238, 144)
			X11_LIGHT_PINK,          // (255, 182, 193)
			X11_LIGHT_SALMON,        // (255, 160, 122)
			X11_LIGHT_SEA_GREEN,     // ( 32, 178, 170)
			X11_LIGHT_SKY_BLUE,      // (135, 206, 250)
			X11_LIGHT_SLATE_GRAY,    // same as X11_LIGHT_SLATE_GREY
			X11_LIGHT_SLATE_GREY,    // (119, 136, 153)
			X11_LIGHT_STEEL_BLUE,    // (176, 196, 222)
			X11_LIGHT_YELLOW,        // (255, 255, 224)
			X11_LIME,                // same as X11_GREEN
			X11_LIME_GREEN,          // ( 50, 205,  50)
			X11_LINEN,               // (250, 240, 230)
			X11_MAGENTA,             // same as MAGENTA
			X11_MAROON,              // (176,  48,  96)
			X11_MEDIUM_AQUAMARINE,   // (102, 205, 170)
			X11_MEDIUM_BLUE,         // (  0,   0, 205)
			X11_MEDIUM_ORCHID,       // (186,  85, 211)
			X11_MEDIUM_PURPLE,       // (147, 112, 219)
			X11_MEDIUM_SEA_GREEN,    // ( 60, 179, 113)
			X11_MEDIUM_SLATE_BLUE,   // (123, 104, 238)
			X11_MEDIUM_SPRING_GREEN, // (  0, 250, 154)
			X11_MEDIUM_TURQUOISE,    // ( 72, 209, 204)
			X11_MEDIUM_VIOLET_RED,   // (199,  21, 133)
			X11_MIDNIGHT_BLUE,       // ( 25,  25, 112)
			X11_MINT_CREAM,          // (245, 255, 250)
			X11_MISTY_ROSE,          // (255, 228, 225)
			X11_MOCCASIN,            // (255, 228, 181)
			X11_NAVAJO_WHITE,        // (255, 222, 173)
			X11_NAVY_BLUE,           // same as X11_NAVY
			X11_NAVY,                // same as NAVY
			X11_OLD_LACE,            // (253, 245, 230)
			X11_OLIVE,               // same as OLIVE
			X11_OLIVE_DRAB,          // (107, 142,  35)
			X11_ORANGE,              // (255, 165,   0)
			X11_ORANGE_RED,          // (255,  69,   0)
			X11_ORCHID,              // (218, 112, 214)
			X11_PALE_GOLDENROD,      // (238, 232, 170)
			X11_PALE_GREEN,          // (152, 251, 152)
			X11_PALE_TURQUOISE,      // (175, 238, 238)
			X11_PALE_VIOLET_RED,     // (219, 112, 147)
			X11_PAPAYA_WHIP,         // (255, 239, 213)
			X11_PEACH_PUFF,          // (255, 218, 185)
			X11_PERU,                // (205, 133,  63)
			X11_PINK,                // (255, 192, 203)
			X11_PLUM,                // (221, 160, 221)
			X11_POWDER_BLUE,         // (176, 224, 230)
			X11_PURPLE,              // (160,  32, 240)
			X11_REBECCA_PURPLE,      // (102,  51, 153)
			X11_RED,                 // same as RED
			X11_ROSY_BROWN,          // (188, 143, 143)
			X11_ROYAL_BLUE,          // ( 65, 105, 225)
			X11_SADDLE_BROWN,        // (139,  69,  19)
			X11_SALMON,              // same as SALMON
			X11_SANDY_BROWN,         // (244, 164,  96)
			X11_SEA_GREEN,           // ( 46, 139,  87)
			X11_SEASHELL,            // (255, 245, 238)
			X11_SIENNA,              // (160,  82,  45)
			X11_SILVER,              // same as LIGHT_GRAY
			X11_SKY_BLUE,            // (135, 206, 235)
			X11_SLATE_BLUE,          // (106,  90, 205)
			X11_SLATE_GRAY,          // same as X11_SLATE_GREY
			X11_SLATE_GREY,          // (112, 128, 144)
			X11_SNOW,                // (255, 250, 250)
			X11_SPRING_GREEN,        // same as SPRING_GREEN
			X11_STEEL_BLUE,          // ( 70, 130, 180)
			X11_TAN,                 // (210, 180, 140)
			X11_TEAL,                // same as TEAL
			X11_THISTLE,             // (216, 191, 216)
			X11_TOMATO,              // (255,  99,  71)
			X11_TURQUOISE,           // ( 64, 224, 208)
			X11_VIOLET,              // (238, 130, 238)
			X11_WHEAT,               // (245, 222, 179)
			X11_WHITE,               // same as WHITE
			X11_WHITE_SMOKE,         // (245, 245, 245)
			X11_YELLOW,              // same as YELLOW
			X11_YELLOW_GREEN,        // (154, 205,  50)

			// W3C colors (name clashed with X11)
			WEB_GRAY,     // same as WEB_GREY
			WEB_GREY,     // same as GREY
			WEB_GREEN,    // same as DARK_GREEN
			WEB_MAROON,   // same as MAROON
			WEB_PURPLE,   // same as PURPLE

			NAME_COUNT  // for counting purposes (does not represent a color)
		};

		/** Creates a color object with the given color name. */
		static Color fromName(ColorName name);

		// shortcut assignment operator for ColorName enum
		inline Color operator=(ColorName name) { return fromName(name); }

		//TODO support color conversions to HSL, HSV and CMYK formats
	};

	// shortcut to create Color from name
	inline Color operator~(Color::ColorName name) { return Color::fromName(name); }
}

#endif /* FGEAL_COLORS_HPP_ */
