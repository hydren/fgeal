/*
 * display.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_DISPLAY_HPP_
#define FGEAL_DISPLAY_HPP_
#include <ciso646>

#include "core.hpp"
#include "geometry.hpp"

#include <vector>
#include <string>
#include <utility>

namespace fgeal
{
	/**
	 * A class representing a display.
	 *
	 * Currently the Display class' behavior is to have a single, global, singleton display. This may change in the future.
	 * To use this class, first call Display::create to trigger the display creation. Then, the returned reference may be
	 * used. The created display will become the current one, optionally accessible through the Display::getInstance() method.
	 * Note that the getInstance() method does not create a instance, only returns the current one. So be sure to only call
	 * it after the call to Display::create(). Its possible to check whether the display have been created or not with the
	 * Display::wasCreated() method. */
	class Display
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_DISPLAY_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_DISPLAY_FRIEND_CLASSES

		friend void fgeal::finalize();

		public:
		struct Options;

		private:
		static Display* instance;

		Display(const Options&);  // private constructor
		Display(const Display&);  // disable copy constructor
		Display& operator=(const Display&);  // disable value-assignent
		~Display();

		public:

		/** A struct to hold display mode information. */
		struct Mode
		{
			unsigned width, height;
			std::pair<unsigned, unsigned> aspectRatio;  // for informative purposes
			std::string description;  // for informative purposes
			//TODO Add color depth information to this struct (and provide ways to specify bpp when initializing displays)

			/** Creates a display mode with given display width and height. */
			Mode(unsigned width, unsigned height);

			/** Returns a list of supported fullscreen display modes, optionally filtered by the given aspect ratio. */
			static std::vector<Mode> getList(std::pair<unsigned, unsigned> aspect=std::make_pair(0, 0));

			/** Returns a list of display modes with common dimensions, optionally filtered by the given aspect ratio.
			 *  Note that there is no guarantee that this list doesn't show unsupported resolutions, like ones that are incompatible with fullscreen mode.
			 *  This list should instead be used to gather windowed mode resolutions. */
			static std::vector<Mode> getGenericList(std::pair<unsigned, unsigned> aspect=std::make_pair(0, 0));

			/** Returns a list of common aspect ratios. */
			static std::vector< std::pair<unsigned, unsigned> > getAspectRatioList();

			private:
			Mode(unsigned width, unsigned height, std::pair<unsigned, unsigned> aspect, std::string description="");
		};

		/** A struct to hold display creation options. */
		struct Options
		{
			Options() : width(0), height(0), fullscreen(false), title(), iconFilename(), positioning(POSITION_UNDEFINED), position(), isUserResizeable(false) {}

			/// The display's size.
			unsigned width, height;

			/// If true, the display will be created in fullscreen mode. If false, the display will be windowed.
			bool fullscreen;

			/// Text shown in the window's title.
			std::string title;

			/// Filename of the icon to be shown as the window's icon. If empty, the window's icon is left unchanged.
			std::string iconFilename;

			/// Determines how the created display will be positioned, relative to the desktop. This field is ignored if 'fullscreen' is true.
			enum Positioning
			{
				POSITION_UNDEFINED, /// Let the library choose the display's position (default)
				POSITION_DEFINED,   /// Define the display's position, through the 'position' field.
				POSITION_CENTERED   /// The display will be centered on the desktop.
			}
			positioning;

			/// The position of the window relative to the desktop. If 'fullscreen' is true or 'positioning' is not set as 'POSITION_DEFINED', this field is ignored.
			Point position;

			/// If true, the display will be user-resizeable; otherwise it will not. This flag is ignored if 'fullscreen' is true.
			bool isUserResizeable;
		};

		/** Creates a display with the given size, title and icon.
		 *  Returns a reference to the created display. After this call, the display is also available through calling getInstance().
		 * 	If this method is called twice, an AdapterException is thrown.
		 * 	If 'fullscreen' is true, the display will be created in fullscreen mode. Otherwise, the display will be windowed.
		 *  If 'iconFilename' is empty, the window's icon is left unchanged. */
		static Display& create(unsigned width=640, unsigned height=480, bool fullscreen=false, const std::string& title="", const std::string& iconFilename="");

		/** Creates a display with the given options.
		 *  Returns a reference to the created display. After this call, the display is also available through calling getInstance().
		 * 	If this method is called twice, an AdapterException is thrown. */
		static Display& create(const Options& options);

		/** Returns the application's Display instance. If the display has not been created, expect undefined behavior (most likely seg.fault). */
		static Display& getInstance();

		/** Returns true if the application's Display instance was created. */
		static bool wasCreated();

		/** Function to refresh/flush all thing rendered in this display. Without this you'll probably get a black screen showing nothing.
		 * 	It is recommended that you call this function after you done all drawing needed in the current program cycle. */
		void refresh();

		/** Cleans the whole window, painting it black. :P */
		void clear();

		/** Returns the width of this display. */
		unsigned getWidth();

		/** Returns the height of this display. */
		unsigned getHeight();

		/** Changes the size of this display. */
		void setSize(unsigned width, unsigned height);

		/** Returns this display's current title text. */
		std::string getTitle() const;

		/** Changes the title text of this display. */
		void setTitle(const std::string& title);

		/** Changes the icon of this display to an image loaded from the given filename. */
		void setIcon(const std::string& iconFilename);

		/** Returns true if the display is in fullscreen mode, or false if the display is in windowed mode. */
		bool isFullscreen();

		/** Changes the display's mode to either fullscreen (if true is passed as argument) or windowed (if false is passed as argument).
		 *  If the display's mode is already the specified one, nothing is done. */
		void setFullscreen(bool fullscreen=true);

		/** Sets the display's position. */
		void setPosition(const Point& pt);

		/** Sets the display's position such that it lies in the center of the desktop. */
		void setPositionOnCenter();

		/** Sets whether the mouse cursor should be visible. */
		void setMouseCursorVisible(bool choice=true);
	};
}

#endif /* FGEAL_DISPLAY_HPP_ */
