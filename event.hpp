/*
 * event.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EVENT_HPP_
#define FGEAL_EVENT_HPP_
#include <ciso646>

#include "core.hpp"
#include "input.hpp"

namespace fgeal
{
	/**
	 * A class representing an event from the engine (keyboard, mouse, etc).
	 * */
	class Event
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_EVENT_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_EVENT_FRIEND_CLASSES

		public:

		/** Creates an empty event object. */
		Event();
		~Event();

		/** Event types emumeration */
		enum Type
		{
			TYPE_UNKNOWN,
			TYPE_DISPLAY_CLOSURE,
			TYPE_KEY_PRESS,
			TYPE_KEY_RELEASE,
			TYPE_MOUSE_BUTTON_PRESS,
			TYPE_MOUSE_BUTTON_RELEASE,
			TYPE_MOUSE_MOTION,
			TYPE_MOUSE_WHEEL_MOTION,
			TYPE_JOYSTICK_BUTTON_PRESS,
			TYPE_JOYSTICK_BUTTON_RELEASE,
			TYPE_JOYSTICK_AXIS_MOTION

			// TODO map more event types...
			// TODO deal with joystick reconfigure events
		};

		/** Returns the type of this event. */
		Event::Type getEventType();

		/** Returns the keyboard key constant related to this event.
		 *  Note that this value only makes sense when this event is a keyboard event.  */
		Keyboard::Key getEventKeyCode();

		/** Returns the mouse button constant related to this event.
		 *  Note that this value only makes sense when this event is a mouse button event.  */
		Mouse::Button getEventMouseButton();

		/** Returns the mouse's X position on this event. */
		int getEventMouseX();

		/** Returns the mouse's Y position on this event. */
		int getEventMouseY();

		/** Returns the mouse's position on this event. */
		inline Point getEventMousePosition() { const Point p = { (float) getEventMouseX(), (float) getEventMouseY() }; return p; }

		/** Returns the amount of ticks the mouse wheel has moved (positive is up, negative is down).
		 *  The returned amount refers to the vertical mouse wheel motion.
		 *  Note that this value only makes sense when this event is a mouse wheel motion event. */
		int getEventMouseWheelMotionAmount();

		// TODO Add support for horizontal mouse wheel motion events.

		/** Returns the index of the joytick that generated the event (device index).
		 *  Note that this value only makes sense when this event is a joystick-related event. */
		int getEventJoystickIndex();

		/** Returns the index of the button that was pressed or released (counting from zero).
		 *  Note that this value only makes sense when this event is a joystick button event. */
		int getEventJoystickButtonIndex();

		/** Returns the index of the axis that was moved (counting from zero).
		 *  Note that this value only makes sense when this event is a joystick axis motion event.*/
		int getEventJoystickAxisIndex();

		/** Returns the position of the axis that was moved (-1.0 to 1.0 range).
		 *  Note that this value only makes sense when this event is a joystick axis motion event.*/
		float getEventJoystickAxisPosition();

		/** Returns the index of the second axis that was moved (counting from zero) in this same event.
		 * 	If there was no secondary axis motion, returns -1.
		 *  Note that this value only makes sense when this event is a joystick axis motion event that involves 2 axes simultaneously.*/
		int getEventJoystickSecondAxisIndex();

		/** Returns the position of the second axis that was moved (-1.0 to 1.0 range) in this same event.
		 *  If there was no secondary axis motion, returns 0.
		 *  Note that this value only makes sense when this event is a joystick axis motion event that involves 2 axes simultaneously.*/
		float getEventJoystickSecondAxisPosition();
	};

	class EventQueue
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_EVENTQUEUE_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_EVENTQUEUE_FRIEND_CLASSES

		friend void fgeal::finalize();

		static EventQueue* instance;

		EventQueue();  // private constructor
		EventQueue(const EventQueue&);  // disable copy constructor
		EventQueue& operator=(const EventQueue&);  // disable value-assignent
		~EventQueue();

		static inline Event::implementation& getImplementation(Event* event) { return event->self; }  // for internal use only
		static inline Event::implementation& getImplementation(Event& event) { return event.self; }  // for internal use only

		public:

		/** Returns the application's event queue instance.
		 *  Note: for now, the current behavior is to have a single, global, singleton event queue. This may change in the future.   */
		static EventQueue& getInstance();

		/** Returns true if the queue is empty. Otherwise returns false. */
		bool isEmpty();

		/** Returns true if the queue has any events. If the queue is empty, returns false. Equivalent to the opposite of EventQueue::isEmpty(). */
		bool hasEvents();

		/** Returns the next event in the queue, and removes it from the queue. If there is no events on the queue, null is returned.
		 *  Note: it is the user's responsability to delete the returned object. */
		Event* getNextEvent();

		/** Puts the next event from the queue on `containerEvent` and removes it from the queue, returning true.
		 *  If there was no events on the queue, false is returned and `containerEvent` is undefined. Passing a null `containerEvent` is undefined behavior. */
		bool getNextEvent(Event* containerEvent);

		/** Skips the next event in the queue, if there are any. Returns true if an event was skipped, or false if the queue was empty. */
		bool skipNextEvent();

		/** Waits indefinitely for an event. If `container` is not null, the event is put on it. Otherwise the event is left on the queue. */
		void waitNextEvent(Event* container=null);

		/** Drops (loses) all events currently on this queue. */
		void flushEvents();

		/** Enables bypassing of all incomming events in this event queue, that is, makes this queue ignore all incomming events.
		 *  If `choice` is false, the effect is inverse, that is, makes this queue listen to all incomming events (default behavior). */
		void setEventBypassingEnabled(bool choice=true);
	};
}

#endif /* FGEAL_EVENT_HPP_ */
