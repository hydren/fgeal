/*
 * cached_drawable_text.cpp
 *
 *  Created on: 27 de mar de 2023
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2019  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "cached_drawable_text.hpp"

using std::string;

namespace fgeal
{
	CachedDrawableText::CachedDrawableText(const string& text, Font* font, Color color)
	: content(text), font(font), color(color), renderedText(null), fontSize()
	{
		updateRenderedText();
	}

	CachedDrawableText::~CachedDrawableText()
	{
		if(renderedText != null)
			delete renderedText;
	}

	float CachedDrawableText::getWidth()
	{
		if(font != null and font->getSize() != fontSize) updateRenderedText();
		return renderedText != null? renderedText->getWidth() : 0;
	}

	float CachedDrawableText::getHeight()
	{
		if(font != null and font->getSize() != fontSize) updateRenderedText();
		return renderedText != null? renderedText->getHeight() : 0;
	}

	void CachedDrawableText::setFont(Font* fnt)
	{
		if(fnt != font or (font != null and font->getSize() != fontSize))
		{
			font = fnt;
			updateRenderedText();
		}
	}

	Font* CachedDrawableText::getFont()
	{
		return font;
	}

	void CachedDrawableText::setContent(const string& str)
	{
		if(str != content or (font != null and font->getSize() != fontSize))
		{
			content = str;
			updateRenderedText();
		}
	}

	string CachedDrawableText::getContent()
	{
		return content;
	}

	void CachedDrawableText::setColor(Color colour)
	{
		if(colour != color or (font != null and font->getSize() != fontSize))
		{
			color = colour;
			updateRenderedText();
		}
	}

	Color CachedDrawableText::getColor()
	{
		return color;
	}

	void CachedDrawableText::updateRenderedText()
	{
		if(renderedText != null)
			delete renderedText, renderedText = null, fontSize = 0;

		if(font != null)
		{
			renderedText = new Image(font->getTextWidth(content), font->getTextHeight());
			Graphics::setDrawTarget(renderedText);
			font->drawText(content, 0, 0, color);
			Graphics::setDefaultDrawTarget();
			fontSize = font->getSize();
		}
	}

	void CachedDrawableText::draw(float x, float y)
	{
		if(font != null and font->getSize() != fontSize)
		{
			updateRenderedText();
			fontSize = font->getSize();
		}

		if(renderedText != null)
			renderedText->draw(x, y);
	}

	void CachedDrawableText::drawScaled(float x, float y, float xScale, float yScale)
	{
		if(font != null and font->getSize() != fontSize)
		{
			updateRenderedText();
			fontSize = font->getSize();
		}

		if(renderedText != null)
			renderedText->drawScaled(x, yScale, xScale, yScale);
	}

	void CachedDrawableText::drawRotated(float x, float y, float angle, float centerX, float centerY)
	{
		if(font != null and font->getSize() != fontSize)
		{
			updateRenderedText();
			fontSize = font->getSize();
		}

		if(renderedText != null)
			renderedText->drawRotated(x, y, angle, centerX, centerY);
	}

	void CachedDrawableText::drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY)
	{
		if(font != null and font->getSize() != fontSize)
		{
			updateRenderedText();
			fontSize = font->getSize();
		}

		if(renderedText != null)
			renderedText->drawScaledRotated(x, yScale, xScale, yScale, angle, centerX, centerY);
	}
}
