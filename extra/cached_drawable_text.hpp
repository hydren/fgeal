/*
 * cached_drawable_text.hpp (originally agnostic_cached_drawable_text.hxx)
 *
 *  Created on: 20 de mai de 2020
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2019  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_CACHED_DRAWABLE_TEXT_HXX_
#define FGEAL_EXTRA_CACHED_DRAWABLE_TEXT_HXX_

#include "fgeal/core.hpp"
#include "fgeal/font.hpp"
#include "fgeal/graphics.hpp"

#include <string>

namespace fgeal
{
	/** A class that behaves like DrawableText, but that always caches the result in a image/texture, ready to be drawn.
	 *  This class should perform badly if drawing parameters (such as the text content, font, color) change too often.
	 *  An implementation for Graphics::setDrawTarget() is necessary for this class to work. */
	class CachedDrawableText
	{
		std::string content;
		Font* font;
		Color color;

		Image* renderedText;
		unsigned fontSize;

		public:

		/** Creates a drawable text object with given text, font and color. If the font is not null, a cache should be created by this constructor. */
		CachedDrawableText(const std::string& text="", Font* font=null, Color color=Color::BLACK);
		~CachedDrawableText();

		/** Returns the expected rendered width of this text when drawn at the screen. */
		float getWidth();

		/** Returns the expected rendered height of this text when drawn at the screen. */
		float getHeight();

		/** Sets the font of this text. This call should recreate the cache if the fonts differs from previous. Note that this method does not delete the previous font. */
		void setFont(Font* font);

		/** Returns the font of this text. */
		Font* getFont();

		/** Sets the content of this text. This call should recreate the cache if the content differs from previous. */
		void setContent(const std::string& str);

		/** Returns the string content of this text. */
		std::string getContent();

		/** Sets the color of this text. This call should recreate the cache if the color differs from previous. */
		void setColor(Color color);

		/** Returns the color of this text. */
		Color getColor();

		void updateRenderedText();

		/** Draws this text in the given position. */
		void draw(float x, float y);
		inline void draw(Point position) { draw(position.x, position.y); }

		/** Draws this text in the given position, scaled by the given scale factors. Note that the font size is not changed by this call. */
		void drawScaled(float x, float y, float xScale, float yScale);
		inline void drawScaled(const Point& p, const Vector2D& s) { drawScaled(p.x, p.y, s.x, s.y); }

		/** Draws this text in the given position, rotated by the given angle. */
		void drawRotated(float x, float y, float angle, float centerX, float centerY);
		inline void drawRotated(const Point& p, float a, const Point& c=Image::IMAGE_CENTER)
		{
			drawRotated(p.x, p.y, a,(&c == &Image::IMAGE_CENTER? getWidth()*0.5f : c.x), (&c == &Image::IMAGE_CENTER? getHeight()*0.5f : c.y));
		}

		/** Draws this text in the given position, scaled by the given scale factors and rotated by the given angle. Note that the font size is not changed by this call. */
		void drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY);
		inline void drawScaledRotated(const Point& p, const Vector2D& s, float a, const Point& c=Image::IMAGE_CENTER)
		{
			drawScaledRotated(p.x, p.y, s.x, s.y, a, (&c == &Image::IMAGE_CENTER? getWidth()*0.5f : c.x), (&c == &Image::IMAGE_CENTER? getHeight()*0.5f : c.y));
		}
	};
}

#endif /* FGEAL_EXTRA_CACHED_DRAWABLE_TEXT_HXX_ */
