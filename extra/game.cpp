/*
 * game.cpp
 *
 *  Created on: 27/11/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "game.hpp"
#include "primitives.hpp"

#include <algorithm>

#ifdef _MSC_VER
	// disables warning about using "this" reference in the DummyState constructor call
	#pragma warning(disable:4355)
#endif

using std::string;

//#include <iostream> //debug
//using std::cout; using std::endl; //debug

namespace fgeal
{
	// aux struct to hold display creation parameters
	struct TempDisplayInitVars { unsigned width, height; string title; };

	// dummy state class. does nothing.
	class DummyState extends public Game::State
	{
		public:
		DummyState(Game* game) : State(*game) {}
		int getId() { return -1; }
		void initialize() {};
		void onEnter() {};
		void onLeave() {};
		void render() {};
		void update(float delta) {};
	};

	// Constructor
	Game::Game(string title, State* state, unsigned width, unsigned height)
	: running(false), paused(false), showFPS(false), input(), display(null),
	  states(), initialState(state), currentState(null), requestedState(null),
	  minimumUpdateInterval(-1), maximumUpdateInterval(-1), maxFps(-1),
	  inputManagerEnabled(false),
	  lastFrameUpdateTime(0), accumulatedDelta(0),
	  lastFpsUpdateTime(0), recordedFps(0), currentSecondFrameCount(0),
	  tempDisplayInitVarsPtr(null), // aux
	  dummyState(new DummyState(this))
	{
		if(initialState == null)
			initialState = dummyState;

		if(state != null)
			this->addState(state);  // also adds the initial state to the states list, if non null

		TempDisplayInitVars* tmp = new TempDisplayInitVars();
		tmp->width  = width;
		tmp->height = height;
		tmp->title  = title;
		tempDisplayInitVarsPtr = tmp;
	}

	Game::~Game()
	{
		for(unsigned i = 0; i < states.size(); i++)
		{
			delete states[i];
			states[i] = null;
		}

		if(dummyState != null)
			delete dummyState;

		// cannot delete display currently as it still behaves as singleton.
		//delete display;
	}

	void Game::initialize()
	{
		// init all states
		for(unsigned i = 0; i < states.size(); i++)
			states[i]->initialize();

		currentState = initialState;
	}

	void Game::start()
	{
		this->setup();
		this->running = true;
		this->currentState->onEnter();
		this->computeDelta();
		this->loop();
	}

	void Game::render()
	{
		this->currentState->render();
	}

	void Game::update(float delta)
	{
		if(this->requestedState != null)
		{
			this->currentState->onLeave();
			this->currentState = this->requestedState;
			this->requestedState = null;
			this->currentState->onEnter();
		}

		currentState->update(delta);
	}

	bool Game::closeRequested()
	{
		return true;
	}

	short Game::getFpsCount()
	{
		return recordedFps;
	}

	Display& Game::getDisplay()
	{
		return *display;
	}

	// state management methods ====================================================


	void Game::addState(State* state)
	{
		for(unsigned i = 0; i < states.size(); i++)
		{
			if(states[i] == state)
				throw GameException("Attempting to add the same state twice to a game.");

			if(states[i]->getId() == state->getId())
				throw GameException("Attempting to add to a game a state with the same ID as one of the game's already added states.");
		}

		states.push_back(state);

		if(initialState == dummyState)
			initialState = state;
	}

	Game::State* Game::getState(int id)
	{
		for(unsigned i = 0; i < states.size(); i++)
			if(states[i]->getId() == id)
				return states[i];

		return null;
	}

	void Game::setInitialState(int stateId)
	{
		for(unsigned i = 0; i < states.size(); i++)
		if(states[i]->getId() == stateId)
		{
			this->initialState = states[i];
			return;
		}

		throw GameException("Attempt to set initial state with a invalid/absent state id.");
	}

	Game::State* Game::getInitialState()
	{
		return initialState;
	}

	void Game::enterState(int stateId)
	{
		for(unsigned i = 0; i < states.size(); i++)
		if(states[i]->getId() == stateId)
		{
			this->requestedState = states[i];
			return;
		}

		throw GameException("Attempt to enter state with a invalid/absent state id.");
	}

	void Game::setInputManagerEnabled(bool choice)
	{
		inputManagerEnabled = choice;
	}

	bool Game::isInputManagerEnabled()
	{
		return inputManagerEnabled;
	}

	void Game::onKeyPressed(Keyboard::Key k) { currentState->onKeyPressed(k); }
	void Game::onKeyReleased(Keyboard::Key k) { currentState->onKeyReleased(k); }
	void Game::onMouseButtonPressed(Mouse::Button btn, int x, int y) { currentState->onMouseButtonPressed(btn, x, y); }
	void Game::onMouseButtonReleased(Mouse::Button btn, int x, int y) { currentState->onMouseButtonReleased(btn, x, y); }
	void Game::onMouseMoved(int x, int y) { currentState->onMouseMoved(x, y); }
	void Game::onMouseWheelMoved(int change) { currentState->onMouseWheelMoved(change); }
	void Game::onJoystickButtonPressed(unsigned j, unsigned b) { currentState->onJoystickButtonPressed(j, b); }
	void Game::onJoystickButtonReleased(unsigned j, unsigned b) { currentState->onJoystickButtonReleased(j, b); }
	void Game::onJoystickAxisMoved(unsigned j, unsigned a, float v) { currentState->onJoystickAxisMoved(j, a, v); }

	// private methods ##############################################################

	float Game::computeDelta()
	{
		float time = uptime();
		float delta = (time - lastFrameUpdateTime);
		lastFrameUpdateTime = time;

		return delta;
	}

	void Game::computeFps()
	{
		if (((float) uptime()) - lastFpsUpdateTime > 1) {
			lastFpsUpdateTime = uptime();
			recordedFps = currentSecondFrameCount;
			currentSecondFrameCount = 0;
		}
		currentSecondFrameCount++;
	}

	void Game::setup()
	{
		TempDisplayInitVars* tmp = static_cast<TempDisplayInitVars*>(tempDisplayInitVarsPtr);

		if(not fgeal::isInitialized())
			fgeal::initialize();

		if(not Display::wasCreated())
			Display::create(tmp->width, tmp->height, false, tmp->title);

		display = &(Display::getInstance()); // acquire current display
		input.removeListener(this);
		input.addListener(this);

		this->initialize();

		//dont need this guy anymore
		delete tmp;
		tempDisplayInitVarsPtr = tmp = null;
	}

	void Game::loop()
	{
		// game loop
		while(running)
		{
			const float frameStartTime = uptime();

			// update and render ----------------------------------------------------------------------------

			// if the input manager is enabled, update it
			if(inputManagerEnabled)
				input.update();

			if(not paused)
			{
				accumulatedDelta += this->computeDelta();

				if(minimumUpdateInterval <= 0  or accumulatedDelta >= minimumUpdateInterval)
				{
					if(maximumUpdateInterval > 0 and accumulatedDelta > maximumUpdateInterval)
					{
						do
						{
							this->update(maximumUpdateInterval);
							accumulatedDelta -= maximumUpdateInterval;
						}
						while(accumulatedDelta >= maximumUpdateInterval);
						if(accumulatedDelta >= minimumUpdateInterval)
						{
							this->update(accumulatedDelta);
							accumulatedDelta = 0;
						}
					}
					else
					{
						this->update(accumulatedDelta);
						accumulatedDelta = 0;
					}
				}

				this->render();

				if(showFPS)
					primitives::drawDigits(this->getFpsCount(), 10, 10);

				if(maxFps > 0) // limiting fps if requested
					rest(std::max(0.0f, (1.0f / (float) maxFps) - (((float) uptime()) - frameStartTime)));
			}
			else this->update(0);
			// finished update and render ----------------------------------------------------------------------------

			// back to game loop
			this->computeFps();
			fgeal::Display::getInstance().refresh();

			if(input.isCloseRequested())
				if(this->closeRequested())
					running = false;
		}
	}

	// state default implemented methods ##############################################################
	void Game::State::onKeyPressed(Keyboard::Key k) {}
	void Game::State::onKeyReleased(Keyboard::Key k) {}
	void Game::State::onMouseButtonPressed(Mouse::Button, int, int) {}
	void Game::State::onMouseButtonReleased(Mouse::Button, int, int) {}
	void Game::State::onMouseMoved(int, int) {}
	void Game::State::onMouseWheelMoved(int) {}
	void Game::State::onJoystickButtonPressed(unsigned, unsigned) {}
	void Game::State::onJoystickButtonReleased(unsigned, unsigned) {}
	void Game::State::onJoystickAxisMoved(unsigned, unsigned, float) {}
}
