/*
 * gui.cpp
 *
 *  Created on: 18/11/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "gui.hpp"

#include "fgeal/graphics.hpp"
#include "fgeal/extra/primitives.hpp"

#include <cmath>
#include <stdexcept>

using std::string;

namespace fgeal
{
	inline static bool isEvenCycle(float period)
	{
		return period == 0 or static_cast<int>(uptime()/period) % 2 == 0;
	}

	inline static float getShapeRoundedRectangleRadius(PlainComponent::Shape shape, float size)
	{
		switch(shape)
		{
			case PlainComponent::SHAPE_SLIM_ROUNDED_RECTANGULAR:    return size * 0.0625f;
			default:case PlainComponent::SHAPE_ROUNDED_RECTANGULAR: return size * 0.1250f;
			case PlainComponent::SHAPE_SQUIRCULAR:                  return size * 0.2500f;
			case PlainComponent::SHAPE_STADIUM_LIKE:                return size * 0.5000f;
			case PlainComponent::SHAPE_CONST_ROUNDED_RECTANGULAR:   return 10;
		}
	}

	void PlainComponent::draw() const
	{
		switch(shape)
		{
			default:case SHAPE_RECTANGULAR:
			{
				// trick to for fewer calls to draw "thick" borders
				if(not borderDisabled and not backgroundDisabled and borderThickness != 1.0f and backgroundColor.isOpaque())
					Graphics::drawFilledRectangle(bounds, borderColor);

				if(not backgroundDisabled)
					Graphics::drawFilledRectangle(bounds.getSpacedOutline(borderDisabled? 0 : -borderThickness), backgroundColor);

				if(not borderDisabled)
				{
					// special case 1px border
					if(borderThickness == 1.0f)
						Graphics::drawRectangle(bounds, borderColor);
					else if(not backgroundColor.isOpaque() or backgroundDisabled)
					{
						Graphics::drawFilledRectangle(bounds.x, bounds.y, bounds.w, borderThickness, borderColor);
						Graphics::drawFilledRectangle(bounds.x, bounds.y + bounds.h - borderThickness, bounds.w, borderThickness, borderColor);
						Graphics::drawFilledRectangle(bounds.x, bounds.y, borderThickness, bounds.h, borderColor);
						Graphics::drawFilledRectangle(bounds.x + bounds.w - borderThickness, bounds.y, borderThickness, bounds.h, borderColor);
					}
				}
			}
			break;

			case SHAPE_ROUNDED_RECTANGULAR:
			case SHAPE_SLIM_ROUNDED_RECTANGULAR:
			case SHAPE_SQUIRCULAR:
			case SHAPE_CONST_ROUNDED_RECTANGULAR:
			case SHAPE_STADIUM_LIKE:
			{
				// draw "thick" borders; FIXME when background is not opaque, this glitches; can't implement properly due to absence of annulus sector primitive in fgeal
				if(not borderDisabled and not backgroundDisabled and borderThickness != 1.0f)
					Graphics::drawFilledRoundedRectangle(bounds, getShapeRoundedRectangleRadius(shape, bounds.h), borderColor);

				if(not backgroundDisabled)
					Graphics::drawFilledRoundedRectangle(bounds.getSpacedOutline(borderDisabled? 0 : -borderThickness), getShapeRoundedRectangleRadius(shape, bounds.h), backgroundColor);

				// special case 1px border; FIXME when background is disabled, this draws 1px border instead of thick border; can't implement properly due to absence of annulus sector primitive in fgeal
				if(not borderDisabled and (borderThickness == 1.0f or backgroundDisabled))
					Graphics::drawRoundedRectangle(bounds, getShapeRoundedRectangleRadius(shape, bounds.h), borderColor);
			}
			break;

			case SHAPE_ELLIPSOIDAL:
			{
				// draw "thick" borders; FIXME when background is not opaque, this glitches; can't implement properly due to absence of thick ellipse primitive in fgeal
				if(not borderDisabled and not backgroundDisabled and borderThickness != 1.0f)
					Graphics::drawFilledEllipse(bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h, 0.5f*bounds.w, 0.5f*bounds.h, borderColor);

				if(not backgroundDisabled)
					Graphics::drawFilledEllipse(bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h, 0.5f*bounds.w - (borderDisabled? 0 : borderThickness), 0.5f*bounds.h - (borderDisabled? 0 : borderThickness), backgroundColor);

				// special case 1px border; FIXME when background is disabled, this draws 1px border instead of thick border; can't implement properly due to absence of thick ellipse primitive in fgeal
				if(not borderDisabled and (borderThickness == 1.0f or backgroundDisabled))
					Graphics::drawEllipse(bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h, 0.5f*bounds.w, 0.5f*bounds.h, borderColor);
			}
			break;
		}
	}

	float Label::alignmentHorizontalOffset(float contentWidth)
	{
		return textHorizontalAlignment == ALIGN_LEADING?  borderThickness + padding.x
		:      textHorizontalAlignment == ALIGN_TRAILING? bounds.w - contentWidth - padding.x - borderThickness
		:      textHorizontalAlignment == ALIGN_CENTER?  (bounds.w - contentWidth) * 0.5f : 0;
	}

	float Label::alignmentVerticalOffset(float contentHeight)
	{
		return textVerticalAlignment == ALIGN_LEADING?  borderThickness + padding.y
		:      textVerticalAlignment == ALIGN_TRAILING? bounds.h - contentHeight - padding.y - borderThickness
		:      textVerticalAlignment == ALIGN_CENTER?  (bounds.h - contentHeight) * 0.5f : 0;
	}

	void Label::draw()
	{
		PlainComponent::draw();
		text.draw(bounds.x + alignmentHorizontalOffset(), bounds.y + alignmentVerticalOffset());
	}

	void Label::pack()
	{
		bounds.w = 2*(borderThickness + padding.x) + text.getWidth();
		bounds.h = 2*(borderThickness + padding.y) + text.getContent().empty() and text.getFont()? text.getFont()->getTextHeight() : text.getHeight();
	}

	string TextField::getTextContentFitFor(const Label& label, const string& content, OverflowBehavior overflowBehavior)
	{
		GUIDrawableText tmpText = label.text;
		string tmpStr = content;
		tmpText.setContent(tmpStr);
		while(not tmpStr.empty() and tmpText.getWidth() + label.borderThickness + label.padding.x >= label.bounds.w)
		{
			tmpStr.resize(tmpStr.length()-1);
			tmpText.setContent(tmpStr);
		}
		const unsigned maxLength = tmpStr.size();
		if(content.length() <= maxLength)
			return content;
		else switch(overflowBehavior)
		{
			default:case OVERFLOW_LET_IT_BE: return content;
			case OVERFLOW_TRUNCATE_LEADING:  return content.substr(content.length() - maxLength);
			case OVERFLOW_TRUNCATE_TRAILING: return content.substr(0, maxLength);
			case OVERFLOW_ABBREVIATE_LEADING:
				if(maxLength >= 3) return "..." + content.substr(content.length() - maxLength + 3);
				else return content.substr(content.length() - maxLength);
			case OVERFLOW_ABBREVIATE_TRAILING:
				if(maxLength >= 3) return content.substr(0, maxLength - 3) + "...";
				else return content.substr(0, maxLength);
		}
	}

	void TextField::updateDrawableText()
	{
		text.setContent(getTextContentFitFor(*this, content, overflowBehavior));
	}

	void TextField::pack()
	{
		text.setContent(content);
		Label::pack();
	}

	void TextField::packToWidth(float width)
	{
		Label::pack();
		bounds.w = width;
		updateDrawableText();
	}

	void TextField::draw()
	{
		Label::draw();
		if(caretVisible and std::cos(2 * M_PI * caretBlinkRate * fgeal::uptime()) > 0)
			Graphics::drawFilledRectangle(bounds.x + Label::alignmentHorizontalOffset() + text.getFont()->getTextWidth(content.substr(0, caretPosition)),
										  bounds.y + Label::alignmentVerticalOffset(), 4, text.getFont()->getTextHeight(), caretColor);
	}

	void TextField::onKeyPressed(Keyboard::Key key)
	{
		const string previousContent(content);
		if(key == Keyboard::KEY_BACKSPACE)
		{
			if(not content.empty() and caretPosition > 0)
			{
				content.erase(content.begin() + caretPosition-1);
				caretPosition--;
			}
		}
		else if(key == Keyboard::KEY_ARROW_LEFT)
		{
			if(caretPosition > 0)
				caretPosition--;
		}
		else if(key == Keyboard::KEY_ARROW_RIGHT)
		{
			if(caretPosition < content.size())
				caretPosition++;
		}
		else
		{
			const char typed = Keyboard::parseKeyStroke(key);
			if(typed != '\n')
			{
				content.insert(content.begin() + caretPosition, 1, typed);
				caretPosition++;
			}
		}
		if(content != previousContent)
			updateDrawableText();
	}

	void TextField::onMouseButtonPressed(Mouse::Button btn, float x, float y)
	{
		if(btn == Mouse::BUTTON_LEFT and bounds.contains(x, y))
		{
			caretPosition = x/text.getFont()->getTextWidth("-");
			if(caretPosition > content.length()-1)
				caretPosition = content.length()-1;
		}
	}

	void Button::draw()
	{
		Label::draw();
		if(highlighted and isEvenCycle(highlightPeriod)) switch(shape)
		{
			default:case SHAPE_RECTANGULAR:
				Graphics::drawRectangle(bounds.getSpacedOutline(highlightSpacing), highlightColor);
				break;

			case SHAPE_ROUNDED_RECTANGULAR:
			case SHAPE_SLIM_ROUNDED_RECTANGULAR:
			case SHAPE_SQUIRCULAR:
			case SHAPE_CONST_ROUNDED_RECTANGULAR:
			case SHAPE_STADIUM_LIKE:
				Graphics::drawRoundedRectangle(bounds.getSpacedOutline(highlightSpacing), getShapeRoundedRectangleRadius(shape, bounds.h), highlightColor);
				break;

			case SHAPE_ELLIPSOIDAL:
				Graphics::drawEllipse(bounds.x + 0.5f*bounds.w, bounds.y + 0.5f*bounds.h, 0.5f*bounds.w + highlightSpacing, 0.5f*bounds.h + highlightSpacing, highlightColor);
				break;
		}
	}

	void IconButton::draw()
	{
		Button::draw();
		const Vector2D absoluteScale = { iconScale.x * bounds.w / icon->getWidth(), iconScale.y * bounds.h / icon->getHeight()};
		icon->drawScaled(bounds.x + bounds.w * 0.5f * (1 - iconScale.x), bounds.y + bounds.h * 0.5f * (1 - iconScale.y), absoluteScale.x, absoluteScale.y);
	}

	void ArrowIconButton::draw()
	{
		Button::draw();
		float rx1, ry1, rx2, ry2, rx3, ry3;
		switch(arrowOrientation) {
			case ARROW_UP:            rx1 = 0.2;   ry1 = 0.8;   rx2 = 0.5;   ry2 = 0.2;   rx3 = 0.8;   ry3 = 0.8; break;
			case ARROW_DOWN:          rx1 = 0.2;   ry1 = 0.2;   rx2 = 0.8;   ry2 = 0.2;   rx3 = 0.5;   ry3 = 0.8; break;
			case ARROW_LEFT:          rx1 = 1/3.f; ry1 = 0.5;   rx2 = 2/3.f; ry2 = 1/6.f; rx3 = 2/3.f; ry3 = 5/6.f; break;
			default:case ARROW_RIGHT: rx1 = 2/3.f; ry1 = 0.5;   rx2 = 1/3.f; ry2 = 1/6.f; rx3 = 1/3.f; ry3 = 5/6.f; break;
		}
		Graphics::drawFilledTriangle(bounds.x + rx1*bounds.w, bounds.y + ry1*bounds.h,
									 bounds.x + rx2*bounds.w, bounds.y + ry2*bounds.h,
									 bounds.x + rx3*bounds.w, bounds.y + ry3*bounds.h, arrowColor);
	}

	Panel::~Panel()
	{
		for(unsigned i = 0; i < components.size(); i++)
			delete components[i];
	}

	void Panel::draw()
	{
		PlainComponent::draw();
		const Point mousePosition = Mouse::getPosition();
		for(unsigned i = 0; i < components.size(); i++)
		{
			ComponentHandler& component = *components[i];
			component.setHoveringDisabled(hoveringDisabled);
			component.checkHovered(not hoveringDisabled and component.bounds.contains(mousePosition));
			component.draw();
		}
	}

	void TabbedPane::draw()
	{
		PlainComponent::draw();
		const Point mousePosition = Mouse::getPosition();
		for(unsigned i = 0; i < tabs.size(); i++)
		{
			Button& tabButton = tabs[i].button;
			tabButton.highlighted = not hoveringDisabled and tabButton.bounds.contains(mousePosition);
			tabButton.draw();
		}
		if(not tabs.empty())
			Graphics::drawFilledRectangle(bounds.x, bounds.y + tabs.back().button.bounds.h, bounds.w, 0.25f*tabs.back().button.bounds.h, backgroundColor);
		if(activeTabIndex < tabs.size())
		{
			tabs[activeTabIndex].panel->hoveringDisabled = this->hoveringDisabled;
			tabs[activeTabIndex].panel->draw();
		}
	}

	void TabbedPane::pack()
	{
		for(unsigned i = 0; i < tabs.size(); i++)
		{
			Button& tabButton = tabs[i].button;
			tabButton.bounds.x = bounds.x + i * (bounds.w / tabs.size());
			tabButton.bounds.y = bounds.y;
			tabButton.bounds.w = bounds.w / tabs.size();
			tabButton.bounds.h = tabButton.text.getFont() != null? tabButton.text.getFont()->getTextHeight() * 1.1 : bounds.h * 0.05;
		}
	}

	void TabbedPane::addTab(Panel& component, std::string label)
	{
		Tab tab = { &component };
		tab.button.shape = Button::SHAPE_ROUNDED_RECTANGULAR;
		tab.button.backgroundColor = tabs.empty()? Color::GREY : Color::create(112, 112, 112);
		tab.button.borderColor = Color::_TRANSPARENT;
		tab.button.highlightColor = Color::LIGHT_GREY;
		tab.button.text.setColor(tabs.empty()? Color::BLACK : Color::DARK_GREY.getDarker());
		tab.button.text = this->text;
		tab.button.text.setContent(label);
		tabs.push_back(tab);
	}

	bool TabbedPane::setActiveTab(Panel& panel)
	{
		for(unsigned i = 0; i < tabs.size(); i++)
		{
			if(&panel == tabs[i].panel)
			{
				setActiveTabByIndex(i);
				return true;
			}
		}
		return false;
	}

	void TabbedPane::setActiveTabByIndex(unsigned index)
	{
		if(index > tabs.size())
			throw std::out_of_range("tab index out of range");

		if(activeTabIndex != index)
		{
			tabs[activeTabIndex].button.backgroundColor = Color::create(112, 112, 112);
			tabs[activeTabIndex].button.text.setColor(Color::DARK_GREY.getDarker());
			activeTabIndex = index;
			tabs[activeTabIndex].button.backgroundColor = Color::GREY;
			tabs[activeTabIndex].button.text.setColor(Color::BLACK);
		}
	}

	bool TabbedPane::setActiveTabByButtonPosition(float x, float y)
	{
		for(unsigned i = 0; i < tabs.size(); i++)
		{
			if(tabs[i].button.bounds.contains(x, y))
			{
				setActiveTabByIndex(i);
				return true;
			}
		}
		return false;
	}


	Menu::Menu()
	 : Label(),
	   entries(), selectedIndex(0), scrollIndexOffset(0),
	   focusedEntryFontColor(Color::WHITE), focusedEntryBgColor(Color::WHITE), disabledEntryTextColor(Color::GREY),
	   layoutMode(PACK_ENTRIES), entrySpacing(0.1), entrySpacingIsRelative(true),
	   scrollVerticalEnabled(true), entryOverflowBehavior(TextField::OVERFLOW_ABBREVIATE_TRAILING), cursorWrapAroundEnabled(false)
	{
		title.setColor(Color::LIGHT_GREY);
		text.setColor(Color::WHITE);
		backgroundColor = Color::BLACK;
		borderColor = Color::WHITE;
		textHorizontalAlignment = ALIGN_LEADING;
	}

	void Menu::addEntry(string labelStr, int index, void* data)
	{
		if(index < 0)
			entries.push_back(Entry()), index = entries.size()-1;
		else
			entries.insert(entries.begin()+index, Entry());

		entries[index].label = labelStr;
		entries[index].enabled = true;
		entries[index].userData = data;
		entries[index].text = this->text;  // copy style, font, etc...
		entries[index].text.setContent(entries[index].label);

		if(entries.size() == 1)
			setSelectedIndex(0);
	}

	void Menu::removeEntry(unsigned index)
	{
		if(index < 0 || index > entries.size()-1)
			return;

		if(index == selectedIndex)
		{
			if(entries.size() == 1)
				selectedIndex = 0;

			else if(index == entries.size()-1)
				selectedIndex = index-1;

			else
				selectedIndex = index+1;
		}
		entries.erase(entries.begin()+index);
	}

	const std::vector<Menu::Entry>& Menu::getEntries() const
	{
		return entries;
	}

	Menu::Entry& Menu::getEntryAt(unsigned index)
	{
		return entries.at(index);
	}

	void Menu::setSelectedEntry(const Entry& entry)
	{
		for(unsigned i = 0, found=false; i < entries.size() and not found; i++)
			if(&entries[i] == &entry)
				setSelectedIndex(i), found=true;
	}

	Menu::Entry& Menu::getSelectedEntry()
	{
		return entries[selectedIndex];
	}

	void Menu::setSelectedIndex(unsigned index)
	{
		if(index < entries.size())
		{
			entries[selectedIndex].text.setColor(entries[selectedIndex].enabled? text.getColor(): disabledEntryTextColor);
			entries[index].text.setColor(focusedEntryFontColor);
			selectedIndex = index;
			updateScroll();
		}
	}

	unsigned Menu::getSelectedIndex()
	{
		return selectedIndex;
	}

	void Menu::moveCursorUp()
	{
		if(selectedIndex > 0)
			setSelectedIndex(selectedIndex-1);
		else if(cursorWrapAroundEnabled and entries.size() > 0)
			setSelectedIndex(entries.size()-1);
	}

	void Menu::moveCursorDown()
	{
		if(selectedIndex + 1 < entries.size())
			setSelectedIndex(selectedIndex+1);
		else if(cursorWrapAroundEnabled)
			setSelectedIndex(0);
	}

	void Menu::setFont(Font* font)
	{
		text.setFont(font);

		if(title.getFont() == null)
			title.setFont(font);

		for(unsigned i = 0; i < entries.size(); i++)
			entries[i].text.setFont(font);
	}

	void Menu::setColor(const Color& color)
	{
		text.setColor(color);
		borderColor = focusedEntryBgColor = color;
	}

	unsigned Menu::getIndexAtLocation(float x, float y)
	{
		if(bounds.contains(x, y) and not entries.empty())
		{
			y -= bounds.y;
			y -= getTitleHeight();
			y += (scrollVerticalEnabled? scrollIndexOffset*getEntryHeight() : 0);

			const unsigned index = y/getEntryHeight();
			return index > entries.size()? 0 : index;
		}
		else return 0;
	}

	void Menu::updateScroll()
	{
		const unsigned maxVisibleEntries = (bounds.h - getTitleHeight())/getEntryHeight();

		if(selectedIndex < scrollIndexOffset)
			scrollIndexOffset = selectedIndex;

		else if(selectedIndex > scrollIndexOffset + maxVisibleEntries - 1)
			scrollIndexOffset = selectedIndex - maxVisibleEntries + 1;
	}

	void Menu::updateDrawableText(Entry& entry)
	{
		entry.text.setContent(TextField::getTextContentFitFor(*this, entry.label, entryOverflowBehavior));
		entry.text.setColor(&entry == &getSelectedEntry()? focusedEntryFontColor : entry.enabled? text.getColor() : Color::GREY);
	}

	void Menu::updateDrawableText(unsigned index)
	{
		Entry& entry = entries[index];
		entry.text.setContent(TextField::getTextContentFitFor(*this, entry.label, entryOverflowBehavior));
		entry.text.setColor(index == selectedIndex? focusedEntryFontColor : entry.enabled? text.getColor() : Color::GREY);
	}

	void Menu::updateDrawableText()
	{
		for(unsigned i = 0; i < entries.size(); i++)
			updateDrawableText(i);
	}

	float Menu::getEntryTextHeight()
	{
		return entries.empty()? (text.getFont() == null? text.drawCharFunctionSize : text.getFont()->getTextHeight()) : entries.front().text.getHeight();
	}

	float Menu::getEntrySpacing()
	{
		return entrySpacingIsRelative? getEntryTextHeight() * entrySpacing : entrySpacing;
	}

	float Menu::getEntryHeight()
	{
		return entrySpacingIsRelative? getEntryTextHeight() * (1+entrySpacing) : getEntryTextHeight() + entrySpacing;
	}

	float Menu::getTitleHeight()
	{
		return title.getContent().empty()? 0 : title.getHeight();
	}

	/** Draw the menu according the menu bounds and number of entries */
	void Menu::draw()
	{
		PlainComponent::draw();
		float offset = bounds.y + borderThickness + padding.y;

		if(not title.getContent().empty())
			title.draw(bounds.x + alignmentHorizontalOffset(title.getWidth()), offset);

		offset += getTitleHeight() + 0.5f * getEntrySpacing();
		if(layoutMode == PACK_ENTRIES)
		{
			const float entryHeight = getEntryHeight(), textEntryHeight = getEntryTextHeight();
			const unsigned maxVisibleEntries = (bounds.h - getTitleHeight())/entryHeight;
			for(unsigned i = scrollIndexOffset; i < scrollIndexOffset + maxVisibleEntries and i < entries.size(); i++, offset += entryHeight)
			{
				if(i == selectedIndex)
					Graphics::drawFilledRectangle(bounds.x, offset, bounds.w, textEntryHeight, focusedEntryBgColor);
				entries[i].text.draw(bounds.x + alignmentHorizontalOffset(entries[i].text.getWidth()), offset);
			}
		}
		else if(layoutMode == STRETCH_SPACING)
		{
			const float distanceBetween = (bounds.h - 2*(borderThickness + padding.y))/entries.size();
			for(unsigned i = 0; i < entries.size(); i++, offset += distanceBetween)
			{
				if(i == selectedIndex)
					Graphics::drawFilledRectangle(bounds.x, offset, bounds.w, distanceBetween, focusedEntryBgColor);
				entries[i].text.draw(bounds.x + alignmentHorizontalOffset(entries[i].text.getWidth()), offset);
			}
		}
	}
}
