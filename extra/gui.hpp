/*
 * gui.hpp
 *
 *  Created on: 18/11/2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_GUI_HPP_
#define FGEAL_EXTRA_GUI_HPP_

#include "primitives.hpp"

#include "fgeal/geometry.hpp"
#include "fgeal/input.hpp"
#include "fgeal/colors.hpp"
#include "fgeal/font.hpp"

#include <vector>
#include <string>

namespace fgeal
{
	/** Very simple struct that can be used to draw basic shape, with background color and border. */
	struct PlainComponent
	{
		enum Shape
		{
			SHAPE_RECTANGULAR, SHAPE_ELLIPSOIDAL, SHAPE_STADIUM_LIKE,
			SHAPE_ROUNDED_RECTANGULAR, SHAPE_SLIM_ROUNDED_RECTANGULAR, SHAPE_SQUIRCULAR, SHAPE_CONST_ROUNDED_RECTANGULAR,
		} shape;

		/// this widget's position and size
		Rectangle bounds;

		/// the background color
		Color backgroundColor;

		/// if is set as true, no background is drawn
		bool backgroundDisabled;

		/// the border's color
		Color borderColor;

		/// if is set as true, no border is drawn
		bool borderDisabled;

		/// (experimental) the border's thickness (a value of 1.0 is considered a special case and is possibly optimized)
		/// note that this may not work on properly on some situations
		float borderThickness;

		PlainComponent()
		: shape(), bounds(),
		  backgroundColor(Color::_TRANSPARENT), backgroundDisabled(),
		  borderColor(Color::_TRANSPARENT), borderDisabled(), borderThickness(1.0) {}

		void draw() const;
	};

	// A struct that supports optional text drawing using drawing function instead of a font. This allows the usage of
	// graphics primitives to draw text, for example. By default, the drawing function pointer is set to fgeal's
	// `primitives::drawCharacter` function, but another one can be used.
	struct GUIDrawableText extends DrawableText
	{
		// a pointer to a function intended to draw a character; when no font is set, this function will be used to draw
		void (*drawCharFunction)(char, float, float, float, const Color&);

		// the char size used in the drawing function
		float drawCharFunctionSize;

		// the aspect ratio (width-to-height) of the characters rendered by the drawing function; by default, it's set
		// as 9:16 (0.5625), but should be informed correctly when the drawing function behaves differently, since this
		// value ensures correct text width computation.
		float drawCharFunctionAspectRatio;

		// a spacing factor (relative to the char size) for the horizontal spacing between chars to be respected when
		// drawing them; by default, it's set as 18.75%.
		float drawCharFunctionSpacingFactor;

		// If true, force positioning of each of the value's characters in fixed-width intervals.
		// Otherwise, it obeys whatever the font being used estipulates.
		bool isMonospaced; // TODO rename to 'isForcedMonospaced'

		GUIDrawableText() : DrawableText(), drawCharFunction(primitives::drawCharacter),
		drawCharFunctionSize(16), drawCharFunctionAspectRatio(0.5625f), drawCharFunctionSpacingFactor(0.1875f), isMonospaced() {}

		/** Draws this text in the given position */
		inline void draw(float x, float y)
		{
			if(this->getFont() != null)
				if(isMonospaced)
					this->getFont()->drawTextMonospaced(this->getContent(), x, y, this->getColor());  // TODO replace this with monospaced "mode" in DrawableText
				else DrawableText::draw(x, y);
			else if(drawCharFunction != null)
				primitives::drawCustomCharacters(drawCharFunction, this->getContent().c_str(), this->getContent().length(), x, y, drawCharFunctionSize, (drawCharFunctionAspectRatio + drawCharFunctionSpacingFactor) * drawCharFunctionSize, this->getColor());
		}
		inline void draw(Point position)
		{
			if(this->getFont() != null)
				if(isMonospaced)
					this->getFont()->drawTextMonospaced(this->getContent(), position, this->getColor());  // TODO replace this with monospaced "mode" in DrawableText
				else DrawableText::draw(position);
			else if(drawCharFunction != null)
				primitives::drawCustomCharacters(drawCharFunction, this->getContent().c_str(), this->getContent().length(), position.x, position.y, drawCharFunctionSize, (drawCharFunctionAspectRatio + drawCharFunctionSpacingFactor) * drawCharFunctionSize, this->getColor());
		}

		inline float getHeight()
		{
			if(this->getFont() != null) return DrawableText::getHeight();
			else return drawCharFunctionSize;
		}

		inline float getWidth()
		{
			if(this->getFont() != null) return isMonospaced? this->getContent().length() * this->getFont()->getTextWidth("0") : DrawableText::getWidth();  // TODO replace this with monospaced "mode" in DrawableText
			else return getDrawCharFunctionTextWidth(this->getContent());
		}

		inline GUIDrawableText& operator=(const std::string& str)
		{
			this->setContent(str);
			return *this;
		}

		inline float getDrawCharFunctionTextWidth(const std::string& text) const { return (drawCharFunctionAspectRatio + drawCharFunctionSpacingFactor) * drawCharFunctionSize * text.length(); }
	};

	/** A simple struct that can be used to draw a label with drawable text content. */
	struct Label extends PlainComponent
	{
		GUIDrawableText text;

		/// The minimum spacing (x, y) between the border and the text.
		Vector2D padding;

		enum Alignment { ALIGN_CENTER,
						 ALIGN_LEADING,
						 ALIGN_TRAILING
		} textHorizontalAlignment, textVerticalAlignment;

		Label() : PlainComponent(), text(), padding(), textHorizontalAlignment(), textVerticalAlignment() {}

		void draw();

		/** Sets a suggested size (width, height) for this Label, according to current text content and font size. */
		void pack();

		protected:

		float alignmentHorizontalOffset(float contentWidth);
		inline float alignmentHorizontalOffset() { return alignmentHorizontalOffset(text.getWidth()); }

		float alignmentVerticalOffset(float contentHeight);
		inline float alignmentVerticalOffset()   { return alignmentVerticalOffset(text.getHeight()); }
	};

	/** A simple struct that can be used to draw a text field, which can store arbitrarily large content.
	 *  Optionally, the text field can be drawn with caret and some input functionality. */
	struct TextField extends Label
	{
		/// the content of this text field (this is different from the Label-inherited drawable text, which controls what is effectively drawn to screen)
		std::string content;

		/// the position of the caret (aka text input cursor) as an index of the content string (therefore not a pixel position)
		unsigned caretPosition;

		/// the color of the caret
		Color caretColor;

		/// indicates whether to draw the caret at all
		bool caretVisible;

		/// specifies how many times the caret blinks per second (...hertz?)
		float caretBlinkRate;

		/** Behavior when content does not fit in this text field. When truncated, the content's string is truncated to
		 *  fit when drawing (original string content is left intact). When abbreviated, a ellipsis (...) will be drawn
		 *  where the text was supposed to be truncated (note that it takes 5 more chars more for the ellipsis).
		 *  TODO implement clipping behavior, which would draw the partially inside char clipped (instead of none). */
		enum OverflowBehavior {
			OVERFLOW_LET_IT_BE,            /// lets content exceed boundaries
			OVERFLOW_ABBREVIATE_LEADING,   /// text is abbreviated at its beginning to fit the field bounds
			OVERFLOW_ABBREVIATE_TRAILING,  /// text is abbreviated at its end to fit the field bounds
			OVERFLOW_TRUNCATE_LEADING,     /// text is truncated at its beginning to fit the field bounds
			OVERFLOW_TRUNCATE_TRAILING,    /// text is truncated at its end to fit the field bounds
			//OVERFLOW_CLIP_LEADING,       /// text is clipped at its beginning to fit the field bounds
			//OVERFLOW_CLIP_TRAILING,      /// text is clipped at its end to fit the field bounds
		} overflowBehavior;  /// Specifies behavior when text content does not fit in this text field horizontally

		TextField()
		: Label(), caretPosition(), caretColor(Color::WHITE), caretVisible(true), caretBlinkRate(3.125f),
		  overflowBehavior() { textHorizontalAlignment = ALIGN_LEADING; }

		void draw();

		/** Updates the drawable text of this field, according to current content and overflow behavior. */
		void updateDrawableText();

		/** Sets a suggested size (width, height) for this TextField, according to current text content and font size. */
		void pack();

		/** Sets a suggested height for this TextField, according to current font size and given content width. */
		void packToWidth(float width);

		void onKeyPressed(Keyboard::Key k);
		void onMouseButtonPressed(Mouse::Button, float x, float y);

		/** Returns a sub-string of given 'content' string that fits inside the given Label. The optional overflow argument specifies how to obtain the substring. */
		static std::string getTextContentFitFor(const Label& field, const std::string& content, OverflowBehavior=OVERFLOW_TRUNCATE_LEADING);
	};

	/** A simple struct that can be used to draw a button with some input functionality. */
	struct Button extends Label
	{
		bool highlighted;
		float highlightSpacing;
		Color highlightColor;
		float highlightPeriod;

		Button() : Label(), highlighted(), highlightSpacing(), highlightColor(Color::_TRANSPARENT), highlightPeriod(0) {}
		void draw();
	};

	struct IconButton extends Button
	{
		Image* icon;
		Vector2D iconScale;

		IconButton() : Button(), icon(null) { iconScale.x = iconScale.y = 1; }
		void draw();
	};

	struct ArrowIconButton extends Button
	{
		enum ArrowOrientation { ARROW_UP, ARROW_DOWN, ARROW_LEFT, ARROW_RIGHT } arrowOrientation;
		Color arrowColor;

		ArrowIconButton() : Button(), arrowOrientation(), arrowColor(backgroundColor.getDarker()) {}
		void draw();
	};

	/** A simple container that can be used to hold references to drawable objects and compose GUI elements. */
	class Panel extends public PlainComponent
	{
		protected:
		/// Base class to be used by a wrapper to a drawable object
		struct ComponentHandler
		{
			Rectangle& bounds;
			ComponentHandler(Rectangle& bounds) : bounds(bounds) {}
			virtual ~ComponentHandler(){}
			virtual void draw() const = 0;
			virtual bool handles(const void* address) const = 0;
			virtual inline void checkHovered(bool isHovered){};
			virtual inline void setHoveringDisabled(bool whether){};
		};

		std::vector<ComponentHandler*> components;

		public:
		bool hoveringDisabled;

		Panel() : PlainComponent(), hoveringDisabled()
		{
			borderDisabled = true;
		}

		virtual ~Panel();
		virtual void draw();

		/// Wrapper class to a drawable object. The referenced object must have a public method with the signature `void draw()`.
		protected:template<typename Component>
		struct BasicComponentHandler extends ComponentHandler  // @suppress("Class has a virtual method and non-virtual destructor")
		{
			Component& drawable;
			BasicComponentHandler(Component& drawable) : ComponentHandler(drawable.bounds), drawable(drawable) {}
			virtual inline void draw() const { drawable.draw(); }
			virtual inline bool handles(const void* address) const { return &drawable == address; }
		};

		// adds to this panel a reference to the given object
		public:template<typename Component>
		void addComponent(Component& drawable)
		{
			components.push_back(new BasicComponentHandler<Component>(drawable));
		}

		/// Same as the BasicComponentHandler class, but references objects that also contain a public field named `highlighted` (of bool type, preferably)
		protected:template<typename HighlightableComponent>
		struct HighlightableComponentHandler extends BasicComponentHandler<HighlightableComponent>
		{
			HighlightableComponentHandler(HighlightableComponent& drawable) : BasicComponentHandler<HighlightableComponent>(drawable) {}
			virtual inline void checkHovered(bool whether){ BasicComponentHandler<HighlightableComponent>::drawable.highlighted = whether; };
		};

		// adds to this panel a reference to the given object
		public:template<typename HighlightableComponent>
		void addHighlightableComponent(HighlightableComponent& drawable)
		{
			components.push_back(new HighlightableComponentHandler<HighlightableComponent>(drawable));
		}
	};

	// specialization of Panel::addHighlightableComponent() for the Panel class itself
	template<>
	struct Panel::BasicComponentHandler<Panel> extends ComponentHandler  // @suppress("Class has a virtual method and non-virtual destructor")
	{
		Panel& panel;
		BasicComponentHandler(Panel& panel) : ComponentHandler(panel.bounds), panel(panel) {}
		virtual inline void draw() const { panel.draw(); }
		virtual inline bool handles(const void* address) const { return &panel == address; }
		virtual inline void setHoveringDisabled(bool whether){ panel.hoveringDisabled = whether; };
	};

	/** A simple container of Panel's that switches the visibility of referenced Panel objects through tab buttons. */
	class TabbedPane extends public Label
	{
		protected:
		struct Tab
		{
			Panel* panel;
			Button button;
		};

		unsigned activeTabIndex;
		std::vector<Tab> tabs;

		public:
		bool hoveringDisabled;

		TabbedPane() : activeTabIndex(), hoveringDisabled() {}

		void draw();

		/** Sets a suggested size (width, height) for this TabbedPane's tab buttons, according to current labels and font size. */
		void pack();

		/** Adds a reference to the given tab and also creates a tab button to enable it. */
		void addTab(Panel& component, std::string label="");

		/** Sets the given panel as active, if it is in this TabbedPane. Otherwise does nothing. Returns true if the given panel was set as active. */
		bool setActiveTab(Panel& panel);

		/** Sets the tab at given index as active. Throws a std::out_of_range exception if `index` is out of bounds. */
		void setActiveTabByIndex(unsigned index);

		/** Sets the tab under the given position as active, if any. Returns true if the a panel was under the given position. */
		bool setActiveTabByButtonPosition(float x, float y);
		inline bool setActiveTabByButtonPosition(const Point& position) { return setActiveTabByButtonPosition(position.x, position.y); }

		/// Returns true if given panel is the active panel in this TabbedPane.
		inline bool isActiveTab(const Panel& panel) { return &panel == tabs[activeTabIndex].panel; }

		/** Returns the active tab index */
		inline unsigned getActiveTabIndex() { return activeTabIndex; }

		/** Returns a reference to the tab at specified index */
		inline Tab& getTabAt(unsigned index) { return tabs[index < tabs.size()? index : 0]; }

		/** Returns a reference to active tab */
		inline Tab& getActiveTab() { return tabs[activeTabIndex]; }
	};

	// specialization of Panel::addHighlightableComponent() for the TabbedPane class
	template<>
	struct Panel::BasicComponentHandler<TabbedPane> extends ComponentHandler  // @suppress("Class has a virtual method and non-virtual destructor")
	{
		TabbedPane& tabbedPane;
		BasicComponentHandler(TabbedPane& tabbedPane) : ComponentHandler(tabbedPane.bounds), tabbedPane(tabbedPane) {}
		virtual inline void draw() const { tabbedPane.draw(); }
		virtual inline bool handles(const void* address) const { return &tabbedPane == address; }
		virtual inline void setHoveringDisabled(bool whether){ tabbedPane.hoveringDisabled = whether; };
	};

	/** Very simple menu class to draw very simple menus. */
	class Menu extends public Label
	{
		public:
		struct Entry
		{
			std::string label;
			bool enabled;
			void* userData;

			protected:
			GUIDrawableText text;
			friend class Menu;
		};

		protected:

		// the menu's entries
		std::vector<Entry> entries;

		// the index of the currently selected index
		unsigned selectedIndex;

		// the amount of vertical scrolling
		unsigned scrollIndexOffset;

		float getEntryHeight(), getEntryTextHeight(), getEntrySpacing(), getTitleHeight();

		public:

		// (optional) the menu's title
		GUIDrawableText title;

		// the color to draw the focused entry's text (default=white)
		Color focusedEntryFontColor;

		// the color to draw the focused entry's background (default=entryColor)
		Color focusedEntryBgColor;

		// the color to draw disabled entries' text (default=grey)
		Color disabledEntryTextColor;

		enum LayoutMode { PACK_ENTRIES, STRETCH_SPACING } layoutMode;
		float entrySpacing; // the spacing between entries
		bool entrySpacingIsRelative; // if true, 'entrySpacing' is a fraction relative to the font's size

		// if true, the menu will be vertically scrolled when the number of entries is big enough to exceed the menu's height. (default=true)
		bool scrollVerticalEnabled;

		// specifies behavior when entries' labels does not fit in this menu horizontally (default=OVERFLOW_ABBREVIATE_TRAILING)
		TextField::OverflowBehavior entryOverflowBehavior;

		// if true, when the cursor is moved up at the zero-index, it will be moved to last index, and vice-versa. (default=false)
		bool cursorWrapAroundEnabled;

		Menu();

		/** Adds an entry to this menu, optionally at the given index, with optional user data pointer.
		 *  If index is not specified, or negative, the entry is created at the end of the menu. */
		void addEntry(std::string label, int index=-1, void* userData=null);

		/** Remove from this menu the entry at the given index. if the given index is out of bounds, nothing is done and
		 *  the menu is left unchanged. Note that the data pointed by the userData pointer is not deleted. */
		void removeEntry(unsigned index);

		/** Remove all entries from this menu. Note that the data pointed by the userData pointer is not deleted. */
		inline void clearEntries() { entries.clear(), selectedIndex = 0; }

		/** Returns a read-only reference to the entries on this menu. */
		const std::vector<Entry>& getEntries() const;

		/** Returns a reference to the entry at the given index. */
		Entry& getEntryAt(unsigned index);

		/** Sets the current entry. If the given entry is not on this menu, nothing is done and the menu is left unchanged. */
		void setSelectedEntry(const Entry& entry);

		/** Returns a reference to the current entry. */
		Entry& getSelectedEntry();

		/** Safe way to set the selected index */
		void setSelectedIndex(unsigned index);

		/** Returns the index of the current entry. */
		unsigned getSelectedIndex();

		/** Decrement the selected index in a safe way, moving the selected entry up. */
		void moveCursorUp();

		/** Increment the selected index in a safe way, moving the selected entry down. */
		void moveCursorDown();

		/** Sets this menu's main font, used to draw the entries' text, replacing any previous font.
		 *  The font won't be deleted when this menu is destroyed. */
		void setFont(Font* font);

		/** Returns a reference to the menu's main font. */
		inline Font* getFont() { return text.getFont(); }

		/** Sets the color of this menu. The meaning of this is somewhat vague but it's the same as the color argument in the
		 *  Menu class constructor: an overall color theme.
		 *  For fine-tuning colors, modify the public fields of this class. */
		void setColor(const Color& color);

		/** Returns the menu index of the entry which the given coordinate is inside. If the coordinate is out of bounds, zero is returned.
		 *  Note: use bounds.contains() to check whether the point is inside the menu at all. */
		unsigned getIndexAtLocation(float x, float y);

		/** Returns the menu index of the entry which the given point is inside. If the point is out of bounds, zero is returned.
		 *  Note: use bounds.contains() to check whether the point is inside the menu at all. */
		inline unsigned getIndexAtLocation(const Point& p) { return getIndexAtLocation(p.x, p.y); }

		/** Sets the current index of the menu as the entry which the given coordinate is inside. It does nothing if the coordinate is out of bounds. */
		void setSelectedIndexByLocation(float x, float y) 	{ if(bounds.contains(x, y) and not entries.empty()) setSelectedIndex(getIndexAtLocation(x, y)); }

		/** Sets the current index of the menu as the entry which the given point is inside. It does nothing if the point is out of bounds. */
		inline void setSelectedIndexByLocation(const Point& p) { setSelectedIndexByLocation(p.x, p.y); }

		/** Draw the menu according the menu bounds and number of entries */
		void draw();

		/** Updates scrolling position. This is normally called automatically where needed except when modifying this menu's bounds, which may require a call to this method manually. */
		void updateScroll();

		/** Updates the drawable text of the given entry, according to its current label and this menu's overflow behavior. */
		void updateDrawableText(Entry& entry);

		/** Updates the drawable text of the entry at given index, according to its current label and this menu's overflow behavior. */
		void updateDrawableText(unsigned index);

		/** Updates the drawable text of this menu's entries, according to their current label and this menu's overflow behavior. */
		void updateDrawableText();
	};
}

#endif /* FGEAL_EXTRA_GUI_HPP_ */
