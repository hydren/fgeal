/*
 * image_transfom.hpp
 *
 *  Created on: 30 de ago de 2019
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_IMAGE_TRANSFOM_HPP_
#define FGEAL_EXTRA_IMAGE_TRANSFOM_HPP_

#include "fgeal/image.hpp"

namespace fgeal
{
	struct ImageTransform
	{
		Point position;
		Image::FlipMode flippingMode;
		Vector2D scale;
		float rotationAngle;
		Vector2D rotationCenter;
		Rectangle region;
	};

	struct TransformedImage extends Image, ImageTransform
	{
		TransformedImage(unsigned width, unsigned height)
		: Image(width, height), position(), flippingMode(), scale(), rotationAngle(), rotationCenter(), region() {}

		TransformedImage(const std::string& filename)
		: Image(filename), position(), flippingMode(), scale(), rotationAngle(), rotationCenter(), region() {}

		inline void draw()
		{
			Image::delegateDraw(region.x, region.y, scale.x, scale.y, rotationAngle, rotationCenter.x, rotationCenter.y, flippingMode, region.x, region.y, region.w, region.h);
		}
	};
}

#endif /* FGEAL_EXTRA_IMAGE_TRANSFOM_HPP_ */
