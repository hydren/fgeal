/*
 * primitives.cpp
 *
 *  Created on: 23 de ago de 2022
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "primitives.hpp"

namespace fgeal
{
	namespace primitives
	{
		void drawCharacter(char character, float x, float y, float size, const Color& color)
		{
			switch(character)
			{
				case '0': case 'O': case 'o':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.250f, size * 0.125f, color);
					break;
				case '1':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					break;
				case '2':
					Graphics::drawFilledRectangle(x,                 y + size * 0.375f, size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					break;
				case '3':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.375f, size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case '4':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case '5':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.375f, size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					break;
				case '6':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.375f, size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case '7':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.375f, size * 0.125f, color);
					break;
				case '8':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.250f, size * 0.125f, color);
					break;
				case '9':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case 'A': case 'a':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case 'B': case 'b':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.125f, size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.375f, size * 0.125f, size * 0.625f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.250f, size * 0.125f, color);
					break;
				case 'C': case 'c':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case 'D': case 'd':
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.125f, size * 0.125f, size * 0.750f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.125f, size * 0.125f, size * 0.750f, color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					break;
				case 'E': case 'e':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case 'F': case 'f':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.375f, size * 0.125f, color);
					break;
				case 'G': case 'g':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.375f, size * 0.125f, size * 0.500f, color);
					break;
				case 'H': case 'h':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case 'I': case 'i':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.125f, size * 0.125f, size * 0.750f, color);
					break;
				case 'J': case 'j':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.725f, size * 0.125f, size * 0.125f, color);
					break;
				case 'K': case 'k':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.125f, size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.375f, size * 0.125f, size * 0.625f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case 'L': case 'l':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.375f, size * 0.125f, color);
					break;
				case 'M': case 'm':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.125f, size * 0.250f, size * 0.125f, color);
					break;
				case 'N': case 'n':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					break;
				case 'P': case 'p':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case 'Q': case 'q':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.725f, size * 0.125f, size * 0.125f, color);
					break;
				case 'R': case 'r':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.500f, size * 0.125f, size * 0.275f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.650f, size * 0.125f, size * 0.350f, color);
					break;
				case 'S': case 's':
					Graphics::drawFilledRectangle(x,                 y + size * 0.125f, size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.500f, size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.500f, size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					break;
				case 'T': case 't':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.125f, size * 0.125f, size * 0.875f, color);
					break;
				case 'U': case 'u':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.250f, size * 0.125f, color);
					break;
				case 'V': case 'v':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.800f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.800f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.675f, size * 0.125f, size * 0.325f, color);
					break;
				case 'W': case 'w':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.775f, size * 0.250f, size * 0.125f, color);
					break;
				case 'X': case 'x':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.250f, size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.750f, size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.750f, size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.625f, size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.375f, size * 0.125f, size * 0.250f, color);
					break;
				case 'Y': case 'y':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.375f, size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.500f, size * 0.125f, size * 0.500f, color);
					break;
				case 'Z': case 'z':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.125f, size * 0.125f, size * 0.3125f,color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.325f, size * 0.125f, size * 0.275f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.500f, size * 0.125f, size * 0.3125f,color);
					break;
				case '-':
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case '_':
					Graphics::drawFilledRectangle(x,                 y + size * 0.875f, size * 0.500f, size * 0.125f, color);
					break;
				case '=':
					Graphics::drawFilledRectangle(x + size * 0.100f, y + size * 0.325f, size * 0.300f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.100f, y + size * 0.675f, size * 0.300f, size * 0.125f, color);
					break;
				case '\'':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y,                 size * 0.125f, size * 0.250f, color);
					break;
				case '"':
					Graphics::drawFilledRectangle(x + size * 0.0625f,y,                 size * 0.125f, size * 0.250f, color);
					Graphics::drawFilledRectangle(x + size * 0.3125f,y,                 size * 0.125f, size * 0.250f, color);
					break;
				case '*':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.250f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.375f, size * 0.250f, size * 0.125f, color);
					break;
				case '?':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.375f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.375f, size * 0.125f, size * 0.325f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case '.':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case ',':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.750f, size * 0.125f, size * 0.250f, color);
					break;
				case '[': case '{':
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y,                 size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case '(':
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.125f, size * 0.125f, size * 0.750f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y,                 size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case ']': case '}':
					Graphics::drawFilledRectangle(x + size * 0.250f, y,                 size * 0.125f, size,          color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case ')':
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.125f, size * 0.125f, size * 0.750f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y,                 size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case ':':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.325f, size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.675f, size * 0.125f, size * 0.125f, color);
					break;
				case ';':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.325f, size * 0.125f, size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.750f, size * 0.125f, size * 0.250f, color);
					break;
				case '+':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.250f, size * 0.125f, size * 0.500f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.4375f,size * 0.1875f,size * 0.125f, color);
					Graphics::drawFilledRectangle(x + size * 0.3125f,y + size * 0.4375f,size * 0.1875f,size * 0.125f, color);
					break;
				case '!':
					Graphics::drawFilledRectangle(x + size * 0.1875f,y,                 size * 0.125f, size * 0.700f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.875f, size * 0.125f, size * 0.125f, color);
					break;
				case '/':
					Graphics::drawFilledRectangle(x + size * 0.375f, y,                 size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.200f, size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.400f, size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x,                 y + size * 0.600f, size * 0.125f, size * 0.375f, color);
					break;
				case '\\':
					Graphics::drawFilledRectangle(x,                 y,                 size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.125f, y + size * 0.200f, size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.250f, y + size * 0.400f, size * 0.125f, size * 0.375f, color);
					Graphics::drawFilledRectangle(x + size * 0.375f, y + size * 0.600f, size * 0.125f, size * 0.375f, color);
					break;
				case '<':
					Graphics::drawFilledRectangle(x + size * 0.3125f,y + size * 0.125f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.275f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.0625f,y + size * 0.425f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.575f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.3125f,y + size * 0.725f, size * 0.125f, size * 0.150f, color);
					break;
				case '>':
					Graphics::drawFilledRectangle(x + size * 0.0625f,y + size * 0.125f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.275f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.3125f,y + size * 0.425f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.1875f,y + size * 0.575f, size * 0.125f, size * 0.150f, color);
					Graphics::drawFilledRectangle(x + size * 0.0625f,y + size * 0.725f, size * 0.125f, size * 0.150f, color);
					break;
				default: break;
			}
		}

		void drawDigit(unsigned char digit, float x, float y, float size, const Color& color)
		{
			if(digit < 10)
				drawCharacter(digit + '0', x, y, size, color);
			else
				drawCharacter(digit - 10 + 'A', x, y, size, color);
		}

		void drawSevenSegmentDisplay(unsigned char flags, float x, float y, float s, const Color& color)
		{
			if(flags & SSD_SEGMENT_A) {
				const Point ptArr[6] = {{x+0.1250f*s, y+0.0625f*s}, {x+0.1875f*s, y+0.0000f*s}, {x+0.3750f*s, y+0.0000f*s}, {x+0.4375f*s, y+0.0625f*s}, {x+0.3750f*s, y+0.1250f*s}, {x+0.1875f*s, y+0.1250f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_B) {
				const Point ptArr[6] = {{x+0.5000f*s, y+0.1250f*s}, {x+0.4375f*s, y+0.1875f*s}, {x+0.4375f*s, y+0.3750f*s}, {x+0.5000f*s, y+0.4375f*s}, {x+0.5625f*s, y+0.3750f*s}, {x+0.5625f*s, y+0.1875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_C) {
				const Point ptArr[6] = {{x+0.5000f*s, y+0.5625f*s}, {x+0.4375f*s, y+0.6250f*s}, {x+0.4375f*s, y+0.8125f*s}, {x+0.5000f*s, y+0.8750f*s}, {x+0.5625f*s, y+0.8125f*s}, {x+0.5625f*s, y+0.6250f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_D) {
				const Point ptArr[6] = {{x+0.1250f*s, y+0.9375f*s}, {x+0.1875f*s, y+0.8750f*s}, {x+0.3750f*s, y+0.8750f*s}, {x+0.4375f*s, y+0.9375f*s}, {x+0.3750f*s, y+1.0000f*s}, {x+0.1875f*s, y+1.0000f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_E) {
				const Point ptArr[6] = {{x+0.0625f*s, y+0.5625f*s}, {x+0.0000f*s, y+0.6250f*s}, {x+0.0000f*s, y+0.8125f*s}, {x+0.0625f*s, y+0.8750f*s}, {x+0.1250f*s, y+0.8125f*s}, {x+0.1250f*s, y+0.6250f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_F) {
				const Point ptArr[6] = {{x+0.0625f*s, y+0.1250f*s}, {x+0.0000f*s, y+0.1875f*s}, {x+0.0000f*s, y+0.3750f*s}, {x+0.0625f*s, y+0.4375f*s}, {x+0.1250f*s, y+0.3750f*s}, {x+0.1250f*s, y+0.1875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & SSD_SEGMENT_G) {
				const Point ptArr[6] = {{x+0.1250f*s, y+0.5000f*s}, {x+0.1875f*s, y+0.4375f*s}, {x+0.3750f*s, y+0.4375f*s}, {x+0.4375f*s, y+0.5000f*s}, {x+0.3750f*s, y+0.5625f*s}, {x+0.1875f*s, y+0.5625f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
		}

		static unsigned char SSD_FLAG_CHAR_0 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F,
							 SSD_FLAG_CHAR_1 = SSD_SEGMENT_B | SSD_SEGMENT_C,
							 SSD_FLAG_CHAR_2 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_3 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_4 = SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_5 = SSD_SEGMENT_A | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_6 = SSD_SEGMENT_A | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_7 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_F,
							 SSD_FLAG_CHAR_8 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_9 = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_A = SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_B = SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_C = SSD_SEGMENT_A | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F,
							 SSD_FLAG_CHAR_D = SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_E = SSD_SEGMENT_A | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G,
							 SSD_FLAG_CHAR_F = SSD_SEGMENT_A | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;

		#define defineDigitToFlags(FLAG_PREFIX)\
		static inline unsigned char digitTo##FLAG_PREFIX##Flags(unsigned char digit)\
		{\
			switch(digit)\
			{\
				case 0: return FLAG_PREFIX##_FLAG_CHAR_0;\
				case 1: return FLAG_PREFIX##_FLAG_CHAR_1;\
				case 2: return FLAG_PREFIX##_FLAG_CHAR_2;\
				case 3: return FLAG_PREFIX##_FLAG_CHAR_3;\
				case 4: return FLAG_PREFIX##_FLAG_CHAR_4;\
				case 5: return FLAG_PREFIX##_FLAG_CHAR_5;\
				case 6: return FLAG_PREFIX##_FLAG_CHAR_6;\
				case 7: return FLAG_PREFIX##_FLAG_CHAR_7;\
				case 8: return FLAG_PREFIX##_FLAG_CHAR_8;\
				case 9: return FLAG_PREFIX##_FLAG_CHAR_9;\
				/* hexadecimal digit extensions*/\
				case 10: return FLAG_PREFIX##_FLAG_CHAR_A;\
				case 11: return FLAG_PREFIX##_FLAG_CHAR_B;\
				case 12: return FLAG_PREFIX##_FLAG_CHAR_C;\
				case 13: return FLAG_PREFIX##_FLAG_CHAR_D;\
				case 14: return FLAG_PREFIX##_FLAG_CHAR_E;\
				case 15: return FLAG_PREFIX##_FLAG_CHAR_F;\
				default: return FLAG_PREFIX##_SEGMENT_A;\
			}\
		}

		defineDigitToFlags(SSD);
		void drawSevenSegmentDigit(unsigned char digit, float x, float y, float size, const Color& color)
		{
			drawSevenSegmentDisplay(digitToSSDFlags(digit), x, y, size, color);
		}

		inline static unsigned char characterToSevenSegmentDisplayFlags(char character)
		{
			switch(character)
			{
				case ' ': return 0;
				case '0': return SSD_FLAG_CHAR_0;
				case '1': return SSD_FLAG_CHAR_1;
				case '2': return SSD_FLAG_CHAR_2;
				case '3': return SSD_FLAG_CHAR_3;
				case '4': return SSD_FLAG_CHAR_4;
				case '5': return SSD_FLAG_CHAR_5;
				case '6': return SSD_FLAG_CHAR_6;
				case '7': return SSD_FLAG_CHAR_7;
				case '8': return SSD_FLAG_CHAR_8;
				case '9': return SSD_FLAG_CHAR_9;
				case 'A': case 'a': return SSD_FLAG_CHAR_A;
				case 'B': case 'b': return SSD_FLAG_CHAR_B;
				case 'C': case 'c': return SSD_FLAG_CHAR_C;
				case 'D': case 'd': return SSD_FLAG_CHAR_D;
				case 'E': case 'e': return SSD_FLAG_CHAR_E;
				case 'F': case 'f': return SSD_FLAG_CHAR_F;
				case 'G': case 'g': return SSD_SEGMENT_A | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'H': case 'h': return SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'I': case 'i': return SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'J': case 'j': return SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D;
				case 'K': case 'k': return SSD_SEGMENT_A | SSD_SEGMENT_C | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'L': case 'l': return SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'M': case 'm': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_D | SSD_SEGMENT_F;
				case 'N': case 'n': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'O': case 'o': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'P': case 'p': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'Q': case 'q': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'R': case 'r': return SSD_SEGMENT_E | SSD_SEGMENT_G;
				case 'S': case 's': return SSD_FLAG_CHAR_5;
				case 'T': case 't': return SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'U': case 'u': return SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F;
				case 'V': case 'v': return SSD_SEGMENT_B | SSD_SEGMENT_D | SSD_SEGMENT_F;
				case 'W': case 'w': return SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_E | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'X': case 'x': return SSD_SEGMENT_A | SSD_SEGMENT_D | SSD_SEGMENT_G;
				case 'Y': case 'y': return SSD_SEGMENT_B | SSD_SEGMENT_C | SSD_SEGMENT_D | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case 'Z': case 'z': return SSD_FLAG_CHAR_2;
				case '-': return SSD_SEGMENT_G;
				case '_': return SSD_SEGMENT_D;
				case '=': return SSD_SEGMENT_G | SSD_SEGMENT_D;
				case '\'': return SSD_SEGMENT_F;
				case '"': return SSD_SEGMENT_B | SSD_SEGMENT_F;
				case '*': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_F | SSD_SEGMENT_G;
				case '?': return SSD_SEGMENT_A | SSD_SEGMENT_B | SSD_SEGMENT_G | SSD_SEGMENT_E;
				case ',': return SSD_SEGMENT_E;
				case '<': return SSD_SEGMENT_A | SSD_SEGMENT_F;
				case '>': return SSD_SEGMENT_B | SSD_SEGMENT_G;
				default: return SSD_SEGMENT_A;
			}
		}

		void drawSevenSegmentCharacter(char character, float x, float y, float size, const Color& color)
		{
			drawSevenSegmentDisplay(characterToSevenSegmentDisplayFlags(character), x, y, size, color);
		}

		void drawFourteenSegmentDisplay(unsigned short flags, float x, float y, float s, const Color& color)
		{
			if(flags & FSD_SEGMENT_A) {
				const Point ptArr[6] = {{x+0.046875f*s, y+0.03125f*s}, {x+0.078125f*s, y+0.0000f*s}, {x+0.484375f*s, y+0.0000f*s}, {x+0.515625f*s, y+0.03125f*s}, {x+0.484375f*s, y+0.0625f*s}, {x+0.078125f*s, y+0.0625f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_B) {
				const Point ptArr[6] = {{x+0.53125f*s, y+0.04875f*s}, {x+0.500000f*s, y+0.078125f*s}, {x+0.500000f*s, y+0.453125f*s}, {x+0.53125f*s, y+0.484375f*s}, {x+0.5625f*s, y+0.453125f*s}, {x+0.5625f*s, y+0.078125f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_C) {
				const Point ptArr[6] = {{x+0.53125f*s, y+0.515625f*s}, {x+0.500000f*s, y+0.546875f*s}, {x+0.500000f*s, y+0.921875f*s}, {x+0.53125f*s, y+0.953125f*s}, {x+0.5625f*s, y+0.921875f*s}, {x+0.5625f*s, y+0.546875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_D) {
				const Point ptArr[6] = {{x+0.046875f*s, y+0.96875f*s}, {x+0.078125f*s, y+0.9375f*s}, {x+0.484375f*s, y+0.9375f*s}, {x+0.515625f*s, y+0.96875f*s}, {x+0.484375f*s, y+1.0000f*s}, {x+0.078125f*s, y+1.0000f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_E) {
				const Point ptArr[6] = {{x+0.03125f*s, y+0.515625f*s}, {x+0.0000f*s, y+0.546875f*s}, {x+0.0000f*s, y+0.921875f*s}, {x+0.03125f*s, y+0.953125f*s}, {x+0.0625f*s, y+0.921875f*s}, {x+0.0625f*s, y+0.546875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_F) {
				const Point ptArr[6] = {{x+0.03125f*s, y+0.04875f*s}, {x+0.0000f*s, y+0.078125f*s}, {x+0.0000f*s, y+0.453125f*s}, {x+0.03125f*s, y+0.484375f*s}, {x+0.0625f*s, y+0.453125f*s}, {x+0.0625f*s, y+0.078125f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_G1) {
				const Point ptArr[6] = {{x+0.046875f*s, y+0.5000f*s}, {x+0.078125f*s, y+0.46875f*s}, {x+0.234375f*s, y+0.46875f*s}, {x+0.265625f*s, y+0.5000f*s}, {x+0.234375f*s, y+0.53125f*s}, {x+0.078125f*s, y+0.53125f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_G2) {
				const Point ptArr[6] = {{x+0.296875f*s, y+0.5000f*s}, {x+0.328125f*s, y+0.46875f*s}, {x+0.484375f*s, y+0.46875f*s}, {x+0.515625f*s, y+0.5000f*s}, {x+0.484375f*s, y+0.53125f*s}, {x+0.328125f*s, y+0.53125f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_H) {
				const Point ptArr[6] = {{x+0.078125f*s, y+0.078125f*s}, {x+0.078125f*s, y+0.125000f*s}, {x+0.187500f*s, y+0.453125f*s}, {x+0.234375f*s, y+0.453125f*s}, {x+0.234375f*s, y+0.406250f*s}, {x+0.125000f*s, y+0.078125f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_I) {
				const Point ptArr[6] = {{x+0.281250f*s, y+0.078125f*s}, {x+0.250000f*s, y+0.109375f*s}, {x+0.250000f*s, y+0.453125f*s}, {x+0.281250f*s, y+0.484375f*s}, {x+0.312500f*s, y+0.453125f*s}, {x+0.312500f*s, y+0.109375f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_J) {
				const Point ptArr[6] = {{x+0.484375f*s, y+0.078125f*s}, {x+0.437500f*s, y+0.078125f*s}, {x+0.328125f*s, y+0.406250f*s}, {x+0.328125f*s, y+0.453125f*s}, {x+0.375000f*s, y+0.453125f*s}, {x+0.484375f*s, y+0.125000f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_K) {
				const Point ptArr[6] = {{x+0.328125f*s, y+0.546875f*s}, {x+0.328125f*s, y+0.593750f*s}, {x+0.437500f*s, y+0.921875f*s}, {x+0.484375f*s, y+0.921875f*s}, {x+0.484375f*s, y+0.875000f*s}, {x+0.375000f*s, y+0.546875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_L) {
				const Point ptArr[6] = {{x+0.281250f*s, y+0.515625f*s}, {x+0.250000f*s, y+0.546875f*s}, {x+0.250000f*s, y+0.890625f*s}, {x+0.281250f*s, y+0.921875f*s}, {x+0.312500f*s, y+0.890625f*s}, {x+0.312500f*s, y+0.546875f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
			if(flags & FSD_SEGMENT_M) {
				const Point ptArr[6] = {{x+0.234375f*s, y+0.546875f*s}, {x+0.187500f*s, y+0.546875f*s}, {x+0.078125f*s, y+0.875000f*s}, {x+0.078125f*s, y+0.921875f*s}, {x+0.125000f*s, y+0.921875f*s}, {x+0.234375f*s, y+0.593750f*s}};
				Graphics::drawFilledPolygon(ptArr, 6, color);
			}
		}

		static unsigned short FSD_FLAG_CHAR_0 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_J | FSD_SEGMENT_M,
							  FSD_FLAG_CHAR_1 = FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_J,
							  FSD_FLAG_CHAR_2 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_3 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_4 = FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_5 = FSD_SEGMENT_A | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_6 = FSD_SEGMENT_A | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_7 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_F,
							  FSD_FLAG_CHAR_8 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_9 = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_A = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_B = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_G2| FSD_SEGMENT_I | FSD_SEGMENT_L,
							  FSD_FLAG_CHAR_C = FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F,
							  FSD_FLAG_CHAR_D = FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_I | FSD_SEGMENT_L,
							  FSD_FLAG_CHAR_E = FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1 | FSD_SEGMENT_G2,
							  FSD_FLAG_CHAR_F = FSD_SEGMENT_A | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2;

		defineDigitToFlags(FSD);
		void drawFourteenSegmentDigit(unsigned char digit, float x, float y, float size, const Color& color)
		{
			drawFourteenSegmentDisplay(digitToFSDFlags(digit), x, y, size, color);
		}

		inline static unsigned short characterToFourteenSegmentDisplayFlags(char character)
		{
			switch(character)
			{
				case ' ': return 0;
				case '0': return FSD_FLAG_CHAR_0;
				case '1': return FSD_FLAG_CHAR_1;
				case '2': return FSD_FLAG_CHAR_2;
				case '3': return FSD_FLAG_CHAR_3;
				case '4': return FSD_FLAG_CHAR_4;
				case '5': return FSD_FLAG_CHAR_5;
				case '6': return FSD_FLAG_CHAR_6;
				case '7': return FSD_FLAG_CHAR_7;
				case '8': return FSD_FLAG_CHAR_8;
				case '9': return FSD_FLAG_CHAR_9;
				case 'A': return FSD_FLAG_CHAR_A;
				case 'B': return FSD_FLAG_CHAR_B;
				case 'C': return FSD_FLAG_CHAR_C;
				case 'D': return FSD_FLAG_CHAR_D;
				case 'E': return FSD_FLAG_CHAR_E;
				case 'F': return FSD_FLAG_CHAR_F;
				case 'G': return FSD_SEGMENT_A | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G2;
				case 'H': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'I': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_I | FSD_SEGMENT_L;
				case 'J': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E;
				case 'K': return FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_J | FSD_SEGMENT_K;
				case 'L': return FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F;
				case 'M': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_H | FSD_SEGMENT_J;
				case 'N': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_H | FSD_SEGMENT_K;
				case 'O': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F;
				case 'P': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'Q': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_K;
				case 'R': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_K;
				case 'S': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_K;
				case 'T': return FSD_SEGMENT_A | FSD_SEGMENT_I | FSD_SEGMENT_L;
				case 'U': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F;
				case 'V': return FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_J | FSD_SEGMENT_M;
				case 'W': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_K | FSD_SEGMENT_M;
				case 'x': case 'X': return FSD_SEGMENT_H | FSD_SEGMENT_J | FSD_SEGMENT_K | FSD_SEGMENT_M;
				case 'Y': return FSD_SEGMENT_B | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_L;
				case 'Z': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_J | FSD_SEGMENT_M;
				case 'a': return FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_L;
				case 'b': return FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'c': return FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'd': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_G2;;
				case 'e': return FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_M;
				case 'f': return FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_J | FSD_SEGMENT_L;
				case 'g': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_H | FSD_SEGMENT_G2;
				case 'h': return FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'i': return FSD_SEGMENT_L;
				case 'j': return FSD_SEGMENT_C | FSD_SEGMENT_D;
				case 'k': return FSD_SEGMENT_I | FSD_SEGMENT_J | FSD_SEGMENT_K | FSD_SEGMENT_L;
				case 'l': return FSD_SEGMENT_E | FSD_SEGMENT_F;
				case 'm': return FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_L;
				case 'n': return FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_L;
				case 'o': return FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case 'p': return FSD_SEGMENT_A | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_J;
				case 'q': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_G2| FSD_SEGMENT_H;
				case 'r': return FSD_SEGMENT_E | FSD_SEGMENT_G1;
				case 's': return FSD_SEGMENT_D | FSD_SEGMENT_G2| FSD_SEGMENT_K;
				case 't': return FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G1;
				case 'u': return FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_E;
				case 'v': return FSD_SEGMENT_E | FSD_SEGMENT_M;
				case 'w': return FSD_SEGMENT_C | FSD_SEGMENT_E | FSD_SEGMENT_K | FSD_SEGMENT_M;
				case 'y': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_G2| FSD_SEGMENT_H;
				case 'z': return FSD_SEGMENT_D | FSD_SEGMENT_G1| FSD_SEGMENT_M;
				case '-': return FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case '_': return FSD_SEGMENT_D;
				case '=': return FSD_SEGMENT_D | FSD_SEGMENT_G1| FSD_SEGMENT_G2;
				case '\'': return FSD_SEGMENT_I;
				case '"': return FSD_SEGMENT_B | FSD_SEGMENT_I;
				case '*': return FSD_SEGMENT_H | FSD_SEGMENT_I | FSD_SEGMENT_J | FSD_SEGMENT_K | FSD_SEGMENT_L | FSD_SEGMENT_M;
				case '?': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_G2| FSD_SEGMENT_L;
				case ',': return FSD_SEGMENT_M;
				case '<': return FSD_SEGMENT_J | FSD_SEGMENT_K;
				case '>': return FSD_SEGMENT_H | FSD_SEGMENT_M;
				case '+': return FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_I | FSD_SEGMENT_L;
				case '/': return FSD_SEGMENT_J | FSD_SEGMENT_M;
				case '$': return FSD_SEGMENT_A | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_I | FSD_SEGMENT_L;
				case '%': return FSD_SEGMENT_C | FSD_SEGMENT_F | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_H | FSD_SEGMENT_J | FSD_SEGMENT_K | FSD_SEGMENT_M;
				case '@': return FSD_SEGMENT_A | FSD_SEGMENT_B | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_F | FSD_SEGMENT_G2| FSD_SEGMENT_I;
				case '&': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_E | FSD_SEGMENT_G1| FSD_SEGMENT_H | FSD_SEGMENT_J | FSD_SEGMENT_K;
				case '^': return FSD_SEGMENT_K | FSD_SEGMENT_M;
				case '#': return FSD_SEGMENT_B | FSD_SEGMENT_C | FSD_SEGMENT_D | FSD_SEGMENT_G1| FSD_SEGMENT_G2| FSD_SEGMENT_I | FSD_SEGMENT_L;
				case '{': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_G1| FSD_SEGMENT_H | FSD_SEGMENT_M;
				case '}': return FSD_SEGMENT_A | FSD_SEGMENT_D | FSD_SEGMENT_G2| FSD_SEGMENT_J | FSD_SEGMENT_K;
				default: return FSD_SEGMENT_A;
			}
		}

		void drawFourteenSegmentCharacter(char character, float x, float y, float size, const Color& color)
		{
			drawFourteenSegmentDisplay(characterToFourteenSegmentDisplayFlags(character), x, y, size, color);
		}
	}
}

