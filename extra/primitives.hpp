/*
 * primitives.hpp
 *
 *  Created on: 23 de ago de 2022
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_EXTRA_PRIMITIVES_HPP_
#define FGEAL_EXTRA_PRIMITIVES_HPP_

#include "fgeal/graphics.hpp"
#include <string>

namespace fgeal
{
	/// Extra graphics primitives, not implemented by any backend adapters directly
	namespace primitives
	{
		// Character primitives. ========================================================================================

		/** Draws a basic character on the screen at given location with the given color. Note that character support is limited. */
		void drawCharacter(char character, float x=0, float y=0, float size=16.f, const Color& color=Color::GREY);

		/** Draws the given characters on the screen at given location with the given color, using a custom character drawing function. */
		inline void drawCustomCharacters(void (*const drawCharacterFunction)(char, float, float, float, const Color&), const char* text, unsigned n, float x=0, float y=0, float size=16.f, float advanceWidth=12.f, const Color& color=Color::GREY)
		{
			for(unsigned i = 0; i < n; i++)
			{
				drawCharacterFunction(text[i], x, y, size, color);
				x += advanceWidth;
			}
		}

		/** Draws the given characters on the screen at given location with the given color, using a custom character drawing function. */
		template<void (*drawCharacterFunction)(char, float, float, float, const Color&)>
		void drawCustomCharacters(const char* text, unsigned n, float x=0, float y=0, float size=16.f, float advanceWidth=12.f, const Color& color=Color::GREY)
		{
			for(unsigned i = 0; i < n; i++)
			{
				drawCharacterFunction(text[i], x, y, size, color);
				x += advanceWidth;
			}
		}

		/** Draws the given characters on the screen at given location with the given color. */
		inline void drawCharacters(const char* str, unsigned n, float x=0, float y=0, float size=16.f, float advanceWidth=12.f, const Color& color=Color::GREY)
		{
			drawCustomCharacters<drawCharacter>(str, n, x, y, size, advanceWidth, color);
		}

		/** Draws the given string on the screen at given location with the given color. */
		inline void drawString(const std::string& text, float x=0, float y=0, float size=16.f, float advanceWidth=12.f, const Color& color=Color::GREY)
		{
			drawCustomCharacters<drawCharacter>(text.c_str(), text.length(), x, y, size, advanceWidth, color);
		}

		// Digit primitives. ========================================================================================

		/** Draws a basic digit (0-9) on the screen at given location with the given color. */
		void drawDigit(unsigned char digit, float x=0, float y=0, float size=16.f, const Color& color=Color::YELLOW);

		/** Draws the given number on the screen at given location with the given color, using a custom digit drawing function. */
		inline void drawCustomDigits(void (*const drawDigitFunction)(unsigned char, float, float, float, const Color&), unsigned number, float x=0, float y=0, float size=16.f, const Color& color=Color::YELLOW)
		{
			std::vector<unsigned> digits;
			do { digits.push_back(number % 10); number /= 10; } while(number > 0);
			for(unsigned i = digits.size(); i > 0; i--)
			{
				drawDigitFunction(digits[i-1], x, y, size, color);
				x += size * 0.75f;
			}
		}

		/** Draws the given number on the screen at given location with the given color, using a custom digit drawing function. */
		template<void (*drawDigitFunction)(unsigned char, float, float, float, const Color&)>
		void drawCustomDigits(unsigned number, float x=0, float y=0, float size=16.f, const Color& color=Color::YELLOW)
		{
			std::vector<unsigned> digits;
			do { digits.push_back(number % 10); number /= 10; } while(number > 0);
			for(unsigned i = digits.size(); i > 0; i--)
			{
				drawDigitFunction(digits[i-1], x, y, size, color);
				x += size * 0.75f;
			}
		}

		/** Draws the given number on the screen at given location with the given color. */
		inline void drawDigits(unsigned number, float x=0, float y=0, float size=16.f, const Color& color=Color::YELLOW)
		{
			drawCustomDigits<drawDigit>(number, x, y, size, color);
		}

		// Seven-segment-display-style digit primitives. ========================================================================================

		/** Draws an imitation of a seven-segment display on the screen at given location with the given color.
		 *  The first argument should contain the bit flags indicating which segments (A to G) to draw. Use the SSD_SEGMENT_x constants to pass an OR'd list of segments. */
		void drawSevenSegmentDisplay(unsigned char flags, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);
		const unsigned char SSD_SEGMENT_A = 1<<0, SSD_SEGMENT_B = 1<<1, SSD_SEGMENT_C = 1<<2, SSD_SEGMENT_D = 1<<3, SSD_SEGMENT_E = 1<<4, SSD_SEGMENT_F = 1<<5, SSD_SEGMENT_G = 1<<6;

		/** Draws a seven-segment display digit (0-9) on the screen at given location with the given color. */
		void drawSevenSegmentDigit(unsigned char digit, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);

		/** Draws a seven-segment display character on the screen at given location with the given color. Note that character support is very limited. */
		void drawSevenSegmentCharacter(char character, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);

		// Fourteen-segment-display-style digit primitives. ========================================================================================

		/** Draws an imitation of a fourteen-segment display on the screen at given location with the given color.
		 *  The first argument should contain the bit flags indicating which segments (A to M) to draw. Use the FSD_SEGMENT_x constants to pass an OR'd list of segments. */
		void drawFourteenSegmentDisplay(unsigned short flags, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);
		const unsigned short FSD_SEGMENT_A = 1<<0, FSD_SEGMENT_B = 1<<1, FSD_SEGMENT_C = 1<<2, FSD_SEGMENT_D = 1<<3, FSD_SEGMENT_E = 1<<4, FSD_SEGMENT_F = 1<<5, FSD_SEGMENT_G1 = 1<<6,
							 FSD_SEGMENT_G2 = 1<<7, FSD_SEGMENT_H = 1<<8, FSD_SEGMENT_I = 1<<9, FSD_SEGMENT_J = 1<<10, FSD_SEGMENT_K = 1<<11, FSD_SEGMENT_L = 1<<12, FSD_SEGMENT_M = 1<<13;

		/** Draws a fourteen-segment display digit (0-9) on the screen at given location with the given color. */
		void drawFourteenSegmentDigit(unsigned char digit, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);

		/** Draws a fourteen-segment display character on the screen at given location with the given color. Note that character support is very limited. */
		void drawFourteenSegmentCharacter(char character, float x=0, float y=0, float size=16.f, const Color& color=Color::RED);
	}
}

#endif /* FGEAL_EXTRA_PRIMITIVES_HPP_ */
