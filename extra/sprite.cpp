/*
 * sprite.cpp
 *
 *  Created on: 16/11/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "sprite.hpp"

#include "fgeal/exceptions.hpp"

#include <climits>

using std::string;

namespace fgeal
{
	inline static unsigned computeRawRowCount(Image& image, unsigned height, int rawOffsetY)
	{
		return height > 0? (image.getHeight() - rawOffsetY) / height : 0;
	}

	inline static unsigned computeRawColumnCount(Image& image, unsigned width, int rawOffsetX)
	{
		return width > 0? (image.getWidth() - rawOffsetX) / width : 0;
	}

	inline static unsigned computeRawFrameCount(Image& image, unsigned width, unsigned height, int rawOffsetX, int rawOffsetY)
	{
		return computeRawRowCount(image, height, rawOffsetY) * computeRawColumnCount(image, width, rawOffsetX);
	}

	Sprite::Sprite(Image& img, unsigned w, unsigned h, float dt, unsigned fc, int rx, int ry)
	: image(img), width(w != 0? w : image.getWidth()), height(h != 0? h : image.getHeight()),
	  rawOffsetX(rx), rawOffsetY(ry), rawFrameCount(std::min(computeRawFrameCount(image, width, height, rx, ry), fc != 0? fc : UINT_MAX)),
	  referencePixelX(0), referencePixelY(0), frameSequence(), frameSequenceCurrentIndex(0),
	  flipmode(Image::FLIP_NONE), scale(Image::NATURAL_SCALE), angle(0), croppingArea(),
	  frameDuration(dt), animationTime(0), animationPhaseOffset(0),
	  imageIsOwned(false), lastUpdateTime(0),
	  rawRowCount(computeRawRowCount(image, height, ry)),
	  rawColumnsCount(computeRawColumnCount(image, width, rx))
	{
		for(unsigned i = 0; i < this->rawFrameCount; i++)
			this->frameSequence.push_back(i);
	}

	Sprite::Sprite(const string& filename, unsigned w, unsigned h, float dt, unsigned fc, int rx, int ry)
	: image(*new Image(filename)), width(w != 0? w : image.getWidth()), height(h != 0? h : image.getHeight()),
	  rawOffsetX(rx), rawOffsetY(ry), rawFrameCount(std::min(computeRawFrameCount(image, width, height, rx, ry), fc != 0? fc : UINT_MAX)),
	  referencePixelX(0), referencePixelY(0), frameSequence(), frameSequenceCurrentIndex(0),
	  flipmode(Image::FLIP_NONE), scale(Image::NATURAL_SCALE), angle(0), croppingArea(),
	  frameDuration(dt), animationTime(0), animationPhaseOffset(0),
	  imageIsOwned(true), lastUpdateTime(0),
	  rawRowCount(computeRawRowCount(image, height, ry)),
	  rawColumnsCount(computeRawColumnCount(image, width, rx))
	{
		for(unsigned i = 0; i < this->rawFrameCount; i++)
			this->frameSequence.push_back(i);
	}

	Sprite::~Sprite()
	{
		if(imageIsOwned)
			delete &image;
	}

	Rectangle Sprite::getFrame(unsigned rawIndex)
	{
		if(rawFrameCount == 0) return Rectangle();
		if(rawIndex > rawFrameCount - 1)
			rawIndex = rawFrameCount - 1;  // prevent frame index out of bounds

		const Rectangle frameBounds = {
			(float) rawOffsetX + (rawIndex % rawColumnsCount) * width,
			(float) rawOffsetY + (rawIndex / rawColumnsCount) * height,
			(float) width, (float) height
		};
		return frameBounds;
	}

	void Sprite::computeCurrentFrame()
	{
		if(frameSequence.size() > 1 and frameDuration > 0 and rawFrameCount != 0)
		{
			const float currentTime = fgeal::uptime();
			if(lastUpdateTime != 0)
			{
				animationTime += currentTime - lastUpdateTime;
				lastUpdateTime = currentTime;
			}
			else  // first time playing
			{
				// sanity check for phase offset
				while(animationPhaseOffset > frameDuration * frameSequence.size())
					animationPhaseOffset -= frameDuration * frameSequence.size();

				animationTime = animationPhaseOffset;
				lastUpdateTime = currentTime;
			}

			while(animationTime >= frameDuration * frameSequence.size())
				animationTime -= frameDuration * frameSequence.size();

			frameSequenceCurrentIndex = (unsigned) (animationTime/frameDuration);
		}
	}

	Rectangle Sprite::getCurrentFrame()
	{
		return getFrame(frameSequence.empty()? 0 : frameSequence.at(frameSequenceCurrentIndex));
	}

	void Sprite::reset()
	{
		// sanity check for phase offset
		while(animationPhaseOffset > frameDuration * frameSequence.size())
			animationPhaseOffset -= frameDuration * frameSequence.size();

		animationTime = animationPhaseOffset;
		frameSequenceCurrentIndex = (unsigned) (animationTime/frameDuration);
	}

	void Sprite::draw(float x, float y)
	{
		Rectangle frame = getCurrentFrame();
		if(frame.isDegenerate()) return;
		const Point center = {frame.w/2, frame.h/2},
					where = {x - referencePixelX + (angle!=0?center.x:0), y - referencePixelY + (angle!=0?center.y:0)};

		frame.x += croppingArea.x;
		frame.y += croppingArea.y;

		if(croppingArea.w > 0)
		{
			if(croppingArea.w < frame.w - croppingArea.x)
				frame.w = croppingArea.w;
			else
				frame.w -= croppingArea.x;
		}

		if(croppingArea.h > 0)
		{
			if(croppingArea.h < frame.h - croppingArea.y)
				frame.h = croppingArea.h;
			else
				frame.h -= croppingArea.y;
		}

		if(angle == 0)
			if(scale.x == 1.0f and scale.y == 1.0f)
				if(flipmode == Image::FLIP_NONE)
					image.drawRegion(where, frame);
				else
					image.drawFlippedRegion(where, flipmode, frame);
			else
				image.drawScaledRegion(where, scale, flipmode, frame);
		else
			if(scale.x == 1.0f and scale.y == 1.0f)
				image.drawRotatedRegion(where, angle, center, flipmode, frame);
			else
				image.drawScaledRotatedRegion(where, scale, angle, center, flipmode, frame);
	}
}
