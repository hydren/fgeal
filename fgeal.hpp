/*
 * fgeal.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
  __                 _
 / _|               | |
| |_ __ _  ___  __ _| |
|  _/ _` |/ _ \/ _` | |
| || (_| |  __/ (_| | |
|_| \__, |\___|\__,_|_|
     __/ |
    |___/           beta
*/

#ifndef INCLUDE_FGEAL_H_
#define INCLUDE_FGEAL_H_

// Versioning
#include "version.h"

// Core headers
#include "core.hpp"
#include "exceptions.hpp"
#include "filesystem.hpp"
#include "geometry.hpp"
#include "colors.hpp"
#include "display.hpp"
#include "graphics.hpp"
#include "image.hpp"
#include "event.hpp"
#include "font.hpp"
#include "sound.hpp"

#endif /* INCLUDE_FGEAL_H_ */
