/*
 * filesystem.hpp
 *
 *  Created on: 3/07/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_FILESYSTEM_HPP_
#define FGEAL_FILESYSTEM_HPP_
#include <ciso646>

#include <vector>
#include <string>

namespace fgeal
{
	namespace filesystem
	{
		/** Returns a list of the files in the given directory path. */
		std::vector<std::string> getFilenamesWithinDirectory(const std::string& directoryPath);

		/** Returns the path of the current working directory. (this is a convenience function, as the similar functionality is available by other means) */
		std::string getCurrentWorkingDirectory();

		/** Sets the path of the current working directory. (this is a convenience function, as the similar functionality is available by other means) */
		void setWorkingDirectory(const std::string& directoryPath);

		/** Returns true if the given path is a directory/folder path. */
		bool isFilenameDirectory(const std::string& path);

		/** Returns true if the given path is a archive/file path. */
		bool isFilenameArchive(const std::string& path);
	}
}

#endif /* FGEAL_FILESYSTEM_HPP_ */
