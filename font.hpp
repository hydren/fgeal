/*
 * font.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_FONT_HPP_
#define FGEAL_FONT_HPP_
#include <ciso646>

#include "colors.hpp"
#include "geometry.hpp"
#include "image.hpp"

#include <string>

namespace fgeal
{
	class DrawableText;

	/** A class containing a implementation of an font that can be used to draw text on the screen. */
	class Font
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_FONT_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_FONT_FRIEND_CLASSES

		public:

		/** Creates a font object from the given font filename and size.  TODO: make the unit of the size parameter clearer (px? em?...)
		 *  If 'antialiasing' is true, hints the constructor to attempt to load the font with anti-aliasing/smoothing.
		 *  If 'kerning' is true, hints the constructor to attempt to load the font with kerning.
		 *  Note that both the anti-aliasing and kerning hints may be ignored altogether depending on the backend. */
		Font(const std::string& filename, unsigned size=12, bool antialiasing=true, bool kerning=true);
		~Font();

		/** Draws the given string on the screen at given location with the given color. */
		void drawText(const std::string& text, float x=0, float y=0, Color color=Color::BLACK);
		inline void drawText(const std::string& text, const Point& point, Color color=Color::BLACK)
		{
			this->drawText(text, point.x, point.y, color);
		}

		/** Draws the given string on the screen at given location with the given color, enforcing fixed char width. */
		inline void drawTextMonospaced(const std::string& text, float x=0, float y=0, Color color=Color::BLACK)
		{
			const float charFixedWidth = this->getTextWidth("0");
			for(unsigned n = 0; n < text.size(); n++)
				this->drawText(text.substr(n, 1), x + n * charFixedWidth, y, color);
		}
		inline void drawTextMonospaced(const std::string& text, const Point& point, Color color=Color::BLACK)
		{
			this->drawTextMonospaced(text, point.x, point.y, color);
		}

		/** Changes the size of this font. Note that this method is most likely going to reload the font from file,
		 *  so it's not exactly a fast call. It's recommended to avoid calling it from loops or performance-critical
		 *  parts of code, since it may cause slowdowns. */
		void setSize(unsigned size);

		/** Returns the size of this font. Note that this is the character size, not the text height, although
		 *  sometimes they are the same. */
		unsigned getSize() const;

		/** Returns the expected rendered height of a text drawn at the screen using this font.
		 *  This method is useful to estimate the recommended spacing between two consecutive text lines. */
		float getTextHeight() const;

		/** Returns the expected rendered width of the given string drawn at the screen using this font. */
		float getTextWidth(const std::string& text) const;
	};

	/** A class that provides facilities for drawing texts on the screen.
	 *  Instances of this class groups together a string, a Color and a Font pointer to do such drawing.
	 *  Width and height of current text content can be obtained. If a null font is set, no drawing is done.
	 *  Note that the font instance is not copied, so it should be not be deleted while this object is using it.
	 *  The font instance is also not deleted by this object when it gets destroyed.
	 *  Also note that this class is optimized for cases when the drawing parameters (such as the text content, font,
	 *  color) doesn't change too often, otherwise it may perform badly. When using backend adapters with particularly
	 *  slow font rendering, this class should perform better, since it attempts to cache the result in an image. */
	class DrawableText
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		Font* font;

		// to be used by some implementations
		void updateRenderedText();

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_DRAWABLE_TEXT_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_DRAWABLE_TEXT_FRIEND_CLASSES

		public:

		/** Creates a drawable text object with given text, font and color. */
		DrawableText(const std::string& text="", Font* font=null, Color color=Color::BLACK);
		~DrawableText();

		// assignment operator
		DrawableText& operator=(const DrawableText&);

		// copy constructor
		DrawableText(const DrawableText&);

		/** Returns the expected rendered width of this text when drawn at the screen. */
		float getWidth();

		/** Returns the expected rendered height of this text when drawn at the screen. */
		float getHeight();

		/** Sets the font of this text. Note that this method does not delete the previous font. */
		void setFont(Font* font);

		/** Returns the font of this text. */
		inline Font* getFont() const { return font; }

		/** Sets the content of this text. */
		void setContent(const std::string& str);

		/** Returns the string content of this text. */
		std::string getContent();

		/** Sets the color of this text. */
		void setColor(Color color);

		/** Returns the color of this text. */
		Color getColor();

		/** Draws this text in the given position. */
		void draw(float x, float y);
		inline void draw(Point position) { draw(position.x, position.y); }
	};
}

#endif /* FGEAL_FONT_HPP_ */
