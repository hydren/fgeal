/*
 * friend_classes.hxx
 *
 *  Created on: 14 de dez de 2018
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2018  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

// All wrapper classes are granted access to each other inner members.

#ifndef FGEAL_DISPLAY_FRIEND_CLASSES
	friend class Display;       // See display.hpp for class details.
#endif
#ifndef FGEAL_GRAPHICS_FRIEND_CLASSES
	friend class Graphics;      // See graphics.hpp for class details.
#endif
#ifndef FGEAL_IMAGE_FRIEND_CLASSES
	friend class Image;         // See image.hpp for class details.
#endif
#ifndef FGEAL_EVENT_FRIEND_CLASSES
	friend class Event;         // See event.hpp for class details.
#endif
#ifndef FGEAL_EVENTQUEUE_FRIEND_CLASSES
	friend class EventQueue;    // See event.hpp for class details.
#endif
#ifndef FGEAL_FONT_FRIEND_CLASSES
	friend class Font;          // See font.hpp for class details.
#endif
#ifndef FGEAL_DRAWABLE_TEXT_FRIEND_CLASSES
	friend class DrawableText;  // See font.hpp for class details.
#endif
#ifndef FGEAL_SOUND_FRIEND_CLASSES
	friend class Sound;         // See sound.hpp for class details.
#endif
#ifndef FGEAL_SOUNDSTREAM_FRIEND_CLASSES
	friend class SoundStream;  // See sound.hpp for class details.
#endif
#ifndef FGEAL_MUSIC_FRIEND_CLASSES
	friend class Music;        // See sound.hpp for class details.
#endif
#ifndef FGEAL_KEYBOARD_FRIEND_CLASSES
	friend class Keyboard;     // See input.hpp for class details.
#endif
#ifndef FGEAL_MOUSE_FRIEND_CLASSES
	friend class Mouse;        // See input.hpp for class details.
#endif
#ifndef FGEAL_JOYSTICK_FRIEND_CLASSES
	friend class Joystick;     // See input.hpp for class details.
#endif
