/*
 * generic_graphics.hxx
 *
 *  Created on: 15 de out de 2021
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GENERIC_GRAPHICS_HXX_
#define GENERIC_GRAPHICS_HXX_

#include "fgeal/graphics.hpp"

namespace fgeal
{
	namespace GenericGraphics
	{
		// note: this is also used in the SDL 1.x adapter, when compiled with older versions of SDL_gfx
		inline static void drawRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c) {
			Graphics::drawArc(x+rd, y+rd, rd, M_PI_2, M_PI, c);
			Graphics::drawArc(x+w-rd, y+rd, rd, 0, M_PI_2, c);
			Graphics::drawArc(x+rd, y+h-rd, rd, M_PI, M_3_PI_2, c);
			Graphics::drawArc(x+w-rd, y+h-rd, rd, M_3_PI_2, 2*M_PI, c);
			Graphics::drawLine(x+rd, y, x+w-rd, y, c);
			Graphics::drawLine(x, y+rd, x, y+h-rd, c);
			Graphics::drawLine(x+w, y+rd, x+w, y+h-rd, c);
			Graphics::drawLine(x+rd, y+h, x+w-rd, y+h, c);
		}

		// note: this is also used in the SDL 1.x adapter, when compiled with older versions of SDL_gfx
		inline static void drawFilledRoundedRectangle(float x, float y, float w, float h, float rd, const Color& c, bool fix=false) {
			const float f = fix? 1 : 0;
			Graphics::drawFilledCircleSector(x+rd, y+rd, rd, M_PI_2, M_PI, c);
			Graphics::drawFilledCircleSector(x+w-rd, y+rd, rd, 0, M_PI_2, c);
			Graphics::drawFilledCircleSector(x+rd, y+h-rd, rd, M_PI, M_3_PI_2, c);
			Graphics::drawFilledCircleSector(x+w-rd, y+h-rd, rd, M_3_PI_2, 2*M_PI, c);
			Graphics::drawFilledRectangle(x+rd+1, y, w-2*rd-2+f, rd-f, c);
			Graphics::drawFilledRectangle(x, y+rd+1-f, w, h-2*rd-2+f, c);
			Graphics::drawFilledRectangle(x+rd+1, y+h-rd, w-2*rd-2+f, rd-f, c);
		}

		// note: this is also used in the SDL 1.x adapter, when compiled with older versions of SDL_gfx
		inline static void drawThickLine(float x1, float y1, float x2, float y2, float thickness, const Color& c)
		{
			const float dx = x1 - x2, dy = y1 - y2, length = std::sqrt(dx * dx + dy * dy);
			if(length > 0)
			{
				const float offsetx = 0.5f * thickness * dy / length, offsety = 0.5f * thickness * dx / length;
				Graphics::drawFilledQuadrangle(x1 - offsetx, y1 + offsety, x1 + offsetx, y1 - offsety, x2 + offsetx, y2 - offsety, x2 - offsetx, y2 + offsety, c);
			}
		}

		// note: this is also used in the SDL 1.x adapter, when compiled with older versions of SDL_gfx
		// draw arc by using Bresenham's midpoint circle algorithm with angle bounds checking
		template <void (drawPixelFunction)(float, float, const Color&)>
		inline static void drawArc(float cx, float cy, float r, float ai, float af, const Color& c)
		{
			using std::sin; using std::cos;
			#define putPixelInBounds(dx, dy) \
				/*Compute dot products with start and end angle vectors*/ \
				doti = (dx * cosai) + (dy * -sinai); \
				dotf = (dx * cosaf) + (dy * -sinaf); \
				dotd = (dx * cosad) + (dy * -sinad); /* for reflex angles */ \
				/* Check the point angle lies between ai and af */ \
				if((doti >= 0 && dotf <= 0) || (ad > 0 && dotf >= 0 && dotd <= 0)) \
					drawPixelFunction(cx + dx, cy + dy, c)

			float ad = af - ai - M_PI;
			double doti, dotf, dotd;
			const double cosai = cos(M_PI_2 + ai),
						 cosaf = cos(M_PI_2 + (ad <= 0? af : ai + M_PI)),
						 cosad = ad <= 0? 0 : cos(M_PI_2 + af),
						 sinai = sin(M_PI_2 + ai),
						 sinaf = sin(M_PI_2 + (ad <= 0? af : ai + M_PI)),
						 sinad = ad <= 0? 0 : sin(M_PI_2 + af);
		    int f = 1 - r, ddF_x = 0, ddF_y = -2 * r, x = 0, y = r;
		    putPixelInBounds(0,  r);
		    putPixelInBounds(0, -r);
		    putPixelInBounds( r, 0);
		    putPixelInBounds(-r, 0);

		    while(x < y)
		    {
		        if(f >= 0)
		        {
		            y--;
		            ddF_y += 2;
		            f += ddF_y;
		        }
		        x++;
		        ddF_x += 2;
		        f += ddF_x + 1;
		        putPixelInBounds( x,  y);
		        putPixelInBounds(-x,  y);
		        putPixelInBounds( x, -y);
		        putPixelInBounds(-x, -y);
		        putPixelInBounds( y,  x);
		        putPixelInBounds(-y,  x);
		        putPixelInBounds( y, -x);
		        putPixelInBounds(-y, -x);
		    }
			#undef putPixelInBounds
		}

		// based on rtrussell's thickArcRGBA() implementation
		template <void (drawLineFunction)(float, float, float, float, const Color&)>
		inline static void drawThickArc(float cx, float cy, float r, float t, float ai, float af, const Color& c)
		{
			using std::sqrt; using std::tan; using std::atan2;
			struct { inline static void hlinecliparc (int x1, int x2, int y, int xc, int yc, double s, double f, const Color& c)
			{
				double a1 = atan2((double)y, x1), a2 = atan2((double)y, x2);
				if (a1 > a2)
				{
					double a = a1; a1 = a2; a2 = a;
					int x = x1; x1 = x2; x2 = x;
				}
				if (f < s)
				{
					if ((a1 > f) && (a2 < s)) return;
					if ((a1 < s) && (a1 > f)) x1 = y / tan(s);
					if ((a2 > f) && (a2 < s)) x2 = y / tan(f);
					if ((a1 < f) && (a2 > s))
					{
						drawLineFunction(x1+xc, y+yc, y/tan(f)+xc, y+yc, c);
						drawLineFunction(y/tan(s)+xc, y+yc, x2+xc, y+yc, c);
						return;
					}
				}
				else
				{
					if ((a1 > f) || (a2 < s)) return;
					if (a1 < s) x1 = y / tan(s);
					if (a2 > f) x2 = y / tan(f);
				}
				drawLineFunction(x1+xc, y+yc, x2+xc, y+yc, c);
			} } func;

			int thick = t;
			if (thick <= 1)
				Graphics::drawArc(cx, cy, r, ai, af, c);
			else
			{
				int ri, ro, x, y, z, xc = cx, yc = cy, rad = r;
				double ri2, ro2, s = -af, f = -ai;

				while (s < -M_PI) s += 2*M_PI;
				while (s >= M_PI) s -= 2*M_PI;
				while (f < -M_PI) f += 2*M_PI;
				while (f >= M_PI) f -= 2*M_PI;
				if (s != f)
				{
					ri = rad - thick / 2;
					ro = ri + thick - 1;
					if (ri > 0)
					{
						ri2 = ri * ri;
						ro2 = ro * ro;

						for (y = -ro; y <= -ri; y++)
						{
							x = sqrt(ro2 * (1.0 - y*y/ro2)) + 0.5;
							func.hlinecliparc(-x, x, y, xc, yc, s, f, c);
						}
						for (y = -ri + 1; y <= ri - 1; y++)
						{
							x = sqrt(ro2 * (1.0 - y*y/ro2)) + 0.5;
							z = sqrt(ri2 * (1.0 - y*y/ri2)) + 0.5;
							func.hlinecliparc(z, x, y, xc, yc, s, f, c);
							func.hlinecliparc(-z, -x, y, xc, yc, s, f, c);
						}
						for (y = ro; y >= ri; y--)
						{
							x = sqrt(ro2 * (1.0 - y*y/ro2)) + 0.5;
							func.hlinecliparc(-x, x, y, xc, yc, s, f, c);
						}
					}
				}
			}
		}
	}
}

#endif /* GENERIC_GRAPHICS_HXX_ */
