/*
 * geometry.hpp
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_GEOMETRY_HPP_
#define FGEAL_GEOMETRY_HPP_
#include <ciso646>

// if getting "undeclared identifier" errors for math constants while compiling with MSVC, check if _USE_MATH_DEFINES is defined globally in the compiler settings
#include <cmath>
#include <string>

#ifndef M_3_PI_2
	/// 3*pi/2
	#define M_3_PI_2 4.71238898038468985769
#endif  /* M_3_PI_2 */

namespace fgeal
{
	/** Returns the given angle, in radians, converted to degrees. */
	template<typename real>
	inline real rad2deg(real radian)
	{
		return radian * (real)(180.0 / M_PI);
	}

	/** A structure that contains parameters for a 2D vector.
	 *  This is a POD to be mostly used as a way to specify coordinates.
	 *  There is also a set of vector operations methods available. */
	struct Vector2D
	{
		float x, y;

		/** Creates a vector with the given magnitude and random direction. */
		static Vector2D random(float magnitude);

		/** Creates a vector with both parameters set to the given value. */
		inline static Vector2D create(float t) { const Vector2D v = {t, t}; return v; }

		/** Creates a vector with the given parameters. */
		inline static Vector2D create(float x, float y) { const Vector2D v = {x, y}; return v; }

		// ------ equality

		/** Returns true if this vector coordinates equals the ones from the given vector */
		bool equals(const Vector2D& v) const;

		inline bool operator ==(const Vector2D& vector) const { return equals(vector); }

		/** Returns true if this vector coordinates differs from the ones of the given vector */
		bool differs(const Vector2D& v) const;

		inline bool operator !=(const Vector2D& vector) const { return differs(vector); }

		/** Creates and returns a copy of this vector via copy constructor */
		inline Vector2D clone() const { return Vector2D(*this); }

		// ------- utils

		/** Creates a string with this vector coordinates (x, y) */
		std::string toString() const;

		/** Creates and returns an array with this Vectors coordinates, in correct order.  */
		float* getCoordinates() const;

		// ------- magnitude/length

		/** Returns the length/magnitude of this vector. */
		float magnitude() const;

		inline float operator ~() const { return magnitude(); }

		/** \see magnitude() */
		inline float length() const { return magnitude(); }

		/** Return true if x = 0 and y = 0; */
		inline bool isZero() const
		{
			return x==0 and y==0;
		}

		// ------- normalization

		/** Creates a vector with length 1 and same direction as this vector. In other words, a new vector that is a normalized version of this vector. Note that <b>the original vector remains unchanged</b>. */
		Vector2D unit() const;

		inline Vector2D operator !() const { return unit(); }

		/** Divides this vector's coordinates by its length/magnitude, normalizing it.
		The returned object is the vector instance itself after normalization. */
		Vector2D& normalize();

		// ------- reflection

		/** Creates and returns the opposite of this vector. In other words, returns a vector with same coordinates as this, but with changed signal. Note that <b>the original vector remains unchanged</b>. */
		Vector2D opposite() const;

		inline Vector2D operator -() const { return opposite(); }

		/** Changes the signal of this vector coordinates, effectively reflecting it.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflect();

		inline Vector2D& operator --() { return reflect(); }

		/** Changes the signal of this vector x coordinate.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflectX();

		/** Changes the signal of this vector y coordinate.
		<br> The returned object is <b>the vector instance itself</b> after reflection. */
		Vector2D& reflectY();

		// ------- basic arithmetic

		/** Creates and returns a vector that represents the sum of this vector and the given vector. Note that <b>the original vector remains unchanged</b>.*/
		Vector2D sum(const Vector2D& v) const;

		inline Vector2D operator +(const Vector2D& v) const { return sum(v); }

		/** Adds to this vector the given vector. In other words, it performs an addition to this vector coordinates.
		<br> The returned object is <b>the vector instance itself</b> after summation. */
		Vector2D& add(const Vector2D& v);

		inline Vector2D& operator +=(const Vector2D& v) { return add(v); }

		/** Creates a vector that represents the difference/displacement of this vector and the given vector, in this order. It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
		<br> Note that <b>the original vector remains unchanged</b>. */
		Vector2D difference(const Vector2D& v) const;

		inline Vector2D operator -(const Vector2D& v) const { return difference(v); }

		/** Subtracts from this vector the given vector. In other words, it performs an subtraction to this vector coordinates.
		It's useful to remember that vector subtraction is <b>not commutative</b>: a-b != b-a.
		<br> The returned object is the <b>the vector instance itself</b> after subtraction. */
		Vector2D& subtract(const Vector2D& v);

		inline Vector2D& operator -=(const Vector2D& v) { return subtract(v); }

		/** Creates a vector that represents the scalar multiplication of this vector by the given factor. Note that <b>the original vector remains unchanged</b>.*/
		Vector2D times(const float factor) const;

		inline Vector2D operator *(const float& factor) const { return times(factor); }

		/** Multiply this vectors coordinates by the given factor. The returned object is <b>the vector instance itself</b> after multiplication.*/
		Vector2D& scale(float factor);

		inline Vector2D& operator *=(const float& factor) { return scale(factor); }

		// ------- miscellaneous operations

		/** Compute the distance between this vector and the given vector. In other words, returns the length/magnitude of the displacement between this and the given vector. */
		float distance(const Vector2D& v) const;

		inline float operator %(const Vector2D& v) const { return distance(v); }

		/** Compute the inner product (aka dot product or scalar product) between this and the given vector. */
		float innerProduct(const Vector2D& v) const;

		inline float operator ^(const Vector2D& v) const { return innerProduct(v); }

		/** Compute the entrywise product (aka Hadamard product or Schur product) between this and the given vector */
		Vector2D entrywiseProduct(const Vector2D& v) const;

		// ------- projection operations (XXX not tested)

		/* Creates a vector that represents the projection of this vector on the given vector v. */
		Vector2D projection(const Vector2D& v) const;

		inline Vector2D operator ||(const Vector2D& v) const { return projection(v); }

		/* Creates a vector that represents the rejection of this vector on the given vector v. The rejection is defined as rej(u, v) = u - proj(u, v) */
		Vector2D rejection(const Vector2D& v) const;

		/* Creates a vector that represents the reflection of this vector in the axis represented by the given vector v. */
		Vector2D reflection(const Vector2D& v) const;

		inline Vector2D operator |(const Vector2D& v) const { return reflection(v); }

		// ------- rotation operations (XXX not tested)

		Vector2D rotation(const float& radians) const;

		inline Vector2D operator <(const float& radians) const { return rotation(radians); }

		Vector2D& rotate(const float& radians);

		inline Vector2D& operator <<(const float& radians) { return rotate(radians); }

		inline Vector2D perpendicular() const { return rotation(M_PI_2); }  // rotate by pi/2 radians

		//-----------------------------------------------------------------

		/** Represents the null/zero vector. It has coordinates (0, 0). */
		const static Vector2D NULL_VECTOR;

		/** A vector codirectional with the X axis, with length 1. */
		const static Vector2D X_VERSOR;

		/** A vector codirectional with the Y axis, with length 1. */
		const static Vector2D Y_VERSOR;
	};

	/** A structure that contains parameters for a point. This POD is used mostly as a way to specify coordinates. */
	typedef Vector2D Point;

	/** A structure that contains parameters for a rectangle. This POD is used mostly as a way to specify a region. */
	struct Rectangle
	{
		float x, y, w, h;

		/** Returns true if this rectangle intersect with given one */
		inline bool intersects(const Rectangle& r) const
		{
			return !(x + w <= r.x or x >= r.x + r.w or y + h <= r.y or y >= r.y + r.h);
		}

		inline bool contains(const Point& p) const
		{
			return p.x > x and p.x < x + w and p.y > y and p.y < y + h;
		}

		inline bool contains(float px, float py) const
		{
			return px > x and px < x + w and py > y and py < y + h;
		}

		// returns a spaced outline, like a margin or something
		inline Rectangle getSpacedOutline(float spacing) const
		{
			const Rectangle outline = {
				x - spacing,
				y - spacing,
				w + 2*spacing,
				h + 2*spacing
			};

			return outline;
		}

		inline float getArea() const
		{
			return w*h;
		}

		inline float getPerimeter() const
		{
			return 2.f*(w+h);
		}

		inline bool isDegenerate() const
		{
			return w == 0 or h == 0;
		}
	};

	/** A structure that contains parameters for a circle. This POD is used mostly as a way to specify a region. */
	struct Circle
	{
		Point center;

		float radius;

		/** Returns true if this circle intersect with given one */
		inline bool intersects(const Circle& other) const
		{
			return center.distance(other.center) < radius + other.radius;
		}

		inline bool contains(const Point& point) const
		{
			return center.distance(point) < radius;
		}

		/** Returns true if this circle intersect with given rectangle */
		inline bool intersects(const Rectangle& rectangle) const
		{
			const float hw = 0.5f*rectangle.w, hh = 0.5f*rectangle.h, cdx = std::fabs(center.x - (rectangle.x + hw)), cdy = std::fabs(center.y - (rectangle.y + hh));
			return cdx <= hw + radius and cdy <= hh + radius and (cdx <= hw or cdy <= hh or (cdx - hw)*(cdx - hw) + (cdy - hh)*(cdy - hh) <= radius * radius);
		}

		inline float getArea() const
		{
			return M_PI * radius * radius;
		}

		inline float getPerimeter() const
		{
			return 2 * M_PI * radius;
		}

		inline bool isDegenerate() const
		{
			return radius == 0;
		}
	};
}

#endif /* FGEAL_GEOMETRY_HPP_ */
