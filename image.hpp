/*
 * image.hpp
 *
 *  Created on: 26/11/2015
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2015  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_IMAGE_HPP_
#define FGEAL_IMAGE_HPP_
#include <ciso646>

#include "core.hpp"
#include "colors.hpp"
#include "geometry.hpp"

namespace fgeal
{
	/** A class containing a implementation of an image that can be drawn in the screen. */
	class Image
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_IMAGE_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_IMAGE_FRIEND_CLASSES

		public:

		/// Default scale vector (scale factor (1.0, 1.0)). Same as performing no scaling at all.
		static const Vector2D NATURAL_SCALE;

		/// Default region rectangle (whole image). Passsing this as an argument specifies that the whole image must be used.
		/// Note: a copy of this constant passed as an argument won't have the same effect as the constant itself.
		static const Rectangle WHOLE_REGION;

		/// Default center of rotation point (width/2.0, height/2.0). Passing this as argument specifies that the center of the image should be considered the center of rotation.
		/// Note: a copy of this constant passed as an argument won't have the same effect as the constant itself.
		static const Point IMAGE_CENTER;

		/** Creates a empty image with the given size. */
		Image(int width, int height);

		/** Creates an image from the given filename. Most common formats are supported. */
		Image(const std::string& filename);

		~Image();

		/** Returns this image's width, in pixels. */
		int getWidth();

		/** Returns this image's height, in pixels. */
		int getHeight();

		Color getPixel(unsigned x, unsigned y) const;
		void getPixels(std::vector<std::vector<Color> >& pixelData, unsigned x, unsigned y, unsigned width, unsigned height) const;

		void setPixel(unsigned x, unsigned y, const Color& color);
		void setPixels(unsigned x, unsigned y, const std::vector<std::vector<Color> >& colors);

		// Drawing functions

		/**
		 * Render this Image in the game screen, at the given (x, y) coordinates.
		 * */
		void draw(float x=0, float y=0);
		inline void draw(const Point& position)
		{
			this->draw(position.x, position.y);
		}

		/**
		 * Render a region of this Image in the game screen, at the given (x, y) coordinates.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * Alternatively, 'position' and 'region' parameters can be passed as well.
		 * */
		void drawRegion(float x, float y, float fromX, float fromY, float fromWidth, float fromHeight);
		inline void drawRegion(const Point& position, const Rectangle& region)
		{
			this->drawRegion(position.x, position.y, region.x, region.y, region.w, region.h);
		}

		/** Possible image flipping modes. */
		enum FlipMode { FLIP_NONE, FLIP_HORIZONTAL, FLIP_VERTICAL };

		/**
		 * Render this Image in the game screen, at the given (x, y) coordinates, flipped as specified.
		 * */
		void drawFlipped(float x, float y, const FlipMode flipmode);
		inline void drawFlipped(const Point& position, const FlipMode flipmode)
		{
			this->drawFlipped(position.x, position.y, flipmode);
		}

		/**
		 * Render a region of this Image in the game screen, at the given (x, y) coordinates, flipped as specified.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * Alternatively, 'position' and 'region' parameters can be passed as well.
		 * */
		void drawFlippedRegion(float x, float y, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight);
		inline void drawFlippedRegion(const Point& position, const FlipMode flipmode, const Rectangle& region)
		{
			this->drawFlippedRegion(position.x, position.y, flipmode, region.x, region.y, region.w, region.h);
		}

		/**
		 * Render this Image in the game screen, at the given (x, y) coordinates, scaled and flipped as specified.
		 * The performed scaling is done horizontally by a factor of 'xScale' and vertically by a factor of 'yScale'.
		 * Alternatively, a 'scale' vector can be specified as well.
		 * */
		void drawScaled(float x, float y, float xScale, float yScale, const FlipMode flipmode=FLIP_NONE);
		inline void drawScaled(const Point& position, const Vector2D& scale, const FlipMode flipmode=FLIP_NONE)
		{
			this->drawScaled(position.x, position.y, scale.x, scale.y, flipmode);
		}

		/**
		 * Render this a region of this Image in the game screen, at the given (x, y) coordinates, scaled and flipped as specified.
		 * The performed scaling is done horizontally by a factor of 'xScale' and vertically by a factor of 'yScale'.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
 		 * Alternatively, 'scale', 'position' and 'region' parameters can be passed as well.
		 * */
		void drawScaledRegion(float x, float y, float xScale, float yScale, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight);
		inline void drawScaledRegion(const Point& position, const Vector2D& scale, const FlipMode flipmode=FLIP_NONE, const Rectangle& region=WHOLE_REGION)
		{
			this->drawScaledRegion(position.x, position.y, scale.x, scale.y, flipmode, region.x, region.y, region.w, region.h);
		}

		/**
		 * Render this Image in the game screen, at the given (x, y) coordinates, rotated and flipped as specified.
		 * The 'angle' parameter specifies the angle of rotation, counter-clockwise, in radians.
		 * The 'centerX' and 'centerY' parameters specifies the point from which the rotation is relative.
		 * Alternatively, 'position' and 'centerOfRotation' parameters can be passed as well.
		 * Note: this method considers the 'x' and 'y' parameters as the position to draw the center point of rotation (instead of the image's top-left).
		 * In other words, the point (centerX, centerY) inside the image will be drawn at position (x, y) of the display.
		 * */
		void drawRotated(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode=FLIP_NONE);
		inline void drawRotated(const Point& position, float angle, const Point& centerOfRotation=IMAGE_CENTER, const FlipMode flipmode=FLIP_NONE)
		{
			this->drawRotated(position.x, position.y, angle,
							  (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getWidth() : centerOfRotation.x),
							  (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getHeight() : centerOfRotation.y),
							  flipmode);
		}

		/**
		 * Render a region of this Image in the game screen, at the given (x, y) coordinates, rotated and flipped as specified.
		 * The 'angle' parameter specifies the angle of rotation, counter-clockwise, in radians.
		 * The 'centerX' and 'centerY' parameters specifies the point from which the rotation is relative.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * Alternatively, 'position', 'centerOfRotation' and 'region' parameters can be passed as well.
		 * Note: this method considers the 'x' and 'y' parameters as the position to draw the center point of rotation (instead of the image's top-left).
		 * In other words, the point (centerX, centerY) inside the image will be drawn at position (x, y) of the display.
		 * */
		void drawRotatedRegion(float x, float y, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight);
		inline void drawRotatedRegion(const Point& position, float angle, const Point& centerOfRotation=IMAGE_CENTER, const FlipMode flipmode=FLIP_NONE, const Rectangle& region=WHOLE_REGION)
		{
			this->drawRotatedRegion(position.x, position.y, angle,
									(&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getWidth() : centerOfRotation.x),
									(&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getHeight() : centerOfRotation.y),
									flipmode, region.x, region.y, region.w, region.h);
		}

		/**
		 * Render this Image in the game screen, at the given (x, y) coordinates, scaled, rotated and flipped as specified.
		 * The performed scaling is done horizontally by a factor of 'xScale' and vertically by a factor of 'yScale'.
		 * The 'angle' parameter specifies the angle of rotation, counter-clockwise, in radians.
		 * The 'centerX' and 'centerY' parameters specifies the point from which the rotation is relative.
		 * Alternatively, 'position', 'scale' and 'centerOfRotation' parameters can be passed as well.
		 * Note: this method considers the 'x' and 'y' parameters as the position to draw the center point of rotation (instead of the image's top-left).
		 * In other words, the point (centerX, centerY) inside the image will be drawn at position (x, y) of the display.
		 * */
		void drawScaledRotated(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode=FLIP_NONE);
		inline void drawScaledRotated(const Point& position, const Vector2D& scale, float angle, const Point& centerOfRotation=IMAGE_CENTER, const FlipMode flipmode=FLIP_NONE)
		{
			this->drawScaledRotated(position.x, position.y, scale.x, scale.y, angle,
									(&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getWidth() : centerOfRotation.x),
									(&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getHeight() : centerOfRotation.y),
									flipmode);
		}

		/**
		 * Render a region of this Image in the game screen, at the given (x, y) coordinates, scaled, rotated and flipped as specified.
		 * The performed scaling is done horizontally by a factor of 'xScale' and vertically by a factor of 'yScale'.
		 * The 'angle' parameter specifies the angle of rotation, counter-clockwise, in radians.
		 * The 'centerX' and 'centerY' parameters specifies the point from which the rotation is relative.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * Alternatively, 'position', 'scale', 'centerOfRotation' and 'region' parameters can be passed as well.
		 * Note: this method considers the 'x' and 'y' parameters as the position to draw the center point of rotation (instead of the image's top-left).
		 * In other words, the point (centerX, centerY) inside the image will be drawn at position (x, y) of the display.
		 * */
		void drawScaledRotatedRegion(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight);
		inline void drawScaledRotatedRegion(const Point& position, const Vector2D& scale, float angle, const Point& centerOfRotation=IMAGE_CENTER, const FlipMode flipmode=FLIP_NONE, const Rectangle& region=WHOLE_REGION)
		{
			this->drawScaledRotatedRegion(position.x, position.y, scale.x, scale.y, angle,
										  (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getWidth() : centerOfRotation.x),
										  (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getHeight() : centerOfRotation.y),
										  flipmode, region.x, region.y, region.w, region.h);
		}

		/**
		 * Render a region of this Image in the given Image, at the given position (default (0, 0)), scaled, rotated and flipped as specified.
		 * The performed scaling is done horizontally by a factor of 'xScale' and vertically by a factor of 'yScale'.
		 * The 'angle' parameter specifies the angle of rotation, counter-clockwise, in radians.
		 * The 'centerX' and 'centerY' parameters specifies the point from which the rotation is relative.
		 * The specified region is a rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * Alternatively, 'position', 'scale', 'centerOfRotation' and 'region' parameters can be passed as well.
		 * Note that this method considers the 'x' and 'y' parameters as the position to draw the center point of rotation (instead of the image's top-left).
		 * In other words, the point (centerX, centerY) inside the image will be drawn at position (x, y) of the display.
		 * If a zero-sized region is passed (either fromWidth=0 or fromHeight=0), this whole image is drawn into 'img'.
		 * */
		void blit(Image& img, float x=0, float y=0, float xScale=1.0, float yScale=1.0, float angle=0, float centerX=0, float centerY=0, const FlipMode flipmode=FLIP_NONE, float fromX=0, float fromY=0, float fromWidth=0, float fromHeight=0);
		inline void blit(Image& img, const Point& position, const Vector2D& scale=NATURAL_SCALE, float angle=0, const Point& centerOfRotation=IMAGE_CENTER, const FlipMode flipmode=FLIP_NONE, const Rectangle& region=WHOLE_REGION)
		{
			this->blit(img, position.x, position.y, scale.x, scale.y, angle,
			           (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getWidth() : centerOfRotation.x),
					   (&centerOfRotation == &Image::IMAGE_CENTER? 0.5f * this->getHeight() : centerOfRotation.y),
					   flipmode,
					   (&region == &Image::WHOLE_REGION? 0 : region.x),
					   (&region == &Image::WHOLE_REGION? 0 : region.y),
					   (&region == &Image::WHOLE_REGION? this->getWidth() : region.w),
					   (&region == &Image::WHOLE_REGION? this->getHeight() : region.h));
		}

		/**
		 * Creates and returns an Image that has the same appearance as this Image, optionally scaled, rotated, flipped and/or cropped.
		 * If 'xScaledBy' is specified (non-unitary), the returned image will be a horizontally-scaled version of this Image (by the given x factor)
		 * If 'yScaledBy' is specified (non-unitary), the returned image will be a vertically-scaled version of this Image (by the given y factor)
		 * If 'rotatedBy' is specified (non-zero), the returned image will be a rotated version of this Image ('rotatedBy' radians)
		 * If 'flipped' is specified (other than FLIP_NONE), the returned image will be a flipped version of this Image (horizontally or vertically, depending on value of 'flipped')
		 * Alternatively, 'scaledBy', 'rotatedBy' and 'croppedBy' parameters can be passed as well.
		 * If specified, the crop region will result in an image which contains the rectangular area positioned on **this** image at coordinates ('fromX', 'fromY'), with dimensions 'fromWidth' x 'fromHeight'.
		 * */
		Image* getCopy(float xScaledBy=1.0, float yScaledBy=1.0, float rotatedBy=0, const FlipMode flipped=FLIP_NONE, float cropFromX=0, float cropFromY=0, float cropFromWidth=0, float cropFromHeight=0);
		inline Image* getCopy(const Vector2D& scaledBy, float rotatedBy=0, const FlipMode flipped=FLIP_NONE, const Rectangle& croppedBy=Image::WHOLE_REGION)
		{
			return this->getCopy(scaledBy.x, scaledBy.y, rotatedBy, flipped, croppedBy.x, croppedBy.y, croppedBy.w, croppedBy.h);
		}

		/** Flag that hints smoothing of all Images when drawn scaled or/and rotated.
		 *  If true, smoothing will be done by the backend library, if possible. (default=false) */
		static bool useImageTransformSmoothingHint;

		// for internal use ========================================================================================
		private:
		void delegateDraw(float x, float y, float xScale, float yScale, float angle, float centerX, float centerY, const FlipMode flipmode, float fromX, float fromY, float fromWidth, float fromHeight);
	};
}

#endif /* FGEAL_IMAGE_HPP_ */
