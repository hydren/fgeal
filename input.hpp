/*
 * input.hpp
 *
 *  Created on: 24/01/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_INPUT_HPP_
#define FGEAL_INPUT_HPP_
#include <ciso646>

#include "core.hpp"

namespace fgeal
{
	/** Class that deals with the keyboard. */
	class Keyboard
	{
		Keyboard();  // non-instantiable

		public:

		/// Keyboard keys enumeration
		enum Key
		{
			KEY_UNKNOWN,
			KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L, KEY_M,
			KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z,
			KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,  // numerical keys (*NOT* on numerical keypad)
			KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12,  // function keys
			KEY_ARROW_UP, KEY_ARROW_DOWN, KEY_ARROW_LEFT, KEY_ARROW_RIGHT,  // arrows keys
			KEY_ENTER,  // aka Return key
			KEY_SPACE,  // aka Spacebar
			KEY_ESCAPE, // aka Esc key

			KEY_LEFT_CONTROL, KEY_RIGHT_CONTROL,  // aka Ctrl keys
			KEY_LEFT_SHIFT, KEY_RIGHT_SHIFT,
			KEY_LEFT_ALT, KEY_RIGHT_ALT,  // aka Alt Gr or Alt Graph key
			KEY_LEFT_SUPER,     // aka Win key (left)
			KEY_RIGHT_SUPER,    // aka Win key (right)
			KEY_MENU, KEY_TAB, KEY_BACKSPACE,

			KEY_MINUS, // Minus key is also known as Dash, Hyphen
			KEY_EQUALS, KEY_LEFT_BRACKET, KEY_RIGHT_BRACKET, KEY_SEMICOLON, KEY_COMMA,
			KEY_PERIOD, // aka Full Stop key
			KEY_SLASH, // aka Stroke key
			KEY_BACKSLASH, KEY_QUOTE, KEY_TILDE, // aka Backquote key

			KEY_INSERT, KEY_DELETE, KEY_HOME, KEY_END,
			KEY_PAGE_UP, KEY_PAGE_DOWN,  // aka PgUp and PgDn keys

			KEY_NUMPAD_0,                      // Numpad keys
			KEY_NUMPAD_1, KEY_NUMPAD_2, KEY_NUMPAD_3,  // Numpad keys
			KEY_NUMPAD_4, KEY_NUMPAD_5, KEY_NUMPAD_6,  // Numpad keys
			KEY_NUMPAD_7, KEY_NUMPAD_8, KEY_NUMPAD_9,  // Numpad keys
			KEY_NUMPAD_ADDITION,        // Numpad Addition/Plus key
			KEY_NUMPAD_SUBTRACTION,     // Numpad Subtraction/Minus key
			KEY_NUMPAD_MULTIPLICATION,  // Numpad Multiplication/Asterisk key
			KEY_NUMPAD_DIVISION,        // Numpad Division/Slash key
			KEY_NUMPAD_DECIMAL,         // Numpad Decimal point/Dot/Comma/Delete key
			KEY_NUMPAD_ENTER           // Numpad Enter/Return key

			// TODO Map more keys...
		};


		/** Returns true if the given key is currently pressed. */
		static bool isKeyPressed(Key k);

		/** Returns a character corresponding to the given key. If the key does not represent a
		 *  valid character, a newline ('\n') is returned.  XXX maybe \n is not a good default return value...
		 *  Note: This function is not yet guaranteed to work with different keyboard layouts. */
		static char parseKeyStroke(Keyboard::Key key);  //FIXME make this honor keyboard layouts
	};

	/** Class that deals with the mouse. */
	class Mouse
	{
		Mouse();  // non-instantiable

		public:

		/// Mouse buttons enumeration
		enum Button
		{
			BUTTON_UNKNOWN,
			BUTTON_LEFT,
			BUTTON_RIGHT,
			BUTTON_MIDDLE

			// TODO Map other mouse buttons...
		};

		/** Returns true if the given mouse button is currently pressed. */
		static bool isButtonPressed(Button b);

		/** Returns the mouse cursor's current position. */
		static Point getPosition();

		/** Set the mouse cursor's current position. */
		static void setPosition(const Point& position);

		/** Returns the mouse cursor's current horizontal position. */
		static int getPositionX();

		/** Returns the mouse cursor's current vertical position. */
		static int getPositionY();

		/** Set the mouse cursor's current position (int version). */
		static void setPosition(int x, int y);
	};

	class Joystick
	{
		friend void fgeal::initialize();
		friend void fgeal::finalize();

		Joystick();  // non-instantiable

		// init. all joysticks.
		static void configureAll();

		// deinit all joysticks.
		static void releaseAll();

		public:

		/** Returns the number of joysticks available to use. */
		static unsigned getCount();

		/** Returns a implementation-dependant name of this joystick. */
		static std::string getJoystickName(unsigned joystickIndex);

		/** Returns the number of buttons on this joystick. */
		static unsigned getButtonCount(unsigned joystickIndex);

		/** Returns the number of axis on this joystick. */
		static unsigned getAxisCount(unsigned joystickIndex);

		/** Returns true if the given button (by index) is being pressed on this joystick. */
		static bool isButtonPressed(unsigned joystickIndex, unsigned buttonIndex);

		/** Returns the current position of the given axis (by index) on this joystick. */
		static float getAxisPosition(unsigned joystickIndex, unsigned axisIndex);
	};
}

#endif /* FGEAL_INPUT_HPP_ */
