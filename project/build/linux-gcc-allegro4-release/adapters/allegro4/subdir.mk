################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/allegro4/core.cpp \
../adapters/allegro4/display.cpp \
../adapters/allegro4/event.cpp \
../adapters/allegro4/font.cpp \
../adapters/allegro4/graphics.cpp \
../adapters/allegro4/image.cpp \
../adapters/allegro4/input.cpp \
../adapters/allegro4/sound.cpp 

OBJS += \
./adapters/allegro4/core.o \
./adapters/allegro4/display.o \
./adapters/allegro4/event.o \
./adapters/allegro4/font.o \
./adapters/allegro4/graphics.o \
./adapters/allegro4/image.o \
./adapters/allegro4/input.o \
./adapters/allegro4/sound.o 

CPP_DEPS += \
./adapters/allegro4/core.d \
./adapters/allegro4/display.d \
./adapters/allegro4/event.d \
./adapters/allegro4/font.d \
./adapters/allegro4/graphics.d \
./adapters/allegro4/image.d \
./adapters/allegro4/input.d \
./adapters/allegro4/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/allegro4/%.o: ../adapters/allegro4/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


