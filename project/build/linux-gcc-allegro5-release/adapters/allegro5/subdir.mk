################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/allegro5/core.cpp \
../adapters/allegro5/display.cpp \
../adapters/allegro5/event.cpp \
../adapters/allegro5/filesystem.cpp \
../adapters/allegro5/font.cpp \
../adapters/allegro5/graphics.cpp \
../adapters/allegro5/image.cpp \
../adapters/allegro5/input.cpp \
../adapters/allegro5/sound.cpp 

OBJS += \
./adapters/allegro5/core.o \
./adapters/allegro5/display.o \
./adapters/allegro5/event.o \
./adapters/allegro5/filesystem.o \
./adapters/allegro5/font.o \
./adapters/allegro5/graphics.o \
./adapters/allegro5/image.o \
./adapters/allegro5/input.o \
./adapters/allegro5/sound.o 

CPP_DEPS += \
./adapters/allegro5/core.d \
./adapters/allegro5/display.d \
./adapters/allegro5/event.d \
./adapters/allegro5/filesystem.d \
./adapters/allegro5/font.d \
./adapters/allegro5/graphics.d \
./adapters/allegro5/image.d \
./adapters/allegro5/input.d \
./adapters/allegro5/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/allegro5/%.o: ../adapters/allegro5/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


