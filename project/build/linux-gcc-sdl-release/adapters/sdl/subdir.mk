################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sdl/core.cpp \
../adapters/sdl/display.cpp \
../adapters/sdl/event.cpp \
../adapters/sdl/font.cpp \
../adapters/sdl/graphics.cpp \
../adapters/sdl/image.cpp \
../adapters/sdl/input.cpp \
../adapters/sdl/sound.cpp 

OBJS += \
./adapters/sdl/core.o \
./adapters/sdl/display.o \
./adapters/sdl/event.o \
./adapters/sdl/font.o \
./adapters/sdl/graphics.o \
./adapters/sdl/image.o \
./adapters/sdl/input.o \
./adapters/sdl/sound.o 

CPP_DEPS += \
./adapters/sdl/core.d \
./adapters/sdl/display.d \
./adapters/sdl/event.d \
./adapters/sdl/font.d \
./adapters/sdl/graphics.d \
./adapters/sdl/image.d \
./adapters/sdl/input.d \
./adapters/sdl/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sdl/%.o: ../adapters/sdl/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


