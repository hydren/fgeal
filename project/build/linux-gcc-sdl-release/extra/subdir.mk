################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../extra/game.cpp \
../extra/gui.cpp \
../extra/input_manager.cpp \
../extra/sprite.cpp 

OBJS += \
./extra/game.o \
./extra/gui.o \
./extra/input_manager.o \
./extra/sprite.o 

CPP_DEPS += \
./extra/game.d \
./extra/gui.d \
./extra/input_manager.d \
./extra/sprite.d 


# Each subdirectory must supply rules for building sources it contributes
extra/%.o: ../extra/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


