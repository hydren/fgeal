################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sdl2/core.cpp \
../adapters/sdl2/display.cpp \
../adapters/sdl2/event.cpp \
../adapters/sdl2/font.cpp \
../adapters/sdl2/graphics.cpp \
../adapters/sdl2/image.cpp \
../adapters/sdl2/input.cpp \
../adapters/sdl2/sound.cpp 

OBJS += \
./adapters/sdl2/core.o \
./adapters/sdl2/display.o \
./adapters/sdl2/event.o \
./adapters/sdl2/font.o \
./adapters/sdl2/graphics.o \
./adapters/sdl2/image.o \
./adapters/sdl2/input.o \
./adapters/sdl2/sound.o 

CPP_DEPS += \
./adapters/sdl2/core.d \
./adapters/sdl2/display.d \
./adapters/sdl2/event.d \
./adapters/sdl2/font.d \
./adapters/sdl2/graphics.d \
./adapters/sdl2/image.d \
./adapters/sdl2/input.d \
./adapters/sdl2/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sdl2/%.o: ../adapters/sdl2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


