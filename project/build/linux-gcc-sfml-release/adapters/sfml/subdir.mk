################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sfml/core.cpp \
../adapters/sfml/display.cpp \
../adapters/sfml/event.cpp \
../adapters/sfml/font.cpp \
../adapters/sfml/graphics.cpp \
../adapters/sfml/image.cpp \
../adapters/sfml/input.cpp \
../adapters/sfml/sound.cpp 

OBJS += \
./adapters/sfml/core.o \
./adapters/sfml/display.o \
./adapters/sfml/event.o \
./adapters/sfml/font.o \
./adapters/sfml/graphics.o \
./adapters/sfml/image.o \
./adapters/sfml/input.o \
./adapters/sfml/sound.o 

CPP_DEPS += \
./adapters/sfml/core.d \
./adapters/sfml/display.d \
./adapters/sfml/event.d \
./adapters/sfml/font.d \
./adapters/sfml/graphics.d \
./adapters/sfml/image.d \
./adapters/sfml/input.d \
./adapters/sfml/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sfml/%.o: ../adapters/sfml/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


