################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../adapters/sfml2/core.cpp \
../adapters/sfml2/display.cpp \
../adapters/sfml2/event.cpp \
../adapters/sfml2/font.cpp \
../adapters/sfml2/graphics.cpp \
../adapters/sfml2/image.cpp \
../adapters/sfml2/input.cpp \
../adapters/sfml2/sound.cpp 

OBJS += \
./adapters/sfml2/core.o \
./adapters/sfml2/display.o \
./adapters/sfml2/event.o \
./adapters/sfml2/font.o \
./adapters/sfml2/graphics.o \
./adapters/sfml2/image.o \
./adapters/sfml2/input.o \
./adapters/sfml2/sound.o 

CPP_DEPS += \
./adapters/sfml2/core.d \
./adapters/sfml2/display.d \
./adapters/sfml2/event.d \
./adapters/sfml2/font.d \
./adapters/sfml2/graphics.d \
./adapters/sfml2/image.d \
./adapters/sfml2/input.d \
./adapters/sfml2/sound.d 


# Each subdirectory must supply rules for building sources it contributes
adapters/sfml2/%.o: ../adapters/sfml2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"BUILD_PATH" -I"BUILD_PATH/.." -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


