/*
 * sound.hpp
 *
 *  Created on: 25/10/2016
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2016  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_SOUND_HPP_
#define FGEAL_SOUND_HPP_
#include <ciso646>

#include <string>

namespace fgeal
{
	/** A class that represents a sound, ready to be played.
	 *  Note that, although the methods of this class are meant to behave in the same way across adapters, some scenarios have
	 *  adapter-dependent behavior. For example, when playing/looping a Sound that is already playing, depending of the backend
	 *  adapter, the result may be either playing the sound in parallel with current playback or stopping current playback and
	 *  playing it again from the start. */
	class Sound
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_SOUND_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_SOUND_FRIEND_CLASSES

		public:

		/** Creates a sound from the given filename. */
		Sound(const std::string& filename, float volume=1.0f);
		~Sound();

		/** Plays this sound once. */
		void play();

		/** Plays this sound repeatedly. */
		void loop();

		/** Halts this sound and rewinds it. */
		void stop();

		/** Halts this sound (but does not rewinds it).*/
		void pause();

		/** Halts this sound. If 'rewind' is true, the sounds is rewinded. */
		inline void halt(bool rewind) { if(rewind) stop(); else pause(); }

		/** Plays this sound from where it stopped, if it was paused. Otherwise plays from the start. */
		void resume();

		/** Returns true if this sound is currently being playing. */
		bool isPlaying();

		/** Returns true if this sound is currently paused. */
		bool isPaused();

		/** Set the volume of this sound, in the range 0-1.0. A value of 1.0 is normal volume level (set by default).
		 *  If no value is passed, the volume is reset. Setting negative values or values greater than 1.0 causes undefined behavior. */
		void setVolume(float value=1.0f);

		/** Returns the volume set for this sound. By default its 1.0. */
		float getVolume();

		/** Get the duration of this sound, in seconds. */
		float getDuration();

		/** Sets the playback speed of this sound. A value of 1.0 is normal speed (set by default). Note that this alters the resulting sound's pitch, as well.
		 *  If the 'hintDynamicPlaybackSpeedChange' hint is passed as 'true', the method is hinted that playback speed may change during playback, even if a speed
		 *  of 1.0 (normal speed) is initially specified. This hint is necessary in some adapters if, before playing this sound, a playback speed of 1.0 is
		 *  specified, as the playback may be optimized for normal speed and further changes in speed will fail to take effect until the next call to Sound::play
		 *  or Sound::loop. If this hint is left unspecified, it is passed as false (as default) and, consequently, changes to playback speed during playback may
		 *  or may not work, depending on adapter used, if a playback speed of 1.0 is specified before start playing. */
		void setPlaybackSpeed(float factor, bool hintDynamicPlaybackSpeedChange=false);

		/** Returns the playback speed set to this sound. */
		float getPlaybackSpeed();

		/** Same as Sound::setPlaybackSpeed, as the pitch is altered by changing the playback speed. */
		inline void setPitch(float factor, bool hintDynamicPitchChange=false) { setPlaybackSpeed(factor, hintDynamicPitchChange); }

		/** Same as Sound::getPlaybackSpeed, as the pitch is altered by changing the playback speed. */
		inline float getPitch() { return getPlaybackSpeed(); }
	};

	/** A class that represents a sound stream, which works the same way as the Sound class, except by some caveats. As opposed to the Sound class, this class'
	 *  instances don't attempt to load the entire sound data into memory. Instead, the sound data is read as it is being played. In other words, its samples
	 *  will be acquired while the sound is playing. Note that sound stream is not supported on all adapters. In adapters that do not support sound stream, this
	 *  class' behavior reverts to the same one as the Sound class. */
	class SoundStream
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_SOUNDSTREAM_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_SOUNDSTREAM_FRIEND_CLASSES

		public:

		/** Creates a sound stream from the given filename. */
		SoundStream(const std::string& filename);
		~SoundStream();

		/** Plays this sound stream once. */
		void play();

		/** Plays this sound stream repeatedly. */
		void loop();

		/** Halts this sound stream and rewinds it. */
		void stop();

		/** Halts this sound stream (but does not rewinds it).*/
		void pause();

		/** Halts this sound stream. If 'rewind' is true, it is rewinded as well. */
		inline void halt(bool rewind) { if(rewind) stop(); else pause(); }

		/** Plays this sound stream from where it stopped, if it was paused. Otherwise plays it from the start. */
		void resume();

		/** Returns true if this sound stream is currently being playing. */
		bool isPlaying();

		/** Returns true if this sound stream is currently paused. */
		bool isPaused();

		/** Set the volume of this sound stream, in the range 0-1.0. The semantic of the 'value' argument is the same as in the Sound class. */
		void setVolume(float value=1.0f);

		/** Returns the volume set for this sound stream. By default its 1.0. */
		float getVolume();

		/** Returns true if this sound stream is, indeed, being streamed. If this returns false, then the current backend doesn't support sound stream and is
		 *  using a fallback behavior of the Sound class instead. */
		bool isStreaming();

		/** Get the duration of this sound stream, in seconds. */
		float getDuration();

		/** Sets the playback speed of this sound stream. The semantic of the arguments are the same as in the Sound class. */
		void setPlaybackSpeed(float factor, bool hintDynamicPlaybackSpeedChange=false);

		/** Returns the playback speed set to this sound stream. */
		float getPlaybackSpeed();

		/** Same as Sound::setPlaybackSpeed, as the pitch is altered by changing the playback speed. */
		void setPitch(float factor, bool hintDynamicPitchChange=false);

		/** Same as Sound::getPlaybackSpeed, as the pitch is altered by changing the playback speed. */
		float getPitch();
	};

	/** Works the same way as sounds, except that only one music can be played at time. */
	class Music
	{
		struct implementation;
		friend struct implementation;
		implementation& self;

		// All wrapper classes are granted access to each other inner members.
		#define FGEAL_MUSIC_FRIEND_CLASSES
		#include "friend_classes.hxx"
		#undef FGEAL_MUSIC_FRIEND_CLASSES

		public:

		/** Creates a music from the given filename. */
		Music(const std::string& filename);
		~Music();

		/** Plays this music once. */
		void play();

		/** Plays this music repeatedly. */
		void loop();

		/** Halts this music and rewinds it. */
		void stop();

		/** Halts this music (but does not rewind it).*/
		void pause();

		/** Halts this music. If 'rewind' is true, the music is rewinded. */
		inline void halt(bool rewind) { if(rewind) stop(); else pause(); }

		/** Plays this music from where it stopped, if it was paused. Otherwise plays from the start. */
		void resume();

		/** Returns true if this music is currently being playing. */
		bool isPlaying();

		/** Returns true if this music is currently paused. */
		bool isPaused();

		/** Set the volume of this music. A value of 1.0 is normal volume level (set by default). */
		void setVolume(float value=1.0f);

		/** Returns the volume set for this music. By default its 1.0. */
		float getVolume();
	};
}

#endif /* FGEAL_SOUND_HPP_ */
