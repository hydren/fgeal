/*
 * version.h
 *
 *  Created on: 3/07/2017
 *
 * This file is part of fgeal.
 *
 * fgeal - Faruolo's game engine/library abstraction layer
 * Copyright (C) 2017  Carlos F. M. Faruolo <5carlosfelipe5@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FGEAL_VERSION_H_
#define FGEAL_VERSION_H_

// Versioning
#define FGEAL_MAJOR_VERSION 0
#define FGEAL_MINOR_VERSION 8
#define FGEAL_REVIS_VERSION 5
#define FGEAL_DEV_VERSION dev

#define FGEAL_VERSION_INT ((FGEAL_MAJOR_VERSION << 24) | (FGEAL_MINOR_VERSION << 16) | (FGEAL_REVIS_VERSION << 8))

#endif /* FGEAL_VERSION_H_ */
